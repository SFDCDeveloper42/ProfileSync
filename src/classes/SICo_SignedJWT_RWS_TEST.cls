@isTest
private class SICo_SignedJWT_RWS_TEST {
    static testMethod void testMethod1() {
        
        String jwtStr = null;
		
        User thisUser = [ Select Id from User where Id = :UserInfo.getUserId() ];
		
		System.runAs( thisUser ){
			JWTSettings__c mycs = JWTSettings__c.getValues('Cham');

			if(mycs == null) {
				mycs = new JWTSettings__c(Name= 'Cham');
				
				mycs.Exp__c=4;
				mycs.Iss__c='C43 SFDC';
				mycs.Sub__c='Cham Test';
				mycs.Aud__c='Cham Test';

				insert mycs;
			}
		}


		test.startTest();
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/getJWT';
		req.addParameter('partnerName', 'Cham');  
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;

        jwtStr = SICo_SignedJWT_RWS.doGet();
        test.stopTest();

        System.assertNotEquals(null, jwtStr);
    }
}