/**
* @author Boris Castellani
* @date 17/10/2016
*
* @description SICo_SiteMeter__c Trigger After update
*/
trigger SICo_SiteMeterAfterUpdate_TRG on SICo_SiteMeter__c(after update) {
	if(PAD.canTrigger('SICo_SiteMeterAfterUpdate_TRG')) {
    	SICo_SiteMeterTrigger_CLS.handleAfterInsertUpdate(Trigger.new);
	}     
}