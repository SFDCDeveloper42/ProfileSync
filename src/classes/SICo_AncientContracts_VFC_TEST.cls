/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Test of SICo_AncientContracts_VFC
History
29/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
@IsTest 
public with sharing class SICo_AncientContracts_VFC_TEST {

	@testSetup static void initData(){
        User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser;

        User us = [SELECT accountId FROM User WHERE Id =:communityUser.Id LIMIT 1];

        SICo_Site__c site = SICo_UtilityTestDataSet.createSite(us.accountID);
        site.BoilerBrand__c = 'brand';
        site.BoilerModel__c = 'model';
        site.IsActive__c = true;
        site.IsSelectedInCustomerSpace__c = true;
        insert site;

	    //Subsiption creation
        Zuora__Subscription__c subSowee = new Zuora__Subscription__c(
            Zuora__Status__c= 'Active',
            Zuora__OriginalId__c='original',
            Zuora__PreviousSubscriptionId__c='original',
            Name= 'soweeContract',
            Zuora__Account__c= us.accountID,
            SiteId__c= site.Id,
            Zuora__TermStartDate__c = Date.today(),
			InvoicingType__c = SICo_UtilityTestDataSet.INVOICING_TYPE_PICKLIST_MENSUEL_REEL,
			Zuora__MRR__c = 22,
			OfferName__c = 'SE_TH_GAZ',
			Zuora__TermEndDate__c = Date.today()
            );
        insert subSowee;

        Zuora__Subscription__c subMaintenance = new Zuora__Subscription__c(
            Zuora__Status__c= 'Active',
            Zuora__OriginalId__c='original',
            Zuora__PreviousSubscriptionId__c='original',
            Name= 'soweeContract',
            Zuora__Account__c= us.accountID,
            SiteId__c= site.Id,
            Zuora__TermStartDate__c = Date.today(),
			InvoicingType__c = SICo_UtilityTestDataSet.INVOICING_TYPE_PICKLIST_MENSUEL_REEL,
			Zuora__MRR__c = 22,
			OfferName__c = 'MAINT',
			Zuora__TermEndDate__c = Date.today()
            );
        insert subMaintenance;

    }

    @isTest
	static void test_AncientContracts() {

		User us = [SELECT accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        //Run as Community User without cr
	    system.runAs(us){
	        Test.startTest();
	            SICo_AncientContracts_VFC controller = new SICo_AncientContracts_VFC();
	            PageReference pRef = Page.AncientContracts;
		        Test.setCurrentPageReference(pRef);

		        System.assertNotEquals(controller.subSowee, null);
		        System.assertNotEquals(controller.subMaintenance, null);
	        Test.stopTest();
        }

	}

}