<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SICO_TVAwf</fullName>
        <description>[SICO] update TVA wf field</description>
        <field>SICO_TauxTVAWF__c</field>
        <formula>TauxTVA__c</formula>
        <name>TVAwf</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TotalAmount</fullName>
        <description>[SICO] retrieve the value of charge amount in total amount</description>
        <field>TotalAmount__c</field>
        <formula>ChargeAmount__c</formula>
        <name>TotalAmount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SICO-ChargeAmount</fullName>
        <actions>
            <name>TotalAmount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>[SICO] retrieve the value of charge amount in total amount</description>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SICO_TVA</fullName>
        <actions>
            <name>SICO_TVAwf</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>[SICO] TVA</description>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
