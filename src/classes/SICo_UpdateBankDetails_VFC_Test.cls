/*------------------------------------------------------------
Author:        Cyril PENGAM 
Company:       Ikumbi
Description:   Test of the controller for the UpdateBankDetail page: SICo_UpdateBankDetails_VFC
History
Sept. 2016      Cyril PENGAM     Create version
------------------------------------------------------------*/
@isTest(seeAllData=True) // required by zuora :-(
private class SICo_UpdateBankDetails_VFC_Test {
    
    @isTest 
    static void test_Constructor() {
    	//Insert new community user
        User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser;
        communityUser = [SELECT AccountId, ContactId, ProfileId FROM User WHERE Id = :communityUser.Id LIMIT 1];

    	//Insert custom settings
    	ZuoraWSconfig__c zc = new ZuoraWSconfig__c();
		zc.Uri__c = 'Test Uri';
		zc.PageIdSEPA__c = 'Test PageId';
        zc.PageIdCB__c = 'Test PageId';
        zc.ExternalPageIdSEPA__c = 'Test PageId';
        zc.ExternalPageIdCB__c = 'Test PageId';
        zc.GatewayNameSEPA__c  = 'SEPA';
        zc.SetupOwnerId = communityUser.ProfileId;
		insert zc;

        //select account
        Account acc = [SELECT Id, IsPersonAccount FROM Account WHERE Id = :communityUser.AccountId LIMIT 1];

        //Insert new payment method
    	Zuora__PaymentMethod__c zuoraPM = SICo_UtilityTestDataSet.createZuoraPaymentMethod(communityUser.AccountId, true);
    	zuoraPM.Zuora__PaymentMethodStatus__c = SICo_Constants_CLS.ACTIVE_PAYMENT_METHOD_STATUS;
    	zuoraPM.Zuora__Type__c = SICo_Constants_CLS.ZUORA_PAYMENT_METHOD_BANK_TRANSFERT;
    	insert zuoraPM;
 
    	//Run as Community User
        system.runAs(communityUser){
            Test.startTest();
        	Test.setMock(HttpCalloutMock.class, new SICo_UpdateBankDetails_Mock());

            //set controller
            SICo_UpdateBankDetails_VFC controller = new SICo_UpdateBankDetails_VFC();
            //set VF
            Test.setCurrentPageReference(new PageReference('Page.UpdateBankDetails'));
            //set url parameters
			System.currentPageReference().getParameters().put('AccountId', communityUser.AccountId);

			String jsonSignature = SICo_UpdateBankDetails_VFC.processSignature();
            
			Boolean updateDefaultAndOldPaymentMethodResult = SICo_UpdateBankDetails_VFC.updateDefaultAndOldPaymentMethod(acc.Id, 'testNewZuoraPaymentMethodId');
			System.debug('### updateDefaultAndOldPaymentMethodResult : ' + updateDefaultAndOldPaymentMethodResult);
            Test.stopTest();

            //asserts
            System.assert(controller.accountId == communityUser.AccountId);
			System.assert(jsonSignature == '{"key":"test","token":"test","signature":"test","pageId":"Test PageId","tenantId":"test"}', 'Fail due to JSON signature that is: ' + jsonSignature);

        }
	} 
	
}