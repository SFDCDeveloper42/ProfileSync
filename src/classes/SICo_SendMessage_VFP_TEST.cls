/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Test class for the SICo_SendMessage_VFP Controller 
Test Class:    SICo_SendMessage_VFP_TEST
History
30/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
@isTest
public class SICo_SendMessage_VFP_TEST {

	//set test data
	@testSetup
	static void testData() {

		User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
	    insert communityUser;
	}

	@isTest
	static void testCasesWithoutURLId() {

		User us = [SELECT Id, accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        //Run as Community User
	    system.runAs(us){
	        Test.startTest();
	            SICo_SendMessage_VFP controller = new SICo_SendMessage_VFP();
	            PageReference pRef = Page.SendMessage;
            	pRef.getParameters().put('accountId', us.accountId);
		        Test.setCurrentPageReference(pRef);

		        controller.description = 'test description';
                controller.isWebcallback =true;
		        controller.createCase();

		        List<Case> listCases = [SELECT Id, Description FROM Case WHERE AccountId = :us.accountId];
		        System.assertNotEquals(listCases.size(), null);
            
            	controller.motif = 'Offres & Services ';
            
            	controller.getObjects();
            
            	controller.getMotifs();
            
            	controller.updateObjects();
                controller.createCase();

            	System.assertNotEquals(controller.listObjet.size(), null);
            
            	PageReference cases = Page.Cases;
            
            	PageReference cancel = controller.cancelCase();
            
            	System.assertEquals(cases.getURL(), cancel.getUrl());
            	
	        Test.stopTest();
        }
	}
    
    @IsTest
    static void testCasesWithURLId() {
        
        Test.startTest();
            SICo_SendMessage_VFP controller = new SICo_SendMessage_VFP();
            PageReference pRef = Page.SendMessage;
            Test.setCurrentPageReference(pRef);
            
            controller.description = 'test description';
            controller.motif = 'Offres & Services';
            controller.isWebcallback = true;
            controller.createCase();
            
            System.assertEquals(controller.listMotif.size(), 0);
            System.assertEquals(controller.listObjet.size(), 0);
        
        Test.stopTest();
    }

}