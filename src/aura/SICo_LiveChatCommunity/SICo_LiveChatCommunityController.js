({

    doInit: function(cmp) {
    	var actionCheck = cmp.get("c.isLiveChatAvailable");

        actionCheck.setCallback(this, function(response) {
        	var state = response.getState();

        	if (cmp.isValid() && state === "SUCCESS") {
        		cmp.set("v.isAvailable", response.getReturnValue());

        	}else if (cmp.isValid()) {
        		var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
        	}
        });
        $A.enqueueAction(actionCheck);
    },

	openPopUp : function(){
		console.log('click');
        var CheminComplet = document.location.href;
        var pathArray = CheminComplet.split( '/' );
		var protocol = pathArray[0];
		var host = pathArray[2];
		var url = protocol + '//' + host;
    	var newwindow = window.open(url+'/apex/LiveChatPopUp', '_blank');
    	newwindow.focus();
    }
})