/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi-solutions
Description:   Controller for the Energy Profile process (CommunitiesLogin)
Test Class:    SICo_EnergyProfile_VFC_TEST
History
29/08/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
public with sharing class SICo_EnergyProfile_VFC {

	//AccountId for filtering all data displayed on the page
	public Id accountId {get; set;}
	// public Id siteId{get;set;}

	public SICo_EnergyProfile_VFC() {
		//Get AccountId
		String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
		try {
			this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
		} catch (exception e) {
			System.debug('Unable to return accountId');
		}
	}

	/**
	* Request current SICo site of the account (id passed as parameter)
	* @param : pAccountId => Id of the current account
	*/
	@AuraEnabled
	public static SICo_Site__c getCurrentSite(Id pAccountId) {
		List<SICo_Site__c> vSiteList = [SELECT Id, SiteName__c, IsSelectedInCustomerSpace__c, HousingType__c, ResidenceType__c,
											Subscriber__c, HousingLocationType__c, IsCornerApartment__c, IsArrangedRoofPresent__c,
											IsVerandaPresent__c, IsFloorPresent__c, SanitaryHotWaterType__c, IsJointHouse__c, GasSource__c,
											IsCeramicGlassPlatePresent__c, ConstructionDate__c, IsWindowRestored__c, IsWallRestored__c,
											IsArrangedRoofRestored__c, IsWineCellarPresent__c, HasSwimmingPool__c, IsAquariumPresent__c,
											SurfaceInSqMeter__c, SurfaceAC__c, InstructionValueAC__c, IsACPresent__c, 
											PrincipalHeatingSystemType__c, ElectricityTariffOption__c, SubscribedPower__c, BoilerAge__c, 
											NoOfOccupants__c, PrincHeatingSystemTypeDetail__c
										FROM SICo_Site__c
										WHERE Subscriber__c = :pAccountId 
										AND IsActive__c = true 
										AND IsSelectedInCustomerSpace__c = true
										ORDER BY IsSelectedInCustomerSpace__c DESC];
		SICo_Site__c vSite = null;
		if (vSiteList != null && vSiteList.size() > 0 ) { 
			vSite = vSiteList[0];
		}

		return vSite;
	}
	
	@AuraEnabled
	public static EneryProfileWrap getCurrentEnergyProfileWrapper(String pAccountId) {
		if (pAccountId == null) {
			return null;
		}
		EneryProfileWrap vEPW = new EneryProfileWrap();
		SICo_Site__c vSite = getCurrentSite(pAccountId);
		if (vSite != null) {
			vEPW.Id                      = vSite.Id;
			vEPW.houseType               = vSite.HousingType__c; 
			vEPW.residenceType           = vSite.ResidenceType__c; 
			vEPW.placementLocation       = vSite.HousingLocationType__c;
			vEPW.positionCorner          = vSite.IsCornerApartment__c;
			vEPW.builtDate               = vSite.ConstructionDate__c;
			vEPW.roofPresent             = vSite.IsArrangedRoofPresent__c;
			vEPW.floorPresent            = vSite.IsFloorPresent__c;
			vEPW.verandaPresent          = vSite.IsVerandaPresent__c;
			vEPW.jointHouse              = vSite.IsJointHouse__c;
			vEPW.roofRestored            = vSite.IsArrangedRoofRestored__c;
			vEPW.wallsRestored           = vSite.IsWallRestored__c;
			vEPW.windowsRestored         = vSite.IsWindowRestored__c;
			vEPW.surface                 = vSite.SurfaceInSqMeter__c;
			vEPW.numberOfOccupants       = vSite.NoOfOccupants__c;
			vEPW.heating                 = vSite.PrincipalHeatingSystemType__c;
			vEPW.age                     = vSite.BoilerAge__c;
			vEPW.hotWater                = vSite.SanitaryHotWaterType__c;
			vEPW.cooking                 = vSite.GasSource__c;
			vEPW.ceramic                 = vSite.IsCeramicGlassPlatePresent__c;			 
			vEPW.wineCellar              = vSite.IsWineCellarPresent__c;
			vEPW.swimmingPool            = vSite.HasSwimmingPool__c;
			vEPW.aquarium                = vSite.IsAquariumPresent__c;			 
			vEPW.surfaceAC               = vSite.SurfaceAC__c;
			vEPW.instructionValueAC      = vSite.InstructionValueAC__c;
			vEPW.noAC                    = vSite.IsACPresent__c;
			vEPW.electricityTariffOption = vSite.ElectricityTariffOption__c;
			vEPW.subscribedPower         = vSite.SubscribedPower__c;
		}

		return vEPW;
	}

	public class EneryProfileWrap {
		@AuraEnabled
		public String Id { get; set; }
		@AuraEnabled
		public String houseType { get; set; } 
		@AuraEnabled
		public String residenceType { get; set; } 
		@AuraEnabled
		public String placementLocation { get; set; } 
		@AuraEnabled
		public Boolean positionCorner { get; set; } 
		@AuraEnabled
		public Decimal builtDate { get; set; } 
		@AuraEnabled
		public Boolean roofPresent { get; set; } 
		@AuraEnabled
		public Boolean floorPresent { get; set; } 
		@AuraEnabled
		public Boolean verandaPresent { get; set; } 
		@AuraEnabled
		public Boolean jointHouse { get; set; } 
		@AuraEnabled
		public Boolean roofRestored { get; set; }
		@AuraEnabled
		public Boolean wallsRestored { get; set; }
		@AuraEnabled
		public Boolean windowsRestored { get; set; }
		@AuraEnabled
		public Decimal surface { get; set; }
		@AuraEnabled
		public Decimal numberOfOccupants { get; set; }
		@AuraEnabled
		public String heating { get; set; }
		@AuraEnabled
		public String age { get; set; }
		@AuraEnabled
		public String hotWater { get; set; }
		@AuraEnabled
		public String cooking { get; set; }
		@AuraEnabled
		public Boolean ceramic { get; set; }
		@AuraEnabled
		public Boolean wineCellar { get; set; }
		@AuraEnabled
		public Boolean swimmingPool { get; set; }
		@AuraEnabled
		public Boolean aquarium { get; set; } 
		@AuraEnabled
		public Decimal surfaceAC { get; set; } 
		@AuraEnabled
		public Decimal instructionValueAC { get; set; } 
		@AuraEnabled
		public Boolean noAC { get; set; } 
		@AuraEnabled
		public String electricityTariffOption { get; set; } 
		@AuraEnabled
		public String subscribedPower { get; set; } 
		
	}

	// LIGHTNING COMPONENTS

	@AuraEnabled
	public static void setInformations_02(Id siteId, String choiceHouse, String choiceResidence) {
		SICo_Site__c oSite = null;

		List<SICo_Site__c> lstSICoSites = [SELECT Id, HousingType__c, ResidenceType__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.HousingType__c = choiceHouse;
			oSite.ResidenceType__c = choiceResidence;
			update oSite;
		}
	}
	//redirect on a error page if the customer does not have active site.
	public  PageReference doRedirect() {

		List<SICo_Site__c> siteList = [SELECT Id, SiteName__c, IsSelectedInCustomerSpace__c
		                               FROM SICo_Site__c
		                               WHERE Subscriber__c = :accountId AND IsActive__c = true AND IsSelectedInCustomerSpace__c = true
		                              ];
		if (siteList.size() == 0) {

			PageReference pageRef = new PageReference(Sico_Constants_CLS.CUSTOMER_NO_SITE_URL);
			return pageRef;

		} else return null;



	}
	@AuraEnabled
	public static void setInformations_03_Appart(Id siteId, String emplacement, Boolean position) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, HousingLocationType__c, IsCornerApartment__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.HousingLocationType__c = emplacement;
			oSite.IsCornerApartment__c = position;
			update oSite;
		}

	}

	@AuraEnabled
	public static void setInformations_03_House(Id siteId, Boolean position, Boolean roof, Boolean floor, Boolean veranda) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, IsJointHouse__c, IsArrangedRoofPresent__c, IsVerandaPresent__c, IsFloorPresent__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.IsArrangedRoofPresent__c = roof;
			oSite.IsFloorPresent__c = floor;
			oSite.IsVerandaPresent__c = veranda;
			oSite.IsJointHouse__c = position;
			update oSite;
		}

	}


	@AuraEnabled
	public static void setInformations_04(Id siteId, Decimal dateConstruction) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, ConstructionDate__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.ConstructionDate__c = dateConstruction;
			update oSite;
		}
	}

	@AuraEnabled
	public static void setInformations_05(Id siteId, Boolean windows, Boolean walls, Boolean roof) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, IsArrangedRoofRestored__c, IsWallRestored__c, IsWindowRestored__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.IsArrangedRoofRestored__c = roof;
			oSite.IsWallRestored__c = walls;
			oSite.IsWindowRestored__c = windows;
			update oSite;
		}
	}

	@AuraEnabled
	public static void setInformations_05Bis(Id siteId, Decimal surface) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, surfaceInSqMeter__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.surfaceInSqMeter__c = surface;
			update oSite;
		}
	}

	@AuraEnabled
	public static void setInformations_06(Id siteId, Decimal numberOfOccupants) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, NoOfOccupants__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.NoOfOccupants__c = numberOfOccupants;
			update oSite;
		}
	}

	@AuraEnabled
	public static void setInformations_07(Id siteId, String heating, String age) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, PrincipalHeatingSystemType__c, BoilerAge__c, PrincHeatingSystemTypeDetail__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.PrincipalHeatingSystemType__c = heating;
			oSite.BoilerAge__c = age;

			if (heating == 'Electricite') {
				oSite.PrincHeatingSystemTypeDetail__c = 'Convecteur';
			} else if (heating == 'PompeAChaleur') {
				oSite.PrincipalHeatingSystemType__c = 'Electricite';
				oSite.PrincHeatingSystemTypeDetail__c = 'SplitAvecPAC';
			} else if (heating == 'Gaz') {
				if (age == 'Moins de 5 ans') {
					oSite.PrincHeatingSystemTypeDetail__c = 'GazChaudiereCondensation';
				} else if (age == 'Plus de 5 ans') {
					oSite.PrincHeatingSystemTypeDetail__c = 'GazChaudiereStandard';
				}
			} else if (heating == 'Autres') {
				oSite.PrincHeatingSystemTypeDetail__c = 'AutreEnergie';
			} else {
				oSite.PrincHeatingSystemTypeDetail__c = '';
			}

			update oSite;
		}

	}


	/**
	 * [Modifies the site informations from the fields filled on the page EnergyProfile08]
	 * @param  siteId               [Id of the site that will be modified]
	 * @param  hotWaterOperation    [Type of operation of hot water]
	 */
	@AuraEnabled
	public static void setInformations_08(Id siteId, String hotWaterOperation) {

		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, SanitaryHotWaterType__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.SanitaryHotWaterType__c = hotWaterOperation;
			update oSite;
		}
	}

	/**
	 * [Modifies the site informations from the fields filled on the page EnergyProfile09]
	 * @param  siteId   [Id of the site that will be modified]
	 * @param  cooking  [Type of cooking used]
	 */
	@AuraEnabled
	public static void setInformations_09(Id siteId, String cooking) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, GasSource__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			if (cooking == 'Electrique') {
				oSite.GasSource__c = null;
				oSite.IsCeramicGlassPlatePresent__c = true;
			} else {
				oSite.GasSource__c = cooking;
				oSite.IsCeramicGlassPlatePresent__c = false;
			}
			update oSite;
		}
	}

	/**
	 * [Modifies the site informations from the fields filled on the page EnergyProfile_10]
	 * @param  siteId       [Id of the site that will be modified]
	 * @param  wineCellar   [Boolean - True if there is a wine cellar]
	 * @param  swimmingPool [Boolean - True if there is a swimming pool]
	 * @param  aquarium     [Boolean - True if there is an aquarium]
	 */
	@AuraEnabled
	public static void setInformations_10(Id siteId, Boolean wineCellar, Boolean swimmingPool, Boolean aquarium) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, IsWineCellarPresent__c, hasSwimmingPool__c, IsAquariumPresent__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.IsWineCellarPresent__c = wineCellar;
			oSite.hasSwimmingPool__c = swimmingPool;
			oSite.IsAquariumPresent__c = aquarium;
			update oSite;
		}
	}

	/**
	 * [Modifies the site informations from the fields filled on the page EnergyProfile_11]
	 * @param  siteId               [Id of the site that will be modified]
	 * @param  surfaceAC            [Decimal - The surface of AC zone]
	 * @param  instructionValueAC   [Decimal - The instruction value for AC]
	 * @param  noAC                 [Boolean - True is there is no AC]
	 */
	@AuraEnabled
	public static void setInformations_11(Id siteId, Decimal surfaceAC, Decimal instructionValueAC, Boolean noAC) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, SurfaceAC__c, InstructionValueAC__c, IsACPresent__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.SurfaceAC__c = surfaceAC;
			oSite.InstructionValueAC__c = instructionValueAC;
			oSite.IsACPresent__c = !noAC;
			update oSite;
		}
	}

	/**
	 * [Modifies the site informations from the fields filled on the page EnergyProfile_12]
	 * @param  siteId                   [Id of the site that will be modified]
	 * @param  electricityTariffOption  [String - Tariff of electricity option]
	 * @param  subscribedPower          [String - Subscribed power value]
	 */
	@AuraEnabled
	public static void setInformations_12(Id siteId, String electricityTariffOption, String subscribedPower) {
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, ElectricityTariffOption__c, SubscribedPower__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId];

		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];
			oSite.ElectricityTariffOption__c = electricityTariffOption;
			oSite.SubscribedPower__c = subscribedPower;
			update oSite;
		}
	}

	/**
	 * [Call the async methode getEvalConsoAsync to calculate consomation evaluation of the current site]
	 * @param  siteId                   [String of the site that will be modified]
	 */
	@AuraEnabled
	public static void getEvalConso(String siteId) {//eval-conso

		getEvalConsoAsync(siteId);
	}

	/**
	 * [Call the private methode getEvalConsoAsyncDoTreatment to calculate consomation evaluation of the current site]
	 * @param  siteId                   [String of the site that will be modified]
	 */
	@testVisible
	@future(callout = true)
	private static void getEvalConsoAsync(String siteId) {

		getEvalConsoAsyncDoTreatment(siteId);
	}

	/**
	 * [Calculate the consomation evaluation of the current site and save it]
	 * @param  siteId                   [String of the site that will be modified]
	 */
	@testVisible
	private static void getEvalConsoAsyncDoTreatment(String siteId) {
		System.debug('#### 000 getConso siteId : ' + siteId);
		// Retrieve the current site
		SICo_Site__c oSite = null;
		List<SICo_Site__c> lstSICoSites = [SELECT Id, PrincipalHeatingSystemType__c, SanitaryHotWaterType__c,
		                                   GasSource__c, NoOfOccupants__c, surfaceInSqMeter__c, HousingType__c,
		                                   InseeCode__c, EnergyDiagnostic__c, RenewableHeating__c, ConstructionDate__c,
		                                   EstimatedGasConsumption__c, GasTariffOption__c, ResidenceType__c,
		                                   isProfileBeSentToEdelia__c, Subscriber__c, Subscriber__r.CustomerNumber__c
		                                   FROM SICo_Site__c
		                                   WHERE Id = :siteId AND IsActive__c = true]; //Multi-site is not managed in MVP. Site must be active before being sent to Edelia

		System.debug('#### 111 getConso lstSICoSites : ' + lstSICoSites);
		// If site is retrieved
		if (!lstSICoSites.isEmpty() && lstSICoSites.size() == 1) {
			oSite = lstSICoSites[0];

			System.debug('#### 222 getConso oSite : ' + oSite);
			// If suffisient fields are filled, call the service
			if (oSite.PrincipalHeatingSystemType__c != null
			        && oSite.SanitaryHotWaterType__c != null
			        // && oSite.GasSource__c != null
			        && oSite.NoOfOccupants__c != null
			        && oSite.surfaceInSqMeter__c != null
			        && oSite.InseeCode__c != null
			        && oSite.ConstructionDate__c != null) {

				// System.debug(System.LoggingLevel.DEBUG, '#### request : ' + JSON.serialize(new EvalConsoRequest(oSite)));
				SICo_BoomiEndpoint__mdt boomiEndpoint = [SELECT EndPoint__c FROM SICo_BoomiEndpoint__mdt WHERE DeveloperName = 'EvalConso'];
				HttpResponse response;

				System.debug('#### 333 getConso boomiEndpoint : ' + boomiEndpoint);
				try {
					response = SICo_Utility.getCallout('POST', boomiEndpoint.EndPoint__c, JSON.serialize(new EvalConsoRequest(oSite)));
					System.debug('#### 444 getConso response : ' + response);
				} catch (Exception e) {
					SICo_LogManagement.sendErrorLog(SICo_Constants_CLS.SICO, SICo_Constants_CLS.BOOMI, e.getMessage());
				}

				if (response != null && response.getStatus() == 'OK') {
					EvalConsoResponse oEvalConsoResponse = new EvalConsoResponse();
					System.debug(System.LoggingLevel.DEBUG, '### 444.111 response.getBody() : ' + response.getBody());
					Map<String, Object> tmpEvalConsoMap = (Map<String, Object>) JSON.deserializeUntyped (response.getBody());
					System.debug('### 444.222 tmpEvalConsoMap: ' + tmpEvalConsoMap);
					if (!tmpEvalConsoMap.isEmpty() && tmpEvalConsoMap.containsKey('data')) {
						Object objData = tmpEvalConsoMap.get('data');
						system.debug('### objData: ' + objData);
						String strData = JSON.serialize(objData);
						system.debug('### strData: ' + strData);
						oEvalConsoResponse = (EvalConsoResponse) JSON.deserialize(strData, EvalConsoResponse.class);
					}

					System.debug('#### 555 getConso oEvalConsoResponse : ' + oEvalConsoResponse);

					// If status is OK, update the site
					if (oEvalConsoResponse.statut == SICo_Constants_CLS.BOOMI_SERVICE_OK_RESPONSE_STATUS) {
						oSite.GasTariffOption__c = oEvalConsoResponse.value.categorie;
						oSite.EstimatedGasConsumption__c = oEvalConsoResponse.value.cso;
						//Send Updated profile to Edelia
						List<SICO_Site__c> listSite = new List<SICO_Site__c>();
						listSite.add(oSite);
						System.debug('#### 666 getConso lauching profile ');
						SICO_FluxSite_CLS.launchProfilesNow(listSite);
						System.debug('#### 666 getConso profile launched ');
						//Update site
						update oSite;
					} else {
						SICo_LogManagement.sendErrorLog(SICo_Constants_CLS.SICO, Label.SICO_LOG_USER_Conso, oEvalConsoResponse.message);
					}
				}
			}
		}//ENDIF !lstSICoSites.isEmpty() && lstSICoSites.size() == 1
	}

	private class EvalConsoRequest {
		public String chauffageGaz;
		public String eauChaudeGaz;
		public String cuissonGaz;
		public String nbrOccupant;
		public String surfaceLogement;
		public String typeLogement;
		public String typeResidence;
		public String codeInsee;
		public String dpe;
		public String chauffAlternatif;
		public String dateConstruction;

		public EvalConsoRequest(SICo_Site__c site) {
			chauffageGaz = (site.PrincipalHeatingSystemType__c == 'Gaz' ? 'O' : 'N');
			eauChaudeGaz = (site.SanitaryHotWaterType__c == 'Gaz' ? 'O' : 'N');
			cuissonGaz = (site.GasSource__c != null ? 'O' : 'N');
			nbrOccupant = (site.NoOfOccupants__c != null ? site.NoOfOccupants__c.intValue() + '' : '');
			surfaceLogement = (site.surfaceInSqMeter__c != null ? site.surfaceInSqMeter__c.intValue() + '' : '');
			typeLogement = (site.HousingType__c != null ? site.HousingType__c.toUpperCase() : '') ;
			typeResidence = (site.ResidenceType__c != null ? site.ResidenceType__c.toUpperCase() : '') ;
			codeInsee = site.InseeCode__c;
			dpe = (site.EnergyDiagnostic__c != null ? site.EnergyDiagnostic__c : 'NR');
			chauffAlternatif = (site.RenewableHeating__c != null ? 'O' : 'N');
			dateConstruction = (site.ConstructionDate__c != null ? site.ConstructionDate__c.intValue() + '' : '');
		}
	}

	public class EvalConsoResponse {
		public String statut;
		public String message;
		public EvalConsoResponseValue value;

		public EvalConsoResponse() { }

		public EvalConsoResponse(String statut, String message, EvalConsoResponseValue value) {
			this.statut = statut;
			this.message = message;
			this.value = value;
		}
	}

	public class EvalConsoResponseValue {
		public Integer cso;
		public String categorie;

		public EvalConsoResponseValue(Integer cso, String categorie) {
			this.cso = cso;
			this.categorie = categorie;
		}
	}
}