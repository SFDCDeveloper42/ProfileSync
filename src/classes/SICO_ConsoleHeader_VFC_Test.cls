@isTest
private class SICO_ConsoleHeader_VFC_Test {
    
	@testSetup
    private static void setup() {
    	Account a = new Account(name='test');
    	insert a;

    	Zuora__CustomerAccount__c b = new Zuora__CustomerAccount__c(name='test aussi',Zuora__Account__c=a.id);
    	insert b;

    	Zuora__Subscription__c c = new Zuora__Subscription__c(name='sub',Zuora__Account__c=a.id,Zuora__Status__c='Active');
    	insert c;
    }

	@isTest
    private static void testOne() {
    	Account a = [SELECT id FROM Account WHERE name = 'test'];
    	PageReference pageRef = Page.SICO_ConsoleHeader_VFP;
    	Test.setCurrentPageReference(pageRef);
    	ApexPages.CurrentPage().getparameters().put('id', a.id);
    	ApexPages.StandardController con = new ApexPages.StandardController(a);
    	SICO_ConsoleHeader_VFC c_myExtension = new SICO_ConsoleHeader_VFC(con);
        
    }
}