({
	displayNextStep : function(pComponentToDisplay, pEventToRaise, pJQueryComponent) { 
		if (pEventToRaise != null) {
			pJQueryComponent.addClass("hiddenComponent");  
			$(".btnValidate").removeClass("btnValidateDisabledTemp");      
			pJQueryComponent.show(); // after a fadeout display: none is added directly to the component, call show function allows to remove it
			pJQueryComponent.scrollTop();
			pEventToRaise.setParams({
				"componentToDisplayNow" : pComponentToDisplay
			});
			pEventToRaise.fire();  
		}
	}
})