/*------------------------------------------------------------
Author:        Mehdi Beggas
Company:       Ikumbi-solutions
Description:   Controller for the Lightning Component Test (SICo_LeftMenu)
History
17/08/2016      Mehdi Beggas      Create version
------------------------------------------------------------*/
@IsTest
private class SICo_LeftMenu_LC_TEST {

	@IsTest
	static void getSiteFromAccountId(){
		//création des données
		User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
		communityUser.IsActive = true;
		insert communityUser;
		List<User> listCommUser = [SELECT Id, AccountId FROM User WHERE Id =: communityUser.Id];

		List<SICo_Site__c> testListSites = new List<SICo_Site__c>();
        testListSites.add(SICo_UtilityTestDataSet.createSite(listCommUser.get(0).AccountId));
        insert testListSites;
        //Run as Community User
        system.runAs(listCommUser.get(0)){
            Test.startTest();
            	testListSites = SICo_LeftMenu_LC.getSites(listCommUser.get(0).AccountId);
            Test.stopTest();
            //Expected result: AccountId of the Community User is returned
            system.assertEquals(1, testListSites.size());
        }
	}

	@IsTest
	static void changeActivSite(){
		//création des données
		User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
		insert communityUser;
		List<User> listCommUser = [SELECT Id, AccountId FROM User WHERE Id =: communityUser.Id LIMIT 1];

        List<SICo_Site__c> testListSites = new List<SICo_Site__c>();
        testListSites.add(SICo_UtilityTestDataSet.createSite(listCommUser.get(0).AccountId));
        testListSites.add(SICo_UtilityTestDataSet.createSite(listCommUser.get(0).AccountId));
        testListSites.add(SICo_UtilityTestDataSet.createSite(listCommUser.get(0).AccountId));
        for(SICo_Site__c site : testListSites)
        	site.IsActive__c = true;
        insert testListSites;
        //Run as Community User
        system.runAs(listCommUser.get(0)){
            Test.startTest();
            	SICo_LeftMenu_LC.setSelectSite(testListSites.get(0).Id);
            Test.stopTest();
            testListSites = [SELECT Id, SiteName__c FROM SICo_Site__c 
                             WHERE Subscriber__c = :listCommUser.get(0).AccountId AND IsSelectedInCustomerSpace__c = true];
            //Expected result: AccountId of the Community User is returned
            system.assertEquals(1, testListSites.size());
        }
    }

}