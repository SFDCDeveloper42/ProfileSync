public with sharing class SICo_Utility_Docusign_CLS {
		
	public 	class DCS_Authentication {
		public String Username;
		public String Password;
		public String IntegratorKey;
	}

	public class DCS_TemplateRoles {
		public String roleName;
		public String name;
		public String email;
		public String clientUserId;
		public DCS_Tab tabs;
	}

	public class DCS_EnvelopeRequest {
		public String emailSubject;
		public String templateId;
		public List<DCS_TemplateRoles> templateRoles;
		public String status;
		public DCS_customFields customFields;

		DCS_EnvelopeRequest() {
			templateRoles = new List<DCS_TemplateRoles>();
		}
	}

	public class DCS_RecipientRequest {
		public String authenticationMethod;
		public String clientUserId;
		public String email;
		public String recipientId;
		public String returnUrl;
		public String userName;
	}

	public class DCS_TextTab {
		public String tabLabel;
		public String value;
	}

	public class DCS_Tab {
		public List<DCS_TextTab> textTabs;

		DCS_Tab(){
			textTabs = new List<DCS_TextTab>();
		}
	}

	public class DCS_customFields {
		public List<DCS_textCustomField> textCustomFields;

		DCS_customFields(){
			textCustomFields = new List<DCS_textCustomField>();
		}
	}

	public class DCS_textCustomField {
		public String name;
	    public boolean show;
	    public String value;

	    DCS_textCustomField(String p_name, String p_value) {
	    	this.name = p_name;
	    	this.value = p_value;
	    	this.show = false;
	    }

	}

	public static String getRecipientUrl(DCS_EnvelopeRequest e, String s_envelopeId, String returnUrl) {
		DCS_RecipientRequest r = new DCS_RecipientRequest();

		r.authenticationMethod = 'email';
		r.clientUserId = e.templateRoles.get(0).clientUserId;
		r.email = e.templateRoles.get(0).email;
		r.returnUrl = returnUrl;
		r.userName = e.templateRoles.get(0).name;

		String res = doCallout(JSON.serialize(r), '/envelopes/'+ s_envelopeId +'/views/recipient');
		System.debug(res);

		Map<String, Object> mResponse = (Map<String, Object>) JSON.deserializeUntyped(res);
		String url = (String) mResponse.get('url');
		return url;
	}

	public static DCS_TextTab createTextTab(String property, String value) {
		DCS_TextTab t = new DCS_TextTab();
		t.value = value;
		t.tabLabel = property;

		return t;
	} 

	public static DCS_EnvelopeRequest fillEnvelope(String accountId, Boolean inLine, String paymentMethodId) {
		
		Docusign_Envelope_Settings__mdt[] dcs_mdt_settings = [SELECT DeveloperName, Value__c FROM Docusign_Envelope_Settings__mdt];
		Map<String, String> dcs_settings = new Map<String, String>();
		for (Docusign_Envelope_Settings__mdt p : dcs_mdt_settings)
			dcs_settings.put(p.DeveloperName, p.Value__c);
		
		Zuora.zApi zApiInstance = SICo_ZuoraUtilities.zuoraApiAccess();
		String Query = 'SELECT BankTransferAccountNumberMask, Id, MandateID FROM PaymentMethod WHERE Id = \'' + paymentMethodId + '\'';
		
		List<Zuora.zObject> l_payment;
		try {
			l_payment = SICo_ZuoraUtilities.queryToZuora(zApiInstance, Query);
		} catch (exception e){
            System.debug('Unable to return payment');
        }

        String iban = '';
        String mandate = '';
        String pmid = '';
        
        for (Zuora.zObject o : l_payment) {
            iban = (String)o.getValue('BankTransferAccountNumberMask');
            mandate = (String)o.getValue('MandateID');
            pmid = (String)o.getValue('Id');
        }
        
		DCS_EnvelopeRequest e = new DCS_EnvelopeRequest();
		DCS_TemplateRoles t = new DCS_TemplateRoles();	

		e.emailSubject = dcs_settings.get('emailSubject'); //'Sowee Signature du mandat Sepa';
		e.templateId = dcs_settings.get('templateId'); //'6c0230e7-eec3-4fea-9bbb-50ff9021bcdb';
		t.roleName = dcs_settings.get('roleName');

		SICo_PersonalInformation_VFC infos = new SICo_PersonalInformation_VFC(accountId);

		t.name = infos.piw.salutationAndName; //'Gerald Ramier';
		t.email = infos.piw.email;//'gramier@salesforce.com';
		t.clientUserId = inLine ? accountId : '';
		//Completion of predefined fields
		DCS_Tab res = new DCS_Tab();
		//res.textTabs = new List<DCS_TextTab>();
		res.textTabs.add(createTextTab('sepa_firstname', infos.piw.firstName));
		res.textTabs.add(createTextTab('sepa_lastname', infos.piw.lastName));
		res.textTabs.add(createTextTab('sepa_address', infos.piw.addressLine1));
		res.textTabs.add(createTextTab('sepa_city', infos.piw.addressLine3));
		res.textTabs.add(createTextTab('sepa_uniq_id', mandate));
		res.textTabs.add(createTextTab('sepa_iban', iban));
		res.textTabs.add(createTextTab('sepa_zipcode_1', infos.piw.addressLine3));
		res.textTabs.add(createTextTab('sepa_zipcode_2',''));
		res.textTabs.add(createTextTab('sepa_zipcode_3',''));
		res.textTabs.add(createTextTab('sepa_zipcode_4',''));
		res.textTabs.add(createTextTab('sepa_zipcode_5',''));
		res.textTabs.add(createTextTab('sepa_country', infos.piw.addressLine4));
		res.textTabs.add(createTextTab('sepa_location',''));
		res.textTabs.add(createTextTab('sepa_date', String.valueOf(Date.today())));
		t.tabs = res;
		//---
		//e.templateRoles = new List<DCS_TemplateRoles>();
		e.templateRoles.add(t);
		e.status = 'sent';

		e.customFields = new DCS_customFields();
		e.customFields.textCustomFields.add(new DCS_textCustomField('##SFAccount',accountId));
		e.customFields.textCustomFields.add(new DCS_textCustomField('DSFSSourceObjectId',paymentMethodId));
		
		return e;
	}


	public static String createEnvelope(Boolean inLine, String returnUrl, String accountId, String typeEnv, String params) {
		
		DCS_EnvelopeRequest e = fillEnvelope(accountId, inLine, params);

		System.debug('>>>>> MSG ' + JSON.serialize(e));

		String res = doCallout(JSON.serialize(e), '/envelopes');

		Map<String, Object> mResponse = (Map<String, Object>) JSON.deserializeUntyped(res);

		String envelopeId = (String) mResponse.get('envelopeId');
		String uri = (String) mResponse.get('uri');
		String statusDateTime = (String) mResponse.get('statusDateTime');
		String status = (String) mResponse.get('status');

		if (inLine) {
			status = getRecipientUrl(e, envelopeId, returnUrl);
		}

		return status;
	}

	public static String doCallout(String s_request, String s_endpoint) {
		CalloutConfig__c dcs_config = 	[SELECT 
											Login__c, Password__c, Account_Id__c, IntegratorKey__c, Endpoint__c 
										FROM 
											CalloutConfig__c WHERE Name = 'Docusign'
										];

		DCS_Authentication dcsCredentials = new DCS_Authentication();
		dcsCredentials.Username = dcs_config.Login__c; //'gramier@salesforce.com';
		dcsCredentials.Password = dcs_config.Password__c; //'p!qQ9Q6SRVAFVWf';
		dcsCredentials.IntegratorKey = dcs_config.IntegratorKey__c; //'ada32849-1532-4875-96c9-9cea80b6badb';
		String dcs_accountId = dcs_config.Account_Id__c; //'2198042';

		HttpRequest req = new HttpRequest();
        
        req.setMethod('POST');

        req.setHeader('Accept', 'application/json');
		req.setHeader('Content-Type', 'application/json');
		req.setHeader('X-DocuSign-Authentication', JSON.serialize(dcsCredentials));

		//Docusign/restapi/v2/accounts/
		req.setEndpoint('callout:' + dcs_config.Endpoint__c + dcs_accountId + s_endpoint);

		req.setBody(s_request);

		Http http = new Http();
        HTTPResponse res = http.send(req);
		String response = res.getBody();
		return response;
	}   
}