({
	validate : function(component) {

		$(".slds-spinner_container").removeClass('slds-hidden');
		$(".btnValidate").addClass("btnValidateDisabledTemp");
		var index = component.get("v.index");
		// "Disable" the validate button
		this.disableValidate(); 
		if (index === undefined || index === null) {
			this.displayError(component, $A.get("{!$Label.c.SICo_MeterRead_BadEntry}"), "Error_MeterRead");
			//alert($A.get("{!$Label.c.SICo_MeterRead_BadEntry}"));
			$(".slds-spinner_container").addClass('slds-hidden');
		}else if (index > 99999 || index < 0){
			this.displayError(component, $A.get("{!$Label.c.SICo_MeterRead_Beetween0and99999}"), "Error_MeterRead");
            //alert($A.get("{!$Label.c.SICo_MeterRead_Beetween0and99999}"));
            $(".slds-spinner_container").addClass('slds-hidden');
        }
         else {
			var action = component.get("c.updatePCEIndex");
			var forcedValue = component.get("v.forced");
			action.setParams(
				{
					accountId : component.get("v.accountId"),
					index : component.get("v.index"),
					forced : (forcedValue !== null && forcedValue != undefined ? forcedValue : false)
				}
			);
            action.setCallback(this, function(response) { this.validateCallback(component, response); });
			$A.enqueueAction(action);
		}
	},

	displayError : function(pComponent, pError, pErrorContainerId) { 
		$A.createComponents(
			[
                ["ui:message",{
                    "title" : "",
                    "severity" : "error",
                }],
                ["ui:outputText",{
					"value" : pError
				}]
			],

			function(components, status, errorMessage) {
				if (status === "SUCCESS") {
					var vMessage = components[0];
					var vOutputText = components[1];
					// set the body of the ui:message to be the ui:outputText
					vMessage.set("v.body", vOutputText);
					var vErrorContainer = pComponent.find(pErrorContainerId);
					// Replace div body with the dynamic component 
					vErrorContainer.set("v.body", vMessage);
				}
			}
		);
	},
    
    validateCallback : function(component, response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            if (response.getReturnValue() === "OK") {
                $(".slds-spinner_container").addClass('slds-hidden');
                alert($A.get("{!$Label.c.SICo_MeterRead_OKMessage}"));
				var apexPath = '/' 
				if (window.location.pathname.indexOf('/apex/') != -1) {
					apexPath = '/apex/';
				}
                window.location.href = apexPath + "MyBills?accountId=" + component.get("v.accountId");
            } else if (response.getReturnValue() === "KO") {
                $(".slds-spinner_container").addClass('slds-hidden');
                alert($A.get("{!$Label.c.SICo_MeterRead_KOMessage}"));
            } else {
                var strconfirm = confirm(response.getReturnValue());
                if (strconfirm == true) {
                    component.set("v.forced", true);
                    this.validate(component);
                }
            }
        } else {
            alert($A.get("{!$Label.c.SICo_MeterRead_KOMessage}"));
        }        
    },
    disableValidate : function() { 
		$(".btnValidate").addClass("btnValidateDisabledTemp");
	},
	enableValidate : function() {
		$(".btnValidate").removeClass("btnValidateDisabledTemp");
	},
	resetErrorContainer : function(pComponent, pErrorContainerId) {
		var vErrorContainer = pComponent.find(pErrorContainerId);
		vErrorContainer.set("v.body", null);
	}
})