/**
* @author Boris Castellani
* @date 25/10/2016
*
* @description Handle MaintenancyQuote Trigger After update
*/
trigger SICo_MaintenancyQuoteAfterUpdate_TRG on MaintenancyQuote__c(after update) {
	if(PAD.canTrigger('SICo_MaintenancyQuoteAfterUpdate_TRG')) {
		SICo_MaintenancyQuoteTrigger_CLS.handleAfterUpdate(trigger.old,trigger.newMap);
	}
      
}