/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi-solutions
Description:   Class for changing Password, for the community (change password page) and the Customer Space (called from PersonalInformation)
Test Class:    SICo_ChangePassword_VFC_TEST
History
20/09/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
public with sharing class SICo_ChangePassword_VFC {

		public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}

    public String errorMessage {get;set;}

		public Id accountId{get;set;}

		public SICo_ChangePassword_VFC(){
			String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
			try{
				this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
			}catch (exception e){
				System.debug('Unable to return accountId');
			}
		}

    public PageReference changePassword() {
    	if(!checkMandatoryFields()){
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SICo_RegistrationError);
            ApexPages.addMessage(msg);
            errorMessage = msg.getDetail();
            return null;
    	}
    	else if(!isValidPassword()){
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            errorMessage = msg.getDetail();
            return null;
    	}
		PageReference pageRef = Site.changePassword(newPassword, verifyNewPassword, oldpassword);
		if(pageRef == null){
			errorMessage = Label.SICo_errorMdpSyntax;
			return null;
		}else{
            //If accessed from personalInformations page, redirect there
            String pageName = ApexPages.currentPage().getParameters().get('currentvfpage');
            if (pageName == 'ChangePasswordPersonalInformations'){
                pageRef = Page.PersonalInformations;
            }
        }

		return pageRef;

    }

    //close mobile error popup
    public PageReference closeErrorPopup(){
        errorMessage = '';
        return null;
    }

    //check verify password
    private boolean isValidPassword() {
        return newPassword == verifyNewPassword;
    }

    //check mandatory fields
    private Boolean checkMandatoryFields () {
        if(!Site.isPasswordExpired())
        {//Client User
            return !String.isBlank(oldPassword) && !String.isBlank(newPassword) && !String.isBlank(verifyNewPassword); //&&password.length() > 1;
        }else{//Guest User
            return !String.isBlank(newPassword) && !String.isBlank(verifyNewPassword); //&&password.length() > 1;
        }
    }

}