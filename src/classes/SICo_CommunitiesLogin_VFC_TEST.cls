/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Test of the controller for the Community Login page (SICo_CommunitiesLogin_VFC)
History
03/08/2016      Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
@isTest
private class SICo_CommunitiesLogin_VFC_TEST {

    /**
     * [Test setup]
     */
    @testSetup static void initData() {
        //Insert inactive user (To avoid mixed DML error)
        User testUser = SICo_UtilityTestDataSet.createCommunityUser();
        testUser.IsActive = false;
        insert testUser;
    }

    /**
     * [Test of login() method, with Invalid email]
     */
    @isTest static void login_noUsername() {
        Test.startTest();
        // Instantiate a new controller with all parameters in the page
        PageReference pref = Page.CommunitiesLogin;
        pref.getParameters().put('startUrl', SICo_Constants_CLS.CUSTOMER_COMMUNITY_START_URL);
        Test.setCurrentPageReference(pref);
        SICo_CommunitiesLogin_VFC controller = new SICo_CommunitiesLogin_VFC ();
        controller.email = 'test@salesforce.com';
        controller.password = '123456'; 
        Test.stopTest();
        //Expected result: login returns null
        System.assertEquals(null, controller.login());
        //Expected result: register returns registration page
        System.assertEquals(Page.CommunitiesSelfReg.getUrl(), controller.register());
    }

    /**
     * [Test of login() method, with 1 matching username]
     */
    @isTest static void login_oneUsername() {

        //Create 1 test user
        Account testAccount = new Account (Name = 'Test');
        insert testAccount;
        Contact testContact = new Contact(AccountId = testAccount.Id, LastName = 'Test');
        insert testContact;
        User testUser = SICo_UtilityTestDataSet.createCommunityUser(testContact.Id);
        insert testUser;
        Test.startTest();

        // Instantiate a new controller with all parameters in the page
        SICo_CommunitiesLogin_VFC controller = new SICo_CommunitiesLogin_VFC ();
        controller.email = testUser.Email;
        controller.password = '123456'; 
        Test.stopTest();
        //Expected result: returns null
        System.assertEquals(null, controller.login());
        System.assertNotEquals(null, controller.register());
    }

    /**
     * [Test of login() method, with 1 matching inactive username]
     */
    @isTest static void login_oneInactiveUsername() {

        //Retrieve inactive user
        User testUser = [SELECT Id, Email, IsActive FROM User WHERE IsActive = false LIMIT 1];
        Test.startTest();

        // Instantiate a new controller with all parameters in the page
        SICo_CommunitiesLogin_VFC controller = new SICo_CommunitiesLogin_VFC ();
        controller.email = testUser.Email;
        controller.password = '123456'; 
        Test.stopTest();
        //Expected result: returns null. Error will be thrown (MIXED_DML) due to Account and Contact created in the sames context as the User beign update
        System.assertEquals(null, controller.login());
    }

    /**
     * [Test of login() method, with several matching usernames]
     */
    @isTest static void login_severalUsernames() {

        //Create 2 test users with the same email
        Account testAccount = new Account (Name = 'Test');
        insert testAccount;
        Contact testContact1 = new Contact(AccountId = testAccount.Id, LastName = 'Test');
        insert testContact1;
        Contact testContact2 = new Contact(AccountId = testAccount.Id, LastName = 'Test');
        insert testContact2;
        User testUser1 = SICo_UtilityTestDataSet.createCommunityUser(testContact1.Id);
        insert testUser1;
        User testUser2 = SICo_UtilityTestDataSet.createCommunityUser(testContact2.Id);
        testUser2.Email = testUser1.Email;
        insert testUser2;

        Test.startTest();
        // Instantiate a new controller with all parameters in the page
        SICo_CommunitiesLogin_VFC controller = new SICo_CommunitiesLogin_VFC ();
        controller.email = testUser1.Email;
        controller.password = '123456';
        Test.stopTest();
        //Expected result: returns null
        System.assertEquals(null, controller.login());
    }

}