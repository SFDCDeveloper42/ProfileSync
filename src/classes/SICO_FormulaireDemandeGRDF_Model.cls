public with sharing class SICO_FormulaireDemandeGRDF_Model {
public class DonneesFournisseur {
	public String CommentaireFournisseur;
	public String TelephoneFournisseur;
	public String GroupeAppartenance;
	public String ReferenceDemande;
}

public class PremierRdvDemande {
	public String RdvDate;
	public String NumeroSynchroIntervention;
	public String Plage;
}

public class ClientFinal {
	public String Civilite;
	public String Complement;
}

public class UtilisateurFinal {
	public String Prenom;
	public String Nom;
	public String RaisonSociale;
	public String CodeNAF;
	public String Civilite;
	public String CodeSIRET;
}

public class ContactCourrier {
	public String NumeroRue;
	public String Commune;
	public String CodePostal;
	public String eMail;
	public String Nom;
	public String Rue;
	public String Pays;
	public String ComplementAdresse;
}

public class Contrat {
	public ClientFinal ClientFinal;
	public String TypeUsageLocal;
	public UtilisateurFinal UtilisateurFinal;
	public ContactCourrier ContactCourrier;
}

public class CJAAJ {
	public String Valeur;
	public String CJAAJDate;
}

public class CJAAM {
	public String Valeur;
	public String CJAAMDate;
}

public class CJA {
	public String CJAA;
	public CJAAJ CJAAJ;
	public CJAAM CJAAM;
}

public class TICGN {
	public String ValeurTICGN;
	public String TICGN;
}

public class IndexReleve {
	public String ValeurIndexReleve;
	public String IndexReleveDate;
}

public class DonneesTypeDemande {
	public String IndexClient;
	public Contrat Contrat;
	public String CAR;
	public String DateIndexClient;
	public CJA CJA;
	public String MIG;
	public TICGN TICGN;
	public String CodeProfil;
	public String OffreHistorique;
	public String FrequenceReleve;
	public String Tarif;
	public String OptionPrestation;
	public IndexReleve IndexReleve;
}

public class ContactIntervention {
	public String Telephone;
	public String CodeAcces;
	public String Nom;
	public String ComplementAdresse;
}

public class Demande {
	public String TypeDemande;
	public DonneesFournisseur DonneesFournisseur;
	public PremierRdvDemande PremierRdvDemande;
	public DonneesTypeDemande DonneesTypeDemande;
	public String NumeroPCE;
	public String CodePostal;
	public String Commentaire;
	public ContactIntervention ContactIntervention;
}

public class Utilisateur {
	public String Login;
}

public class DemandeGRDF {
	public String SF_ID_Task;
	public String DNEntreprise;
	public String NumeroCAD;
	public Demande Demande;
	public String NumeroVersion;
	public Utilisateur Utilisateur;
}

public class ListeDemandeGRDF {

public List<DemandeGRDF> listeFormulaire {get; set;}

public  ListeDemandeGRDF parse(String jsonStr) {
	return (ListeDemandeGRDF) JSON.deserialize(jsonStr, ListeDemandeGRDF.class);
}
}
    
}