/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Controller for the Lightning Component SICo_LiveChatCommunity
Test Class:    SICo_LiveChatCommunity_LC_TEST
History
12/01/2017     Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
public with sharing class SICo_LiveChatCommunity_LC {

	//Constants
	private static final String NAMED_CREDENTIAL_LIVE_AGENT  = 'callout:LiveAgentApiEndpoint'; //Named Credential for the Live Agent API
	private static final String AVAILABILTY_ENDPONT = '/Visitor/Availability'; //Endpoint for checking agent availabilty in Live Agent API
	private static final String ENDPOINT_API_VERSION  = '37'; //Live Agent API version
	private static final String AVAILABLE_AGENT_STRING  = '"isAvailable":true'; //String in Live Agent API response when there is an agant available
	//Constants used is Test Class
	public static final String CACHE_KEY_ISLIVECHATAVAILABLE = 'IsLiveChatAvailable'; //Key for Live Chat availability in Platform Cache
	public static final Integer CACHE_TTL_ISLIVECHATAVAILABLE = 300; //TTL for Live Chat availability in Platform Cache, in seconds. 300s is the minimun value in W17

	/**
	 * [isLiveChatAvailable checks if the live chat button should be displayed]
	 * @return [returns true if the Live Chat button should be displayed, and false otherwise]
	 */
	@AuraEnabled
	public static Boolean isLiveChatAvailable(){
		Boolean isLiveChatAvailable = false;
		isLiveChatAvailable = checkLiveChatAvailability();
		return isLiveChatAvailable;
	}


	/**
	 * [checkLiveChatAvailability checks in Platform cache if Live Chat is marked has available. 
	 * 	If no value is found in platform cache, the live chat API is called, and the result is stored in cache for 1min]
	 * @return [returns true if Live Chat seems available, and false otherwise]
	 */
	private static Boolean checkLiveChatAvailability(){
		Boolean isLiveChatAvailable = false;
		//Is there a value in Cache?
		Boolean isLiveChatAvailableInPlatformCache = (Boolean)SICo_PlatformCacheManager_CLS.getObject(CACHE_KEY_ISLIVECHATAVAILABLE);
        if(isLiveChatAvailableInPlatformCache != null) {
        	isLiveChatAvailable = isLiveChatAvailableInPlatformCache;
        }else{//If no value in cache, check availability in API add it to Cache (ttl = 1 min)
        	isLiveChatAvailable = isLiveChatAvailableFromApi();
        	SICo_PlatformCacheManager_CLS.putObject(CACHE_KEY_ISLIVECHATAVAILABLE, isLiveChatAvailable, CACHE_TTL_ISLIVECHATAVAILABLE);
        }
		return isLiveChatAvailable;
	}


	/**
	 * [isLiveChatAvailableFromApi calls the Live Agent API to check if there is at least 1 agent available]
	 * @return [returns true if there is at least 1 agent available, and false otherwise]
	 */
	private static Boolean isLiveChatAvailableFromApi(){
		Boolean isAvailable = false; //Default to false;
		try {

			//Prepare Query
			SICo_Live_Agent_Setting__c liveAgentConfig = SICo_Live_Agent_Setting__c.getInstance();
			String orgId = UserInfo.getOrganizationId().left(15);
			String buttonId = liveAgentConfig.IdShow__c;
			String deploymentId = liveAgentConfig.IdLiveAgentInit1__c;
			//Sample endpoint: https://d.la1-c2cs-frf.salesforceliveagent.com/chat/rest/Visitor/Availability?org_id=00D4E0000008v7j&Availability.ids=[5734E00000000Bi]&deployment_id=5724E00000000BE&Availability.prefix=Visitor
			String parameters = '?org_id='+orgId+'&Availability.ids=['+buttonId+']&deployment_id='+deploymentId+'&Availability.prefix=Visitor';

			//Make HTTP GET request
		  	Http http = new Http();
		    HttpRequest request = new HttpRequest();
		    request.setEndpoint(NAMED_CREDENTIAL_LIVE_AGENT+AVAILABILTY_ENDPONT+parameters);
		    request.setMethod('GET');
		    request.setHeader('X-LIVEAGENT-API-VERSION', ENDPOINT_API_VERSION);
		    request.setTimeout(1000);
		    HttpResponse response = http.send(request);

		    //Check if response contains "isAvailable": true
		    System.debug(response.getBody());

		    isAvailable = response.getBody().contains(AVAILABLE_AGENT_STRING);

		} catch (Exception e) {
			System.debug('SICo_LiveChatCommunity_LC - FAILED to make callout with error: '+e);
		}
		return isAvailable;

	}


}