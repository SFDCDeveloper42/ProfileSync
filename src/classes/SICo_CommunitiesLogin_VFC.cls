/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Controller for the Community Login page (CommunitiesLogin)
Test Class:    SICo_CommunitiesLogin_VFC_TEST
History
03/08/2016      Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
public with sharing class SICo_CommunitiesLogin_VFC {

    public String email {get; set;} //Email of the user who is trying to login
    public transient String password {get; set;} //User password

    public String errorMessage {get; set;}
    public Boolean isRegistrationAllowed {get; set;}

    public String startUrl {get; set;} //startURL parameter in the URL: Redirect URL once successfully logged

    public Boolean isFromDrupal {get;set;}

    /**
     * [Contructor]
     */
    public SICo_CommunitiesLogin_VFC(){
         //Retrieve StartUrl
        this.startUrl = System.currentPageReference().getParameters().get('startURL');
        this.isRegistrationAllowed = SICo_UtilityCommunities_CLS.registrationUrl(startUrl) != '' ;

        if(System.currentPageReference().getParameters().get('source') != null){
            this.isFromDrupal = true;
        }
        else{
            this.isFromDrupal = false;
        }
    }

    /**
     * [Retrieve the username associated with the email, and then log the user]
     * @return [Site.login, or null no matching username has been found]
     */
    public PageReference login() {
        //Get username associated with this user
        String trimedEmail = email.trim();
        String username = getUsername(trimedEmail);
        if(username != null) 
        {//log user
            PageReference pageRef = Site.login(username, password, startUrl);
            if(pageRef != null){
                return pageRef;
            }else{
                errorMessage = Label.SICo_UnableToLogin;
                return null;
            }
        }else{//no matching user: display an Error
            ApexPages.Message msgNoUsername = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SICo_UnableToLogin);
            ApexPages.addMessage(msgNoUsername);
            errorMessage = Label.SICo_UnableToLogin;
            return null;
        }
    }

    /**
     * [Registration link calculation]
     * @return [URL of the registration page to use]
     */
    public String register() {
        return SICo_UtilityCommunities_CLS.registrationUrl(startUrl);
    }

    /* HELPER METHODS */

    /**
     * [Retrieve valid username from the associated email. This method is public has it is also used for the forgot password page]
     * @param  email [User email]
     * @return       [Username, or null if no matching user is found]
     */
    public static String getUsername(String email){
        User thisUser = null;
        //If there is a supplied email, find matching user
        if(email != null)
        {
            //retrieve matching users (might be inactove - in this case it will be reactivated)
            List<User> matchingUserList = [SELECT Id, Username, IsActive FROM USER 
                                            WHERE Email =:email LIMIT 10]; //A given email should not be associated with more than 1 or 2 users
            //If there is 1 single match, use it
            if(matchingUserList.size() == 1)
            {
                thisUser = matchingUserList[0];
            }else if(matchingUserList.size() > 1){
                //If there are several  users, pick the one who is not a guest (guest usernames end with a dedicated suffix)
                for(User us : matchingUserList){
                    if(thisUser == null || thisUser.username.endsWith(SICo_Constants_CLS.CUSTOMER_COMMUNITY_GUEST_SUFFIX)){
                        thisUser = us;
                    }
                }
            }//ENDIF matchingUserList.size() == 1
        }//ENDIF email != null
        //If matching user is found, return it (and reactive it if inactive)
        if(thisUser != null)
        {
            //If the user is inactive, try to reactive it
            if (thisUser.IsActive == false)
            {
                SICo_WithoutSharing_CLS.reactivateUser(thisUser);
            }
            return thisUser.username;
        }else{
            //if thisUser == null, return null
            return null;
        }//ENDIF thisUser != null
    }

    public PageReference closeErrorPopup(){
        errorMessage = '';
        return null;
    }

}