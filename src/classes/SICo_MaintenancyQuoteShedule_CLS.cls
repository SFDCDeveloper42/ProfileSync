/**
* @author Boris Castellani
* @date 25/10/2016
*
* @description Schedulable: Update MaintenancyQuote statut 
*/
global class SICo_MaintenancyQuoteShedule_CLS implements Schedulable {

	// default: 200
    global void execute(SchedulableContext sc) {
    	Database.executeBatch(new SICo_MaintenancyQuoteBatch_CLS());
    }
}