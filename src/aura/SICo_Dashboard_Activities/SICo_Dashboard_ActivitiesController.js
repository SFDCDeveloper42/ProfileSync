({ 
	doInit : function(pComponent, pEvent, pHelper) {  
		//get the account id and pass it into the apex controller
		var vGetActivitiesMethod = pComponent.get("c.queryActvities"); 
		vGetActivitiesMethod.setParams({ pAccountId : pComponent.get("v.accountId") });
		vGetActivitiesMethod.setCallback(pComponent, function(pResponse) {
				var vState = pResponse.getState();
				if (vState === "SUCCESS") {  
					pComponent.set("v.activitiesWrap", pResponse.getReturnValue());
					if (pResponse.getReturnValue() != null) {
						pComponent.set("v.hasActivities", pResponse.getReturnValue().length > 0);
						pComponent.set("v.activitiesLength", pResponse.getReturnValue().length);
					} else {
						pComponent.set("v.hasActivities", false);
						pComponent.set("v.activitiesLength", 0);
					}
				} else {
					pComponent.set("v.hasActivities", false);
					console.log(pResponse.getError());
				}
		});
		$A.enqueueAction(vGetActivitiesMethod);
	}
})