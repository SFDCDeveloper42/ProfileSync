/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Web Service REST in order to pair object - call by Mobile
Test Class:    SICo_PairObject_RWS_TEST
History
25/07/2016     Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
@RestResource(urlMapping='/PairObject')
global with sharing class SICo_PairObject_RWS {

    @HttpPost
    /*global static Boolean pairObject ( String sApplication,
                                       String sAssociationCode,
                                       String sUsername,
                                       String sCustomerNumber,
                                       String sAccess_Token,
                                       String sRefresh_Token,
                                       String sSiteId ) {*/

    global static Boolean pairObject ( String sApplication,
                                       String sAssociationCode,
                                       String sUsername,
                                       String sCustomerNumber,
                                       String sAuthCode,
                                       String sSiteId ) {

        //STATIC VARIABLES
        CalloutConfig__c oCC = CalloutConfig__c.getValues( 'OSF' );
        String sEndPoint = oCC.Endpoint__c;
        String sXAPIKEY = oCC.Password__c;

        CalloutConfig__c oCS = CalloutConfig__c.getValues( 'Community' );
        String sEndPointSalesforce = oCS.Endpoint__c;

        CalloutConfig__c oCO = CalloutConfig__c.getValues( 'ConnectedObject' );
        String sClientId = oCO.Login__c;
        String sClientSecret = oCO.Password__c;
        String sRedirectUri = oCO.Endpoint__c;

        Boolean result = false;
        String token = '';

        try {

            SICo_Pairing__c oPairing = [ SELECT Id, TemporaryKey__c, ObjectId__c, ActionType__c, ObjectType__c 
                                        FROM SICo_Pairing__c
                                        WHERE TemporaryKey__c = : sAssociationCode
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];

            if ( oPairing != null ) {

                String sDeviceId = oPairing.ObjectId__c;
                String sIssued_at = String.valueOf(Datetime.now().getTime());

                String sAccess_Token = '';
                String sRefresh_Token = '';

                Http oHttp = new Http();
                HttpRequest oRequest = new HttpRequest();
                oRequest.setTimeout( 2 * 60 * 1000 );
                oRequest.setEndpoint( sEndPointSalesforce + '/services/oauth2/token' );
                oRequest.setMethod( 'POST' );
                oRequest.setHeader('content-type', 'application/x-www-form-urlencoded');
                oRequest.setBody( 'code=' + sAuthCode + '&grant_type=authorization_code&client_id=' + sClientId + '&client_secret=' + sClientSecret + '&redirect_uri=' + sRedirectUri );

                HttpResponse oResponse;
                if ( Test.isRunningTest() ) {
                    oResponse = new HttpResponse();
                    oResponse.setStatusCode( 200 );
                    oResponse.setBody('{"access_token":"00D4E0000008v7j!AQoAQA.5WR7FXngE62v2oC9awymxnao8in4byDXu8ClWTs0_tDzXqixcMh4TLrMf09P4XH_rNFPFN60jbQPD3sU3sH5UK3bG","refresh_token":"5Aep861tlMDNuVXI5oNfQ9HNoYliE82VjcaeZFqg9eNguotbQ4uvBuMzt2TettgWQkff7IaWVRVkU_WDubdvWeo","sfdc_community_url":"https://dev-c43.cs83.force.com","sfdc_community_id":"0DB580000008On8GAE","signature":"XzUfmn4n49CMwa9+FzvmdzQEHq9H1j9+XU8TDUHxuIA=","scope":"refresh_token full","id_token":"eyJraWQiOiIyMDQiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiM3V6YV94bmVfS05EUWtuWWJjcmxWQSIsInN1YiI6Imh0dHBzOi8vdGVzdC5zYWxlc2ZvcmNlLmNvbS9pZC8wMEQ0RTAwMDAwMDh2N2pVQUEvMDA1NEUwMDAwMDB1eFlvUUFJIiwiYXVkIjoiM01WRzl3OHVYdWkyYUJfb1hUSTUzaXJuOEtPaEg0ekkwRTVyZ3JJOGY4Qkp6RW5rbGpaZDRFazNWRmNodTI3ODNLWnE4VXlyNWpnZTQ4cGJ3U3RNdSIsImlzcyI6Imh0dHBzOi8vZGV2LWM0My5jczgzLmZvcmNlLmNvbS8iLCJleHAiOjE0NzcyOTcxODAsImlhdCI6MTQ3NzI5NzA2MH0.iFYZOOx19309_dmGnySYub-Ty1vkO-4-psE0H3MqQJLVqQSj8RgbzMiluEQ1nvUQocDKdrPW2Ujd2e-n_jHwIOxOnhnRtjYsdli9CAnK6cQzdYjFLrI9XxegfyEh1EVU7S-MWvOI_7CU6mcefEA_h8mluV54cNzGSgofGYf_bZzRl3lROKVqzOccB1p3mADwpDZUgTxZOZkoQ7b3hXCDjiD5OKVrmA8cBjhjV8nRsdjz92vju3mURKG9qa-DFuZ2bL-cvs2VyixJvm6UK0nQZwXniIDg4Wj-4AKg5s38mPJXcpcu0jjb6K8HosPu_pHOdI4xdeWzo74_5hxHHh_K3hiUzZBy5jxD3SkiHS6Vnw5wYyBczKPEv7GD9_qio5mgPc_cRsYbY8_4Crd1nfYUIFsOKhgOi4aZI-70TltZV395EUwt8cLDNZPRLjhtgXEXKWMCuUvRx1_2fphcz5T0Ou20MC3knxR4tmO9dStaO_vhLuByiQc-leVMTisRwF29L1kuNQLFqcuP_ZSKGUynh0yVgnue5wX3QuC5TZJjMwm3zHH0iYTBUQ8JjqELKtljaaTD8S8SHAknj2H-hF6EmvxOFf0pg0gpsVG8bs5uT-ChpjyWuUku8fTSryJVtrHxKrE4OrfWTE9Vz6hZKpZr_6WSAbpay_4uozU5r7o7r3o","instance_url":"https://c43--dev.cs83.my.salesforce.com","id":"https://test.salesforce.com/id/00D4E0000008v7jUAA/0054E000000uxYoQAI","token_type":"Bearer","issued_at":"1477297060493"}');
                } else {
                    oResponse = oHttp.send( oRequest );
                }
                System.debug( '#### response AT & RT : ' + oResponse.getBody() );


                System.JSONParser oJSONParser;
                String fieldName;
                if ( oResponse.getStatusCode() == 200 ) {
                    oJSONParser = JSON.createParser( oResponse.getBody() );
                    sAccess_Token = getJsonValue( oJSONParser, 'access_token' );
                    sRefresh_Token = getJsonValue( oJSONParser, 'refresh_token' );
                }

                System.debug( '#### access_token : ' + sAccess_Token );
                System.debug( '#### refresh_token : ' + sRefresh_Token );


                if ( sAccess_Token == '' || sRefresh_Token == '' ) {
                    throw new PairingException('OAuth AT & RT HTTP Request issue : ' + oResponse.getStatusCode() + ' ' + oResponse.getStatus());

                } else {

                    token = SICo_JsonSignature_CLS.getJWT( 'ConnectedObject',
                                                           sUsername,
                                                           SICO_UTility.generate18CharId(sCustomerNumber),
                                                           sAccess_Token,
                                                           sIssued_at,
                                                           SICO_UTility.generate18CharId(sSiteId) );

                    System.debug( '##### sSiteId = ' + sSiteId );
                    SICo_Site__c oSite = [ SELECT Id
                                           FROM SICo_Site__c
                                           WHERE SiteNumber__c = : sSiteId
                                           LIMIT 1 ];

                    Http http = new Http();
                    HttpRequest request = new HttpRequest();
                    request.setTimeout( 2 * 60 * 1000 );
                    request.setEndpoint( sEndPoint );
                    request.setMethod( 'POST' );
                    request.setHeader( 'Content-Type', 'application/json;charset=UTF-8' );
                    request.setHeader( 'x-api-key', sXAPIKEY );

                    // Set the body as a JSON object
                    request.setBody(
                        '{' +
                            '"associationCode" : "' + sAssociationCode + '",' +
                            '"deviceId" : "' + sDeviceId + '",' +
                            '"refresh_token" : "' + sRefresh_Token + '",' +
                            '"jwt" : "' + token + '"' +
                        '}'
                    );

                    HttpResponse response = http.send( request );
                    System.debug( '#### response OSF : ' + response );

                    // Parse the JSON response
                    if ( response.getStatusCode() == 400 ) {
                        throw new PairingException('OSF Bad Request (wrong payload) : ' + response.getStatusCode() + ' ' + response.getStatus());

                    } else if ( response.getStatusCode() == 409 ) {
                        throw new PairingException('OSF Conflict (Already associated or wrong association code) : ' + response.getStatusCode() + ' ' + response.getStatus());

                    } else if ( response.getStatusCode() == 500 ) {
                        throw new PairingException('OSF Server error : ' + response.getStatusCode() + ' ' + response.getStatus());
                    } else {

                        System.debug( '##### OSF Response = ' + response.getBody() );

                        Asset oAsset = [ SELECT Id, InstallDate, Status, SerialNumber, ObjectType__c 
                                         FROM Asset
                                         WHERE Site__c =: oSite.Id 
                                         LIMIT 1 ];//AND Status != 'Renvoyé' AND Status != 'Installé'

                        oAsset.InstallDate = date.today();
                        oAsset.UsageEndDate = null;
                        oAsset.Status = 'Installé';
                        oAsset.SerialNumber = sDeviceId;
                        oAsset.ObjectType__c = oPairing.ObjectType__c;
                        update oAsset;

                        result = true;

                        // FERMETURE DE LA TACHE OBJET D'APPAIRAGE
                        Case oCase = [ SELECT Id FROM CASE WHERE ProvisioningType__c = :System.Label.SICo_ProvisionningObjet AND Site__r.SiteNumber__c = : sSiteId LIMIT 1 ];
                        if ( oCase != null ) {
                            Task oTask = [ SELECT Id, Status
                                           FROM Task
                                           WHERE Subject = 'Appairage' AND WhatId = : oCase.Id ];
                            oTask.Status = 'Fermée';
                            update oTask;
                        }

                        // DELETE PAING REQUEST AT THE END OF THE PAIRING PROCESS IF RESULT = TRUE
                        SICo_WithoutSharing_CLS.deletePairingRequest( oPairing );

                    }

                }

            } else {
                throw new PairingException('Salesforce server error : No pairing request related this association code : ' + sAssociationCode);
            }

        } catch ( Exception e ) {

            SICo_LogManagement.sendErrorLog(
                'SICo',
                Label.SICO_LOG_USER_PCE,
                String.valueOf( e )
            );
            result = false;

        }

        return result;

    }

    public Static String getJsonValue( System.JSONParser oJSONParser, String sFieldName ) {

        String fieldName;
        String sValueToReturn;
        while (oJSONParser.nextToken() != null) {
            oJSONParser.getText();
            if ( oJSONParser.getCurrentToken() == JSONToken.FIELD_NAME ){
                fieldName = oJSONParser.getText();
                oJSONParser.nextToken(); 
                if( fieldName == sFieldName ){
                    sValueToReturn = oJSONParser.getText();
                    break;
                }
            }
        }
        return sValueToReturn;

    }

    public class PairingException extends Exception {}

}