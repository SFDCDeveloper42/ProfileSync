/**
* @author Sebastien Colladon
* @date 11/08/2016
*
* @description Console Side Bar extension controller
*/
public with sharing class SICO_ConsoleSideBar_VFC {

	public transient List<Asset> ol_asset {get;private set;}

	public transient List<SICo_Site__c> ol_site {get;private set;}

	public transient Map<String,Map<String,Map<String,List<Case>>>> rtSlashTypeSlashStatusSlashCase {get;private set;}

	/*******************************************************************************************************
	* @description Constructor
	* @param con Account StandardController
	*/
  public SICO_ConsoleSideBar_VFC(ApexPages.StandardController con) {
    	Account o_account = (Account) con.getRecord();

    	this.rtSlashTypeSlashStatusSlashCase = new Map<String,Map<String,Map<String,List<Case>>>>();

    	try {
	    	this.ol_asset = [SELECT id, Status FROM Asset where AccountId =:o_account.id];

	    	this.ol_site = [SELECT id, PostalCode__c, City__c FROM SICo_Site__c where Subscriber__c =:o_account.id];

	    	for(Case aCase : [SELECT id, RecordTypeid, ProvisioningType__c, Heading__c, Subject, Status FROM Case where AccountId =:o_account.id order by recordtypeid, createddate ]) {
	    		String caseRT = aCase.recordtypeid == SICo_Utility.m_RTbyDeveloperName.get(Schema.SObjectType.Case.Name + '_' + SICO_Constants_CLS.STR_CASE_SYSTEM_RT) 
	    				? Label.SICO_Console_Produits_Souscrits
	    				: Label.SICO_Console_Reclamations;	    		
	    		String type = aCase.recordtypeid == SICo_Utility.m_RTbyDeveloperName.get(Schema.SObjectType.Case.Name + '_' + SICO_Constants_CLS.STR_CASE_SYSTEM_RT)
	    				? aCase.ProvisioningType__c
	    				: aCase.Heading__c;
	    		if(!rtSlashTypeSlashStatusSlashCase.containsKey(caseRT)){
	    			rtSlashTypeSlashStatusSlashCase.put(caseRT,new Map<String,Map<String,List<Case>>>());
	    		}
	    		if(!rtSlashTypeSlashStatusSlashCase.get(caseRT).containsKey(type)){
	    			rtSlashTypeSlashStatusSlashCase.get(caseRT).put(type,new Map<String,List<Case>>());
	    		}

	    		if(!rtSlashTypeSlashStatusSlashCase.get(caseRT).get(type).containsKey(aCase.Status)){
	    			rtSlashTypeSlashStatusSlashCase.get(caseRT).get(type).put(aCase.Status,new List<Case>());
	    		}

	    		rtSlashTypeSlashStatusSlashCase.get(caseRT).get(type).get(aCase.Status).add(aCase);
	    	}
	    } catch (Exception ex) {
	    	// TODO Log
	    }
    }
}