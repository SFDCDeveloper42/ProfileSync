/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Controller for the AttestationsReports page 
Test Class:    SICo_AttestationsReports_VFC_TEST
History
29/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
public with sharing class SICo_AttestationsReports_VFC {

	public Id accountId{get;set;}

	public List<SICo_CR__c> crsRequest{get;set;}

    public List<CrWrapper> listCrWrapper{get;set;}

    public CrWrapper wrapper;
    
    public Map<Id, Attachment> mapAttachments;
    public Map<Id, String> mapAttachmentURL;

	public SICo_AttestationsReports_VFC() {

        //Get AccountId
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        try{
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }catch (exception e){
            System.debug('Unable to return accountId');
        }

        if(accountId != null){

        	crsRequest = [SELECT Id, PublicLabel__c, CreatedDate, DownloadLinkLabel__c
        			FROM SICo_CR__c
        			WHERE Site__c IN (SELECT Id
                                        FROM SICo_Site__c
                                        WHERE Subscriber__c = :accountId)

                    LIMIT 1000];
            
            mapAttachments = new Map<Id, Attachment>([SELECT Id, ParentId, CreatedDate
                              FROM Attachment
                              WHERE ParentId IN :crsRequest
                              AND Name LIKE 'CR_%'
                              ORDER BY CreatedDate desc
                              LIMIT 1000]);
            
            mapAttachmentURL = new Map<Id, String>();
            
            for(Attachment att: mapAttachments.values()){
                if(mapAttachmentURL.get(att.ParentId) == null)
                	mapAttachmentURL.put(att.ParentId, '/servlet/servlet.FileDownload?file=' + att.Id);
            }

            listCrWrapper = new List<CrWrapper>();

            for(SICo_CR__c cr: crsRequest){
                if(cr.PublicLabel__c != '' && cr.PublicLabel__c != null && mapAttachmentURL.containsKey(cr.Id))
                    wrapper = new CrWrapper(cr.CreatedDate, cr.PublicLabel__c, mapAttachmentURL.get(cr.Id), cr.DownloadLinkLabel__c);

                    listCrWrapper.add(wrapper);
            }

        }
    }

    public class CrWrapper{

        public String createdDate{get;set;}
        public String name{get;set;}
        public String link{get;set;}
        public String downloadLinkLabel{get;set;}

        public CrWrapper(DateTime createdDate, String name, String link, String downloadLinkLabel){
        	if (createdDate != null) {
        		this.createdDate =  String.valueOf(createdDate.day()) + ' ' + SICo_Constants_CLS.MONTHES.get(createdDate.month()) + ' ' + String.valueOf(createdDate.year()); 
        	}

            this.name = name;

            this.link = link;
            
            this.downloadLinkLabel = downloadLinkLabel;
        }
    }

}