/**
* @author Boris Castellani
* @date 01/02/2017
* @LastModify 
*
* @description 
*/
public with sharing class SICo_ChamJetonService_CLS {

	public List<Account> getAccounts(List<Id> accountIds){
		List<Account> accounts = [SELECT Name, ThursdayTimeRange2__c, ThursdayTimeRange3__c, ThursdayTimeRange4__c, 
															ThursdayTimeRange1__c, MondayTimeRange2__c, MondayTimeRange3__c, MondayTimeRange4__c, 
															MondayTimeRange1__c, TuesdayTimeRange2__c, TuesdayTimeRange3__c, TuesdayTimeRange4__c, 
															TuesdayTimeRange1__c, WednesdayTimeRange2__c, WednesdayTimeRange3__c, WednesdayTimeRange4__c, 
															WednesdayTimeRange1__c, SaturdayTimeRange2__c, SaturdayTimeRange3__c, SaturdayTimeRange4__c, 
										 					SaturdayTimeRange1__c, FridayTimeRange2__c, FridayTimeRange3__c, FridayTimeRange4__c, FridayTimeRange1__c
										 					FROM Account
										 					WHERE Id IN :accountIds];
		return accounts;
	}

	@TestVisible
	private List<SICO_AvailabilyWeeklyCalendar__c> getWeeklyCalendars(Id accountId){
		return [SELECT StartDate__c, Status__c, CHAMAgency__c,
						ThursdayTimeRange2__c, ThursdayTimeRange3__c, ThursdayTimeRange4__c, ThursdayTimeRange1__c, 
						MondayTimeRange2__c, MondayTimeRange3__c, MondayTimeRange4__c, MondayTimeRange1__c,
						TuesdayTimeRange2__c, TuesdayTimeRange3__c, TuesdayTimeRange4__c, TuesdayTimeRange1__c, 
						WednesdayTimeRange2__c, WednesdayTimeRange3__c, WednesdayTimeRange4__c, WednesdayTimeRange1__c, 
						SaturdayTimeRange2__c, SaturdayTimeRange3__c, SaturdayTimeRange4__c, SaturdayTimeRange1__c,
	 					FridayTimeRange2__c, FridayTimeRange3__c, FridayTimeRange4__c, FridayTimeRange1__c
	 					FROM SICO_AvailabilyWeeklyCalendar__c
	 					WHERE Status__c = :label.SICo_weeklyCalendars_Open
	 					AND CHAMAgency__c = :accountId];
	}

	public SICo_ChamJetonModel.ChamJeton buildJsonWeeklyCalendars(Id accountId){
		final List<SICO_AvailabilyWeeklyCalendar__c> weeklyCalendars = getWeeklyCalendars(accountId);

		if(weeklyCalendars.isEmpty()){
			throw new ChamTokenException();
		}
		Integer aTime;
		SICo_ChamJetonModel.ChamJeton chamJeton = new SICo_ChamJetonModel.ChamJeton();
		List<SICo_ChamJetonModel.WeeklyCalendar> weeklyCalendar = new List<SICo_ChamJetonModel.WeeklyCalendar>();
		for(SICO_AvailabilyWeeklyCalendar__c aweeklyCalendar : weeklyCalendars){
			aTime =0;
			SICo_ChamJetonModel.WeeklyCalendar calendar = new SICo_ChamJetonModel.WeeklyCalendar();
			calendar.startDate = aweeklyCalendar.StartDate__c;
			List<SICo_ChamJetonModel.Days> weeklyDays = new List<SICo_ChamJetonModel.Days>();			
			for(Integer iDay = 0; iDay < 6; iDay++){
				SICo_ChamJetonModel.Days aWeeklyDays = new SICo_ChamJetonModel.Days();
				aWeeklyDays.day = String.valueOf(aweeklyCalendar.StartDate__c.addDays(iDay));
				aWeeklyDays.timeRange1 = String.valueOf(aweeklyCalendar.get(SICo_Constants_CLS.DAYS.get(aTime)));
				aTime = aTime+1;
				aWeeklyDays.timeRange2 = String.valueOf(aweeklyCalendar.get(SICo_Constants_CLS.DAYS.get(aTime)));
				aTime = aTime+1;
				aWeeklyDays.timeRange3 = String.valueOf(aweeklyCalendar.get(SICo_Constants_CLS.DAYS.get(aTime)));
				aTime = aTime+1;
				aWeeklyDays.timeRange4 = String.valueOf(aweeklyCalendar.get(SICo_Constants_CLS.DAYS.get(aTime)));
				aTime = aTime+1;
				weeklyDays.add(aWeeklyDays);
			}	
			calendar.days = weeklyDays;
			weeklyCalendar.add(calendar);
		}
		chamJeton.weeklyCalendar = weeklyCalendar;
		return chamJeton;
	}

			
	/** 
	 * Process for init calendars for the first time.
	 * Number of week is control by one variable in custom labels Weeks.
	 * /!\ Methode call by Trigger after insert new Agence.
	 */ 
	public void initCalendars(List<Account> accounts){
		final List<SICO_AvailabilyWeeklyCalendar__c> weeklyCalendars = new List<SICO_AvailabilyWeeklyCalendar__c>();  
		Date weeklyDate = Date.today().toStartOfWeek();
		for(Integer i = 0; i < Integer.valueOf(label.SICo_weeklyCalendars_Number); i++){
			weeklyCalendars.addAll(buildCalendar(accounts,weeklyDate));
			weeklyDate = weeklyDate.addDays(Integer.valueOf(label.SICo_weeklyCalendars_Days));
		}
		if(!weeklyCalendars.isEmpty()){
			INSERT weeklyCalendars;
		}

	}

	/** 
	 * Process for get calendar for this week by agence account.
	 * Change the status of this week by one custom label Close (Archivée).
	 * Get the new date for the next calendar (35 days after).
	 * Call methode for build new calendar with this new date.
	 */ 
	public void processCalendar(List<Account> accounts){
		final Date weeklyDate = Date.today().toStartOfWeek();
		final List<SICO_AvailabilyWeeklyCalendar__c> weeklyCalendars = [SELECT CHAMAgency__c, StartDate__c, Status__c
																																		FROM SICO_AvailabilyWeeklyCalendar__c
																																		WHERE StartDate__c = :weeklyDate
																																		AND CHAMAgency__c IN :accounts];
		if(!weeklyCalendars.isEmpty()){
			for(SICO_AvailabilyWeeklyCalendar__c aWeeklyCalendar : weeklyCalendars){
				aWeeklyCalendar.Status__c = label.SICo_weeklyCalendars_Close;
			}	
			final Integer nbrDay = Integer.valueOf(label.SICo_weeklyCalendars_Number)*Integer.valueOf(label.SICo_weeklyCalendars_Days);													
			final List<SICO_AvailabilyWeeklyCalendar__c> nextWeeklyCalendars = buildCalendar(accounts, weeklyDate.addDays(nbrDay));
			if(!nextWeeklyCalendars.isEmpty()){
				weeklyCalendars.addAll(nextWeeklyCalendars);
			}
			UPSERT weeklyCalendars;
		}
	}

	/** 
	 * Process for build calendar for one week by agence account.
	 * The status of this calendar is one custom label Open (En ligne).
	 * Fill the calendar field by the account field.
	 */ 
	public List<SICO_AvailabilyWeeklyCalendar__c> buildCalendar(List<Account> accounts, Date weeklyDate){
		final List<SICO_AvailabilyWeeklyCalendar__c> weeklyCalendars = new List<SICO_AvailabilyWeeklyCalendar__c>();
		for(Account aAccount : accounts){     
			final SICO_AvailabilyWeeklyCalendar__c aWeeklyCalendar = new SICO_AvailabilyWeeklyCalendar__c();
      aWeeklyCalendar.Name = '['+aAccount.Name +'] '+weeklyDate.format()+' - '+weeklyDate.addDays(6).format();
			aWeeklyCalendar.StartDate__c = weeklyDate;
			aWeeklyCalendar.CHAMAgency__c = aAccount.Id;
			aWeeklyCalendar.Status__c = label.SICo_weeklyCalendars_Open;
			for(String aTimeSlot : SICo_Constants_CLS.DAYS.values()){
				aWeeklyCalendar.put(aTimeSlot,aAccount.get(aTimeSlot));
			}
			weeklyCalendars.add(aWeeklyCalendar);
		}
		return weeklyCalendars;
	}

	/** 
	 * Decrement creneau and upsert SICO_AvailabilyWeeklyCalendar__c.
	 * The field can be negative.
	 */ 
	public void decrementCreneau(Lead aLead){
		if(aLead.CHAMInstallationAppointmentDate__c != null || aLead.CHAMEntretienAppointmentDate__c != null){

      Date weeklyDate = aLead.CHAMInstallationAppointmentDate__c != null
      ? aLead.CHAMInstallationAppointmentDate__c
      : aLead.CHAMEntretienAppointmentDate__c;

      Id chamAgency = aLead.CHAMInstallationAppointmentDate__c != null
      ? aLead.CHAMAgencyInstallation__c
      : aLead.CHAMAgencyEntretien__c;

			final SICO_AvailabilyWeeklyCalendar__c aWeeklyCalendar = [SELECT StartDate__c, ThursdayTimeRange2__c, ThursdayTimeRange3__c, 
																																ThursdayTimeRange4__c, ThursdayTimeRange1__c, MondayTimeRange2__c,
																																MondayTimeRange3__c, MondayTimeRange4__c, MondayTimeRange1__c,
																																TuesdayTimeRange2__c, TuesdayTimeRange3__c, TuesdayTimeRange4__c, 
																																TuesdayTimeRange1__c, WednesdayTimeRange2__c, WednesdayTimeRange3__c, 
																																WednesdayTimeRange4__c, WednesdayTimeRange1__c, SaturdayTimeRange2__c,
																																SaturdayTimeRange3__c, SaturdayTimeRange4__c, SaturdayTimeRange1__c,
																											 					FridayTimeRange2__c, FridayTimeRange3__c, FridayTimeRange4__c, 
																											 					FridayTimeRange1__c, Status__c
																											 					FROM SICO_AvailabilyWeeklyCalendar__c
																											 					WHERE CHAMAgency__c = :chamAgency
																											 					AND StartDate__c = :weeklyDate.toStartOfWeek() Limit 1];

			if(aWeeklyCalendar != null){
				String weeklyCreneau = aLead.CHAMInstallationAppointmentDate__c != null
				? aLead.CHAMInstallationTimeSlot__c
				: aLead.CHAMEntretienTimeSlot__c;

				Integer day =  weeklyDate.toStartOfWeek().daysBetween(weeklyDate);
				String fieldCreneau = SICo_Constants_CLS.CRENEAUX.get(day).get(weeklyCreneau);
				Decimal newScore = (Decimal)aWeeklyCalendar.get(fieldCreneau) -1;
				aWeeklyCalendar.put(fieldCreneau,newScore);
				UPSERT aWeeklyCalendar;			
			}
		}
	}

  public class SICo_accountDomain {
    final protected Id accountId;
    final protected Account aAccount;

    public SICo_accountDomain(final Id anId) {
      this.accountId = anId;
      try{
        if(this.isAccount()){
          this.aAccount = [SELECT Id FROM Account where id = :this.accountId limit 1][0];
        }
      }catch (Exception ex){
        system.debug(ex);
      }
    }

    public boolean isAccount() {
      final String inputPrefix = String.valueOf(this.accountId).substring(0,3);
      final String accountPrefix = Schema.SObjectType.Account.getKeyPrefix();
      return inputPrefix == accountPrefix;
    }
  }

  public class ChamTokenException extends Exception {
    public override String getMessage(){
      return 'No available calendars';
    }
  }
} //END