/**
* @author Boris Castellani
* @date 12/12/2016
*
* @description Case Trigger before insert
*/
trigger SICo_CaseBeforeInsert_TRG on Case(before insert) {
  if(PAD.canTrigger('SICo_CaseBeforeInsert_TRG')) {
    SICo_CaseTrigger_CLS.HandleBeforeInsert(Trigger.new);
  }    
}