global class SICO_ProvisioningRetryScheduler implements Schedulable {
	global void execute(SchedulableContext SC) {
      Database.executeBatch(new SICO_ProvisioningRetry_BAT(), 1); 
   }
}