/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Test of the controller for the Community Registration Confirm page(SICo_CommunitiesSelfRegConfirm_VFC)
History
03/08/2016      Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
@isTest
private class SICo_CommunitiesSelfRegConfirm_VFC_TEST {
    
    @isTest static void test_Confirm() {
        PageReference thepage = Page.CommunitiesSelfRegConfirm;
        User testUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert testUser;
        thepage.getParameters().put('userName', testUser.username) ;
        Test.startTest();
        Test.setCurrentPage(thepage);
        SICo_CommunitiesSelfRegConfirm_VFC controller = new SICo_CommunitiesSelfRegConfirm_VFC();
        Test.stopTest();
        System.assert(!ApexPages.hasMessages());

        PageReference pageRef = controller.redirectUser();
        System.assertEquals(pageRef.getUrl(), '/apex/CommunitiesLogin');

    }

}