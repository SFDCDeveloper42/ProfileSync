/**
* @author Boris Castellani
* @date 10/11/2016
* @LastModify 22/02/2016 
*
* @description  Handle Anomaly Trigger Business Rule
*/
public with sharing class SICo_AnomalyTrigger_CLS {
	
	public static void HandleBeforeInsert(List<SICo_Anomaly__c> anomalys){

		Map<Id,Id> anomalyIdCrId = new Map<Id,Id>();
		for(SICo_Anomaly__c anomaly : anomalys){
			anomalyIdCrId.put(anomaly.Id,anomaly.CR__c);
		}

		List<SICo_CR__c> crsList = [ SELECT Id, Account__r.PersonEmail 
									 FROM SICo_CR__c
									 WHERE Id IN :anomalyIdCrId.values() ];

		Map<Id, String> crs = new Map<Id, String>();
		for ( SICo_CR__c oCr : crsList ) {
			crs.put( oCr.Id, oCr.Account__r.PersonEmail );
		}

		if(!crs.isEmpty()){
			for(SICo_Anomaly__c anomaly : anomalys){
				anomaly.CustomerEmail__c = crs.get(anomaly.CR__c);
			}			
		}
	} 
}