<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>P0006</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">LOG</value>
    </values>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">SE0001</value>
    </values>
    <values>
        <field>IsVisibleDashboard__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsVisibleOrderTracking__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Object__c</field>
        <value xsi:type="xsd:string">Task</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:type="xsd:string">TaskProvisionning_Logistique</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">EQUIPMENT</value>
    </values>
</CustomMetadata>
