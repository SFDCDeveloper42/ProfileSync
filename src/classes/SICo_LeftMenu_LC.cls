/*------------------------------------------------------------
Author:        Mehdi Beggas
Company:       Ikumbi-solutions
Description:   Controller for the Lightning Component (SICo_LeftMenu)
Test Class:    SICo_LeftMenu_LC_TEST
History
17/08/2016      Mehdi Beggas      Create version
------------------------------------------------------------*/
public with sharing class SICo_LeftMenu_LC {

    @AuraEnabled public static List<SICo_Site__c> getSites(String idAccount) {

        List<SICo_Site__c> listSites = [SELECT Id, SiteName__c, IsSelectedInCustomerSpace__c FROM SICo_Site__c 
                                        WHERE Subscriber__c = :idAccount
                                        ORDER BY IsSelectedInCustomerSpace__c DESC];
        
        if(!listSites.isEmpty() && !listSites.get(0).IsSelectedInCustomerSpace__c){
            listSites.get(0).IsSelectedInCustomerSpace__c = true;
            update listSites.get(0);
        }
            
        return listSites;
    }
    
    @AuraEnabled public static void setSelectSite(String idNewSite) {

        List<SICo_Site__c> listSites = [SELECT Id, SiteName__c, IsSelectedInCustomerSpace__c FROM SICo_Site__c 
                                        WHERE Id = :idNewSite OR IsSelectedInCustomerSpace__c = true];
        for(SICo_Site__c site : listSites){
            if(site.Id == idNewSite)
                site.IsSelectedInCustomerSpace__c = true;
            else
                site.IsSelectedInCustomerSpace__c = false;
        }
        update listSites;
    }
    
}