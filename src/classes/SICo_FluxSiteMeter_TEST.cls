/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Test SICo_FluxSiteMeter_CLS & SICo_FluxSiteMeter_TRIGGER
History
01/08/2016      Boris Castellani     Create version
------------------------------------------------------------*/
@isTest
public class SICo_FluxSiteMeter_TEST {

  /*************
      PCE
  **************/

  @isTest static void error55CalloutPCE() {
    CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
    insert boomiConfig;

    list<Account> listAccounts= new list<Account> ();
    for(Integer i = 0, j = 0; i < 55; i++) {
      listAccounts.add(SICo_UtilityTestDataSet.createAccount(SICo_Utility.generateRandomString(5, 'CARACTERE'),'Dupond','PersonAccount'));
      }
    insert listAccounts;

    list<SICo_Site__c> listSite= new list<SICo_Site__c> ();
    for(Account acc : listAccounts) {
      listSite.add(SICo_UtilityTestDataSet.createSite(acc.Id));
      }
    insert listSite;

    list<SICo_Meter__c> listMeter= new list<SICo_Meter__c> ();
    for(Account acc : listAccounts) {
      listMeter.add(SICo_UtilityTestDataSet.createMeter());
      }
    insert listMeter;

    list<SICo_SiteMeter__c> siteMeters = new list<SICo_SiteMeter__c> ();
    for(Integer i = 0, j = 0; i < 55; i++) {
      siteMeters.add(SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE,listSite.get(i).Id,listMeter.get(i).Id));
      }
    for(SICo_SiteMeter__c forSiteMeter : siteMeters){
      forSiteMeter= SICo_UtilityTestDataSet.addFieldForSiteMeter(forSiteMeter);
      }
    insert siteMeters;

    // TEST & ASSERT
    Test.startTest();
      List<Database.SaveResult> result = Database.update(siteMeters);
    Test.stopTest();
    Account accounts= [SELECT Id, ErrorLog__c FROM Account LIMIT 1];
    //System.assertEquals('20 Callout GAZ Reached',accounts.ErrorLog__c);
    }

  @isTest static void succesCalloutPCE() {
    CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
    insert boomiConfig;
    Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
    insert account;
    SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
    insert site;
    SICo_Meter__c meter= SICo_UtilityTestDataSet.createMeter();
    insert meter;
    SICo_SiteMeter__c siteMeter= SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE,site.Id,meter.Id);
    siteMeter= SICo_UtilityTestDataSet.addFieldForSiteMeter(siteMeter);
    insert siteMeter;

    // TEST & ASSERT
    Test.startTest();
      Database.SaveResult result = Database.update(siteMeter);
    Test.stopTest();
    System.assert(result.isSuccess());
    }

    @isTest static void errorCalloutPCE() {
      CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
      insert boomiConfig;
      Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
      insert account;
      SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
      insert site;
      SICo_Meter__c meter= SICo_UtilityTestDataSet.createMeter();
      insert meter;
      SICo_SiteMeter__c siteMeter= SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE,site.Id,meter.Id);
      siteMeter= SICo_UtilityTestDataSet.addFieldForSiteMeter(siteMeter);
      // Just for TEST - if Millesime__c equals 10 => Mock test return Error
      siteMeter.Millesime__c= 10;
      insert siteMeter;

      // TEST & ASSERT
      Test.startTest();
        Database.SaveResult result = Database.update(siteMeter);
      Test.stopTest();
      System.assert(result.isSuccess());
      }

  /*************
      PDL
  **************/

  @isTest static void error55CalloutPDL() {
    CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
    insert boomiConfig;

    list<Account> listAccounts= new list<Account> ();
    for(Integer i = 0, j = 0; i < 55; i++) {
      listAccounts.add(SICo_UtilityTestDataSet.createAccount(SICo_Utility.generateRandomString(5, 'CARACTERE'),'Dupond','PersonAccount'));
      }
    insert listAccounts;

    list<SICo_Site__c> listSite= new list<SICo_Site__c> ();
    for(Account acc : listAccounts) {
      listSite.add(SICo_UtilityTestDataSet.createSite(acc.Id));
      }
    insert listSite;

    list<SICo_Meter__c> listMeter= new list<SICo_Meter__c> ();
    for(Account acc : listAccounts) {
      listMeter.add(SICo_UtilityTestDataSet.createMeter());
      }
    insert listMeter;

    list<SICo_SiteMeter__c> siteMeters = new list<SICo_SiteMeter__c> ();
    for(Integer i = 0, j = 0; i < 55; i++) {
      siteMeters.add(SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PDL,listSite.get(i).Id,listMeter.get(i).Id));
      }
    for(SICo_SiteMeter__c forSiteMeter : siteMeters){
      forSiteMeter= SICo_UtilityTestDataSet.addFieldForSiteMeter(forSiteMeter);
      }
    insert siteMeters;

    // TEST & ASSERT
    Test.startTest();
      List<Database.SaveResult> result = Database.update(siteMeters);
    Test.stopTest();
    Account accounts= [SELECT Id, ErrorLog__c FROM Account LIMIT 1];
    //System.assertEquals('20 Callout ELEC Reached',accounts.ErrorLog__c);
    }

  @isTest static void succesCalloutPDL() {
    CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
    insert boomiConfig;
    // Set mock callout class
    Test.setMock(HttpCalloutMock.class, new SICo_FluxSubscription_MOCK());
    Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
    insert account;
    SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
    insert site;
    SICo_Meter__c meter= SICo_UtilityTestDataSet.createMeter();
    insert meter;
    SICo_SiteMeter__c siteMeter= SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PDL,site.Id,meter.Id);
    siteMeter= SICo_UtilityTestDataSet.addFieldForSiteMeter(siteMeter);
    insert siteMeter;

    // TEST & ASSERT
    Test.startTest();
      Database.SaveResult result = Database.update(siteMeter);
    Test.stopTest();
    System.assert(result.isSuccess());
    }

  @isTest static void errorCalloutPDL() {
    CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
    insert boomiConfig;
    // Set mock callout class
    Test.setMock(HttpCalloutMock.class, new SICo_FluxSubscription_MOCK());
    Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
    insert account;
    SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
    insert site;
    SICo_Meter__c meter= SICo_UtilityTestDataSet.createMeter();
    insert meter;
    SICo_SiteMeter__c siteMeter= SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PDL,site.Id,meter.Id);
    siteMeter= SICo_UtilityTestDataSet.addFieldForSiteMeter(siteMeter);
    // Just for TEST - if Millesime__c equals 10 => Mock test return Error
    siteMeter.Millesime__c= 10;
    insert siteMeter;

    // TEST & ASSERT
    Test.startTest();
      Database.SaveResult result = Database.update(siteMeter);
    Test.stopTest();
    System.assert(result.isSuccess());
    }

  /*************
      CALLOUT
  **************/

  @isTest static void calloutTEST() {
    CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
    insert boomiConfig;
    // Set mock callout class
    Test.setMock(HttpCalloutMock.class, new SICo_FluxSiteMeterGAZ_MOCK());
    Test.startTest();
    // This causes a fake response to be sent
    // from the class that implements HttpCalloutMock.
    HttpResponse response = SICo_Utility.getCallout('POST','https://test.connect.boomi.com/ws/rest/connected-services/edelia-iot/v1/subscriptions',
    '{"zone":"Z4","tss":null,"tariff":null,"siteId":"a1N4E0000001QbrUAE","personId":"0014E00000EdI1NQAV","parameters":{},"option":null,"millesime":null,"contractName":null,"changeDate":"2016-08-01T12:22:13.140Z"}');
    // Verify that the response received contains fake values
    String contentType = response.getHeader('Content-Type');
    System.assert(contentType == 'application/json');
    System.assertEquals(200, response.getStatusCode());
    String json = response.getBody();
    SICo_FluxProfileModel.ReturnRequest fluxReturnRequest = new SICo_FluxProfileModel.ReturnRequest().parse(json);
    Test.stopTest();

    String awaitedJSON = 'ReturnRequest:[errorDescription=NO ERROR, ownerId='+fluxReturnRequest.ownerId+', statusCode=200]';
    System.assertEquals(String.valueof(fluxReturnRequest), awaitedJSON);
    }

}