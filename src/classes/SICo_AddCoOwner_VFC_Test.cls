/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi-solutions
Description:   Controller for the AddCoOwner page (CommunitiesLogin)
Test Class:    SICo_AddCoOwner_VFC_Test
History
09/09/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
@isTest
private class SICo_AddCoOwner_VFC_Test {
	@testSetup static void initTestData() {

		Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
		insert account;
		SICo_Site__c oSICoSite = SICo_UtilityTestDataSet.createSite(account.Id);
		oSICoSite.IsActive__c = true;
		oSICoSite.IsSelectedInCustomerSpace__c=true;
		oSICoSite.PrincipalHeatingSystemType__c = 'Gaz';
		oSICoSite.SanitaryHotWaterType__c = 'Electrique';
		oSICoSite.NoOfOccupants__c = 4;
		oSICoSite.surfaceInSqMeter__c = 80;
		oSICoSite.InseeCode__c = '44109';
		oSICoSite.ConstructionDate__c = 1980;
		insert oSICoSite;


		CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
	    insert boomiConfig;
	}

	@isTest static void methods_called_by_ligthning_test(){
		SICo_Site__c oSICoSite = [SELECT Id FROM SICo_Site__c WHERE Subscriber__r.FirstName = 'Test01' LIMIT 1];
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		SICo_AddCoOwner_VFC.getSite(account.Id);
		SICo_AddCoOwner_VFC.insertBeneficiary(account.Id, 'Mme', 'firstaNameTest1', 'lastNameTest');

		SICo_Beneficiary__c beneficiary = new SICo_Beneficiary__c(Salutation__c = 'Mme', FirstName__c = 'Test', LastName__c = 'TestName');
		System.assertEquals('Test', beneficiary.FirstName__c);
	}

	@isTest static void constructor_test() {
		SICo_AddCoOwner_VFC controller = new SICo_AddCoOwner_VFC();
		SICo_Site__c oSICoSite = [SELECT Id FROM SICo_Site__c WHERE Subscriber__r.FirstName = 'Test01' LIMIT 1];
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		controller.accountId = account.Id;
		controller.siteId = oSICoSite.Id;
	}

	/*test if there is several sites*/
	@isTest static void getSite_test(){
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		SICo_Site__c oSICoSite2 = SICo_UtilityTestDataSet.createSite(account.Id);
		oSICoSite2.IsActive__c = true;
		oSICoSite2.IsSelectedInCustomerSpace__c=true;
		insert oSICoSite2;

		Test.startTest();

		SICo_AddCoOwner_VFC.getSite(account.Id);

		Test.stopTest();
	}

	/*test if there is several sites*/
	@isTest static void insertBeneficiary_test(){
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		SICo_Site__c oSICoSite2 = SICo_UtilityTestDataSet.createSite(account.Id);
		oSICoSite2.IsActive__c = true;
		oSICoSite2.IsSelectedInCustomerSpace__c=true;
		insert oSICoSite2;

		Test.startTest();

		SICo_AddCoOwner_VFC.insertBeneficiary(account.Id, 'Mme', 'firstaNameTest1', 'lastNameTest');

		Test.stopTest();
	}

}