public with sharing class Invoice_Utility {

	public static void mapSiteMeter(List<Zuora__ZInvoice__c> l_invoices) {
		Map<ID,List<Zuora__ZInvoice__c>> m_billingAccounts = new Map<ID,List<Zuora__ZInvoice__c>>();
		for(Zuora__ZInvoice__c v_invoice : l_invoices){
			if(!m_billingAccounts.containsKey(v_invoice.Zuora__Account__c)) {
				m_billingAccounts.put(v_invoice.Zuora__Account__c, new List<Zuora__ZInvoice__c>());
			}
			m_billingAccounts.get(v_invoice.Zuora__Account__c).add(v_invoice);
		}

		for(Zuora__Subscription__c v_subscription : [SELECT siteId__r.ActiveSiteMeterElec__c,siteId__r.ActiveSiteMeterGas__c,Zuora__Account__c 
													 FROM Zuora__Subscription__c 
													 WHERE Zuora__Account__c 
													 IN :m_billingAccounts.keySet()]) {
			for(Zuora__ZInvoice__c v_invoice : m_billingAccounts.get(v_subscription.Zuora__Account__c)) {
				v_invoice.SiteMeterElec__c = v_subscription.siteId__r.ActiveSiteMeterElec__c;
				v_invoice.SiteMeterGaz__c = v_subscription.siteId__r.ActiveSiteMeterGas__c;
			}
		}	
	}


	public static void updateDebitDay(List<Zuora__ZInvoice__c> l_invoices) {

       	System.debug('### INVOICES to be processed ==> ' + l_invoices);

		try {

			PublicationParameters__mdt param = new PublicationParameters__mdt();
			param = [SELECT Value__c FROM PublicationParameters__mdt
						WHERE DeveloperName = 'DebitDateParameter'];

	        System.debug('### PARAMETER ==> ' + param);

			Map<ID,List<Zuora__ZInvoice__c>> m_billingAccounts = new Map<ID,List<Zuora__ZInvoice__c>>();
			for(Zuora__ZInvoice__c v_invoice : l_invoices){
				if(!m_billingAccounts.containsKey(v_invoice.Zuora__BillingAccount__c)) {
					m_billingAccounts.put(v_invoice.Zuora__BillingAccount__c, new List<Zuora__ZInvoice__c>());
				}
				m_billingAccounts.get(v_invoice.Zuora__BillingAccount__c).add(v_invoice);
			}

			System.debug('### MAP Billing Account ==> ' + m_billingAccounts);

			for (Zuora__CustomerAccount__c v_customers : [SELECT Id, Zuora__Account__c, Zuora__Batch__c, Zuora__BillCycleDay__c 
															FROM Zuora__CustomerAccount__c 
															WHERE Id in :m_billingAccounts.keySet()]) {
				
				System.debug('### BEGIN FOR');

				String v_billcycleday = v_customers.Zuora__BillCycleDay__c.left(2).isNumeric() ?  v_customers.Zuora__BillCycleDay__c.left(2) : v_customers.Zuora__BillCycleDay__c.left(1);
				String  v_batch = v_customers.Zuora__Batch__c.right(2).isNumeric() ? v_customers.Zuora__Batch__c.right(2) : v_customers.Zuora__Batch__c.right(1);

				System.debug('### BILINIG DAY = ' + v_billcycleday);
				System.debug('### BATCH = ' + v_batch);

				Date v_debitDate;
				Integer vi_billcycleday = Integer.valueOf(v_billcycleday);
				Integer vi_batch = Integer.valueOf(v_batch);
				Integer vi_ddParameter = Integer.valueOf(param.Value__c);
				Integer nb_month;

				If (vi_billcycleday < vi_batch - vi_ddParameter) {
					// debit date = billing cycle day / mois courant / année courante
					//v_debitDate = Date.newInstance(Date.today().year(), Date.today().month(), vi_batch);
					nb_month = 0;
				} else {
					// debit date = billing batch / mois suivant / année courante (attention si fin d'année alors année suivante)
					//v_debitDate = Date.newInstance(Date.today().year(), Date.today().month(), vi_batch).addMonths(1);
					nb_month = 1;
				}

				System.debug('### Debit date = ' + v_debitDate);

				for (Zuora__ZInvoice__c invoices : m_billingAccounts.get(v_customers.Id)) {
					v_debitDate = Date.newInstance(invoices.Zuora__InvoiceDate__c.year(), invoices.Zuora__InvoiceDate__c.month(), vi_batch).addMonths(nb_month);
					invoices.scheduleDebitDate__c = v_debitDate;
				}

				System.debug('### END FOR');
			}

    	} catch (Exception e) {
    		   SICo_LogManagement.sendErrorLog( 
                'SICo',
                Label.SICO_LOG_INVOICE_DEBITDAY,
                String.valueOf( e ));
        }

	}

}