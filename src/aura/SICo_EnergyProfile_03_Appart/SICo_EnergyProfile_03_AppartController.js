({  
	updateInformations : function (component, event, pHelper) {
		var idAccount = component.get("v.accountId"); 
		//set the attibute choiceType and choiceResidence  
		component.set("v.epw.placementLocation", $("input[name='emplacement']:checked").val());
		component.set("v.epw.positionCorner", $("input[name='positionAppart']:checked").val() == 'true'); 
		// "Disable" the validate button
		pHelper.disableValidate();
		if (component.get("v.epw.placementLocation") == undefined || component.get("v.epw.positionCorner") == undefined) { 			
			pHelper.displayError(component, $A.get("{!$Label.c.SICo_ErrorNoValue}"), "Error_03_Appart");
		} else {
			//put the values in the apex controller parameters
			var action = component.get("c.setInformations_03_Appart");
			action.setParams({
				siteId : component.get("v.epw.Id"),
				emplacement : component.get("v.epw.placementLocation"),
				position : component.get("v.epw.positionCorner")
			});

			//redirect to the next page
			action.setCallback(this, function (response) {
				pHelper.moveToNextStep();
			});
			
			$A.enqueueAction(action);
		}
	}, 

	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_03_Appart");
	}
})