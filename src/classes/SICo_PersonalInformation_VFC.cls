/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Controller for the Personal Information page (CommunitiesLogin)
Test Class:    SICo_PersonalInformation_VFC_TEST
History
11/08/2016      Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
public with sharing class SICo_PersonalInformation_VFC {
    
    public Id accountId{get;set;}
    public String bankingAccountId{get;set;}
    public Id siteId{get;set;}
    public Id zuoraOldPaymentMethodId{get;set;}
    public PersonalInformationsWrapper piw{get;set;}
    public Id userId{get;set;}
    public String docusignEvent {get;set;}

/**
* [Contructor]
*/
    public SICo_PersonalInformation_VFC() {
        
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        docusignEvent = System.currentPageReference().getParameters().get('event');
        doInit(requestedAccountId);
    }
    public SICo_PersonalInformation_VFC(String requestedAccountId) {
        doInit(requestedAccountId);
    }
    
    private void doInit(String requestedAccountId) {
        //Get AccountId
        try{
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }catch (exception e){
            System.debug('Unable to return accountId');
        }
        try{
            User oUser = [SELECT id from User Where AccountId=:accountid LIMIT 1];
            this.userId = oUser.Id;
        }catch(exception e){
            
            System.debug('Unable to catch userId');
        }
        
        //If AccountId found, retrieve associated personal information
        if(accountId != null)
        {
            //Query Account
            Account thisAccount = queryAccount();
            
            //Query Site
            SICo_Site__c thisSite = querySite();
            if(thisSite != null) siteId = thisSite.Id;
            
            //Query Payment Method (Bank Information)
            Zuora__PaymentMethod__c thisPaymentMethod = queryPaymentMethod();
            
            List<Zuora__CustomerAccount__c> listZCA = [SELECT Zuora__External_Id__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c = :thisAccount.Id LIMIT 1];
            if (!listZCA.isEmpty()){
                bankingAccountId  = (!listZCA.isEmpty() ? '' + listZCA.get(0).Zuora__External_Id__c : null);
            }
            
            //Query Subscription
            List<Zuora__Subscription__c> thisSubscriptionList = querySubscriptionList();
            
            //Query Beneficiaries ("Co-Titulaires")
            List<SICo_Beneficiary__c> thisBeneficiaryList = null;
            
            //Create Wrapper with the data to be displayed on the page
            if (thisAccount != null || thisSite != null || thisPaymentMethod != null){
                piw = new PersonalInformationsWrapper   (
                    thisAccount,
                    thisSite,
                    thisPaymentMethod,
                    thisSubscriptionList,
                    bankingAccountId
                );
            }
            
        }//ENDIF (accountId != null)
    }
    
    public class PersonalInformationsWrapper{
        //Mes informations de contact
        public String salutationAndName {get;set;}
        public String firstName {get;set;}
        public String lastName {get;set;}
        public String addressLine1 {get;set;}
        public String addressLine2 {get;set;}
        public String addressLine3 {get;set;}
        public String addressLine4 {get;set;}
        public String phone {get;set;}
        public String mobilePhone {get;set;}
        public Boolean isPersonAccount{get; set; }
        
        //Identifiant SOWEE
        public String email {get;set;}
        
        //newsletters
        public Boolean soweeOptin {get;set;}
        public Boolean partnerOptin {get;set;}
        
        //Données bancaires
        public String accountOwner {get;set;}
        public String iban {get;set;}
        public Integer ibansize {get;set;}
        public String bankingAccountId {get;set;}
        public String zuoraOldPaymentMethodId {get;set;}
        
        public Integer subscriptionType {get;set;}
        
        //Profil énergie
        public String profileCompletion {get;set;}
        public String residenceType {get;set;}
        public String housingType {get;set;}
        public String house {get;set;}
        public String flat {get;set;}
        public String workDone {get;set;}
        public String accomodationSurface {get;set;}
        public String constructionDate {get;set;}
        public String numberOfOccupants {get;set;}
        public String heatingSystemType {get;set;}
        public String hotWater {get;set;}
        public String gasSource {get;set;}
        public String electricalEquipment {get;set;}
        public String airConditioner {get;set;}
        public String electricityContract {get;set;}
        public Boolean isPaymentMethodUpdated {get; set;}
        
        
        
        public PersonalInformationsWrapper(Account account, SICo_Site__c site, Zuora__PaymentMethod__c zpm, List<Zuora__Subscription__c> listZS, String baId){
            
            Id userId = UserInfo.getUserId(); 
            
            List<User> listUser = [SELECT Id, AccountId FROM User WHERE id=:userId LIMIT 1]; 
            if (listUser[0].AccountId != null){
                isPersonAccount = true; 
            }
            //Mes informations de contact
            salutationAndName = ((account.Salutation != null) ? account.Salutation + ' ' : '');
            salutationAndName += ((account.FirstName != null) ? account.FirstName + ' ' : '');
            salutationAndName += (account.LastName != null) ? account.LastName : '';

            firstName = account.LastName;
            lastName = account.FirstName;
            
            addressLine1 = (account.ContactAddressNumberStreet__c != null) ? account.ContactAddressNumberStreet__c : '';
            
            addressLine2 = ((account.ContactAdresseBuilding__c != null) ? account.ContactAdresseBuilding__c+ ' ' : '');
            addressLine2 += ((account.ContactAddressFloor__c != null) ? account.ContactAddressFloor__c+ ' ' : '');
            addressLine2 += ((account.ContactAddressStair__c != null) ? account.ContactAddressStair__c+ ' ' : '');
            addressLine2 += (account.ContactAddressFlat__c != null) ? account.ContactAddressFlat__c : '';
            
            addressLine3 = ((account.ContactAddressZipCode__c != null) ? account.ContactAddressZipCode__c + ' ' : '');
            addressLine3 += (account.ContactAddressCity__c != null) ? account.ContactAddressCity__c : '';
            
            addressLine4 = (account.ContactAddressCountry__c != null) ? account.ContactAddressCountry__c : '';
            
            phone = (account.Phone != null) ? account.Phone : '';
            
            mobilePhone = (account.PersonMobilePhone != null) ? account.PersonMobilePhone : '';
            
            //Identifiant SOWEE
            email = (account.PersonEmail != null) ? account.PersonEmail : '';
            
            //newsletters
            soweeOptin = (account.SoweeOptin__c != null) ? account.SoweeOptin__c : false;
            partnerOptin = (account.PartnerOptin__c != null) ? account.PartnerOptin__c : false;
            
            //Données bancaires
            accountOwner = (zpm != null && zpm.Zuora__FirstName__c != null) ? zpm.Zuora__FirstName__c+' '+zpm.Zuora__LastName__c : null;
            iban = (zpm != null && zpm.Zuora__BankTransferAccountNumber__c != null) ? zpm.Zuora__BankTransferAccountNumber__c : null;
            if (iban != null){
                ibansize = iban.length();
            }
            else {
                ibansize = 0;
            }
            bankingAccountId = baId;
            //Payment Method flagged as IsPaymentMethodUpdated?
            isPaymentMethodUpdated  = (zpm != null && zpm.Zuora__External_Id__c != null) ? zpm.IsPaymentMethodUpdated__c : false;
            //User Session has flag of recently updated payment method
            String paymentMethodRecentlyUpdated = (String)SICo_PlatformCacheManager_CLS.getSessionObject(SICo_Constants_CLS.SESSION_CACHE_KEY_PAYMENTMETHODUPDATE+account.Id);
            if(zpm ==  null && paymentMethodRecentlyUpdated != null) isPaymentMethodUpdated = true;
            
            //Matériel
            subscriptionType = 0;
            if (listZS != null && !listZS.isEmpty()){
                for (Zuora__Subscription__c zs : listZS){
                    if (zs.OfferName__c != null && zs.OfferName__c.length() >= 5 && zs.OfferName__c.substring(0,5) == SICo_Constants_CLS.OFFER_SE_TH){
                        subscriptionType = 2;
                        break;
                    }
                    else if (zs.OfferName__c != null && zs.OfferName__c.length() >= 2 && zs.OfferName__c.substring(0,2) == SICo_Constants_CLS.OFFER_SE){
                        subscriptionType = 1;
                    }
                }
            }
            //Energy Profile
            if (site != null){
                profileCompletion = ((site.ProfileCompletion__c  != null) ? site.ProfileCompletion__c + ''  : '');
                residenceType = ((site.ResidenceType__c != null) ? site.ResidenceType__c : '');
                housingType = ((site.HousingType__c != null) ? site.HousingType__c : '');
                //Housing Type: House
                if (housingType == Label.SICo_House) {
                    flat = null;
                    house = ((site.IsArrangedRoofPresent__c) ? Label.SICo_ArrangedRoofPresent : '');
                    //add a comma if needed
                    house += ((site.IsArrangedRoofPresent__c && site.IsFloorPresent__c) ? ', ' : '');
                    house += ((site.IsFloorPresent__c) ? Label.SICo_FloorPresent : '');
                    //add a comma if needed
                    house += (((site.IsArrangedRoofPresent__c || site.IsFloorPresent__c) && site.IsVerandaPresent__c) ? ', ' : '');
                    house += ((site.IsVerandaPresent__c ) ? Label.SICo_VerandaPresent : '');
                    //add a comma if needed
                    house += (((site.IsArrangedRoofPresent__c || site.IsFloorPresent__c  || site.IsVerandaPresent__c ) && site.IsJointHouse__c  ) ? ', ' : '');
                    house += ((site.IsJointHouse__c) ? Label.SICo_JointHouse : '');
                }
                //Housing Type: Appartment
                else if (housingType == Label.SICo_Flat) {
                    house = null;
                    if(site.HousingLocationType__c == SICo_Constants_CLS.SITE_PARAM_SSTERRASSE){
                        flat = Label.SICo_LastFloor;
                    }else if(site.HousingLocationType__c  == SICo_Constants_CLS.SITE_PARAM_INTERMEDIAIRE){
                        flat = Label.SICo_IntermediateFloor;
                    }else if(site.HousingLocationType__c  == SICo_Constants_CLS.SITE_PARAM_RDC){
                        flat = Label.SICo_GroundFloor;
                    }else{
                        flat = '';
                    }
                    //add a comma if needed
                    flat += ((site.HousingLocationType__c != null && site.IsCornerApartment__c  != null) ? ', ' : '');
                    
                    flat += ((site.IsCornerApartment__c != null) ? Label.SICo_CornerApartment : '');
                }
                else {
                    house = null;
                    flat = null;
                }
                //Building work - Renvovation
                workDone = ((site.IsWindowRestored__c) ? Label.SICo_WindowRestored : '');
                //add a comma if needed
                workDone += ((site.IsWindowRestored__c && site.IsWallRestored__c) ? ', ' : '');
                workDone += ((site.IsWallRestored__c) ? Label.SICo_WallRestored : '');
                //add a comma if needed
                workDone += (((site.IsWindowRestored__c || site.IsWallRestored__c) && site.IsArrangedRoofRestored__c) ? ', ' : '');
                workDone += ((site.IsArrangedRoofRestored__c) ? Label.SICo_ArrangedRoofRestored : '');
                //Surface
                accomodationSurface = ((site.SurfaceInSqMeter__c != null) ? site.SurfaceInSqMeter__c + '' : '');
                //Construction Date
                constructionDate = ((site.ConstructionDate__c != null) ? site.ConstructionDate__c + '' : '');
                //Nb of occupants
                numberOfOccupants = ((site.NoOfOccupants__c != null) ? site.NoOfOccupants__c + '' : '');
                //Heating Type
                if(site.PrincHeatingSystemTypeDetail__c == SICo_Constants_CLS.SITE_PARAM_PAC){
                    heatingSystemType = Label.SICo_HeatPump;
                }else if (site.PrincipalHeatingSystemType__c == SICo_Constants_CLS.SITE_PARAM_ELEC){
                    heatingSystemType = Label.SICo_Electricity;
                }else if (site.PrincipalHeatingSystemType__c != null){
                    heatingSystemType = site.PrincipalHeatingSystemType__c;
                }else{
                    heatingSystemType = '';
                }
                //Hot Water
                if(site.SanitaryHotWaterType__c == SICo_Constants_CLS.SITE_PARAM_ELEC){
                    hotWater = System.Label.SICo_Electricity;
                }else if(site.SanitaryHotWaterType__c != null){
                    hotWater = '';
                }
                //Cooking - Gas Source
                if(site.GasSource__c == SICo_Constants_CLS.SITE_PARAM_VILLE){
                    gasSource = System.Label.SICo_NaturalGas;
                }
                else if(site.GasSource__c == SICo_Constants_CLS.SITE_PARAM_BOUT){
                    gasSource = site.GasSource__c;
                }
                else if(site.IsCeramicGlassPlatePresent__c == true){
                    gasSource = Label.SICo_Electricity;
                }
                else{
                    gasSource = '';
                }
                //Electrical Equipments
                electricalEquipment = ((site.IsWineCellarPresent__c) ? Label.SICo_Equipment_WineCellar : '');
                //add a comma if needed
                electricalEquipment += ((site.IsWineCellarPresent__c && site.HasSwimmingPool__c) ? ', ' : '');
                electricalEquipment += ((site.HasSwimmingPool__c) ? Label.SICo_Equipment_SwimmingPool : '');
                //add a comma if needed
                electricalEquipment += (((site.IsWineCellarPresent__c || site.HasSwimmingPool__c) && site.IsAquariumPresent__c) ? ', ' : '');
                electricalEquipment += ((site.IsAquariumPresent__c) ? Label.SICo_Equipment_Aquarium : '');
                //AC
                airConditioner = ((site.IsACPresent__c != null && site.IsACPresent__c) ? '✔' : '-');
                //Electricity Contract
                electricityContract = '';
                if(site.ElectricityTariffOption__c == SICo_Constants_CLS.SITE_PARAM_HPHC){
                   electricityContract = Label.SICo_ElectricityContract_TariffOption2Inline;
                }else if(site.ElectricityTariffOption__c  != null){
                    electricityContract = site.ElectricityTariffOption__c;
                }
                electricityContract += ((site.ElectricityTariffOption__c != null && site.SubscribedPower__c != null) ? ' - ' : '');
                electricityContract += ((site.SubscribedPower__c != null) ? site.SubscribedPower__c : '');
            }
        }
    }
    
    
    @AuraEnabled
    public static void setNewsLettersSubscription(Id accountId, Boolean checkboxValue, String newsLetter) {
        List<Account> lsAccount =   [
            SELECT SoweeOptin__c, PartnerOptin__c
            FROM Account
            WHERE Id = :accountId
            LIMIT 1
        ];
        if (!lsAccount.isEmpty()){
            Account accountToUpdate = lsAccount.get(0);
            if (newsLetter == 'sowee'){
                accountToUpdate.SoweeOptin__c = checkboxValue;
            }
            else if (newsLetter == 'partners'){
                accountToUpdate.PartnerOptin__c = checkboxValue;
            }
            update accountToUpdate;
        }
    }
    
    /**** HELPER METHODS ****/
    
    private Account queryAccount(){
        return [SELECT Id,
                Salutation,
                FirstName,
                LastName,
                PersonEmail,
                Phone,
                PersonMobilePhone,
                ContactAddressNumberStreet__c,
                ContactAddressAdditionToAddress__c,
                ContactAdresseBuilding__c,
                ContactAddressStair__c,
                ContactAddressFloor__c,
                ContactAddressFlat__c,
                ContactAddressZipCode__c,
                ContactAddressCity__c,
                ContactAddressCountry__c,
                SoweeOptin__c,
                PartnerOptin__c
                FROM Account
                WHERE Id =:accountId AND IsPersonAccount = true LIMIT 1];
    }
    
    private SICo_Site__c querySite(){
        SICo_Site__c theSite = null;
        List <SICo_Site__c> siteList = [SELECT Id,
                                        IsSelectedInCustomerSpace__c,
                                        ProfileCompletion__c,
                                        NumberStreet__c,
                                        Floor__c,
                                        Stairs__c,
                                        AdditionToAddress__c,
                                        Building__c,
                                        Flat__c,
                                        PostalCode__c,
                                        City__c,
                                        Country__c,
                                        ResidenceType__c,
                                        HousingType__c,
                                        IsArrangedRoofPresent__c,
                                        IsFloorPresent__c,
                                        IsVerandaPresent__c,
                                        IsJointHouse__c,
                                        HousingLocationType__c,
                                        IsCornerApartment__c,
                                        IsWindowRestored__c,
                                        IsWallRestored__c,
                                        IsArrangedRoofRestored__c,
                                        surfaceInSqMeter__c,
                                        ConstructionDate__c,
                                        NoOfOccupants__c,
                                        PrincipalHeatingSystemType__c,
                                        PrincHeatingSystemTypeDetail__c,
                                        SanitaryHotWaterType__c,
                                        GasSource__c,
                                        IsCeramicGlassPlatePresent__c,
                                        IsWineCellarPresent__c,
                                        hasSwimmingPool__c,
                                        IsAquariumPresent__c,
                                        ElectricityTariffOption__c,
                                        IsACPresent__c,
                                        SubscribedPower__c
                                        FROM SICo_Site__c
                                        WHERE Subscriber__c =:accountId LIMIT 100];
        //If 1 single site is retrieved, use it
        if(siteList.size() == 1)
        {
            theSite = siteList[0];
            //if several Sites, use the one selected in Customer Space
        }else if(siteList.size() > 1){
            for(SICo_Site__c custSite : siteList){
                if(custSite.IsSelectedInCustomerSpace__c) theSite = custSite;
            }
        }//ENDIF siteList.size()
        return theSite;
    }
    
    private Zuora__PaymentMethod__c queryPaymentMethod(){
        Zuora__PaymentMethod__c thePaymentMethod;
        List<Zuora__PaymentMethod__c> paymentMethodList = [SELECT Id,
                                                           Zuora__FirstName__c ,
                                                           Zuora__LastName__c,
                                                           Zuora__DefaultPaymentMethod__c,
                                                           Zuora__BankTransferAccountName__c,
                                                           Zuora__IBAN__c,
                                                           Zuora__BankTransferAccountNumber__c,
                                                           Zuora__BillingAccount__r.Zuora__External_Id__c,
                                                           Zuora__External_Id__c,
                                                           IsPaymentMethodUpdated__c
                                                           FROM Zuora__PaymentMethod__c
                                                           WHERE Zuora__PaymentMethodStatus__c =: SICo_Constants_CLS.ACTIVE_PAYMENT_METHOD_STATUS
                                                           AND (Zuora__Type__c =: SICo_Constants_CLS.ZUORA_PAYMENT_METHOD_BANK_TRANSFERT OR Zuora__Type__c =: SICo_Constants_CLS.ZUORA_PAYMENT_METHOD_BANK_TRANSFERT_WHITESPACE)
                                                           AND Zuora__BillingAccount__r.Zuora__Account__c =:accountId]; //Note: there should be 1 single billing account per person account
        //If 1 single payment method is active, use it
        if(paymentMethodList.size() == 1)
        {
            thePaymentMethod = paymentMethodList[0];
            //if several payment methods, use the default one
        }else if(paymentMethodList.size() > 1){
            for(Zuora__PaymentMethod__c meth : paymentMethodList){
                if(meth.Zuora__DefaultPaymentMethod__c) thePaymentMethod = meth;
            }
        }//ENDIF siteList.size()
        return thePaymentMethod;
    }
    
    
    @AuraEnabled
    public static List<SICo_Beneficiary__c> queryBeneficiaryList(Id accountId, Id siteId){
        List<SICo_Beneficiary__c> beneficiaryList = [SELECT Id,
                                                     Salutation__c,
                                                     LastName__c,
                                                     FirstName__c,
                                                     Email__c
                                                     FROM SICo_Beneficiary__c
                                                     WHERE Client__c =:accountId AND Site__c=:siteId];
        return beneficiaryList;
    }
    
    
    private List<Zuora__Subscription__c> querySubscriptionList(){
        return [SELECT Id,
                OfferName__c
                FROM Zuora__Subscription__c
                WHERE Zuora__Account__c =:accountId
                AND SiteId__c=:siteId];
    }
    
} //END SICo_PersonalInformation_VFC()