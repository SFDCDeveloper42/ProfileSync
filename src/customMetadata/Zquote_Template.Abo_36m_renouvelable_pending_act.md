<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Abo 36m, renouvelable, pending act</label>
    <protected>false</protected>
    <values>
        <field>ApplyCreditBalance__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>AutoRenew__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ContractEffectiveDate__c</field>
        <value xsi:type="xsd:string">0</value>
    </values>
    <values>
        <field>CustomerAcceptanceDate__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Nouvel Abonnement 36 mois, renouvelable 36 mois.
Contract Effective Date = TODAY
Service Activation Date = j+30
Customer Acceptance Date = J+30</value>
    </values>
    <values>
        <field>GenerateInvoice__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>InitialTerm__c</field>
        <value xsi:type="xsd:double">36.0</value>
    </values>
    <values>
        <field>PaymentTerm__c</field>
        <value xsi:type="xsd:string">Due Upon Receipt</value>
    </values>
    <values>
        <field>ProcessPayment__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>QuoteBusinessType__c</field>
        <value xsi:type="xsd:string">New</value>
    </values>
    <values>
        <field>RenewalSetting__c</field>
        <value xsi:type="xsd:string">RENEW_WITH_SPECIFIC_TERM</value>
    </values>
    <values>
        <field>RenewalTerm__c</field>
        <value xsi:type="xsd:double">36.0</value>
    </values>
    <values>
        <field>ServiceActivationDate__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SubscripitonTermType__c</field>
        <value xsi:type="xsd:string">Termed</value>
    </values>
</CustomMetadata>
