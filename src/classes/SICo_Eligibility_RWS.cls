/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Web Service REST in order to provide eligibility ( GRDF & CHAM ) - call by Mobile & Xively Platform
Test Class:    SICo_Eligibility_RWS_TEST
History
23/07/2016     	Olivier Vrbanac     Create & develope first version
09/08/2016		Dario Correnti		Add RateZone__c to WS response					
------------------------------------------------------------*/
@RestResource(urlMapping='/Eligibility')
global with sharing class SICo_Eligibility_RWS {

    @HttpGet
    global static Response getEligibility_RWS() {

    	///REST SERVICES PARAMETERS
        String sINSEECode = RestContext.request.params.get( 'insee_code' );
        Response customerJSON = new Response(false, null, null);

        try {

            // INFORMATION TO SEND BACK IN THE RESPONSE OBJECT CLASS
            SICo_Eligibility__c oGaz = SICo_Eligibility_CLS.getGasEligibility( sINSEECode );
            Account oAgency = SICo_Eligibility_CLS.getMaintenanceEligibility( sINSEECode );
            
            Boolean bGaz = oGaz != null;
            String sGazRateZone = (oGaz != null)
                ? oGaz.RateZone__c
                : '';
            
            Response res = new Response( bGaz, sGazRateZone, oAgency );

            customerJSON = res;

        } catch ( Exception e ) {

            SICo_LogManagement.sendErrorLog(
                'SICo',
                Label.SICO_LOG_LEAD_Eligibility,
                String.valueOf( e )
            );

        }

    	return customerJSON;

	}

	//WRAPPER RESPONSE
	global class Response {

		Boolean bGaz;
        String sRateZone;
		Account oAgency;
		
		private Response ( Boolean bGazToUse, String sRateZoneToUse, Account oAgencyToUse ) {
			bGaz = bGazToUse;
            sRateZone = sRateZoneToUse;
			oAgency = oAgencyToUse;
		}
	}

}