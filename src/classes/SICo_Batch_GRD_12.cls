/*------------------------------------------------------------
Author:        Gerald Ramier
Company:       Salesforce.com
Description:   Web Service REST in order to provide notify SFDC of Integration process completion for Batch processing
Test Class:    SICo_BatchNotify_RWS_TEST
History
12/08/2016     Gerald Ramier     Creation
------------------------------------------------------------*/

global with sharing class SICo_Batch_GRD_12 extends SICo_Batch {
    
    global integer recordsProcessed = 0;
    
    global SICO_Batch_GRD_12() {
    }

    override global Database.QueryLocator start(Database.BatchableContext BC) {
        
        Id rtId = SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_Gaz');

        return Database.getQueryLocator([SELECT whatID, OmegaRequestId__c, PCE__c, LastModifiedDate, InterventionType__c, TaskID__c
                                          FROM Task
                                        WHERE 
                                          PCE__c <> null
                                          AND OmegaRequestId__c <> null
                                          AND whatid = null
                                          AND BatchFlag__c = true
                                          AND RecordTypeId = :rtId
                                          ORDER by PCE__c]);
    }

    override global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Set<String> pces = new Set<String>();
        List<Task> eligibleScope = new List<Task>();

        System.debug('LIST TASKS ===> ' + scope);

        for (Task t : (List<Task>) scope) {
            pces.add(t.PCE__c);
        }

        System.debug('LIST PCES ===> ' + pces);

        Map<String, ID> siteMap = new Map<String, ID>();

        for(SICO_Site__c s :[SELECT PCE__c, ID FROM SICO_Site__c 
                                WHERE PCE__c IN :pces 
                                AND IsActive__c = true]) {
            siteMap.put(s.PCE__c, s.ID);            
         }

        System.debug('MAP PCE and SITES ===> '+siteMap);

        if (siteMap.isEmpty())
            return;

        for (Task t : (List<Task>) scope) if (siteMap.get(t.PCE__c) != null) {
            t.SiteID__c = siteMap.get(t.PCE__c);
            t.NotTakenByCRC__c = true;
            eligibleScope.add(t);
        }

        SICO_Batch_Utility.getActivityCases(eligibleScope, 'GRD_12');

    }
    
}