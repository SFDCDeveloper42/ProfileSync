public virtual with sharing class SICo_LeadServiceQuote_CLS{

  private static final Map<String,Sico_AdminUpsell__mdt> upsellTable = new Map<String,Sico_AdminUpsell__mdt>();

  static {
    List<Sico_AdminUpsell__mdt> adminUpsells = [SELECT DeveloperName, RatePlanType__c, ToAmend__c 
                                                FROM Sico_AdminUpsell__mdt];

    for(Sico_AdminUpsell__mdt aAdminUpsell : adminUpsells){
      SICo_LeadServiceQuote_CLS.upsellTable.put(aAdminUpsell.DeveloperName, aAdminUpsell);
    }
  }

  private Id aAccountId;
  final protected Map<SICo_LeadService.QuoteTemplate,String> amendMap;
  public boolean hasSubscription {get;private set;}

  private static final String OFFER_SEPARATOR = '_';

  public SICo_LeadServiceQuote_CLS(final Map<SICo_LeadService.QuoteTemplate,Set<Zqu__ProductRatePlan__c>> ratePlanPerContractType, 
                                    final Map<String,Set<String>> offerNamePerQuoteType, 
                                    final Id theAccount){

    this.amendMap = new Map<SICo_LeadService.QuoteTemplate,String>();
    this.aAccountId = theAccount;

    this.populateAmendTable(ratePlanPerContractType,offerNamePerQuoteType);
    System.debug('aAccountId: '+ this.aAccountId);
    System.debug('upsellTable: '+ SICo_LeadServiceQuote_CLS.upsellTable);
    System.debug('offerNamePerQuoteType: '+ offerNamePerQuoteType);
    System.debug('ratePlanPerContractType: '+ ratePlanPerContractType);   
  }

  public Map<SICo_LeadService.QuoteTemplate,String> getMapAmend(){
    return this.amendMap;
  }

  //Returns a map with RatePlan Code and Subscription ID
  protected Map<String,Zuora__Subscription__c> getExistingSubscriptions() {
    this.hasSubscription = false;
    Map<String,Zuora__Subscription__c> subscriptionsMap = new Map<String,Zuora__Subscription__c>();
    List<Zuora__Subscription__c> subscriptions = [SELECT Id, Quote__c, Quote__r.OfferName__c, Zuora__External_Id__c, SICO_Subscribed_offers__c
                                                  FROM Zuora__Subscription__c WHERE Zuora__Account__c = :this.aAccountId];

    for(Zuora__Subscription__c aSubscription : subscriptions){
      this.hasSubscription = true;
      subscriptionsMap.put(aSubscription.Quote__r.OfferName__c,aSubscription);
    }
    return subscriptionsMap;
  }

  protected void populateAmendTable(final Map<SICo_LeadService.QuoteTemplate,Set<Zqu__ProductRatePlan__c>> ratePlanPerContractType, 
                                    final Map<String,Set<String>> offerNamePerQuoteType){
    Map<String,Zuora__Subscription__c> subscriptionsMap = this.getExistingSubscriptions();
    System.debug('subscriptionsMap: '+ subscriptionsMap);

    for(SICo_LeadService.QuoteTemplate aQuoteTemplate : ratePlanPerContractType.keySet()){
      System.debug('### aQuoteTemplate: '+ aQuoteTemplate);

      for(Sico_AdminUpsell__mdt aAdminUpsell : SICo_LeadServiceQuote_CLS.upsellTable.values()){
        System.debug('### aAdminUpsell: '+ aAdminUpsell);

        for(String st : offerNamePerQuoteType.get(aQuoteTemplate.getUniqueId())){
          System.debug('### st: '+ st);

          if(aAdminUpsell.RatePlanType__c == st && subscriptionsMap.containsKey(aAdminUpsell.ToAmend__c)){
            Zuora__Subscription__c aSubscription = subscriptionsMap.get(aAdminUpsell.ToAmend__c);
            this.amendMap.put(aQuoteTemplate,aSubscription.Zuora__External_Id__c);
          }
        }
      }     
    }
  }
}