/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Controller for UnpaidBills page. This class is global due to the remote action, used wihin an iframe.
Test Class:    SICo_UnpaidBills_VFC_TEST
History
16/09/2016      Gaël BURNEAU        Create version
24/10/2016      Mehdi CHERFAOUI     Fix Invoice Payment
------------------------------------------------------------*/
global with sharing class SICo_UnpaidBills_VFC {

    public Id accountId { get; set; }
    public Decimal totalUnpaidBillsAmount { get; set; }
    public Integer numberUnpaidBills { get; set; }
    public String zuoraUrl {get; set;}
    public String cbGatewayName {get; set;}
    public String urlJs {get;set;}

    public Class UnpaidInvoiceWrapper {
        public Id id { get; set; }
        public Boolean isChecked { get; set; }
        public String sInvoiceDate { get; set; }
        public Decimal dInvoiceAmount { get; set; }

        public String sInvoiceAttachmentId { get; set; }
        public String zuoraInvoiceId { get; set; }

        public UnpaidInvoiceWrapper(Zuora__ZInvoice__c oZuoraZInvoice, String sInvoiceDate, Id attachmentId, String zuoraInvoiceId) {
            this.id = oZuoraZInvoice.Id;
            this.isChecked = true;
            this.sInvoiceDate = sInvoiceDate;
            this.dInvoiceAmount = oZuoraZInvoice.TotalInvoiceAmountTTC__c;
            this.sInvoiceAttachmentId = attachmentId;
            this.zuoraInvoiceId = zuoraInvoiceId;
            
        }
    }

    public List<UnpaidInvoiceWrapper> listUnpaidInvoiceWrappers { get; set; }

    // Account wrapped 
    public AccountWrapper accountWrap { get; set; }
    public Class AccountWrapper {
        public String lastName { get; set; }
        public String firstName { get; set; }
        public String zuoraId { get; set; }
        public String ContactAddressNumberStreet { get; set; }
        public String ContactAddressZipCode { get; set; }
        public String ContactAddressCity { get; set; }
        public String ContactAddressCountry { get; set; }
        public String PersonEmail { get; set; }
        public String Phone { get; set; }
    }
    // Payment result
    public Class paymentResultWrapper { 
        public Boolean isSuccess { get; set; }
        public String errorMessage { get; set; }
        public String redirectURL { get; set; }
    }
    public paymentResultWrapper paymentResult { get; set; }

    /**
     * [Contructor]
     */
    public SICo_UnpaidBills_VFC() {

        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        doInit(requestedAccountId);
    }
    public SICo_UnpaidBills_VFC(String requestedAccountId) {
        doInit(requestedAccountId);
    }

    private void doInit(String requestedAccountId) {
    // public SICo_UnpaidBills_VFC() {

        // String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);

        try {
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        } catch (exception e) {
            System.debug('Unable to return accountId');
        }
        ZuoraWSconfig__c js = ZuoraWSconfig__c.getInstance();
        this.urlJs = js.ZuoraJSResource__c;


        if (accountId != null) {
            // Get account informations  and wrap it
            Account vAccount = queryAccount();
            accountWrap = getAccountWrapped(vAccount);
            
            // create the zuora URL
            this.zuoraUrl = ZuoraWSconfig__c.getInstance().Uri__c;
            this.cbGatewayName = ZuoraWSconfig__c.getInstance().GatewayNameCB__c;
            //Retrieve all unpaid invoices of the account
            List<Zuora__ZInvoice__c> listZuoraZInvoices = new List<Zuora__ZInvoice__c>();
            listZuoraZInvoices = [SELECT Id, Zuora__InvoiceDate__c, TotalInvoiceAmountTTC__c, NextInvoiceDate__c,
                                         Zuora__Status__c, Zuora__Balance2__c, IsPaymentInProgress__c, Zuora__External_Id__c,
                                         (SELECT Id FROM Attachments ORDER BY CreatedDate DESC LIMIT 1) //IKUMBI-480
                                    FROM Zuora__ZInvoice__c
                                    WHERE Zuora__Account__c = :accountId
                                    AND IsPaymentInProgress__c = false
                                    AND IsUnpaid__c = true
                                ORDER BY Zuora__InvoiceDate__c DESC LIMIT 1000];

            // IKUMBI-480 : remove invoices with no pdf
            for (Integer i = 0; i < listZuoraZInvoices.size(); i++) {
                System.debug('#######' + listZuoraZInvoices[i].attachments);
                if (listZuoraZInvoices[i].attachments == null || listZuoraZInvoices[i].attachments.isEmpty()) {
                    listZuoraZInvoices.remove(i);
                    i--;
                }
            }

            //If there is more than one invoice
            if (!listZuoraZInvoices.isEmpty()) {

                totalUnpaidBillsAmount = 0;
                numberUnpaidBills = 0;
                for (Zuora__ZInvoice__c inv : listZuoraZInvoices) {
                    totalUnpaidBillsAmount += inv.TotalInvoiceAmountTTC__c;
                    numberUnpaidBills++;
                }

                //Create wrapper for all the invoices and add them in a list
                listUnpaidInvoiceWrappers = new List<UnpaidInvoiceWrapper>();
                for (Integer i = 0; i < listZuoraZInvoices.size(); i++) {
                    Zuora__ZInvoice__c oInvoice = listZuoraZInvoices[i];
                    String sInvDate = String.valueOf(oInvoice.Zuora__InvoiceDate__c.day())
                                        + ' ' + SICo_Constants_CLS.MONTHES.get(oInvoice.Zuora__InvoiceDate__c.month()).toUpperCase()
                                        + ' ' + String.valueOf(oInvoice.Zuora__InvoiceDate__c.year());
                    Id attId = oInvoice.attachments[0].Id;
                    if (attId != null) {                        
                        String zuoraInvoiceId = oInvoice.Zuora__External_Id__c;
                        listUnpaidInvoiceWrappers.Add(new UnpaidInvoiceWrapper(oInvoice, sInvDate, attId, zuoraInvoiceId));
                    }
                }
            }
        }
    }

    /*
    * Return the link URL to the dashboard
    */
    public String getDashboardLink() {
        return Page.Dashboard.getUrl() + '?accountId=' + accountId;
    }

    /*
    * Call Zuora WS REST (see utility community class)
    */
    @RemoteAction
    global static String processSignature(){
        Map<String, String> mapSignature = SICo_UtilityCommunities_CLS.callZuoraLogin('CB');
        String signature = JSON.serialize(mapSignature);
        return signature;
    }
    
    public String callZuoraPayment(String paymentMethodId){
        String zuoraAccountId = accountWrap.zuoraId;
        Decimal paymentAmount = 0;
        if (zuoraAccountId != null && listUnpaidInvoiceWrappers != null){
            try {

                /*** InvoicePaymentData ***/
                List<Zuora.zObject> invoicePaymentsList = new List<Zuora.zObject>();
                //LOOP on each invoice (listUnpaidInvoiceWrappers)
                for (UnpaidInvoiceWrapper uiw : listUnpaidInvoiceWrappers){
                    Zuora.zObject invoicePayment = new Zuora.zObject('InvoicePayment');
                    if (uiw.isChecked){
                        paymentAmount += uiw.dInvoiceAmount;
                        //InvoicePayment
                        invoicePayment.setValue('Amount', uiw.dInvoiceAmount);
                        invoicePayment.setValue('InvoiceId', uiw.zuoraInvoiceId);
                        invoicePaymentsList.add(invoicePayment);
                    }
                }//END LOOP listUnpaidInvoiceWrappers
                Zuora.zObject invoicePaymentData = new Zuora.zObject('InvoicePaymentData');
                invoicePaymentData.setValue('InvoicePayment', invoicePaymentsList);

                /*** PAYMENT ***/
                Zuora.zObject payment = new Zuora.zObject('Payment');

                //AccountId Zuora
                payment.setValue('AccountId', zuoraAccountId);

                //Amount
                payment.setValue('Amount', paymentAmount);
                payment.setValue('InvoicePaymentData', invoicePaymentData);
                //EffectiveDate
                Date d = date.today();
                String dt = DateTime.newInstance(d.year(), d.month(), d.day()).format('yyyy-MM-dd');
                payment.setValue('EffectiveDate', dt);

                //PaymentMethodId
                payment.setValue('PaymentMethodId', paymentMethodId);

                //Status
                payment.setValue('Status', 'Processed');

                //Gateway
                payment.setValue('Gateway', cbGatewayName);

                //Type
                payment.setValue('Type', 'Electronic');

                //AppliedCreditBalanceAmount
                payment.setValue('AppliedCreditBalanceAmount', 0);

                List<Zuora.zObject> objs = new List<Zuora.zObject> {payment};
                List<Zuora.zApi.SaveResult> results = new List<Zuora.zApi.SaveResult>();
                
                if (paymentAmount > 0){
                    Zuora.zApi zApiInstance = SICo_ZuoraUtilities.zuoraApiAccess();
                    results = SICo_ZuoraUtilities.createZuoraObjects(zApiInstance, objs);  
                }
                if (results.size() > 0){
                    Zuora.zApi.SaveResult result = results.get(0);
                    if (result.Success){
                        String paymentId = result.Id;
                        if(confirmZuoraPayment(paymentId)){
                            return updateInvoices();
                        }
                        else {
                            return 'Payment could not be processed.';
                        }
                    } else { 
                        SICo_ZuoraUtilities.analyzeSaveResult(result);
                    }
                }
            } catch (Zuora.zRemoteException ex) {
                if ('INVALID_FIELD' == ex.code) {
                    system.debug('INVALID_FIELD: '+ex);
                } else {
                    system.debug('zRemoteException '+ex);
                }
            } catch (Zuora.zAPIException ex) {
                ex.getMessage();
                system.debug('zAPIException'+ex);
            } catch (Zuora.zForceException ex) {
                system.debug('zForceException '+ex);
            }
        }
        else {
            return 'zuoraAccountId is null or paymentAmount = 0';
        }
        return 'callZuoraPayment error';
    }

    /*
    * Returns the current account with the necessary information
    */
    private Account queryAccount(){
        return  [
                    SELECT Id, Salutation, FirstName, LastName, ContactAddressNumberStreet__c, 
                            ContactAddressZipCode__c, ContactAddressCity__c, PersonEmail, Phone 
                    FROM Account 
                    WHERE Id =:accountId 
                    AND IsPersonAccount = true 
                    LIMIT 1
                ];
    }

    /*
    * Returns the account wrapped
    */
    private AccountWrapper getAccountWrapped(Account pAccount) {
        if (pAccount == null) {
            return null;
        }
        AccountWrapper vAccountWrapped = new AccountWrapper();
        vAccountWrapped.firstName = pAccount.FirstName;
        vAccountWrapped.lastName = pAccount.LastName;
        vAccountWrapped.ContactAddressNumberStreet = pAccount.ContactAddressNumberStreet__c;
        vAccountWrapped.ContactAddressZipCode = pAccount.ContactAddressZipCode__c;
        vAccountWrapped.ContactAddressCity = pAccount.ContactAddressCity__c;
        vAccountWrapped.PersonEmail = pAccount.PersonEmail;
        vAccountWrapped.Phone = pAccount.Phone;
        List<Zuora__CustomerAccount__c> zca = [SELECT Zuora__External_Id__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c = :pAccount.Id LIMIT 1];
        vAccountWrapped.zuoraId = (zca != null && zca.size() > 0 && zca[0].Zuora__External_Id__c != null ? zca[0].Zuora__External_Id__c : null);

        return vAccountWrapped;
    }

    /*
    * call Zuora and proceed to payment
    * @param : the refID returned by zuora after having sumitted the credit card informations
    */
    public PageReference proceedToPayment() {
        paymentResult = new paymentResultWrapper();
        String pRefId;
        if (Test.isRunningTest()) {
            pRefId = 'testId';
        }
        else {
            pRefId = apexpages.currentpage().getparameters().get('pRefId');
        }
        if (pRefId == null) {
            paymentResult.errorMessage = 'Error no ref Id returned by Zuora';
            paymentResult.isSuccess = false;
            return null;
        } 
        else {
            String callZuoraPaymentRet = callZuoraPayment(pRefId);
            if (callZuoraPaymentRet == 'success'){
                paymentResult.isSuccess = true;
                paymentResult.redirectURL = Page.PaymentSuccess.getUrl() + '?accountId=' + accountId;
                return null;
            } else {
                paymentResult.isSuccess = false;
                paymentResult.errorMessage = callZuoraPaymentRet;
                paymentResult.redirectURL = '';
                return null;
            }
        }
        return null;
    }

    public Boolean confirmZuoraPayment(String paymentId){
        Zuora.zApi zApiInstance = SICo_ZuoraUtilities.zuoraApiAccess();

        String zoql = 'SELECT Status FROM Payment WHERE Id = \'' + paymentId + '\'';

        List<Zuora.zObject> invzobjs = new List<Zuora.zObject>();
        invzobjs = SICo_ZuoraUtilities.queryToZuora(zApiInstance, zoql);
        
        for (Zuora.zObject o : invzobjs) {
            String zPaymentStatus = (String)o.getValue('Status');
            if (zPaymentStatus == 'Processed') {
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }

    public String updateInvoices(){
        List<Id> listIdInvoicesToUpdate = new List<Id>();

        if(listUnpaidInvoiceWrappers != null){
            for (UnpaidInvoiceWrapper uiw : listUnpaidInvoiceWrappers){
                if (uiw.isChecked){
                    listIdInvoicesToUpdate.add(uiw.id);
                }
            }
        }
        if (listIdInvoicesToUpdate.size() > 0){
            List<Zuora__ZInvoice__c> listInvoicesToUpdate = [SELECT Id, IsPaymentInProgress__c FROM Zuora__ZInvoice__c WHERE Id IN :listIdInvoicesToUpdate];
            if (listInvoicesToUpdate.size() > 0){
                for (Zuora__ZInvoice__c zi : listInvoicesToUpdate){
                    zi.IsPaymentInProgress__c = true;
                }
                update listInvoicesToUpdate;

                return 'success';
            }
        }
        paymentResult.isSuccess = false;// Flag as error returned
        
        return 'updateInvoices error';
    }

}