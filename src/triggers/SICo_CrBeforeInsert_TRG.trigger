/**
* @author Boris Castellani
* @date 06/12/2016
*
* @description SICo_CR__c Trigger Before insert
*/
trigger SICo_CrBeforeInsert_TRG on SICo_CR__c(before insert) {
  if(PAD.canTrigger('SICo_CrBeforeInsert_TRG')) {
    SICo_CrTrigger_CLS.HandleBeforeInsert(Trigger.new);
  }
}