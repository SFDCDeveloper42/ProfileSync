/*------------------------------------------------------------
Author:        Dario Correnti
Company:       Salesforce.com
Description:   Web Service REST to notify SFDC that the Zuora bill run has completed
Test Class:    SICo_BillRunNotify_RWS
History
03/10/2016     Dario Correnti     Creation
------------------------------------------------------------*/

@RestResource(urlMapping='/NotifyBillRunCompletion')
global with sharing class SICo_BillRunNotify_RWS {
	
	@HttpGet
    global static void create() {
        Database.executeBatch(new SICo_Batch_BILL_RUN(SICO_ImportZuoraUsage.InvoceItemReportName));
    	
    }
}