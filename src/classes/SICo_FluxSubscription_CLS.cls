/**
* @author Boris Castellani
* @date 06/07/2016
* @LastModify 27/01/2017
*
* @description Build JSON message
*/
public with sharing class SICo_FluxSubscription_CLS {

  public static Boolean executeFluxSubscription(String methode, String endPoint, String body){
    
    HttpResponse response = SICo_Utility.getCallout(methode,endPoint,body);
    String json = response.getBody();
    SICo_FluxSubscriptionModel.ReturnRequest fluxReturnRequest = SICo_FluxSubscriptionModel.parseReturnRequest(json);
    Set<Id> tasks = new Set<Id>();

    Map<id,string> taskErrorList = new Map<id,string>();
    for(SICo_FluxSubscriptionModel.Responses resp : fluxReturnRequest.responses){
      if(resp.Response.statusCode !='200'){
        taskErrorList.put(Id.valueOf(resp.Response.activityId),resp.Response.errorDescription);
      }
      tasks.add(Id.valueOf(resp.Response.activityId));
    }

    List<Task> taskUpdateList= [SELECT Id, Status, ErrorLog__c 
                                FROM Task WHERE Id in :tasks];

    Boolean result = true;
    for(Task aTask : taskUpdateList){
      if(taskErrorList.containsKey(aTask.Id)){
        aTask.ErrorLog__c= taskErrorList.get(aTask.Id);
        aTask.FluxStatus__c= 'KO';
        result = false;
      }else{
        aTask.Status= 'Completed';
        aTask.FluxStatus__c= 'OK';
      }
    }
    if(!taskUpdateList.isEmpty()){
      update taskUpdateList;
    }      
    return result;
  }

  @future(callout=true)
  public static void executeFluxSubscriptionAsync(String methode, String endPoint, String body){
    executeFluxSubscription(methode,endPoint,body);
  }

  public static String buildFluxSubscription(Map<Id,Task> taskIdKeyTaskMap, Map<Id,Sico_AdminTask__mdt> taskIdKeyServiceMap,
                                             SICo_FluxSubscriptionModel.Address newSiteAdressExternal){

    if(taskIdKeyTaskMap.size() >0){
      List<Task> listTasks = [SELECT Id, OrderFlux__c, ProvisioningCallOut__c, TaskId__c, WhatId, FluxStatus__c, Parameters__c
                              FROM Task WHERE Id IN :taskIdKeyTaskMap.keyset()];
            
      /* 0- Recup information "Account ID, Quote ID, Opportunity ID" */
      Case cases = [SELECT Account.CustomerNumber__c, Site__r.SiteNumber__c, Account.FirstName, Account.PersonTitle, 
                    Account.LastName, Account.Phone, Account.PersonMobilePhone, Account.BillingStreet, Account.BillingCity, 
                    Account.BillingPostalCode, Account.BillingCountry, Account.PersonEmail, Site__r.NumberStreet__c, 
                    Site__r.PostalCode__c, Site__r.City__c, Site__r.Country__c 
                    FROM Case WHERE ID = :listTasks[0].WhatId];
      
      /* 1- BUILD JSON */
      SICo_FluxSubscriptionModel.Address newAccountAdress = new SICo_FluxSubscriptionModel.Address();
      List<String> newAccountLines = new List<String>();
      newAccountLines.add(cases.Account.BillingStreet);
      newAccountAdress.lines = newAccountLines;
      newAccountAdress.zipCode = cases.Account.BillingPostalCode;
      newAccountAdress.city = cases.Account.BillingCity;
      newAccountAdress.country = cases.Account.BillingCountry;

      // Site Address
      SICo_FluxSubscriptionModel.Address newSiteAdress = new SICo_FluxSubscriptionModel.Address();
      if(newSiteAdressExternal != null){
        newSiteAdress = newSiteAdressExternal;
      }else{
        List<String> newSiteLines = new List<String>();
        newSiteLines.add(cases.Site__r.NumberStreet__c);
        newSiteAdress.lines = newSiteLines;
        newSiteAdress.zipCode = cases.Site__r.PostalCode__c;
        newSiteAdress.city = cases.Site__r.City__c;
        newSiteAdress.country = cases.Site__r.Country__c;
      }           

      // Person
      SICo_FluxSubscriptionModel.Phones newPhones = new SICo_FluxSubscriptionModel.Phones();
      newPhones.principal = cases.Account.Phone;
      newPhones.portable = cases.Account.PersonMobilePhone;
      SICo_FluxSubscriptionModel.Person newPerson = new SICo_FluxSubscriptionModel.Person();
      newPerson.title = cases.Account.PersonTitle;
      newPerson.firstName = cases.Account.FirstName;
      newPerson.lastName = cases.Account.LastName;
      newPerson.address = newAccountAdress;
      newPerson.email = cases.Account.PersonEmail;
      newPerson.phones = newPhones;

      // SubscriptionRequest
      SICo_FluxSubscriptionModel.PersonParameters newPersonParameters = new SICo_FluxSubscriptionModel.PersonParameters();
      SICo_FluxSubscriptionModel.SiteParameters newSiteParameters = new SICo_FluxSubscriptionModel.SiteParameters();
      SICo_FluxSubscriptionModel.SubscribedOfferParameters newSubscribedOfferParameters = new SICo_FluxSubscriptionModel.SubscribedOfferParameters();
      SICo_FluxSubscriptionModel.SubscriptionRequest newSubscriptionRequest = new SICo_FluxSubscriptionModel.SubscriptionRequest();
      newSubscriptionRequest.ts = Datetime.now();
      newSubscriptionRequest.person = newPerson;
      newSubscriptionRequest.siteAddress = newSiteAdress;
      newSubscriptionRequest.PersonParameters = newPersonParameters;
      newSubscriptionRequest.SiteParameters = newSiteParameters;
      newSubscriptionRequest.SubscribedOfferParameters = newSubscribedOfferParameters;

      // build with Dynamique
      // Dynamique Fields: subscribedOfferExternalId, endpoint
      Map<Decimal,SICo_FluxSubscriptionModel.Action> mapActions = new Map<Decimal,SICo_FluxSubscriptionModel.Action>();
      List<SICo_FluxSubscriptionModel.Action> listActions = new List<SICo_FluxSubscriptionModel.Action>();
      List<Decimal> listActionSort = new List<Decimal>();       
      for(Task aTask :listTasks){
        // Action
        SICo_FluxSubscriptionModel.Action newAction = new SICo_FluxSubscriptionModel.Action();
        newAction.personId = SICo_Utility.generate18CharId(cases.Account.CustomerNumber__c);
        newAction.siteId = SICo_Utility.generate18CharId(cases.Site__r.SiteNumber__c);
        newAction.activityId = aTask.ID;
        newAction.endpoint = String.isNotBlank(aTask.Parameters__c) ? aTask.Parameters__c.substringBefore(';') : taskIdKeyServiceMap.get(aTask.Id).Parameters__c.substringBefore(';');
        newAction.externalOfferCode = String.isNotBlank(aTask.Parameters__c) ? aTask.Parameters__c.substringAfter(';') : taskIdKeyServiceMap.get(aTask.Id).Parameters__c.substringAfter(';');
        newAction.SubscriptionRequest = newSubscriptionRequest;
        mapActions.put(aTask.OrderFlux__c,newAction);
        listActionSort.add(aTask.OrderFlux__c);
      }
      listActionSort.sort();
      for(Decimal i : listActionSort){
          listActions.add(mapActions.get(i));
      }
            
      // Build Ojbject & finalise JSON
      SICo_FluxSubscriptionModel.Request FluxRequest = new SICo_FluxSubscriptionModel.Request();
      FluxRequest.action = listActions;
      String requestJSON = JSON.serialize(FluxRequest);
      return requestJSON;
    }
    return '';
  }
}