/**
* @author Boris Castellani
* @date 18/10/2016
*
* @description SICo_SiteMeter__c Trigger After update
*/
trigger SICo_ZSubscriptionBeforeInsert_TRG on Zuora__Subscription__c(before insert) {
	if(PAD.canTrigger('SICo_ZSubscriptionBeforeInsert_TRG')) {
		SICo_SubscriptionTrigger_CLS.retrieveSubScriptionInfo(Trigger.new);
    //SICo_SubscriptionTrigger_CLS.calculateOfferPicklist(Trigger.new);
	}    
}