/**
* @author Dario Correnti
* @date 30/09/2016
*
* @description Use SICo batch pattern
* Called after Zuora Bill Run to handle Usage & Invoce Item import
* To Launch : Database.executeBatch(new SICo_Batch_BILL_RUN());
*/
global with sharing class SICo_Batch_BILL_RUN extends SICo_Batch  implements Database.Batchable<sObject>,  Database.AllowsCallouts {

    global  String reportName;

    global SICo_Batch_BILL_RUN (String inreportName) {
      this.reportName=inreportName;
    }

    global override Database.QueryLocator start(Database.BatchableContext BC) {
      //Launch Execute
      return Database.getQueryLocator([SELECT id FROM Account limit 1]);
    }

    global override void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(SICO_ImportZuoraUsage.InvoceItemReportName.equals(this.reportName)){
	        SICO_ImportZuoraUsage.launchProcess(SICO_ImportZuoraUsage.InvoceItemReportName);    
        }else{
          SICO_ImportZuoraUsage.launchProcess(SICO_ImportZuoraUsage.UsageReportName);
        }
    }
     
    global override void finish(Database.BatchableContext BC){
      //SICO_ImportZuoraUsage.launchProcess(SICO_ImportZuoraUsage.UsageReportName);
    }

}