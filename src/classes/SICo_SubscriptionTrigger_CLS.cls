/**
* @author ???
* @date Modification 18/10/16 by Boris Castellani (add handleActiveSiteMeter)
*
* @description SICo_SubscriptionTrigger_CLS Trigger After update
*/

public with sharing class SICo_SubscriptionTrigger_CLS {

	private static final String OFFER_SEPARATOR = '_';
	private static final String SEPARATOR_PICKLIST = ';';
	private static final String STATUS_INACTIVE = 'inactive';
	private static final String STATUS_CANCELLED = 'cancelled';


	/* -------- BEFORE METHODE -------- */

	public static void retrieveSubScriptionInfo(List<Zuora__Subscription__c> listSubscription){
		handleActiveSiteMeter(listSubscription);
		Set<String> quotes = new set<String>() ;
		Map<String,Zuora__Subscription__c> mapsubquote = new Map<String,Zuora__Subscription__c>();
		for ( Zuora__Subscription__c sub : listSubscription) {
			mapsubquote.put(sub.Zuora__QuoteNumber__c,sub);
			quotes.add(sub.Zuora__QuoteNumber__c);
			// Get Millesime
			if (String.isEmpty(sub.Millesime__c) && sub.Zuora__OriginalCreated_Date__c != null){
				Date originalDate = sub.Zuora__OriginalCreated_Date__c.date();
				for(SICo_Millesime__c mill : SICo_Utility.list_Millesimes){
					if(mill.StartDate__c <= originalDate && mill.EndDate__c >= originalDate){
						sub.Millesime__c = mill.Millesime__c;
						break;	
					}
				}						
			}
		}
		List<zqu__Quote__c> zQuotes = [SELECT Id, SiteId__c, zqu__Number__c, zqu__Opportunity__c
            											 FROM zqu__Quote__c 
            											 WHERE zqu__Number__c in :quotes];

		for (zqu__Quote__c aZquote: zQuotes ){
			Zuora__Subscription__c sub = mapsubquote.get(aZquote.zqu__Number__c);
			sub.Quote__c = aZquote.id;
			sub.SiteId__c = aZquote.SiteId__c;
      sub.Opportunity__c = aZquote.zqu__Opportunity__c;
		}
	}

	public static void calculateOfferPicklist(List<Zuora__Subscription__c> newSubscription) {

		Map<Id,List<Zuora__Subscription__c>> toTreat = new Map<Id,List<Zuora__Subscription__c>>();
		for(Zuora__Subscription__c aSub : newSubscription) {
			if(!toTreat.containsKey(aSub.SiteId__c)) {
				toTreat.put(aSub.SiteId__c, new List<Zuora__Subscription__c>());
			}
			toTreat.get(aSub.SiteId__c).add(aSub);	
		}

		List<SICo_Site__c> sites = new List<SICo_Site__c>();
		for(SICo_Site__c aSite : [SELECT Id, SICO_Subscribed_offers__c, (SELECT Id, SICO_Subscribed_offers__c,Zuora__Status__c FROM Subscriptions__r) 
															FROM SICo_Site__c WHERE id IN :toTreat.keySet()]) {

			system.debug(aSite);
			system.debug(aSite.Subscriptions__r);

			Set<String> subscribedOffers = new Set<String>();
			for(Zuora__Subscription__c aSubscription : aSite.Subscriptions__r){
				if(String.isNotBlank(aSubscription.SICO_Subscribed_offers__c) && String.isNotBlank(aSubscription.Zuora__Status__c) 
					&& aSubscription.Zuora__Status__c.toLowerCase() != STATUS_INACTIVE && aSubscription.Zuora__Status__c.toLowerCase() != STATUS_CANCELLED){
						subscribedOffers.addAll(aSubscription.SICO_Subscribed_offers__c.split(SEPARATOR_PICKLIST));
				}
			}
			// NEW SUBSCRIPTION
			for(Zuora__Subscription__c aNewSubscription : toTreat.get(aSite.id)){
				if(String.isNotBlank(aNewSubscription.SICO_Subscribed_offers__c) && String.isNotBlank(aNewSubscription.Zuora__Status__c) 
					&& aNewSubscription.Zuora__Status__c.toLowerCase() != STATUS_INACTIVE && aNewSubscription.Zuora__Status__c.toLowerCase() != STATUS_CANCELLED){
						subscribedOffers.addAll(aNewSubscription.SICO_Subscribed_offers__c.split(SEPARATOR_PICKLIST));
				}	
			}
			aSite.SICO_Subscribed_Offers__c = '';
			if(!subscribedOffers.isEmpty()){
				aSite.SICO_Subscribed_Offers__c = String.join(new List<String>(subscribedOffers),SEPARATOR_PICKLIST);		
			}
			sites.add(aSite);		
		}
		update sites;
	}

  /* -------- AFTER METHODE -------- */

	public static void handleSpreadAttachement(Map<Id,Zuora__Subscription__c> subscriptions){

		Set<ID> accountIds = new Set<ID>();
		for(Zuora__Subscription__c aSub : subscriptions.values()) {
			accountIds.add(aSub.Zuora__Account__c);
		}

		List<Account> accounts = new List<Account>();
		for(Id anId : accountIds) {
			accounts.add(new Account(id=anId,SICO_HasSubscriptionAttachment__c=true));
		}

		update accounts;
	}

  /* -------- OTHER METHODE -------- */

  // CALL BY SICo_Provisioning_TRG.trigger
	public static void buildSubscribedOffers(List<Zuora__SubscriptionProductFeature__c> subscriptionFeatures){

		Set<Zuora__Subscription__c> subscriptions = new Set<Zuora__Subscription__c>();
		Set<String> productZuoraIds = new Set<String>();
		Map<Id,set<String>> productZuoraBySubscription = new Map<Id,set<String>>();
		for(Zuora__SubscriptionProductFeature__c  aSubProductFeature : subscriptionFeatures){
			subscriptions.add(new Zuora__Subscription__c (Id = aSubProductFeature.Zuora__Subscription__c));
			productZuoraIds.add(aSubProductFeature.Zuora__ProductZuoraId__c);
			if(!productZuoraBySubscription.containsKey(aSubProductFeature.Zuora__Subscription__c)){
				productZuoraBySubscription.put(aSubProductFeature.Zuora__Subscription__c, new Set<String>());
			}
			productZuoraBySubscription.get(aSubProductFeature.Zuora__Subscription__c).add(aSubProductFeature.Zuora__ProductZuoraId__c);
		}

		if(!productZuoraBySubscription.isEmpty()){
			Map<String,Set<String>> ratePlanTypeByProductZuora = new Map<String,Set<String>>();
			for(Zqu__ProductRatePlan__c aProductRatePlan : [SELECT Id, RatePlanType__c, Zqu__ZProduct__c, Zqu__ZProduct__r.Zqu__ZuoraId__c
																											 FROM Zqu__ProductRatePlan__c
																											 WHERE Zqu__ZProduct__r.Zqu__ZuoraId__c IN :productZuoraIds]){

				if(!ratePlanTypeByProductZuora.containsKey(aProductRatePlan.Zqu__ZProduct__r.Zqu__ZuoraId__c)){
					ratePlanTypeByProductZuora.put(aProductRatePlan.Zqu__ZProduct__r.Zqu__ZuoraId__c, new Set<String>());
				}
				ratePlanTypeByProductZuora.get(aProductRatePlan.Zqu__ZProduct__r.Zqu__ZuoraId__c).add(aProductRatePlan.RatePlanType__c);
			}

			system.debug('subscriptions: '+subscriptions);
			if(!ratePlanTypeByProductZuora.isEmpty()){
				for(Zuora__Subscription__c aSubscription : subscriptions){
					String offerName = '';
					for(String productZuoraId : productZuoraBySubscription.get(aSubscription.Id)){
						offerName = offerName + String.join(new list<String>(ratePlanTypeByProductZuora.get(productZuoraId)), OFFER_SEPARATOR);
						offerName = offerName + OFFER_SEPARATOR;
					}
					offerName = offerName.rightPad(offerName.length()-1);
					Set<String> offerNameJoin = new Set<String>();
					offerNameJoin.addAll(offerName.split(OFFER_SEPARATOR));
					aSubscription.SICO_Subscribed_offers__c = String.join(new list<String>(offerNameJoin),SEPARATOR_PICKLIST);
					aSubscription.OfferName__c = String.join(new list<String>(offerNameJoin),SEPARATOR_PICKLIST).replace(SEPARATOR_PICKLIST,OFFER_SEPARATOR);
				}
			}

			if(!subscriptions.isEmpty()){
				system.debug('UPDATE');
				Database.SaveResult[] srList = Database.update(new List<Zuora__Subscription__c>(subscriptions), false);
			}
		}
	}

  /* -------- PRIVATE METHODE -------- */

	public static void handleActiveSiteMeter(List<Zuora__Subscription__c> listSubscription){

		final Set<Id> setSite = new Set<Id>();
		final Map<Id,Zuora__Subscription__c> mapSiteSub = new Map<Id,Zuora__Subscription__c>();
		for(Zuora__Subscription__c sub : listSubscription){
			if(sub.SiteId__c !=null){
				setSite.add(sub.SiteId__c);
				mapSiteSub.put(sub.SiteId__c,sub);		
			}
		}

		final List<SICo_Site__c> sites = [SELECT Id, ActiveSiteMeterGas__c
											                FROM SICo_Site__c
											                WHERE Id IN :setSite];

		for(SICo_Site__c site : sites){
			Zuora__Subscription__c sub = mapSiteSub.get(site.Id);
			if(site.ActiveSiteMeterGas__c !=null){
				sub.SiteMeterGasActive__c = site.ActiveSiteMeterGas__c;
			}
		}
	}

}