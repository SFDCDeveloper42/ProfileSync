public with sharing class SICo_ZPaymentTrigger_CLS {

/* -------- BEFORE METHODE -------- */

	public static void handleBeforeInsert(List<Zuora__Payment__c> payments){
		Map<Id,Account> accs = getAccount(payments);
		if(!accs.isEmpty()){
			for(Zuora__Payment__c payment : payments){
				if(accs.containsKey(payment.Zuora__Account__c)){
					payment.Email__c =  accs.get(payment.Zuora__Account__c).PersonEmail;
				}
			}
		}		
	} 


/* -------- PRIVATE METHODE -------- */

	private static Map<Id,Account> getAccount(List<Zuora__Payment__c> payments){
		Set<Id> accId = new Set<Id>();
		for(Zuora__Payment__c payment : payments){
			if(payment.Zuora__Account__c != null){
				accId.add(payment.Zuora__Account__c);
			}
		}
		return new Map<Id,Account>([SELECT Id, PersonEmail FROM Account WHERE Id in :accId]);	
	} 

} //END