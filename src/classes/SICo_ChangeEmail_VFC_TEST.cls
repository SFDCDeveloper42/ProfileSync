/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi-solutions
Description:   Test method for SICo_ChangeEmail_VFC
Test Class:
History
20/09/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
@isTest
private class SICo_ChangeEmail_VFC_TEST {

	@isTest static void initData() {
		CommunityConfig__c commSett = SICo_UtilityTestDataSet.createCustomSetting ();
		insert commSett;
	}

	@IsTest
	public static void testChangeEmailEmailDiffer(){
		SICo_ChangeEmail_VFC controller = new SICo_ChangeEmail_VFC();
		Account testAccount = new Account (Name = 'Test');
		insert testAccount;
		Contact testContact = new Contact(AccountId = testAccount.Id, LastName = 'Test');
		insert testContact;
		User user = SICo_UtilityTestDataSet.createCommunityUser(testContact.Id);
		user.Email = 'testChangeEmail@gmail.com';
		insert user;

		Test.startTest();
		controller.oldEmail = user.Email;
		controller.newEmail = 'def@gmail.com';
		controller.verifyNewEmail = 'gef@gmail.com';
		controller.changeEmail();
		Test.stopTest();

		System.assertEquals(user.email, 'testChangeEmail@gmail.com');
	}

	@IsTest
	public static void testChangeEmailFailNoAccount(){
		SICo_ChangeEmail_VFC controller = new SICo_ChangeEmail_VFC();
		Account testAccount = new Account (Name = 'Test');
		insert testAccount;
		Contact testContact = new Contact(AccountId = testAccount.Id, LastName = 'Test');
		insert testContact;
		User user = SICo_UtilityTestDataSet.createCommunityUser(testContact.Id);
		insert user;

		Test.startTest();
		user.Email = 'testChangeEmail@gmail.com';
		controller.accountId = null; //No Account is passed, so that no mixed-DML error is thrown
		controller.oldEmail = user.Email;
		controller.newEmail = 'Azerty123zzztestChangeEmailPass@gzmail.com';
		controller.verifyNewEmail = controller.newEmail;
		controller.changeEmail();
		controller.getEmail();
		Test.stopTest();

		System.assertNotEquals(null, controller.errorMessage);
	}

	@IsTest
	public static void testChangeEmailFailDML(){
		SICo_ChangeEmail_VFC controller = new SICo_ChangeEmail_VFC();
		Account testAccount = new Account (Name = 'Test');
		insert testAccount;
		Contact testContact = new Contact(AccountId = testAccount.Id, LastName = 'Test');
		insert testContact;
		User user = SICo_UtilityTestDataSet.createCommunityUser(testContact.Id);
		insert user;

		Test.startTest();
		user.Email = 'testChangeEmail@gmail.com';
		controller.accountId = testAccount.Id; //will throw a mixed-DML error on update
		controller.oldEmail = user.Email;
		controller.oldEmail = user.Email;
		controller.newEmail = 'Azerty123zzztestChangeEmailPass@gzmail.com';
		controller.verifyNewEmail = controller.newEmail;
		controller.changeEmail();
		controller.getEmail();
		Test.stopTest();

		System.assertNotEquals(null, controller.errorMessage);
	}



}