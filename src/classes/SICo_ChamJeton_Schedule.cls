/**
* @author Boris Castellani
* @date 24/02/2016
*
* @description Schedulable: Update MaintenancyQuote statut 
*/
global class SICo_ChamJeton_Schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
      Database.executeBatch(new SICo_ChamJeton_BATCH());       
    }
}