/**
* @author Boris Castellani
* @date 19/10/2016
*
* @description SICo_Site__c Trigger Before insert
*/
trigger SICo_SiteBeforeInsert_TRG on SICo_Site__c(before insert) {
	if(PAD.canTrigger('SICo_SiteBeforeInsert_TRG')) {
		SICo_SiteTrigger_CLS.handleBeforeInsert(Trigger.new);
	}   
}