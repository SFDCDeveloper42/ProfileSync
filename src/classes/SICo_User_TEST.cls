/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi Solutions
Description:   Test Class for User Trigger
History
01/08/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
@isTest
private class SICo_User_TEST {

	@testSetup static void initTestData() {

		User user1 = SICo_UtilityTestDataSet.createCommunityUser();
		user1.Email = 'test.email@myemail.com';
		User user2 = SICo_UtilityTestDataSet.createCommunityUser();
		user2.Email = 'test.email.copy@myemail.com';
		insert new User[] {user1, user2};
	}

	@isTest static void trigger_test() {

		User user1 = [SELECT Id, Email FROM User WHERE Email = 'test.email@myemail.com'];

		Test.startTest();

		user1.Email = 'test.email.nocopy@myemail.com';
		update user1;

		Test.stopTest();

		user1 = [SELECT Id, Email FROM User WHERE Id = :user1.Id];
		System.assertEquals('test.email.nocopy@myemail.com', user1.Email);
	}

	@isTest static void trigger_test2() {

		User user1 = [SELECT Id, Email FROM User WHERE Email = 'test.email@myemail.com'];

		Test.startTest();

		user1.Email = 'test.email.copy@myemail.com';
		String error = '';
		try {
			update user1;
		} catch (DmlException e) {
			error = e.getDmlMessage(0);
		}

		Test.stopTest();

		user1 = [SELECT Id, Email FROM User WHERE Id = :user1.Id];
		System.assertEquals(Label.SICo_UserTrg_ChangeEmail_Error, error);
		System.assertEquals('test.email@myemail.com', user1.Email);
	}
}