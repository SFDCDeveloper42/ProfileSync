/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Test of the Controller for the Personal Information page (SICo_PersonalInformation_VFC)
History
11/08/2016      Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
@isTest
private class SICo_PersonalInformation_VFC_TEST {


    
    @isTest static void test_ConstructorUserHome() {
        //Insert new community user
        User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser1;
        communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

        //select account
        Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

        //Insert new site
        SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(communityUser1.AccountId);
		site1.IsActive__c = true;
        site1.SICO_Subscribed_offers__c = 'SE;TH;GAZ';
        //Energy profile
        site1.HousingType__c = Label.SICo_House;
        site1.PrincipalHeatingSystemType__c = SICo_Constants_CLS.SITE_PARAM_ELEC;
        site1.SanitaryHotWaterType__c = SICo_Constants_CLS.SITE_PARAM_ELEC2;
        site1.IsCeramicGlassPlatePresent__c = true;
        insert site1;
        site1 = 	[
	    			SELECT Id,
                        IsSelectedInCustomerSpace__c,
                        ProfileCompletion__c,
                        NumberStreet__c,
                        Floor__c,
                        Stairs__c,
                        AdditionToAddress__c,
                        Building__c,
                        Flat__c,
                        PostalCode__c,
                        City__c,
                        Country__c,
                        ResidenceType__c,
                        HousingType__c,
                        IsActive__c,
                        IsArrangedRoofPresent__c,
                        IsFloorPresent__c,
                        IsVerandaPresent__c,
                        IsJointHouse__c,
                        HousingLocationType__c,
                        IsCornerApartment__c,
                        IsWindowRestored__c,
                        IsWallRestored__c,
                        IsArrangedRoofRestored__c,
                        surfaceInSqMeter__c,
                        ConstructionDate__c,
                        NoOfOccupants__c,
                        PrincipalHeatingSystemType__c,
                        SanitaryHotWaterType__c,
                        GasSource__c,
                        IsCeramicGlassPlatePresent__c,
                        IsWineCellarPresent__c,
                        hasSwimmingPool__c,
                        IsAquariumPresent__c,
                        ElectricityTariffOption__c,
                        IsACPresent__c,
                        Subscriber__c,
                        SubscribedPower__c
	    			FROM SICo_Site__c 
	    			WHERE Id = :site1.Id
	    			LIMIT 1
				];

        //Insert new opportunity
    	Opportunity testOpportunity1 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity1', communityUser1.AccountId, site1.Id);
    	insert testOpportunity1;

        //Insert new quote
    	zqu__Quote__c testQuote1 = SICo_UtilityTestDataSet.createZuoraQuote('testQuote1', communityUser1.AccountId, communityUser1.ContactId, testOpportunity1.Id);
    	insert testQuote1;
        testQuote1 = [SELECT Id, zqu__Number__c FROM zqu__Quote__c WHERE Id = :testQuote1.Id LIMIT 1];

        //Insert new subscription
    	Zuora__Subscription__c testSubscription1 = SICo_UtilityTestDataSet.createZuoraSubscription('testSubscription1', communityUser1.AccountId, testQuote1.Id, testQuote1.zqu__Number__c, site1.Id);
    	testSubscription1.OfferName__c = 'SE_TH_GAZ';
    	insert testSubscription1;
    	testSubscription1 = [SELECT Id, Zuora__Account__c, SiteId__c FROM Zuora__Subscription__c WHERE Id = :testSubscription1.Id LIMIT 1];

        //Insert new payment method
    	Zuora__PaymentMethod__c zuoraPM1 = SICo_UtilityTestDataSet.createZuoraPaymentMethod(communityUser1.AccountId, true);
    	insert zuoraPM1;

        //Insert new beneficiary
    	SICo_Beneficiary__c benef1 = SICo_UtilityTestDataSet.createBeneficiary(communityUser1.AccountId, site1.Id);
    	insert benef1;

        //Run as Community User
        system.runAs(communityUser1){
            Test.startTest();

            //set controller
            SICo_PersonalInformation_VFC controller1 = new SICo_PersonalInformation_VFC();
            //set VF
            Test.setCurrentPageReference(new PageReference('Page.PersonalInformations'));
            //set url parameter
			System.currentPageReference().getParameters().put('AccountId', communityUser1.AccountId);

            //call static methods
			SICo_PersonalInformation_VFC.setNewsLettersSubscription(communityUser1.AccountId, true, 'sowee');
			SICo_PersonalInformation_VFC.setNewsLettersSubscription(communityUser1.AccountId, true, 'partners');
			SICo_PersonalInformation_VFC.queryBeneficiaryList(communityUser1.AccountId, site1.Id);
			acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];
            //asserts
            system.assert(acc1.SoweeOptin__c == true);
            system.assert(acc1.PartnerOptin__c == true);
            system.assert(controller1.piw != null);
        }
    }

    @isTest static void test_ConstructorUserFlat() {
        //Insert new community user
        User communityUser2 = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser2;
        communityUser2 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser2.Id LIMIT 1];

        //select account
        Account acc2 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser2.AccountId LIMIT 1];

        //Insert new site
        SICo_Site__c site2 = SICo_UtilityTestDataSet.createSite(communityUser2.AccountId);
		site2.IsActive__c = true;
		site2.HousingType__c = Label.SICo_Flat;
        insert site2;
        site2 = 	[
	    			SELECT Id,
                        IsSelectedInCustomerSpace__c,
                        ProfileCompletion__c,
                        NumberStreet__c,
                        Floor__c,
                        Stairs__c,
                        AdditionToAddress__c,
                        Building__c,
                        Flat__c,
                        PostalCode__c,
                        City__c,
                        Country__c,
                        ResidenceType__c,
                        HousingType__c,
                        IsActive__c,
                        IsArrangedRoofPresent__c,
                        IsFloorPresent__c,
                        IsVerandaPresent__c,
                        IsJointHouse__c,
                        HousingLocationType__c,
                        IsCornerApartment__c,
                        IsWindowRestored__c,
                        IsWallRestored__c,
                        IsArrangedRoofRestored__c,
                        surfaceInSqMeter__c,
                        ConstructionDate__c,
                        NoOfOccupants__c,
                        PrincipalHeatingSystemType__c,
                        SanitaryHotWaterType__c,
                        GasSource__c,
                        IsCeramicGlassPlatePresent__c,
                        IsWineCellarPresent__c,
                        hasSwimmingPool__c,
                        IsAquariumPresent__c,
                        ElectricityTariffOption__c,
                        IsACPresent__c,
                        Subscriber__c,
                        SubscribedPower__c
	    			FROM SICo_Site__c 
	    			WHERE Id = :site2.Id
	    			LIMIT 1
				];

        //Insert new opportunity
    	Opportunity testOpportunity2 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity2', communityUser2.AccountId, site2.Id);
    	insert testOpportunity2;

        //Insert new quote
    	zqu__Quote__c testQuote2 = SICo_UtilityTestDataSet.createZuoraQuote('testQuote2', communityUser2.AccountId, communityUser2.ContactId, testOpportunity2.Id);
    	insert testQuote2;
        testQuote2 = [SELECT Id, zqu__Number__c FROM zqu__Quote__c WHERE Id = :testQuote2.Id LIMIT 1];

        //Insert new subscription
    	Zuora__Subscription__c testSubscription2 = SICo_UtilityTestDataSet.createZuoraSubscription('testSubscription2', communityUser2.AccountId, testQuote2.Id, testQuote2.zqu__Number__c, site2.Id);
    	testSubscription2.OfferName__c = 'SE_TEST';
    	insert testSubscription2;
    	testSubscription2 = [SELECT Id, Zuora__Account__c, SiteId__c FROM Zuora__Subscription__c WHERE Id = :testSubscription2.Id LIMIT 1];

        //Insert new payments methods
    	Zuora__PaymentMethod__c zuoraPM2 = SICo_UtilityTestDataSet.createZuoraPaymentMethod(communityUser2.AccountId, false);
    	insert zuoraPM2;
    	Zuora__PaymentMethod__c zuoraPM3 = SICo_UtilityTestDataSet.createZuoraPaymentMethod(communityUser2.AccountId, true);
    	insert zuoraPM3;

        //Insert new beneficiary
    	SICo_Beneficiary__c benef2 = SICo_UtilityTestDataSet.createBeneficiary(communityUser2.AccountId, site2.Id);
    	insert benef2;

        //Run as Community User
        system.runAs(communityUser2){
            Test.startTest();

            //set controller
            SICo_PersonalInformation_VFC controller2 = new SICo_PersonalInformation_VFC();
            //set VF
            Test.setCurrentPageReference(new PageReference('Page.PersonalInformations'));
            //set url parameters
			System.currentPageReference().getParameters().put('AccountId', communityUser2.AccountId);
            //call static methods
			SICo_PersonalInformation_VFC.setNewsLettersSubscription(communityUser2.AccountId, false, 'sowee');
			SICo_PersonalInformation_VFC.setNewsLettersSubscription(communityUser2.AccountId, false, 'partners');
			SICo_PersonalInformation_VFC.queryBeneficiaryList(communityUser2.AccountId, site2.Id);
			acc2 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser2.AccountId LIMIT 1];
            //asserts
            system.assert(acc2.SoweeOptin__c == false);
            system.assert(acc2.PartnerOptin__c == false);
            system.assert(controller2.piw != null);

            Test.stopTest();
        }
    }

    @isTest
    static void test_setNewsLettersSubscription() {
		//Insert new community user
        User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser1;
        communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

        //select account
        Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];


        //Run as Community User
        system.runAs(communityUser1){ 
            Test.startTest();

            //set controller
            SICo_PersonalInformation_VFC controller1 = new SICo_PersonalInformation_VFC();
            //set VF
            Test.setCurrentPageReference(new PageReference('Page.PersonalInformations'));
            //set url parameter
			System.currentPageReference().getParameters().put('AccountId', communityUser1.AccountId);

			SICo_PersonalInformation_VFC.setNewsLettersSubscription(communityUser1.accountID, true, 'sowee');
			Account vAccount = [SELECT ID,SoweeOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];
			System.assert(vAccount.SoweeOptin__c == true, 'vAccount.SoweeOptin__c : ' + vAccount.SoweeOptin__c);
			SICo_PersonalInformation_VFC.setNewsLettersSubscription(communityUser1.accountID, true, 'partners');
			vAccount = [SELECT ID,PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];
			System.assert(vAccount.PartnerOptin__c == true, 'vAccount.PartnerOptin__c : ' + vAccount.PartnerOptin__c);
		 
			Test.stopTest();
		}
	}

}