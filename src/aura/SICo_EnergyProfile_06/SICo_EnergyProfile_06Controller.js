({ 
	updateInformations : function (component, event, pHelper) {
		var idAccount = component.get("v.accountId");

		// "Disable" the validate button
		pHelper.disableValidate(); 
		//put the values in the apex controller parameters 
		if (component.get("v.epw.numberOfOccupants") == undefined 
				|| component.get("v.epw.numberOfOccupants") == null 
				|| isNaN(component.get("v.epw.numberOfOccupants"))
				|| component.get("v.epw.numberOfOccupants") == 0) { 
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorNoOccupants"), "Error_06");  
		} else if (component.get("v.epw.numberOfOccupants") > 15) {
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorOccupants"), "Error_06");   
		} else if (component.get("v.epw.surface") < 40 && component.get("v.epw.numberOfOccupants") > 4) { 
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorOccupants"), "Error_06"); 
		} else {
			var action = component.get("c.setInformations_06");
			action.setParams({
				siteId : component.get("v.epw.Id"),
				numberOfOccupants : component.get("v.epw.numberOfOccupants")
			});

			action.setCallback(this, function (response) {				
					//redirect to the next page
					pHelper.moveToNextStep();
			}); 
			$A.enqueueAction(action);
		}
	},

	addition : function (component, event, helper) {
		//add 1 to the attribute numberOfOccupants
		var nb = component.get("v.epw.numberOfOccupants") || 0;
		var newNb = nb + 1;
		component.set("v.epw.numberOfOccupants", newNb);
		helper.enableValidate();
		helper.resetErrorContainer(component, "Error_06");
	},

	subtraction : function (component, event, helper) {
		//substract 1 to the attribute numberOfOccupants
		var nb = component.get("v.epw.numberOfOccupants") || 0;
		var newNb = nb - 1;

		if (newNb < 0) {
			newNb = 0;
		}
		component.set("v.epw.numberOfOccupants", newNb);
		helper.enableValidate();
		helper.resetErrorContainer(component, "Error_06");
	},

	validate : function (component, event, helper) {
		//to verify that the characters are numbers
		var input = component.get('v.epw.numberOfOccupants');
		if (input != null && isNaN(input)) {
			//if when the key is pressed, it is not a number, replace the character
			component.set('v.epw.numberOfOccupants', input.substring(0, input.length - 1));
		}
		helper.enableValidate();
		helper.resetErrorContainer(component, "Error_06");
	},
	
	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	}
 
})