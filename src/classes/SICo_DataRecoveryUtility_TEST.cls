@isTest
private class SICo_DataRecoveryUtility_TEST {

  @testSetup
  static void initializeData() {
    Case caseTest1 = new case();
    caseTest1.Subject__c = 'Demande client';
    caseTest1.Heading__c = 'Maintenance';
    caseTest1.SuppliedCompany = '';
    caseTest1.SuppliedName = '';
    caseTest1.ProvisioningType__c = 'Objet';
    insert caseTest1;

    User user1 = SICo_UtilityTestDataSet.createCommunityUser();
    insert user1;

    user1 = [SELECT AccountId, ContactId FROM User WHERE Id = :user1.Id];

    Account a = SICo_UtilityTestDataSet.createAccount('User', 'Test', 'PersonAccount');
    insert a;
    
    SICo_Site__c s = SICo_UtilityTestDataSet.createSite(a.Id);
    insert s;

    Opportunity o = new Opportunity();
    o.Name = 'Test Opp';
    o.ShippingMode__c = 'Colissimo';
    o.StageName = 'Closed Won';
    o.CloseDate = System.today();
    insert o;


    Zuora__Subscription__c sub = createZuoraSubscription('Test 1', a, s, o, 'GAZ');
    insert sub;
    Zuora__Subscription__c sub1 = createZuoraSubscription('Test 2', a, s, o, 'MAINT');
    insert sub1;

    Case c1 = SICo_UtilityTestDataSet.createCase('GAZ',a.Id, o.Id, s.Id);
    c1.Subscription__c = sub.Id;
    insert c1;

    Case c2 = SICo_UtilityTestDataSet.createCase('MAINT',a.Id, o.Id, s.Id);
    c2.Subscription__c = sub1.Id;
    insert c2;

    Task t  = createTask(a.Id, c1.Id, 'TaskProvisioning', 'Test1', 'SE0001');
    insert t;

    Task t_se4_c1  = createTask(a.Id, c1.Id, 'TaskProvisionning_SC', 'Test1', 'SE0004');
    t_se4_c1.WhoId = user1.ContactId;
    insert t_se4_c1;
    Task t_se3_c1  = createTask(a.Id, c1.Id, 'TaskProvisioning', 'Test1', 'SE0003');
    t_se3_c1.WhoId = user1.ContactId;
    insert t_se3_c1;

    Task t2  = createTask(a.Id, c2.Id, 'TaskProvisioning', 'Test1', 'SE0001');
    insert t2;

    Task t_se4_c2  = createTask(a.Id, c2.Id, 'TaskProvisionning_SC', 'Test1', 'SE0004');
    t_se4_c2.WhoId = user1.ContactId;
    insert t_se4_c2;
    Task t_th2_c1  = createTask(a.Id, c2.Id, 'TaskProvisionning_Logistique', 'Test1', 'TH0002');
    t_th2_c1.WhoId = user1.ContactId;
    insert t_th2_c1;

    ID rtEvent = SICo_Utility.m_RTbyDeveloperName.get('Event_EventProvisioning');
    Datetime event_time = System.now();
    Event e = new Event(
      TaskId__c= 'IN0001',
      Subject= 'Installation Test',
      WhatId= c1.Id,
      WhoId= a.PersonContactId,
      RecordTypeId= rtEvent,
      DurationInMinutes= 300,
      StartDateTime= event_time,
      ActivityDateTime= event_time,
      ProvisioningCallOut__c=true,
      SiteID__c = s.Id,
      InterventionNumber__c= 'TTT0001',
      FluxStatus__c = 'Traitement non démarré'
    );
    insert e;

  }

  static testMethod void DynamicChangeRecordType(){
    System.Debug([SELECT Id, RecordTypeId, TaskId__c, WhatId FROM Task]);
    Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Death Star','Agency');
    agency.AgencyNumber__c = 'ANUMBER';
    Insert agency;

    System.Debug([SELECT Id, RecordTypeId, TaskId__c, WhatId FROM Task]);

    Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Anakin', 'PersonAccount');
    Insert acc;

    System.Debug([SELECT Id, RecordTypeId, TaskId__c, WhatId FROM Task]);

    SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    site.CHAMAgencyEntretien__c = agency.Id;
    Insert site;

    System.Debug([SELECT Id, RecordTypeId, TaskId__c, WhatId FROM Task]);

    Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('Croiseur interstellaire',acc.Id,Site.Id);
    opportunity.MaintenanceParts__c = 'Avec pièces';
    opportunity.Maintenance_Mode_de_facturation__c = 'Annuel';
    Insert opportunity;
    
    System.Debug([SELECT Id, RecordTypeId, TaskId__c, WhatId FROM Task]);

    Case aCase = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, acc.Id, opportunity.Id , site.Id);
    Insert aCase;

    System.Debug([SELECT Id, RecordTypeId, TaskId__c, WhatId FROM Task]);

    List<Task> tasks = new List<Task>();
    tasks.add(SICo_UtilityTestDataSet.createProvisioningTask('GA0001', 'GA0001 TEST', aCase.Id, SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_CHAM'),false));
    tasks.add(SICo_UtilityTestDataSet.createProvisioningTask('MA0003', 'MA0003 TEST', aCase.Id, SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_Logistique'),false));
    Insert tasks;
    system.debug('tasks: '+tasks);


    SICo_DataRecoveryUtility_CLS.DynamicChangeRecordType(
      'Task',
      'TaskId__c', 
      new Map<String,String>{'GA0001' => 'TaskProvisionning_Gaz', 'MA0003' =>'TaskProvisionning_CHAM'}
    );

    List<Task> theTasks = new List<Task>();
    theTasks = [SELECT Id, RecordTypeId, TaskId__c, WhatId FROM Task];
    system.debug('theTasks: '+theTasks);
    for(Task atask : theTasks){
      if(atask.TaskId__c == 'GA0001'){
        System.assertEquals(atask.RecordTypeId, SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_Gaz'));       
      }else if(atask.TaskId__c == 'MA0003'){
        System.assertEquals(atask.RecordTypeId, SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_CHAM')); 
      }
    }
  }  

  static testMethod void DynamicChangeFields(){

    Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Death Star','Agency');
    agency.AgencyNumber__c = 'ANUMBER';
    Insert agency;

    Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Anakin', 'PersonAccount');
    Insert acc;

    SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    site.CHAMAgencyEntretien__c = agency.Id;
    Insert site;

    Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('Croiseur interstellaire',acc.Id,Site.Id);
    opportunity.MaintenanceParts__c = 'Avec pièces';
    opportunity.Maintenance_Mode_de_facturation__c = 'Annuel';
    Insert opportunity;

    Case aCase = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, acc.Id, opportunity.Id , site.Id);
    Insert aCase;

    Task aTask = SICo_UtilityTestDataSet.createProvisioningTask('GA0001', 'GA0001 TEST', aCase.Id, SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_Gaz'),false);
    aTask.ShippingAddressFloor__c = 'MES';
    Insert aTask;

    SICo_DataRecoveryUtility_CLS.DynamicChangeFields(
      'Task', 
      new Map<String,String>{'InterventionType__c' => 'ShippingAddressFloor__c', 'ErrorLog__c' =>'TaskId__c'}, 
      new Map<String,String>(),
      new Map<String,String>{'RecordTypeId' => 'TaskProvisionning_Gaz'},
      new Map<String,List<String>>{
          'TaskId__c' => new List<String>{'GA0001','GA0001_B','GRD0012B'}
      }
    );  

    Task theTask = [SELECT Id, InterventionType__c, ErrorLog__c, TaskId__c, ShippingAddressFloor__c FROM Task WHERE Id = :aTask.Id];
    System.assertEquals(theTask.InterventionType__c, 'MES');
    System.assertEquals(theTask.ErrorLog__c, 'GA0001');
  }
    
	@isTest
  static void testRecoverFields() {

    Map<String, String> m_fields = new Map<String, String>();
  	m_fields.put('Subject__c', 'SuppliedCompany');
  	m_fields.put('Heading__c', 'SuppliedName');

  	List<Case> l_case = [SELECT Subject__c, Heading__c FROM Case
  				 		 WHERE
  							Subject__c <> null OR Heading__c <> null];

  	test.startTest();
  	 SICo_DataRecoveryUtility_CLS.recoverFields(l_case, m_fields);
  	test.stopTest();


  	List<Case> l_updated_cases = [SELECT Subject__c, Heading__c, SuppliedCompany, SuppliedName FROM Case];
  	for (Case l : l_updated_cases) {
  		System.assertEquals(l.Subject__c, l.SuppliedCompany);
  		System.assertEquals(l.Heading__c, l.SuppliedName);
  	}
  }

	@isTest
  static void testUpdateTaskData() {

		test.startTest();
		  SICo_DataRecoveryUtility_CLS.updateTaskData(new List<Task>([SELECT Id, WhatId, InterventionType__c, OmegaRequestType__c, TaskId__c FROM Task WHERE Subject = 'Test1']));
		test.stopTest();

		List<Task> l_updated_tasks = [SELECT Id, Subscription__c, ErrorLog__c, Reason__c FROM Task];
		for (Task t : l_updated_tasks) {
			System.assertEquals(t.ErrorLog__c, t.Reason__c);
      System.assert(t.Subscription__c <> null);
		}
  }

  @isTest
  static void testUpdateSite() {

    List<SICo_Site__c> l_sites = new List<SICo_Site__c>([SELECT Id, SICO_Subscribed_offers__c FROM SICo_Site__c]);
    for (SICo_Site__c s : l_sites) {
        System.debug('>>>> testUpdateSite Before : SICo_Site__c ' + s);
    }

    test.startTest();
      SICo_DataRecoveryUtility_CLS.UpdateSite(new List<SICo_Site__c>([SELECT Id FROM SICo_Site__c]));
    test.stopTest();

    l_sites = new List<SICo_Site__c>([SELECT Id, SICO_Subscribed_offers__c FROM SICo_Site__c]);
    for (SICo_Site__c s : l_sites) {
      System.debug('>>>> testUpdateSite After : SICo_Site__c ' + s);
      System.assert(s.SICO_Subscribed_offers__c <> null);
    }
  }

  @isTest
  static void testMigrateEvent() {

    test.startTest();
    SICo_DataRecoveryUtility_CLS.migrateEventToTask(new List<Event>([SELECT Id, WhoId, WhatId, TaskId__c, ProvisioningCallOut__c, BatchFlag__c,
    CHAMInterventionStatus__c, StartDateTime, EndDateTime, InterventionNumber__c, SiteID__c, Subject, RecordTypeId FROM Event]));
    test.stopTest();

    List<Task> l_tasks = [SELECT Id FROM Task WHERE TaskId__c = 'IN0001' and Subject= 'Installation Test'];
    System.assert(!l_tasks.isEmpty());

  }

  @isTest
  static void testChangeCaseType() {
    test.startTest();
      SICo_DataRecoveryUtility_CLS.changeCaseType(new List<Case>([SELECT ID, Subject, ProvisioningType__c FROM Case where ProvisioningType__c = 'Objet']));
    test.stopTest();
  }

  @isTest
  static void testsetSiteOnCaseFromAccount() {
    test.startTest();
      SICo_DataRecoveryUtility_CLS.setSiteOnCaseFromAccount([SELECT Id FROM Account WHERE IsPersonAccount = true]);
    test.stopTest();
  }

  @isTest
  static void testSetParamsOnTask() {
    test.startTest();
      SICo_DataRecoveryUtility_CLS.setParamsOnTask([SELECT Id FROM Account WHERE IsPersonAccount = true]);
    test.stopTest();
  }

  public static Zuora__Subscription__c createZuoraSubscription(
    String name, 
    Account account, 
    SICo_Site__c site, 
    Opportunity opty,
    String offer) {

    zqu__Quote__c quote = new zqu__Quote__c(
      Name= name + '_Quote-Test',
      zqu__Account__c= account.Id,
      zqu__Opportunity__c= opty.Id,
      zqu__ValidUntil__c= System.today(),
      zqu__BillToContact__c= Account.PersonContactId,
      zqu__SoldToContact__c= Account.PersonContactId
    );
    insert quote;

    Zuora__Subscription__c zuoraSub = new Zuora__Subscription__c(
      Zuora__Status__c= 'Active',
      Zuora__OriginalId__c='original',
      Zuora__PreviousSubscriptionId__c='original',
      Name= name + '_Subscription-Test',
      Zuora__Account__c= account.Id,
      Quote__c= quote.Id,
      Zuora__QuoteNumber__c= quote.zqu__Number__c,
      SiteId__c= site.Id,
      OfferName__c= offer,
      Zuora__SubscriptionNumber__c= quote.zqu__Number__c,
      Zuora__ContractEffectiveDate__c= date.today()
    );
    return zuoraSub;
  }

  public static Task createTask (Id accountId, Id caseId, String rtName, String subject, String taskId) {
    ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + rtName);
    Task newTask = new Task(
        TaskId__c= taskId,
        Subject= subject,
        WhatId= caseId,
        RecordTypeId= rtTask
    );
    //newTask.TICGN__c=Boolean.valueOf('true');
    //newTask.requestedInterventionDate__c=date.today();
    newTask.interventionPlannedDate__c=date.today();
    return newTask;
  }    
}