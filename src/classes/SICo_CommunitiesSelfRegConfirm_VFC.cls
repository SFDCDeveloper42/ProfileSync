/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Controller for the Community Registration Confirm page (execute an immediate Password Reset)
Test Class:    SICo_CommunitiesSelfRegConfirm_VFC_TEST
History
17/08/2016      Mehdi Cherfaoui      Create version
------------------------------------------------------------*/

public with sharing class SICo_CommunitiesSelfRegConfirm_VFC {

    /*
    Constructor
     */
    public SICo_CommunitiesSelfRegConfirm_VFC() {
        String userName = System.currentPageReference().getParameters().get('userName');
        Site.forgotPassword(userName);
    }

    public PageReference redirectUser(){
	    PageReference ref = new PageReference('/apex/CommunitiesLogin');
	    return ref;
	}
}