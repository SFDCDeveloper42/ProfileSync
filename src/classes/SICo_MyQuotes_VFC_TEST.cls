/*------------------------------------------------------------
Author:        David LEROUX
Company:       Ikumbi Solutions
Description:   Controller for MyQuotes page.
Test Class:    SICo_MyQuotes_VFC_TEST
History
25/10/2016     David LEROUX     Create version
------------------------------------------------------------*/
@isTest
private class SICo_MyQuotes_VFC_TEST {

	private final static String COMMUNITY_USER_NAME = 'userTest1';
	private final static String QUOTE_NUMBER = '999999';

	@testSetup 
	static void initData() {
		Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
		insert account;
		SICo_Site__c oSICoSite = SICo_UtilityTestDataSet.createSite(account.Id);
		oSICoSite.IsActive__c = true;
		insert oSICoSite;

		//Insert new community user
        User vCommunityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
       	vCommunityUser1.LastName = COMMUNITY_USER_NAME;
        //vCommunityUser1.BypassTrigger__c = 'SICo_ProvisioningEvent_TRG';
        insert vCommunityUser1;
        vCommunityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :vCommunityUser1.Id LIMIT 1];

        //select account
        Account vAccount = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :vCommunityUser1.AccountId LIMIT 1];

		//Insert new site
        SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(vCommunityUser1.AccountId);
		site1.IsSelectedInCustomerSpace__c = true;
		site1.HousingType__c = Label.SICo_House;
        insert site1;        
 	
        // Insert quotes
		MaintenancyQuote__c vQuote       = new MaintenancyQuote__c();
		vQuote.MaintenancyQuoteNumber__c = QUOTE_NUMBER;
		vQuote.MaintenancyQuoteStatus__c = SICo_Quotes_Helper.STATUS_VALIDATION_TODO;
		vQuote.Site__c                   = site1.Id; 
		vQuote.Customer__c               = vAccount.Id; 
		insert vQuote;

        // Insert lines of quote
        List<MaintenancyLineOfQuote__c> vLinesOfQuotes = new List<MaintenancyLineOfQuote__c>();
        for (Integer i = 0; i < 5; i++) {
			MaintenancyLineOfQuote__c vLineQuote = new MaintenancyLineOfQuote__c();
			vLineQuote.LineNumber__c             = 1;
			vLineQuote.Label__c                  = 'My label';
			vLineQuote.Quantity__c               = 1515; 
			vLineQuote.UnitPriceWithoutTax__c    = 53.25;
			vLineQuote.TaxRate__c                = '20'; 
			vLineQuote.MaintenancyQuote__c       = vQuote.Id;
			vLinesOfQuotes.add(vLineQuote);
        }
        insert vLinesOfQuotes;
 	}
 
	@isTest 
	static void queryQuotesTest() { 
		User vCommunityUser1 = [SELECT Id, AccountId FROM User WHERE lastName = :COMMUNITY_USER_NAME LIMIT 1];		
		MaintenancyQuote__c vQuote = [SELECT Id FROM MaintenancyQuote__c WHERE MaintenancyQuoteNumber__c = :QUOTE_NUMBER LIMIT 1];
        //Run as Community User
        system.runAs(vCommunityUser1) {
        	Test.startTest();

        	Test.setCurrentPageReference(new PageReference('Page.MyQuotes'));
            //set url parameter
			System.currentPageReference().getParameters().put(SICo_Constants_CLS.URL_PARAM_ACCOUNTID, vCommunityUser1.AccountId);
			System.currentPageReference().getParameters().put(SICo_Constants_CLS.URL_PARAM_QUOTEID, vQuote.Id);
        	SICo_MyQuotes_VFC vController = new SICo_MyQuotes_VFC();

        	// Get Quote Number
        	List<SICo_Quotes_Helper.QuoteWrapLite> vQuotes = SICo_MyQuotes_VFC.queryQuotes(vCommunityUser1.AccountId);
        	System.assert(vQuotes != null && vQuotes.size() > 0, 'Quotes list retrieved should not be empty');


        	Test.stopTest();
        }
	}  


}