/**
* @author Salesforce Team
* @date 18/01/2017
* @LastModify 
*
* @description 
*/

// TODO Comment this class
public with sharing virtual class SICo_LeadService {

  private static final LeadStatus CONVERT_STATUS = [select MasterLabel from LeadStatus  where IsConverted = true limit 1][0];
  private static final String OFFER_SEPARATOR = '_';

  protected Lead aLead;
  final protected Id aLeadId;
   
  public SICo_LeadService(final ID LeadId){
    this.aLeadId = leadId;
  }

  public void convertLeadWithBusinessRules(final Id accountId) {
    final Database.LeadConvertResult leadConvertResult = this.convertLead(true,accountId);
    final ConvertLeadWithBusinessRulesHandler convertLeadHandler = new ConvertLeadWithBusinessRulesHandler(this.aLead, leadConvertResult);
    convertLeadHandler.convert();
  }

  private virtual class ConvertLeadWithBusinessRulesHandler {
    final protected Lead aLead;
    final protected Id opportunityId;
    final protected Id accountId;
    protected Boolean isNewClient;

    protected Account theAccount;
    protected SICo_Site__c theSite;
    protected Opportunity theOpportunity;

    public ConvertLeadWithBusinessRulesHandler(final Lead aLead, final Database.LeadConvertResult leadConvertResult){
      this.aLead = aLead;
      this.opportunityId = leadConvertResult.getOpportunityId();
      this.accountId = leadConvertResult.getAccountId();
      this.isNewClient = false;
    }

    public void Convert(){
      this.treatmentAccount();
      this.treatmentDecrementCreneau();
      this.treatmentSite();
      this.treatmentOpportunity();
      this.treatmentBeneficiary();
      this.treatmentMeter();
      this.treatmentAsset();
      this.treatmentZQuote();
    }

    protected void treatmentDecrementCreneau(){
      SICo_ChamJetonService_CLS chamJeton = new SICo_ChamJetonService_CLS();
      chamJeton.decrementCreneau(this.aLead);
    }

    protected void treatmentAccount() {
      try{
        this.theAccount= [SELECT Id, PersonContactId, PersonEmail, FirstName, LastName, PersonMailingCountry,
                          PersonMobilePhone,Phone,Salutation, CustomerNumber__c, SendWelcomeEmail__c, Type, 
                          SICO_HasSubscriptionAttachment__c, Tech_PaymentCB__c, Tech_PaymentSEPA__c
                          FROM Account WHERE Id = :this.accountid LIMIT 1];
      }catch(Exception ex){ 
      }

      if(this.theAccount.CustomerNumber__c == null) {
        this.isNewClient = true;
        this.theAccount.CustomerNumber__c = SICo_Utility.generate18CharId(this.aLead.Id);
        this.theAccount.SendWelcomeEmail__c = true;
        this.theAccount.PersonEmail= this.aLead.Email;
        this.theAccount.PersonMailingCountry= SICo_Constants_CLS.STR_DATA_FRANCE;
        this.theAccount.Type= SICo_Constants_CLS.STR_DATA_CUSTOMER;
        this.theAccount.FirstName= this.aLead.FirstName;
        this.theAccount.LastName= this.aLead.LastName;
        this.theAccount.Salutation= this.aLead.Salutation;
        this.theAccount.PersonMobilePhone= this.aLead.MobilePhone;
        this.theAccount.Phone= this.aLead.Phone;      
        this.theAccount.SICO_HasSubscriptionAttachment__c = true;
        this.theAccount.AccountSource = this.aLead.LeadSource;   
      }


      this.theAccount.Tech_PaymentCB__c = this.theAccount.Tech_PaymentCB__c == null ? this.aLead.PaymentMethodCreditCard__c : this.theAccount.Tech_PaymentCB__c;
      this.theAccount.Tech_PaymentSEPA__c = this.theAccount.Tech_PaymentSEPA__c == null ? this.aLead.PaymentMethodSepa__c : this.theAccount.Tech_PaymentSEPA__c;

      update this.theAccount;

      List<Attachment> attachementToUpdate = [Select Id, ParentId from attachment WHERE ParentId = :this.aLead.id];
      if(!attachementToUpdate.isEmpty()){
        for(Attachment anAttachment : attachementToUpdate){
          anAttachment.ParentId = accountId;
        }
        update attachementToUpdate;
      }
    }

    protected void treatmentSite() {
      try{
        this.theSite = [SELECT id,
                              InitialEstimatedCar__c,
                              IsActive__c,
                              EstimatedGasConsumption__c,
                              EligibleForCham__c,
                              EligibleForThermostat__c,
                              EligibleForGas__c,
                              BoilerBrand__c,
                              BoilerModel__c,
                              NoOfOccupants__c,
                              SurfaceInSqMeter__c,
                              PrincipalHeatingSystemType__c,
                              ResidenceType__c,
                              Usage__c,
                              PCE__c,
                              InseeCode__c,
                              EnergyDiagnostic__c,
                              RenewableHeating__c,
                              ConstructionDate__c,
                              CHAMAgencyEntretien__c,
                              CHAMAgencyInstallation__c,
                              CAR__c,
                              HousingType__c,
                              Thermostat_connecte__c
                        FROM SICo_Site__c 
                        where Subscriber__c = :this.theAccount.id
                        LIMIT 1]; 

      } catch(Exception ex){

        this.theSite = new SICo_Site__c(
          Flat__c= this.aLead.SiteAddressFlat__c,
          Building__c= this.aLead.SiteAddressBuilding__c,
          PostalCode__c= this.aLead.SiteAddressZipCode__c,
          AdditionToAddress__c= this.aLead.SiteAddressAdditionToAddress__c,
          Stairs__c= this.aLead.SiteAddressStairs__c,
          Floor__c= this.aLead.SiteAddressFloor__c,
          NumberStreet__c= this.aLead.SiteAddressNumberStreet__c,
          Country__c= this.aLead.SiteAddressCountry__c,
          City__c= this.aLead.SiteAddressCity__c,
          Subscriber__c= this.theAccount.Id,
          PDL__c= this.aLead.PDL__c,
          Thermostat_connecte__c= this.aLead.Thermostat_connecte__c

        );
        //TODO - REVOIR ACTIVATION
        this.theSite.IsActive__c= this.aLead.OfferName__c.contains(SICo_Constants_CLS.OFFER_GAZ) || 
                                  this.aLead.OfferName__c.contains(SICo_Constants_CLS.OFFER_MAINT) || 
                                  this.aLead.OfferName__c.contains(SICo_Constants_CLS.OFFER_INST);
      }
        
      Map<String,String> fields = new Map<String,String>{
        'InitialEstimatedCar__c' => 'EstimatedGasConsumption__c',
        'EligibleForCham__c' => 'EligibleForCham__c',
        'EligibleForThermostat__c' => 'EligibleForThermostat__c',
        'EligibleForGas__c' => 'EligibleForGas__c',
        'BoilerBrand__c' => 'BoilerBrand__c',
        'BoilerModel__c' => 'BoilerModel__c',
        'NoOfOccupants__c' => 'NumberOfInhabitants__c',
        'SurfaceInSqMeter__c' => 'SurfaceArea__c',
        'PrincipalHeatingSystemType__c' => 'HeatingType__c',
        'ResidenceType__c' => 'HomeType__c',
        'Usage__c' => 'Usage__c',
        'PCE__c' => 'PCE__c',
        'InseeCode__c' => 'InseeCode__c',
        'EnergyDiagnostic__c' => 'EnergyDiagnostic__c',
        'RenewableHeating__c' => 'RenewableHeating__c',
        'ConstructionDate__c' => 'ConstructionYear__c',
        'CHAMAgencyEntretien__c' => 'CHAMAgencyEntretien__c',
        'CHAMAgencyInstallation__c' => 'CHAMAgencyInstallation__c',
        'CAR__c' => 'CAR__c',
        'HousingType__c' => 'HousingType__c'
      };

      for(String field : fields.keyset()) {

        if(this.theSite.get(field) == null || String.isBlank(String.valueOf(this.theSite.get(field)))) {
          this.theSite.put(field,this.aLead.get(fields.get(field)));
        }
      }
      if(this.theSite.Thermostat_connecte__c != 'Non' && this.theSite.Thermostat_connecte__c != 'Oui_Tybox5100') {
        this.theSite.Thermostat_connecte__c= this.aLead.Thermostat_connecte__c;
      }

      upsert this.theSite;
    }

    protected void treatmentOpportunity() {
      this.theOpportunity = [SELECT Id, Name, AccountId, Site__c, StageName 
                             FROM Opportunity 
                             WHERE ID = :this.opportunityId];

      this.theOpportunity.AccountId= this.theAccount.Id;
      this.theOpportunity.Site__c= this.theSite.Id;
      this.theOpportunity.StageName= SICo_Constants_CLS.STR_DATA_WON;
      update this.theOpportunity;

    }

    protected void treatmentBeneficiary() {
      final List<SICo_Beneficiary__c> beneficiaries = new List<SICo_Beneficiary__c>();
      SICo_Beneficiary__c beneficiary;
      for (SICo_LeadBeneficiary__c leadBeneficiary : [SELECT FirstName__c,LastName__c,Salutation__c,Lead__c, Lead__r.ToConvert__c, Lead__r.IsConverted 
                                                      FROM SICo_LeadBeneficiary__c 
                                                      WHERE Lead__c = :this.aLead.Id]){
        beneficiary = new SICo_Beneficiary__c(
          Client__c= this.theAccount.Id,
          Site__c= this.theSite.Id,
          LastName__c = leadBeneficiary.FirstName__c,
          FirstName__c = leadBeneficiary.LastName__c,
          Salutation__c = leadBeneficiary.Salutation__c
        );
        beneficiaries.add(beneficiary);
      }
      insert beneficiaries;
    }

    protected void treatmentAsset() {
      final List<Asset> assets = new List<Asset>();
      Asset aAsset;
      for(SICo_Basket__c aAssetBasket : this.aLead.Panier__r)
        if(aAssetBasket.ObjectType__c != null) {
        aAsset = new Asset(
          Name= aAssetBasket.ChargeName__c,
          RecordTypeId= SICo_Utility.m_RTbyDeveloperName.get(SICo_Constants_CLS.STR_DATA_ASSET + aAssetBasket.ObjectType__c),
          AccountId= this.theAccount.Id,
          Site__c= this.theSite.Id,
          Option__c= aAssetBasket.Option__c,
          ObjectType__c= aAssetBasket.ObjectType__c,
          Status= SICo_Constants_CLS.STR_DATA_ACHETE,
          Quantity= SICo_Constants_CLS.INT_DATA_QTS,
          PurchaseDate= this.aLead.CPVValidationDate__c.date(),
          ContactId= this.theAccount.PersonContactId
        );
        assets.add(aAsset);
      }
      insert assets;
    }

    protected void treatmentMeter() {
      SICo_Meter__c pdlMeter;
      SICo_Meter__c pceMeter;
      List<SICo_Meter__c> meters = new List<SICo_Meter__c>();
      List<SICo_SiteMeter__c> siteMeters = new List<SICo_SiteMeter__c>();
      List<SICo_MeterReading__c> meterReadings = new List<SICo_MeterReading__c>();

      if(this.aLead.PCE__c !=null){
        pceMeter = createMeter(SICo_Constants_CLS.STR_METER_PCE);
        meters.add(pceMeter);
      }
      if(this.aLead.PDL__c !=null){
        pdlMeter = createMeter(SICo_Constants_CLS.STR_METER_PDL,this.aLead.PDL__c);
        meters.add(pdlMeter);
      }
      if(!meters.isEmpty()){
        upsert meters Id__c;
      }
      if(this.aLead.PCE__c !=null){
        siteMeters.add(createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE, pceMeter.Id));
        meterReadings.add(createMeterReading(SICo_Constants_CLS.STR_METERREAD_PCE,pceMeter.Id, this.aLead.GasMeter__c));
      }
      if(this.aLead.PDL__c !=null){
        siteMeters.add(createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PDL, pdlMeter.Id));
        meterReadings.add(createMeterReading(SICo_Constants_CLS.STR_METERREAD_PDL,pdlMeter.Id, 0 /*variable not defined by the business'*/));
      }
      
      insert siteMeters;
      insert meterReadings;
    }

    protected void treatmentZQuote() {
      /** Build the rate plan per type of contract structure
       *** The algorithm :
       * foreach product rate plan related to our basket
       *  if the rate plan as a type
       *   explode the offer name into set
       *   store the rate plan per exploded offer name
       *   store the union of the exploded offer name (used in the build data structure)
       *  endif
       * endforeach
       *
       *** Just information here
       * A type of contract correspond to a set of offer name
       * ex : 
       * - {'SE'} => '<zuora rate plan id 1>'
       * - {'SE','TH'} => '<zuora rate plan id 2>'
       * - {'GAZ'} => '<zuora rate plan id 3>'
       */ 
      final Map<Set<String>,Set<Zqu__ProductRatePlan__c>> productPerContractType = new Map<Set<String>,Set<Zqu__ProductRatePlan__c>>();
      final Set<String> ratePlanIds = new Set<String>();
      final Set<String> allOfferType = new Set<String>();
      for(SICo_Basket__c aAssetBasket : this.aLead.Panier__r) {
        ratePlanIds.add(aAssetBasket.RatePlanId__c);
      }
      for (Zqu__ProductRatePlan__c aProductRatePlan: [SELECT Id, Zqu__Product__c, RatePlanType__c 
                                                      FROM Zqu__ProductRatePlan__c 
                                                      WHERE Zqu__ZuoraId__c IN :ratePlanIds]) {
        if(String.isNotBlank(aProductRatePlan.RatePlanType__c)){
          Set<String> offers = new Set<String>(aProductRatePlan.RatePlanType__c.split(OFFER_SEPARATOR));
          if(!productPerContractType.containsKey(offers)){
            productPerContractType.put(offers, new Set<Zqu__ProductRatePlan__c>());
          }
          productPerContractType.get(offers).add(aProductRatePlan);
          allOfferType.addAll(offers);
        }
      }
      System.debug('############### alloffertype ' + allOfferType.size());
      /** Build data structure enabling the Quote creation
       * Product are linked to offer name
       * Offer name are linked to a Quote type
       * So we have to group product into same type Quote
       * A type of Quote (or contract type) is defined by the couple Zquote_Creation__mdt.ZquoteToCreate__c & Zquote_Template__mdt
       * 
       * We are going to build a structure to hold type of quote per offers
       * As many ofers can be related to one template and we are iterating over the configuration without knowing it
       * we need to build a map for the offers per quote type {offerNamePerQuoteType} (1)
       * and then reverse it by aggregating all the offers per quote type and put it in {quoteTypePerOfferName} (2)
       *** The algorithm :
       * foreach Quote To Create related to our offers
       *  build the quote template
       *  explode the offer name into set
       *  store the quote template per unique id 
       *  store the offers per quote unique id
       * endforeach
       * foreach offers per quote unique id
       *  get the offers
       *  foreach contract type per offer
       *   if the offer type type equals the iterating offer type
       *    add the offers to the set to the quote type per offers
       *    nullify offers (it's our condition)
       *    break loop
       *   endif
       *  endforeach
       *  if offers is not null
       *   store the current quote template per offers
       *  endif
       * endforeach
       *
       * /!\ we have to store everything with a unique id because, even if the quote templates have the same data across loop
       *     when they are created they have not the same reference
       */ 
      final Map<String,Set<String>> offerNamePerQuoteType = new Map<String,Set<String>>();
      final Map<String,QuoteTemplate> quoteTemplates = new Map<String,QuoteTemplate>();
      // (1)
      for(Zquote_Creation__mdt aZquoteMeta : [SELECT DeveloperName, ZquoteToCreate__c, ZquoteTemplate__r.DeveloperName, ZquoteTemplate__r.ApplyCreditBalance__c, 
                                              ZquoteTemplate__r.AutoRenew__c, ZquoteTemplate__r.ContractEffectiveDate__c,
                                              ZquoteTemplate__r.CustomerAcceptanceDate__c, ZquoteTemplate__r.Description__c, ZquoteTemplate__r.GenerateInvoice__c, 
                                              ZquoteTemplate__r.InitialTerm__c, ZquoteTemplate__r.PaymentTerm__c, ZquoteTemplate__r.ProcessPayment__c, 
                                              ZquoteTemplate__r.QuoteBusinessType__c, ZquoteTemplate__r.RenewalSetting__c, 
                                              ZquoteTemplate__r.RenewalTerm__c, ZquoteTemplate__r.ServiceActivationDate__c, ZquoteTemplate__r.SubscripitonTermType__c 
                                              FROM Zquote_Creation__mdt 
                                              WHERE DeveloperName IN :allOfferType]) {
        final Set<String> offers = new Set<String>(aZquoteMeta.DeveloperName.split(OFFER_SEPARATOR));
        final QuoteTemplate aQuoteTemplate = new QuoteTemplate(aZquoteMeta.ZquoteToCreate__c, aZquoteMeta.ZquoteTemplate__r);
        if(!quoteTemplates.containsKey(aQuoteTemplate.getUniqueId())) {
          quoteTemplates.put(aQuoteTemplate.getUniqueId(), aQuoteTemplate);
        }
        if(!offerNamePerQuoteType.containsKey(aQuoteTemplate.getUniqueId())) {
          offerNamePerQuoteType.put(aQuoteTemplate.getUniqueId(), new Set<String>());
        }
        offerNamePerQuoteType.get(aQuoteTemplate.getUniqueId()).addAll(offers);
      }
      System.debug('############### offerNamePerQuoteType ' + offerNamePerQuoteType.size());

      final Map<Set<String>,String> quoteTypePerOfferName = new Map<Set<String>,String>();
      // (2)
      for(String aQuoteTemplateId : offerNamePerQuoteType.keySet()) {
        Set<String> offers = offerNamePerQuoteType.get(aQuoteTemplateId);
        for(Set<String> tempSet : quoteTypePerOfferName.keySet()) {
          if(quoteTypePerOfferName.get(tempSet) == aQuoteTemplateId) {
            tempSet.addAll(offers);
            offers = null;
            break;
          }
        }
        if(offers != null) {
          quoteTypePerOfferName.put(offers,aQuoteTemplateId);
        }
      }

      System.debug('############### quoteTypePerOfferName ' + quoteTypePerOfferName.size());
      
      /** Map datas
       * Store the rate plan per type of quote
       * map our rate plan per offer with the quote type per offers to build the rate plan per quote type
       * the map is done based on set inclusion in the keys of the map
       *** The algorithm :
       * foreach product per contract type
       *  foreach quote type per contract type
       *   if current quote type contains upper current quote type
       *    store the product rate plan per quote type
       *   endif
       *  endforeach
       * endforeach
       */ 
      final Map<QuoteTemplate,Set<Zqu__ProductRatePlan__c>> ratePlanPerContractType = new Map<QuoteTemplate,Set<Zqu__ProductRatePlan__c>>();
      for(Set<String> aTypeSet : productPerContractType.keySet()) {
        for(Set<String> tempSet : quoteTypePerOfferName.keySet()) {
          if(tempSet.containsAll(aTypeSet)) {
            final QuoteTemplate aQuoteTemplate = quoteTemplates.get(quoteTypePerOfferName.get(tempSet));
            if(!ratePlanPerContractType.containsKey(aQuoteTemplate)) {
              ratePlanPerContractType.put(aQuoteTemplate, new Set<Zqu__ProductRatePlan__c>());
            }
            ratePlanPerContractType.get(aQuoteTemplate).addAll(productPerContractType.get(aTypeSet));
            break;
          }
        }
      }
      System.debug('############### ratePlanPerContractType ' + ratePlanPerContractType.size());

      //TEST CALL NEW CLASS FOR OLD CLIENT
      //system.debug('### isNotNewClient: '+this.isNotNewClient);
      SICo_LeadServiceQuote_CLS serviceQuote = new SICo_LeadServiceQuote_CLS(ratePlanPerContractType, offerNamePerQuoteType, this.accountId);
      Map<QuoteTemplate, String> mapAmend = serviceQuote.getMapAmend();

      final Map<String,List<zqu.Quote>> zquotes = new Map<String,List<zqu.Quote>>();
      // final Map<String, List<zqu.Quote>> m_zquotes = new Map<String, List<zqu.Quote>>();

      for(QuoteTemplate aQuoteTemplate : ratePlanPerContractType.keySet()){
        // Create quote
        zqu.Quote aZquote;

        aZquote = this.buildQuote(aQuoteTemplate, offerNamePerQuoteType.get(aQuoteTemplate.getUniqueId()), mapAmend.get(aQuoteTemplate));
        
        Set<zqu.Product> quoteZProducts = new Set<zqu.Product>();
        for(Zqu__ProductRatePlan__c aRatePlan : ratePlanPerContractType.get(aQuoteTemplate)) {
          final zqu.Product zp = zqu.Product.loadProduct(aRatePlan.zqu__Product__c);

          // Remove each rate plan from the product which are not related to our rateplan
          // As we cannot set a new list for zp.productRatePlans we have to remove them on the fly
          // BAD practice doing so with an iterator so we are doing it with a classic for loop
          // But using a backwards iteration
          for(Integer i = zp.productRatePlans.size() - 1 ; i >= 0 ; --i) {
            if(zp.productRatePlans.get(i).sfdcProductRatePlan.id != aRatePlan.id){
              zp.productRatePlans.remove(i);
            }
          }
          quoteZProducts.add(zp);
        }
        aZquote.save();
        zqu__Quote__c zquQuote = aZquote.getSObject();
        zquQuote.Name = SICo_utility.generate18CharId(zquQuote.Id);
        aZquote.addQuoteProducts(new List<zqu.Product>(quoteZProducts));
        aZquote.save();

        final String zquoteKey = aZquote.getSObject().zqu__ExistSubscriptionId__c == null ? '' : aZquote.getSObject().zqu__ExistSubscriptionId__c;

        if (!zquotes.containsKey(zquoteKey)) {
          zquotes.put(zquoteKey, new List<zqu.Quote>());  
        }
        zquotes.get(zquoteKey).add(aZquote);
     }


      System.debug('#### ZQUOTE : ' + zQuotes.keyset());

      if (!zquotes.isEmpty()) {
        for(List<zqu.Quote> zs : zquotes.values()) {
          SICO_QueueChaining_Utility.queueChainingOrchestrator.addQueue(new SendQuoteToZuora(zs));
        }
        SICO_QueueChaining_Utility.queueChainingOrchestrator.executeQueueChain();
      }
    }

    protected zqu.Quote buildQuote(final QuoteTemplate aQuoteTemplate, final Set<String> offerNames, String subscriptionID){
      final zqu.Quote aZquote = zqu.Quote.getNewInstance();
      final zqu__Quote__c zquQuote = aZquote.getSObject();
      final Zquote_Template__mdt quoteTpl = aQuoteTemplate.quoteTemplate;

      zquQuote.zqu__Opportunity__c= this.theOpportunity.Id;
      zquQuote.zqu__Account__c= this.theAccount.Id;
      zquQuote.zqu__BillToContact__c= this.theAccount.PersonContactId;
      zquQuote.zqu__SoldToContact__c= this.theAccount.PersonContactId;
      zquQuote.SiteId__c= this.theSite.Id;
      zquQuote.OfferName__c= String.join(new List<String>(offerNames), OFFER_SEPARATOR);
      zquQuote.PCE__c= this.aLead.PCE__c;
      zquQuote.PDL__c= this.aLead.PDL__c;
      zquQuote.MarketingOfferName__c = this.aLead.MarketingOfferName__c;
      zquQuote.Renonciation_Retractation_Gaz__c= this.aLead.Renonciation_Retractation_Gaz__c;
      zquQuote.Renonciation_Retractation_Maintenance__c= this.aLead.Renonciation_Retractation_Maintenance__c;
      zquQuote.LeadSource__c= this.aLead.LeadSource;
      zquQuote.zqu__StartDate__c= Date.today();
      zquQuote.zqu__ValidUntil__c= Date.today();
      zquQuote.zqu__Currency__c= SICo_Constants_CLS.STR_DATA_EUR;
      zquQuote.zqu__QuoteBusinessType__c= quoteTpl.QuoteBusinessType__c;
      zquQuote.zqu__Subscription_Term_Type__c= quoteTpl.SubscripitonTermType__c;
      zquQuote.zqu__BillingMethod__c= SICo_Constants_CLS.STR_DATA_EMAIL;
      zquQuote.zqu__PaymentTerm__c= quoteTpl.PaymentTerm__c;
      zquQuote.zqu__RenewalSetting__c= quoteTpl.RenewalSetting__c;
      zquQuote.zqu__ApplyCreditBalance__c= quoteTpl.ApplyCreditBalance__c;
      zquQuote.zqu__InvoiceProcessingOption__c = SICo_Constants_CLS.STR_DATA_ACCOUNT;
      zquQuote.zqu__InitialTerm__c = quoteTpl.InitialTerm__c;
      zquQuote.zqu__RenewalTerm__c = quoteTpl.RenewalTerm__c;
      zquQuote.zqu__AutoRenew__c = quoteTpl.AutoRenew__c;
      zquQuote.zqu__GenerateInvoice__c = quoteTpl.GenerateInvoice__c;
      zquQuote.zqu__ProcessPayment__c = quoteTpl.ProcessPayment__c;

      if (!String.isEmpty(subscriptionID)) {
        zquQuote.zqu__ExistSubscriptionId__c =  subscriptionID;
        zquQuote.zqu__SubscriptionType__c = 'Amend Subscription';
      }

      try{
        zquQuote.zqu__TermStartDate__c = String.isNotBlank(quoteTpl.ContractEffectiveDate__c)
          ? (Date)this.aLead.get(quoteTpl.ContractEffectiveDate__c) 
          : null;
      } catch (Exception ex) {}
      zquQuote.zqu__Customer_Acceptance_Date__c = String.isNotBlank(quoteTpl.CustomerAcceptanceDate__c) && quoteTpl.CustomerAcceptanceDate__c.isNumeric()
        ? Date.today().addDays(Integer.valueOf(quoteTpl.CustomerAcceptanceDate__c)) 
        : null;
      zquQuote.zqu__Service_Activation_Date__c = String.isNotBlank(quoteTpl.ServiceActivationDate__c)  && quoteTpl.ServiceActivationDate__c.isNumeric()
        ? Date.today().addDays(Integer.valueOf(quoteTpl.ServiceActivationDate__c)) 
        : null;
      zquQuote.zqu__PaymentMethod__c = '';
      zquQuote.InvoicingType__c = this.aLead.GasBillingMode__c;
      return aZquote;
    }

    //----------------------------------
    protected SICo_Meter__c createMeter(String rtName, String IdData){
      final ID rtMeter = SICo_Utility.m_RTbyDeveloperName.get(rtName);
      return new SICo_Meter__c(
        RecordTypeId= rtMeter,
        Id__c= IdData,
        MeterSerialNumber__c=rtName.substringAfterLast('_')+IdData
      );
    }
  
    protected SICo_Meter__c createMeter(String rtName){
      final ID rtMeter = SICo_Utility.m_RTbyDeveloperName.get(rtName);
      return new SICo_Meter__c(
        RecordTypeId = rtMeter,
        Id__c = this.aLead.PCE__c,
        CAR__c = this.aLead.CAR__c,
        ReadingFrequency__c = this.aLead.ReadingFrequency__c,
        MeterSerialNumber__c = this.aLead.MeterSerialNumber__c,
        DTR__c = this.aLead.DTR__c,
        MeterNumber__c=this.aLead.MeterNumber__c
      );
    }

    protected SICo_SiteMeter__c createSiteMeter(String rtName, Id meterId){
      final ID rtSiteMeter = SICo_Utility.m_RTbyDeveloperName.get(rtName);
      final SICo_SiteMeter__c siteMeter = new SICo_SiteMeter__c(
        RecordTypeId= rtSiteMeter,
        Site__c= this.theSite.id,
        Meter__c= meterId,
        ConsumptionGrouping__c= this.aLead.ConsumptionGrouping__c
      );
      siteMeter.Zone__c=this.aLead.GasZone__c;
      siteMeter.GasType__c  = this.aLead.GasType__c;
      siteMeter.SuscribedGas__c=this.aLead.SuscribedGas__c;
      return siteMeter;
    }

    protected SICo_MeterReading__c createMeterReading(String rtName, Id meterId, Decimal gasMeter){
      final ID rtMeterR = SICo_Utility.m_RTbyDeveloperName.get(rtName);
      SICo_MeterReading__c meterReading = new SICo_MeterReading__c(
        RecordTypeId= rtMeterR,
        Meter__c= meterId,
        Type__c= SICo_Constants_CLS.STR_DATA_READING,
        ReadingDate__c= this.aLead.MeterReadingDate__c,
        MeterReading__c= gasMeter
      );
      return meterReading;
    }
  }


  public class SendQuoteToZuora extends SICO_QueueChaining_Utility implements Database.AllowsCallouts {
    public List<zqu.Quote> quotes;

    public SendQuoteToZuora(final List<zqu.Quote> quotes){
      this.quotes = quotes;
    }

    protected override void doJob(){
      if(this.quotes.isEmpty()){
        return;
      }

      String zAccountId = SICo_Constants_CLS.STR_DATA_NEW;
      try {
        Zuora__CustomerAccount__c theBillingAccount = [Select Zuora__Zuora_Id__c from Zuora__CustomerAccount__c where Zuora__Account__c = :this.quotes[0].getSObject().zqu__Account__c LIMIT 1][0];
        zAccountId = theBillingAccount.Zuora__Zuora_Id__c;
      } catch(Exception ex) {}

      final zqu.zQuoteUtil.ZBillingQuoteCollection quoteCollection = new zqu.zQuoteUtil.ZBillingQuoteCollection();
      quoteCollection.sfdcAccountId = this.quotes[0].getSObject().zqu__Account__c;
      quoteCollection.zAccountId = zAccountId;
      quoteCollection.quoteRequests = new List<zqu.zQuoteUtil.ZBillingQuoteRequest>();
      zqu.GlobalCustomFieldDefinition.QUOTE_FIELDS = SICo_Constants_CLS.SET_STR_ZCUSTOMFIELD;
      

      for(zqu.Quote aQuote : this.quotes){
        final zqu.zQuoteUtil.ZBillingQuoteRequest req = new zqu.zQuoteUtil.ZBillingQuoteRequest();
        req.sfdcQuoteId = SICo_utility.generate18CharId(aQuote.getSObject().id);
        if(aQuote.getSObject().zqu__GenerateInvoice__c == true){
          req.generateInvoice = aQuote.getSObject().zqu__GenerateInvoice__c;
          req.invoiceProcessingOption = 'Account';
          req.effectiveDate = Date.today();
        }
        quoteCollection.quoteRequests.add(req);
      }

      if(!test.isrunningtest()){
        system.debug('### ICI');
        List<zqu.zQuoteUtil.zBillingResult> results = zqu.zQuoteUtil.sendToZBilling(
          new List<zqu.zQuoteUtil.ZBillingQuoteCollection>{quoteCollection}
        );
        system.debug('### results: '+results);
      }
    }
  }

  public Database.LeadConvertResult convertLead(final Boolean allOrNothing, final Id accountid) {
    final SICo_LeadSelector aLeadSelector = new SICo_LeadSelector(this.aLeadId);
    this.aLead = aLeadSelector.getLeadForConversion();
    Database.LeadConvert lc = new Database.LeadConvert();
    lc.setLeadId(this.aLead.Id);
    if(String.isNotBlank(accountid)) {
      lc.setAccountId(accountid);
    }
    lc.setOpportunityName(this.aLead.Name);
    lc.setConvertedStatus(SICo_LeadService.CONVERT_STATUS.MasterLabel);
    return Database.convertLead(lc,allOrNothing);
  }

  public class SICo_LeadDomain {
    final protected Id leadId;
    final protected Lead aLead;

    public SICo_LeadDomain(final Id anId) {
      this.leadId = anId;
      try{
        if(this.isLead()){
          this.aLead = [SELECT ConvertedAccountId, email FROM Lead where id = :this.leadId limit 1][0];
        }
      } catch (Exception ex){
        system.debug(ex);
      }
    }

    public boolean isLead() {
      final String inputPrefix = String.valueOf(this.leadId).substring(0,3);
      final String leadPrefix = Schema.SObjectType.Lead.getKeyPrefix();
      return inputPrefix == leadPrefix;
    }

    public Id isConverted() {
      return aLead == null ? null : aLead.ConvertedAccountId;
    }

    public Id isRelatedToAnExistingAccount() {
      try {
        if(this.isLead()){
          final Account anAccount = [SELECT id FROM Account WHERE EmailAddress__c = :this.aLead.email AND RecordType.DeveloperName = 'PersonAccount'];
          if(anAccount != null) {
            return anAccount.id;
          }
        }
      } catch (Exception ex) {
        // Add logic if necessary
        //throw ex;
      }
      return null;
    }
  }


  public class SICo_LeadSelector {
    final protected Id leadId;

    public SICo_LeadSelector(final Id leadId) {
      this.leadId = leadId;
    }

    public Lead getLeadForConversion() {
      final Lead aLead;

      try{
        aLead = [SELECT id, Name, SiteAddressFlat__c, SiteAddressBuilding__c, SiteAddressZipCode__c, SiteAddressAdditionToAddress__c, 
                SiteAddressStairs__c, SiteAddressFloor__c, SiteAddressNumberStreet__c, SiteAddressCountry__c, SiteAddressCity__c, 
                EstimatedGasConsumption__c, EligibleForCham__c, EligibleForThermostat__c, EligibleForGas__c, BoilerBrand__c, BoilerModel__c,
                NumberOfInhabitants__c, SurfaceArea__c, HeatingType__c, HomeType__c, Usage__c, PCE__c, PDL__c, InseeCode__c, EnergyDiagnostic__c, 
                RenewableHeating__c, ConstructionYear__c, ConsumptionGrouping__c, MeterReadingDate__c, GasMeter__c, GasBillingMode__c,
                Email, FirstName, LastName, Phone, MobilePhone, Salutation, CPVValidationDate__c, CAR__c,HousingType__c, Thermostat_connecte__c,
                OfferName__c, Renonciation_Retractation_Gaz__c, Renonciation_Retractation_Maintenance__c, PaymentMethodCreditCard__c, PaymentMethodSepa__c,
                ReadingFrequency__c,MeterSerialNumber__c, MeterNumber__c, DTR__c, GasZone__c, GasType__c, SuscribedGas__c, MarketingOfferName__c, LeadSource,
                CHAMInstallationAppointmentDate__c, CHAMEntretienAppointmentDate__c, CHAMAgencyInstallation__c, CHAMAgencyEntretien__c, CHAMInstallationTimeSlot__c,
                CHAMEntretienTimeSlot__c, (SELECT Id, Lead__c,RatePlanId__c,ObjectType__c,ChargeName__c,Option__c FROM Panier__r)
                FROM lead WHERE id = :this.LeadId];
      } catch(Exception ex) {
        System.debug(ex);
      }

      return aLead;
    }
  }

  public class QuoteTemplate {
    String uniqueIdentifier;
    Zquote_Template__mdt quoteTemplate;
    public QuoteTemplate(final String uniqueIdentifier, final Zquote_Template__mdt quoteTemplate) {
      this.uniqueIdentifier = uniqueIdentifier;
      this.quoteTemplate = quoteTemplate;
    }

    public String getUniqueId() {
      return this.quoteTemplate.id + uniqueIdentifier;
    }
  }
}