/*------------------------------------------------------------
Author:        Mehdi CHERFAOUI
Company:       Salesforce
Description:   Test of the utility class for managing the Platform Cache (SICo_PlatformCacheManager_CLS)
History
Nov. 2016      Mehdi CHERFAOUI     Create version
------------------------------------------------------------*/
@isTest
private class SICo_PlatformCacheManager_TEST {
    
    @isTest static void test_OrgCache() {
    	Test.startTest();
    	//Add Value to Org. Cache
        SICo_PlatformCacheManager_CLS.putObject('testKey','testValue', null);
        //Retrieve it
        String retrievedValue = (String)SICo_PlatformCacheManager_CLS.getObject('testKey');
        Test.stopTest();
        //Check that the value it correctly retrieved
        system.assertEquals('testValue', retrievedValue);
    }
    
    @isTest static void test_SessionCache() {
    	Test.startTest();
    	//Add Value to Session Cache
        SICo_PlatformCacheManager_CLS.putSessionObject('testKey','testValue', null);
        //Retrieve it
        String retrievedValue = (String)SICo_PlatformCacheManager_CLS.getSessionObject('testKey');
        Test.stopTest();
        //Check that the value it correctly retrieved
        system.assertEquals('testValue', retrievedValue);
    }
    
}