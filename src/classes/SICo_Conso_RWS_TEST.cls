@isTest
private class SICo_Conso_RWS_TEST {
	@isTest
    static void testMethod1() {
    	//do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Conso';  
        req.addParameter('conso', '1000');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        SICo_Conso_RWS.getCategorie();

    	String json = RestContext.response.responseBody.toString();
        system.Debug(json);
    	Conso aConso = SICo_Conso_RWS_TEST.parse(json);
    	System.assert(aConso.categorie_conso == 'C1');
    	System.assert(aConso.zone_distribution == 'T1');
    }

    private class Conso {
		public String categorie_conso;
		public String zone_distribution;
	}

	private static Conso parse(String jsonStr) {
		return (Conso) JSON.deserialize(jsonStr, Conso.class);
	}
}