({
	doInit: function(cmp) {
			var action = cmp.get("c.queryBeneficiaryList");
			action.setParams({
					accountId : cmp.get("v.accountId"),
					siteId : cmp.get("v.siteId")
			});
			action.setCallback(this, function(response){
					var state = response.getState();
					if (state === "SUCCESS") {
							cmp.set("v.beneficiaryList", response.getReturnValue());
					}
			});
			$A.enqueueAction(action);
	}
})