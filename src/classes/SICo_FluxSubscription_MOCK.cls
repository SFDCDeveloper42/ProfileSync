/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Mock responce for SICo_FluxSubscription
History
13/07/2016      Boris Castellani     Create version
------------------------------------------------------------*/
@isTest
global class SICo_FluxSubscription_MOCK implements HttpCalloutMock {

	// Implement this interface method
	global HTTPResponse respond(HTTPRequest request) {
		// Create a fake response
		HttpResponse response = new HttpResponse();
		response.setHeader('Content-Type', 'application/json');
		/*response.setBody('{'+
			'\"Response\": [{'+
			'       \"ActivityId\": \"PER00001\",'+
			'       \"errorCode\": \"SITE-0001\",'+
			'       \"errorDescription\": \"0013E0000039aVI\"'+
			'   },'+
			'		{'+
			'       \"ActivityId\": \"PER00001\",'+
			'       \"errorCode\": \"SITE-0001\",'+
			'       \"errorDescription\": \"0013E0000039aVI\"'+
			'   }]'+
			'}');*/
		response.setBody('{"Responses":[' +
		 	'{"Response":{"statusCode":"SITE-0001","errorDescription":"PER00001","ActivityId":"0013E0000039aVI"}}' +
		 	',{"Response":{"statusCode":"SITE-0001","errorDescription":"PER00001","ActivityId":"0013E0000039aVI"}}]' + 
		 	'}'
		);	
		response.setStatusCode(200);
		return response;
		}

}