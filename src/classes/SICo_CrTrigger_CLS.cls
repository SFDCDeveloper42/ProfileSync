public with sharing class SICo_CrTrigger_CLS {

/* -------- BEFORE METHODE -------- */

	public static void handleBeforeInsert(List<SICo_CR__c> crs){
		Map<Id,Account> accs = getAccount(crs);
		if(!accs.isEmpty()){
			for(SICo_CR__c cr : crs){
				if(accs.containsKey(cr.Account__c)){
					cr.Email__c =  accs.get(cr.Account__c).PersonEmail;
				}
			}
		}
	}

	public static void handleBeforeUpdate(List<SICo_CR__c> crs){
		Map<Id,Account> accs = getAccount(crs);
		if(!accs.isEmpty()){
			for(SICo_CR__c cr : crs){
				if(accs.containsKey(cr.Account__c) && accs.get(cr.Account__c).PersonEmail != cr.Email__c){
					cr.Email__c =  accs.get(cr.Account__c).PersonEmail;
				}
			}
		}
	}

/* -------- AFTER METHODE -------- */

	public static void HandleAfterInsert(List<SICo_CR__c> crs){
		Map<Id,List<String>> sites = new Map<Id,List<String>>();
		for(SICo_CR__c cr : crs){
			if(cr.CRType__c == 'Full' && !String.isBlank(cr.BoilerBrand__c) && !String.isBlank(cr.BoilerModel__c)){
				if(!String.isEmpty(cr.Site__c)){
					if(!sites.containsKey(cr.Site__c)){
						sites.put(cr.Site__c, new List<String>());
					}
					sites.get(cr.Site__c).add(cr.BoilerBrand__c);
					sites.get(cr.Site__c).add(cr.BoilerModel__c);	
				}
			}
		}
		if(!sites.isEmpty()){
			updateSite(sites);
		}
	}

	public static void HandleAfterUpdate(List<SICo_CR__c> crs){
		HandleAfterInsert(crs);
	}

/* -------- PRIVATE METHODE -------- */

	private static Map<Id,Account> getAccount(List<SICo_CR__c> crs){
		Set<Id> accId = new Set<Id>();
		for(SICo_CR__c cr : crs){
			if(cr.Account__c != null){
				accId.add(cr.Account__c);
			}
		}
		return new Map<Id,Account>([SELECT Id, PersonEmail FROM Account WHERE Id in :accId]);	
	}

	private static void updateSite(Map<Id,List<String>> sites){
		List<SICo_Site__c> updateSite = new List<SICo_Site__c>();
		for (Id siteId : sites.keySet()) {
			SICo_Site__c site = new SICo_Site__c(Id= siteId);
			site.BoilerBrand__c = sites.get(siteId)[0];
			site.BoilerModel__c = sites.get(siteId)[1];
			updateSite.add(site);
		}
		if(!updateSite.isEmpty()){
			Update updateSite;
		}
	}
    
}