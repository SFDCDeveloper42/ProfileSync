/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi-solutions
Description:   Class for the update of the user's email. Access is without sharing, as Community users are otherwise unable to update their User.
Test Class:    SICo_ChangeEmail_VFC_TEST
History
20/09/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
public without sharing class SICo_ChangeEmail_VFC {
    public String oldEmail {get; set;}
    public String newEmail {get; set;}
    public String verifyNewEmail {get; set;}

    public String errorMessage {get;set;}
    public String error{get; set;}

    public Id accountId{get;set;}

    public SICo_ChangeEmail_VFC() {
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        try{
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }catch (exception e){
            System.debug('Unable to return accountId');
        }
    }

    //get the actual Email
    public String getEmail(){
        String email = Label.SICo_UserUnkown;
        List<User> currentUser = [SELECT Email FROM User WHERE AccountId=:accountId LIMIT 1];
        if (currentUser.size()>0) email = currentUser[0].Email;
        return email;
    }

    public PageReference changeEmail(){
        if (!isValidEmail()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.SICo_EmailsDontMatch);
            errorMessage = msg.getDetail();
            return null;
        }
        //change the Email of the user
        boolean noUserFound = false;
        try{
            //Update User
            List<User> currentUserList = [SELECT Email FROM User WHERE AccountId=:accountId LIMIT 1]; //Might be null if User is not created yet
            if(currentUserList.size() > 0 && accountId != null){
                currentUserList[0].Email = newEmail;
                update currentUserList;
                //Update Account
                Account theAccount = new Account(Id = accountId, PersonEmail = newEmail);
                update theAccount;
                //reset error message if update was sucessfull
                error = null;
            }else{
                noUserFound = true;
                error = Label.SICo_UserUnkown;
            }
        }
        catch(DMLException e){
            error = e.getMessage();
            System.debug(error);
        }
        if(error != null){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.SICo_EmailAdressAlreadyExist);
            errorMessage = msg.getDetail();
            if(noUserFound) errorMessage = error;
            return null;
        }
        PageReference pageRef = new PageReference('/apex/PersonalInformations?AccountId=' + accountId);
        pageRef.setRedirect(true);
        return pageRef;
    }

    public boolean isValidEmail(){
        return newEmail == verifyNewEmail;
    }

}