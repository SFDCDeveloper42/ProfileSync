({
 	updateInformations : function (component, event, pHelper) {
		var idAccount = component.get("v.accountId");
		var valueSurface = component.get("v.epw.surface");

		// "Disable" the validate button
		pHelper.disableValidate(); 
		if ((valueSurface === undefined || valueSurface === null || valueSurface === '')) {
			pHelper.displayError(component, $A.get("$Label.c.SICo_EnergyProfile_AC_BadEntry"), "Error_05Bis"); 
		} else if (isNaN(valueSurface) || valueSurface < 10 || valueSurface > 1000) {
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorSurface"), "Error_05Bis");  
		} else {
			var action = component.get("c.setInformations_05Bis");
			action.setParams({
				siteId : component.get("v.epw.Id"),
				surface : component.get("v.epw.surface")
			});
			action.setCallback(this, function (response) {				
				pHelper.moveToNextStep();
			}); 
			$A.enqueueAction(action);
		}
	}, 
	
	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_05Bis");
	}

})