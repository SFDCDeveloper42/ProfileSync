({ 

	updateInformations : function (component, event, pHelper) {
		var idAccount = component.get("v.accountId"); 
		//define the actual year to set the max value of the year that the user is able to select
		var now = new Date();
		var annee = now.getFullYear(); 
		// "Disable" the validate button
		pHelper.disableValidate(); 
		if (component.get("v.epw.builtDate") > annee || component.get("v.epw.builtDate") < 1000) { 
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorYear"), "Error_04");
		} else if (isNaN(component.get("v.epw.builtDate"))
				|| component.get("v.epw.builtDate") == 0 
				|| component.get("v.epw.builtDate") == null 
				|| component.get("v.epw.builtDate") == undefined) {
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorYear"), "Error_04");
		} else { 
			//put the values in the apex controller parameters
			var action = component.get("c.setInformations_04");
			action.setParams({
				siteId : component.get("v.epw.Id"),
				dateConstruction : component.get("v.epw.builtDate")
			});
 
			action.setCallback(this, function (response) {				
				//redirect to the next page
				pHelper.moveToNextStep(); 
			}); 
			$A.enqueueAction(action);
		}
	},

	validate : function (component, event, helper) {
		//to verify that the characters are numbers
		var input = component.get('v.epw.builtDate'); 
		if (isNaN(input) && input != undefined) {
			//if when the key is pressed, it is not a number, replace the character
			component.set('v.epw.builtDate', input.substring(0, input.length - 1));  
		} 
		helper.enableValidate(); 
		helper.resetErrorContainer(component, "Error_04"); 
	},
	moveToPreviousStep : function(pComponent, pEvent, pHelper) { 
		pHelper.moveToPreviousStep(pComponent);
		return false;
	} 

})