/**
* @author Sebastien Colladon
* @date 16/11/2016
*
* @description Usage trigger handler helper class
*/
public with sharing class SICo_UsageTriggerHandler_CLS {
    public static void handleAfterInsert(List<Usage__c> usages) {
      List<SICo_ExtraBilledLign__c> billedLigns = new List<SICo_ExtraBilledLign__c>();
      for(Usage__c anUsage : usages) {
        if(String.isNotBlank(anUsage.ExtraBilledLine__c)){
          SICo_ExtraBilledLign__c aBilledLign = new SICo_ExtraBilledLign__c(
            id=anUsage.ExtraBilledLine__c.trim(),
            Usage__c=anUsage.id
          );
          billedLigns.add(aBilledLign);
        }
      }
      List<Database.SaveResult> SRL = Database.update(billedLigns, false);
      System.Debug(SRL);
    }
}