({
	updateInformations : function (component, event, pHelper) {
		var idAccount = component.get("v.accountId");
		component.set("v.epw.hotWater", $("input[name='operationOfHotWater']:checked").val());

		// "Disable" the validate button
		pHelper.disableValidate(); 
		// Save informations entered at screen on the current site
		var action = component.get("c.setInformations_08");
		if (component.get("v.epw.hotWater") == undefined) { 
				pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorNoValue"), "Error_08");    
			} else {
			action.setParams({
				siteId : component.get("v.epw.Id"),
				hotWaterOperation : component.get("v.epw.hotWater")
			});
			action.setCallback(this, function (response) {				
				pHelper.moveToNextStep();				
			}); 
			$A.enqueueAction(action);
		}
	},	

	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_08");
	}
})