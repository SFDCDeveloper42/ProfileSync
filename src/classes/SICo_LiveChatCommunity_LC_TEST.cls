/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Test class for SICo_LiveChatCommunity_LC
History
12/01/2017     Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
@isTest
private class SICo_LiveChatCommunity_LC_TEST {

	/*
	Test that Live Chat is marked available when API returns true
	 */
	@isTest static void testChatAvailable() {
		String apiResponseBodyAvailable = '{"messages":[{"type":"Availability", "message":{"results":[{"id":"5734E00000000Bi","isAvailable":true}]}}]}';
		//Set Mock
		Test.setMock(HttpCalloutMock.class, new SICo_SimpleCallout_MOCK(200, 'OK', apiResponseBodyAvailable));
		//Test
		Test.startTest();
		Boolean testApiNotAvailability = SICo_LiveChatCommunity_LC.isLiveChatAvailable();
		Test.stopTest();
		//Expected result: chat is available
		system.assertEquals(true, testApiNotAvailability);
	}
	
	/*
	Test that Live Chat is marked not available when API does not returns true
	 */
	@isTest static void testChatNotAvailable() {
		String apiResponseBodyNotAvailable = '{"messages":[{"type":"Availability", "message":{"results":[{"id":"5734E00000000Bi"}]}}]}';
		//Set Mock
		Test.setMock(HttpCalloutMock.class, new SICo_SimpleCallout_MOCK(200, 'OK', apiResponseBodyNotAvailable));
		//Test
		Test.startTest();
		Boolean testApiNotAvailability = SICo_LiveChatCommunity_LC.isLiveChatAvailable();
		Test.stopTest();
		//Expected result: chat is not available
		system.assertEquals(false, testApiNotAvailability);
	}
	
	/*
	Test that Live Chat is marked not available when API does not respond
	 */
	@isTest static void testApiNotAvailable() {
		//Test - no mock
		Test.startTest();
		Boolean testApiNotAvailability = SICo_LiveChatCommunity_LC.isLiveChatAvailable();
		Test.stopTest();
		//Expected result: chat is not available
		system.assertEquals(false, testApiNotAvailability);
	}

	/*
	Test that Live Chat is marked available when platform cache returns true
	 */
	@isTest static void testValueInPlatformCahe() {
		//Set available = true in Platform cache
		SICo_PlatformCacheManager_CLS.putObject(SICo_LiveChatCommunity_LC.CACHE_KEY_ISLIVECHATAVAILABLE, true, SICo_LiveChatCommunity_LC.CACHE_TTL_ISLIVECHATAVAILABLE);
		//Test - no mock
		Test.startTest();
		Boolean testApiNotAvailability = SICo_LiveChatCommunity_LC.isLiveChatAvailable();
		Test.stopTest();
		//Expected result: chat is available
		system.assertEquals(true, testApiNotAvailability);
	}

}