<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>THO1</label>
    <protected>false</protected>
    <values>
        <field>AmendType__c</field>
        <value xsi:type="xsd:string">New Product</value>
    </values>
    <values>
        <field>Options__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>RatePlanType__c</field>
        <value xsi:type="xsd:string">TH</value>
    </values>
    <values>
        <field>ToAmend__c</field>
        <value xsi:type="xsd:string">SE</value>
    </values>
</CustomMetadata>
