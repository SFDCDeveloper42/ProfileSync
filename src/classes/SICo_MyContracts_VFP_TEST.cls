/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Test class of the Controller for the Contrats page (Contracts)
History
25/08/2016      Mehdi BEGGAS      Create version
07/12/2016		Mehdi CHERFAOUI	  IKUMBI-497: Handle Product-Only contracts	
------------------------------------------------------------*/
@isTest
public class SICo_MyContracts_VFP_TEST {

	//set test data
	@testSetup
	static void testData() {
		//Insert new community user
        User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser;

        User us = [SELECT accountId FROM User WHERE Id =:communityUser.Id LIMIT 1];

        //Site creation
        SICo_Site__c site = SICo_UtilityTestDataSet.createSite(us.accountID);
        site.BoilerBrand__c = 'brand';
        site.BoilerModel__c = 'model';
        site.IsActive__c = true;
        site.IsSelectedInCustomerSpace__c = true;
        insert site;

        //Subscription creation
        Zuora__Subscription__c subSowee = new Zuora__Subscription__c(
            Zuora__Status__c= 'Active',
            Zuora__OriginalId__c='original',
            Zuora__PreviousSubscriptionId__c='original',
            Name= 'soweeContract',
            Zuora__Account__c= us.accountID,
            SiteId__c= site.Id,
            Zuora__TermStartDate__c = Date.today(),
			Zuora__MRR__c = 22,
			OfferName__c = 'SE_TH_GAZ',
			Zuora__TermEndDate__c = Date.today(),
			MarketingOfferName__c ='test Offer'
            );
        insert subSowee;
        
        Zuora__Subscription__c subMaintenance = new Zuora__Subscription__c(
            Zuora__Status__c= 'Active',
            Zuora__OriginalId__c='original',
            Zuora__PreviousSubscriptionId__c='original',
            Name= 'soweeContract',
            Zuora__Account__c= us.accountID,
            SiteId__c= site.Id,
            Zuora__TermStartDate__c = Date.today(),
			Zuora__MRR__c = 22,
			OfferName__c = 'MAINT',
			Zuora__TermEndDate__c = Date.today(),
			MarketingOfferName__c ='test Offer'
            );
        insert subMaintenance;
        
        Attachment att1 = new Attachment(
			parentId = subSowee.Id,
			Name = 'GAZ_CPV_Test',
            Body = EncodingUtil.base64Decode('abcd')            
        );
        insert att1;

        Attachment att2 = new Attachment(
			parentId = subMaintenance.Id,
			Name = 'MAINT_CPV_Test',
            Body = EncodingUtil.base64Decode('abcd')            
        );
        insert att2;


        //Bundle creation
		SICo_Bundle__c bundleSowee = new SICo_Bundle__c(
			Name = 'sowee bundle',
			StartDate__c = Date.today(),
			TechOfferName__c = 'SE_TH_GAZ',
			PublicLabel__c = 'Contrat Gaz Sérénité',
			PublicLabelLine2__c = 'Gaz, Station & Thermostat Sowee'
		);
		insert bundleSowee;

		//cr création
		SICo_Bundle__c bundleMaintenance = new SICo_Bundle__c(
			Name = 'maintenance bundle',
			StartDate__c = Date.today(),
			TechOfferName__c = 'MAINT',
			PublicLabel__c = 'Contrat maintenance pièces et main d’oeuvre'
		);
		insert bundleMaintenance;

        //Run as Community User without cr
	    system.runAs(us){
	        Test.startTest();
	            SICo_MyContracts_VFP controller = new SICo_MyContracts_VFP();
	            PageReference pRef = Page.Contracts;
		        Test.setCurrentPageReference(pRef);
	        Test.stopTest();
	   }
	}

	/*
	Contracts without CR
	 */
	@isTest
	static void test_WithoutCr() {

		User us = [SELECT accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        //Run as Community User without cr
	    system.runAs(us){
	        Test.startTest();
	            SICo_MyContracts_VFP controller = new SICo_MyContracts_VFP();
	            PageReference pRef = Page.Contracts;
		        Test.setCurrentPageReference(pRef);

		        System.assertNotEquals(controller.soweeContract, null);
		        System.assertNotEquals(controller.maintenanceContract, null);
		        System.assertEquals(controller.productContract, null);

	        Test.stopTest();
        }
	}

	/*
	Contracts with CR
	 */
	@isTest
	static void test_Cr() {

		User us = [SELECT accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

		SICo_Site__c site = [SELECT Id FROM SICo_Site__c];

        SICo_CR__c cr = new SICo_CR__c(
        	Site__c = site.Id,
        	InterventionDate__c = Date.today()
        );
        insert cr;

        //Run as Community User with cr
	    system.runAs(us){
	        Test.startTest();
	            SICo_MyContracts_VFP controller = new SICo_MyContracts_VFP();
	            PageReference pRef = Page.Contracts;
		        Test.setCurrentPageReference(pRef);
		        System.assertNotEquals(controller.soweeContract, null);
		        System.assertNotEquals(controller.maintenanceContract, null);
		        System.assertEquals(controller.productContract, null);
	        Test.stopTest();
        }
	}

	/*
	No contract (products only)
	 */
	@isTest
	static void test_ProductOnly() {

		User us = [SELECT accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];
		//Delete all sub
		List<Zuora__Subscription__c> currentSub = [SELECT Id FROM Zuora__Subscription__c];
		Delete currentSub;
		//Create product-only subscription instead
		SICo_Site__c site = [SELECT ID FROM SICo_Site__c LIMIT 1];
		Zuora__Subscription__c subSoweeProduct = new Zuora__Subscription__c(
            Zuora__Status__c= 'Active',
            Zuora__OriginalId__c='original',
            Zuora__PreviousSubscriptionId__c='original',
            Name= 'soweeContract',
            Zuora__Account__c= us.accountID,
            SiteId__c= site.Id,
            Zuora__TermStartDate__c = Date.today(),
			Zuora__MRR__c = 22,
			Zuora__TermEndDate__c = Date.today(),
			OfferName__c = 'SE_TH',
			MarketingOfferName__c ='test Offer'
            );
        insert subSoweeProduct;
        Attachment att3 = new Attachment(
			parentId = subSoweeProduct.Id,
			Name = 'Product_Test',
            Body = EncodingUtil.base64Decode('abcd')            
        );
        insert att3;
        //Run as Community User
	    system.runAs(us){
	        Test.startTest();
	            SICo_MyContracts_VFP controller = new SICo_MyContracts_VFP();
	            PageReference pRef = Page.Contracts;
		        Test.setCurrentPageReference(pRef);
		        System.assertEquals(controller.soweeContract, null);
		        System.assertEquals(controller.maintenanceContract, null);
		        System.assertNotEquals(controller.productContract, null);
	        Test.stopTest();
        }

	}

}