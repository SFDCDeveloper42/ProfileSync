<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AdresseC43</label>
    <protected>false</protected>
    <values>
        <field>Autre__c</field>
        <value xsi:type="xsd:string">ZAC du Bois Rigault Nord</value>
    </values>
    <values>
        <field>C43Phone__c</field>
        <value xsi:type="xsd:string">0800700300</value>
    </values>
    <values>
        <field>Cedex__c</field>
        <value xsi:type="xsd:string">CS 70001</value>
    </values>
    <values>
        <field>HelpPhoneNumber__c</field>
        <value xsi:type="xsd:string">0176543265</value>
    </values>
    <values>
        <field>Name__c</field>
        <value xsi:type="xsd:string">SoWee Service Clients</value>
    </values>
    <values>
        <field>OrchideeCity__c</field>
        <value xsi:type="xsd:string">Courbevoie</value>
    </values>
    <values>
        <field>OrchideePostalCode__c</field>
        <value xsi:type="xsd:double">92240.0</value>
    </values>
    <values>
        <field>StreetAndNumberOrchidee__c</field>
        <value xsi:type="xsd:string">4 place des Vosges</value>
    </values>
    <values>
        <field>UrgentNumber__c</field>
        <value xsi:type="xsd:string">0800473333</value>
    </values>
    <values>
        <field>WebSiteC43__c</field>
        <value xsi:type="xsd:string">www.c43.fr</value>
    </values>
</CustomMetadata>
