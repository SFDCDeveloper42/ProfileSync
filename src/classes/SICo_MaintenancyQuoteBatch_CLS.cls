/**
* @author Boris Castellani
* @date 26/10/2016
*
* @description Batch job SICo_MaintenancyQuoteBatch_CLS
*/

global class SICo_MaintenancyQuoteBatch_CLS implements Database.Batchable<sObject> {
    public String query;
    public Datetime limitDate;

    global SICo_MaintenancyQuoteBatch_CLS() {
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {

        DevisCaduque__c maintenancyQuote = DevisCaduque__c.getInstance();
        system.debug('*** maintenancyQuote'+ maintenancyQuote);
        Integer nbrDay = Integer.valueOf(maintenancyQuote.nbrDay__c);
        system.debug('*** nbrDay'+ nbrDay);

        this.limitDate = Datetime.now().addDays(-nbrDay);
        this.query = 'SELECT Id, CreatedDate, MaintenancyQuoteStatus__c FROM MaintenancyQuote__c WHERE CreatedDate < :limitDate AND MaintenancyQuoteStatus__c != \'Accepté\' AND MaintenancyQuoteStatus__c != \'Refusé\'';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scopes) {

        List<MaintenancyQuote__c> maintenancyQuotes = new List<MaintenancyQuote__c>();

        for(sObject scope : scopes){
            MaintenancyQuote__c maintenancyQuote = (MaintenancyQuote__c) scope;
            maintenancyQuote.MaintenancyQuoteStatus__c = 'Caduque';
            maintenancyQuotes.add(maintenancyQuote);
        }

        update maintenancyQuotes;

    }

    global void finish(Database.BatchableContext BC) {
    }
}