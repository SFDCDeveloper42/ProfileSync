/**
* @author Boris Castellani
* @date 21/02/2017
* @LastModify 
*
* @description TEST SICo_ChamJeton_BATCH.
*/
@isTest
private class SICo_ChamJeton_BATCH_TEST {
  @testSetup 
    static void setup() {
      Id rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
      Account prestaAccount = new Account(Name='CHAM', RecordTypeId=rtAccount);
      prestaAccount.MondayTimeRange1__c = 4; prestaAccount.MondayTimeRange2__c = 4;
      prestaAccount.MondayTimeRange3__c = 4; prestaAccount.MondayTimeRange4__c = 4;
      prestaAccount.TuesdayTimeRange1__c = 4; prestaAccount.TuesdayTimeRange2__c = 4;
      prestaAccount.TuesdayTimeRange3__c = 4; prestaAccount.TuesdayTimeRange4__c = 4;
      prestaAccount.WednesdayTimeRange1__c = 4; prestaAccount.WednesdayTimeRange2__c = 4;
      prestaAccount.WednesdayTimeRange3__c = 4; prestaAccount.WednesdayTimeRange4__c = 4;
      prestaAccount.ThursdayTimeRange1__c = 4; prestaAccount.ThursdayTimeRange2__c = 4;
      prestaAccount.ThursdayTimeRange3__c = 4; prestaAccount.ThursdayTimeRange4__c = 4;    
      prestaAccount.FridayTimeRange1__c = 4; prestaAccount.FridayTimeRange2__c = 4;
      prestaAccount.FridayTimeRange3__c = 4; prestaAccount.FridayTimeRange4__c = 4;      
      prestaAccount.SaturdayTimeRange1__c = 4; prestaAccount.SaturdayTimeRange2__c = 4;
      prestaAccount.SaturdayTimeRange3__c = 4; prestaAccount.SaturdayTimeRange4__c = 4; 
      INSERT prestaAccount;

      SICo_ChamJetonService_CLS chamJeton = new SICo_ChamJetonService_CLS();
      List<SICO_AvailabilyWeeklyCalendar__c> availabilyWeeklyCalendars = new List<SICO_AvailabilyWeeklyCalendar__c>();
      availabilyWeeklyCalendars.addAll(chamJeton.buildCalendar(new List<Account>{prestaAccount},Date.today().addDays(-7).toStartOfWeek()));
      availabilyWeeklyCalendars.addAll(chamJeton.buildCalendar(new List<Account>{prestaAccount},Date.today().toStartOfWeek()));
      availabilyWeeklyCalendars.addAll(chamJeton.buildCalendar(new List<Account>{prestaAccount},Date.today().addDays(7).toStartOfWeek()));
      availabilyWeeklyCalendars.addAll(chamJeton.buildCalendar(new List<Account>{prestaAccount},Date.today().addDays(14).toStartOfWeek()));
      availabilyWeeklyCalendars.addAll(chamJeton.buildCalendar(new List<Account>{prestaAccount},Date.today().addDays(21).toStartOfWeek()));

      INSERT availabilyWeeklyCalendars;

      system.debug('TEST '+availabilyWeeklyCalendars);

    }

  static testMethod void processCalendar() {

    Test.startTest();
      SICo_ChamJeton_BATCH processCalendar = new SICo_ChamJeton_BATCH();
      Id batchId = Database.executeBatch(processCalendar);
    Test.stopTest();
    

    Account aAccount= [SELECT Id, RecordTypeId, (SELECT StartDate__c, Status__c FROM Dispo_hebdomadaire__r) 
                       FROM Account WHERE RecordTypeId = :SICo_Utility.m_RTbyDeveloperName.get('Account_Agency')];

    Boolean close = false;
    for(SICO_AvailabilyWeeklyCalendar__c calendar : aAccount.Dispo_hebdomadaire__r){
      if(calendar.Status__c == LABEL.SICo_weeklyCalendars_Close){
        close = true;
      }
    }

    System.assertEquals(close,true);
  }
}