/**********************************************************************
 * 
 *
 * @url: /services/apexrest/rest
 * @data:
 *  {
        
    }
*************************************************************************/
@RestResource(urlMapping='/postAttachement')
global with sharing class SICo_Attachment_Utility_RWS {

	global class Request {
		String parentId;
		String description;
		String name;
		String documentBody;
	}

    @HttpPost
    global static String doPost(SICo_Attachment_Utility_RWS.Request inputRequest) {
        
        try {
     	  Attachment a = new Attachment();
     	  a.ParentId = inputRequest.parentId;
     	  a.Name = inputRequest.name;
     	  a.Body = EncodingUtil.base64Decode(inputRequest.documentBody);
          insert a;
        } catch (Exception e) {
            SICo_LogManagement.sendErrorLog(SICo_Constants_CLS.SICO,Label.SICO_LOG_CHAM_Attachment,e.getMessage());
        }
      	return 'Attachment added';
    }
}