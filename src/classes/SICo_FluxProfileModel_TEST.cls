/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Test SICo_FluxProfileModel
History
28/07/2016      Boris Castellani     Create version
------------------------------------------------------------*/
@isTest
private class SICo_FluxProfileModel_TEST {
	
	@testSetup static void serialiseProfileModel() {
		SICo_FluxProfileModel.GenericProfile genericProfile = SICo_UtilityFluxDataSet.createFluxGenericProfile();
		String responceJSON = JSON.serialize(genericProfile);
	}

	@isTest static void parseElecModel() {
		String json ='{'+
		'\"siteId\": \"a1N4E00000015BwUAI\",'+
		'\"personId\": \"0014E00000COhaTQAT\",'+
		'\"changeDate\": \"2015-11-05T08:15:30-05:00\",'+
		'\"tariff\": \"BLEU\",'+
		'\"tariffOption\": \"HP_HC\",'+
		'\"subscribedPower\": \"9\",'+
		'\"tpnCategory\": \"1\",'+
		'\"peakLoadGroup\": \"1\",'+
		'\"millesime\": \"1\",'+
		'\"parameters\": {}'+
			'}';
		system.debug('json: '+json);
		SICo_FluxProfileModel.ElecSupplyContractChange FluxRequest = new SICo_FluxProfileModel.ElecSupplyContractChange().parse(json);
		system.debug('FluxRequest: '+FluxRequest);
	}

	@isTest static void parseGazModel() {
		String json ='{'+
		'\"siteId\": \"a1N4E00000015BwUAI\",'+
		'\"personId\": \"0014E00000COhaTQAT\",'+
		'\"changeDate\": \"2015-11-05T08:15:30-05:00\",'+
		'\"tariff\": \"GAZ_PF2_1\",'+
		'\"option\": \"Conso1\",'+
		'\"millesime\": \"9\",'+
		'\"tss\": \"1\",'+
		'\"zone\": \"Z4\",'+
		'\"contractName\": \"1\",'+
		'\"parameters\": {}'+
			'}';
		system.debug('json: '+json);
		SICo_FluxProfileModel.GasSupplyContractChange FluxRequest = new SICo_FluxProfileModel.GasSupplyContractChange().parse(json);
		system.debug('FluxRequest: '+FluxRequest);
	}
}