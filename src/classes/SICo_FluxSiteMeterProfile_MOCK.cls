/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Mock GAZ responce for SICo_FluxSiteMeter
History
01/08/2016      Boris Castellani     Create version
------------------------------------------------------------*/
@isTest
global class SICo_FluxSiteMeterProfile_MOCK implements HttpCalloutMock {
	// Implement this interface method
	global HTTPResponse respond(HTTPRequest request) {
		SICo_FluxProfileModel.GenericProfile profile = new SICo_FluxProfileModel.GenericProfile().parse(request.getBody());

		// Create a fake response
		HttpResponse response = new HttpResponse();
		response.setHeader('Content-Type', 'application/json');
		if(profile.housing.constructionDate == '10'){
			response.setBody('{"ownerId":"'+profile.personExternalId+'","StatusCode":"400","errorDescription":"NO ERROR"}');
			}else{
				response.setBody('{"ownerId":"'+profile.personExternalId+'","StatusCode":"200","errorDescription":"NO ERROR"}');
				}
		response.setStatusCode(200);
		return response;
		}
}