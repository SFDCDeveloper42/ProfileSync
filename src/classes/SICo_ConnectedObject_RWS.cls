/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   REST Service in order to create a pairing request
Test Class:    SICo_ConnectedObject_RWS_TEST
History
08/07/2016      Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
@RestResource(urlMapping='/ConnectedObject')
global with sharing class SICo_ConnectedObject_RWS  {

    @HttpPost
    global static Boolean declareConnectedObject_RWS( String sKey,
                                                      String sId,
                                                      String sType,
                                                      String sAction ) {

    /*@HttpGet
    global static Boolean declareConnectedObject_RWS() {
        String sKey = RestContext.request.params.get('sKey');
        String sId = RestContext.request.params.get('sId');
        String sType = RestContext.request.params.get('sType');
        String sAction = RestContext.request.params.get('sAction');*/

        Boolean bStatus = true;

        try {
            
            if ( sAction == 'INSERT' ) {

                //CREATE PAIRING REQUEST
                SICo_Pairing__c oAp = New SICo_Pairing__c();
                oAp.TemporaryKey__c = sKey;
                oAp.ObjectId__c = sId;
                oAp.ObjectType__c = sType;
                oAp.ActionType__c = sAction;
                insert oAp;

            } else if ( sAction == 'RETURN' ) {

                //TO VALIDATE
                Asset oAsset = [ SELECT Id, Status, UsageEndDate FROM Asset WHERE SerialNumber = : sId LIMIT 1 ];
                if ( oAsset != null ) {
                    oAsset.Status = 'Retourné';
                    oAsset.UsageEndDate = Date.Today();
                    oAsset.SerialNumber = 'XXXXX';
                    update oAsset;
                }

            }

        } catch ( Exception e ) {

            SICo_LogManagement.sendErrorLog( 
            	'SICo',
            	Label.SICO_LOG_STATION_Pairing_Request,
            	String.valueOf( e )
            );
            bStatus = false;

        }

        return bStatus;

    }

}