/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi-solutions
Description:   Class for the update of the account's informations
Test Class:    SICo_ChangeInformations_VFC_TEST
History
20/09/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
public with sharing class SICo_ChangeInformations_VFC {
	public Id accountId{get;set;}

	public SICo_ChangeInformations_VFC() {
		String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
		try{
			this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
		}catch (exception e){
			System.debug('Unable to return accountId');
		}
	}


	@AuraEnabled
	public static Account getAccount(Id accountId){
		Account theAccount = [SELECT FirstName, LastName, Salutation, ContactAddressNumberStreet__c, ContactAddressAdditionToAddress__c, ContactAdresseBuilding__c,
												ContactAddressStair__c, ContactAddressFloor__c, ContactAddressFlat__c, ContactAddressZipCode__c,
												ContactAddressCity__c, ContactAddressCountry__c, PersonEmail, Phone, PersonMobilePhone
												FROM Account
												WHERE Id=:accountId LIMIT 1];
		return theAccount;
	}

	/*
	* Update the account
	*/
	@AuraEnabled
	public static void updateInformations(Id accountId, String address,
												String addressAddition, String building, String stairs, String floor,
												String flat, String zipCode, String city, String country, String email, String phone, String mobilePhone){

		Account account = [SELECT FirstName, LastName, Salutation, ContactAddressNumberStreet__c, ContactAddressAdditionToAddress__c, ContactAdresseBuilding__c,
												ContactAddressStair__c, ContactAddressFloor__c, ContactAddressFlat__c, ContactAddressZipCode__c,
												ContactAddressCity__c, ContactAddressCountry__c, PersonEmail, Phone, PersonMobilePhone
												FROM Account
												WHERE Id=:accountId LIMIT 1];

		account.ContactAddressNumberStreet__c = address;
		account.ContactAddressAdditionToAddress__c = addressAddition;
		account.ContactAdresseBuilding__c = building;
		account.ContactAddressStair__c = stairs;
		account.ContactAddressFloor__c = floor;
		account.ContactAddressFlat__c = flat;
		account.ContactAddressZipCode__c = zipCode;
		account.ContactAddressCity__c = city;
		account.ContactAddressCountry__c = country;
		account.Phone = phone;
		account.PersonMobilePhone = mobilePhone;

		try {
			update account;
		}
		catch(DmlException e){
			System.debug('############## An unexpected error has occurred: ' + e.getMessage());
		}
	}

}