<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Confirmation_de_RDV</fullName>
        <description>Confirmation de RDV</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>user.madteam@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/HTML_Mail_pour_confirmation_rdv_GRDF</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_de_RDV_GRDF</fullName>
        <description>Confirmation de RDV GRDF</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/HTML_Mail_pour_confirmation_rdv_GRDF</template>
    </alerts>
    <alerts>
        <fullName>Confirmation_modification_rdv_Grdf</fullName>
        <description>Confirmation modification rdv Grdf</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/HTML_Mail_pour_modification_rdv_GRDF</template>
    </alerts>
    <alerts>
        <fullName>Informations_Station_envoyees_du_compte</fullName>
        <description>Informations_Station_envoyees_du_compte</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HTML_Infos_fonctionnalites_station_account</template>
    </alerts>
    <alerts>
        <fullName>Livraison_colis_en_point_relais</fullName>
        <description>Livraison colis en point relais</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HTML_Livraison_en_Point_Relais</template>
    </alerts>
    <alerts>
        <fullName>RDV_maintenance</fullName>
        <description>RDV maintenance</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HTML_Mail_Confirmation_Cham_Entretien</template>
    </alerts>
    <alerts>
        <fullName>Rappel_rdv_Cham_installation</fullName>
        <description>Rappel rdv Cham installation</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HTML_Mail_Confirmation_Cham_Installation</template>
    </alerts>
    <fieldUpdates>
        <fullName>CarrierPopulationColiposte</fullName>
        <description>SICo : Population of Carrier according to shipping mode</description>
        <field>Carrier__c</field>
        <literalValue>Coliposte</literalValue>
        <name>MAJ champ transporteur Coliposte</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CarrierPopulationColissimo</fullName>
        <description>SICo : Population of Carrier according to shipping mode</description>
        <field>Carrier__c</field>
        <literalValue>Colissimo</literalValue>
        <name>MAJ champ transporteur Colissimo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopulateCarrierKiala</fullName>
        <description>[SICo] : Populate value in carrier according to shipping mode</description>
        <field>Carrier__c</field>
        <literalValue>Kiala</literalValue>
        <name>MAJ champ transporteur Kiala</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopulateCarrierUPS</fullName>
        <description>[SICo] : Populate value in carrier according to shipping mode</description>
        <field>Carrier__c</field>
        <literalValue>UPS Access Point</literalValue>
        <name>MAJ champ transporteur UPS Access Point</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SICo_FU_Status_Fermee</fullName>
        <description>Fermeture de la tâche</description>
        <field>Status</field>
        <literalValue>Fermée</literalValue>
        <name>SICo FU Status Fermée</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>STOP_PROVISIONING</fullName>
        <field>Tech_ProvisioningReady__c</field>
        <literalValue>0</literalValue>
        <name>STOP PROVISIONING</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MailSenttoCham</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Envoie Mail à Cham</value>
        </criteriaItems>
        <description>[SICO] WF to send en email to CHAM</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SICO - Rappel Rdv CHAM installation</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.RequestState__c</field>
            <operation>equals</operation>
            <value>Intervention programmée</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.InterventionType__c</field>
            <operation>equals</operation>
            <value>POSE STATION ENERGIE AVEC FOURNITURE,POSE STATION ENERGIE SANS FOURNITURE</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Rappel_rdv_Cham_installation</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Task.InterventionPlannedDate__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SICO Livraison en Point Relais</fullName>
        <actions>
            <name>Livraison_colis_en_point_relais</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.LogisticProcessType__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Task.DeliveryStatus__c</field>
            <operation>equals</operation>
            <value>Disponible en relais colis</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ShippingMode__c</field>
            <operation>equals</operation>
            <value>Relais colis</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.DeliveryTrackingNumber__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Ce WF permet d&apos;envoyer un email au client afin de le prévenir que l&apos;objet a été livré en point colis.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SICo - MAJ Champ Transporteur Coliposte</fullName>
        <actions>
            <name>CarrierPopulationColiposte</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Task Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ShippingMode__c</field>
            <operation>equals</operation>
            <value>Colissimo</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ShippingMode__c</field>
            <operation>equals</operation>
            <value>Installateur</value>
        </criteriaItems>
        <description>Populate field Carrier according to the value in ShippingMode</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SICo - MAJ Champ Transporteur Colissimo</fullName>
        <actions>
            <name>CarrierPopulationColissimo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Task Provisionning Logistique,Logistique</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ShippingMode__c</field>
            <operation>equals</operation>
            <value>Colissimo</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ShippingMode__c</field>
            <operation>equals</operation>
            <value>Installateur</value>
        </criteriaItems>
        <description>Populate field Carrier according to the value in ShippingMode</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SICo - MAJ Champ Transporteur Kiala</fullName>
        <actions>
            <name>PopulateCarrierKiala</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Task Provisioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ShippingMode__c</field>
            <operation>equals</operation>
            <value>Relais colis</value>
        </criteriaItems>
        <description>Populate field Carrier according to the value in ShippingMode</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SICo - MAJ Champ Transporteur UPS Access Point</fullName>
        <actions>
            <name>PopulateCarrierUPS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Task Provisionning Logistique,Logistique</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ShippingMode__c</field>
            <operation>equals</operation>
            <value>Relais colis</value>
        </criteriaItems>
        <description>Populate field Carrier according to the value in ShippingMode</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SICo Confirmer RDV Gaz</fullName>
        <actions>
            <name>Confirmation_de_RDV</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Confirmation_de_RDV_GRDF</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Ouverte</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.RequestState__c</field>
            <operation>equals</operation>
            <value>RDV programmé </value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.InterventionPlannedDate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Ce WF est utilisé pour envoyer une confirmation de rendez vous à un client lorsque la date d&apos;intervention est renseigné et que le statut de la tâche = RDV Programmé.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SICo Confirmer modification RDV Gaz</fullName>
        <actions>
            <name>Confirmation_modification_rdv_Grdf</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Ce WF est utilisé pour envoyer une confirmation de rendez vous à un client lorsque la date d&apos;intervention a été modifiée.</description>
        <formula>AND ( NOT( ISBLANK( InterventionPlannedDate__c)), ISCHANGED(InterventionPlannedDate__c), InterventionPlannedDate__c&gt;TODAY(), ISPICKVAL( RequestState__c ,&quot;RDV Programmé&quot;),  $User.BypassWF__c =false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SICo Email Modification RDV maintenance</fullName>
        <actions>
            <name>RDV_maintenance</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED ( InterventionPlannedDate__c  ) &amp;&amp; ISCHANGED ( PlannedRangedHours__c ) &amp;&amp; NOT(ISNULL( InterventionPlannedDate__c )) &amp;&amp; OR ( ISPICKVAL( InterventionType__c , &quot;VISITE D&apos;ENTRETIEN&quot;), ISPICKVAL( InterventionType__c , &quot;VISITE DE CONFORMITE&quot;), ISPICKVAL( InterventionType__c , &quot;DEPANNAGE&quot;)) &amp;&amp; ISPICKVAL(   RequestState__c , &quot;Intervention programmée&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SICo Email RDV maintenance</fullName>
        <actions>
            <name>RDV_maintenance</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.InterventionPlannedDate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Task.PlannedRangedHours__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Task.InterventionType__c</field>
            <operation>equals</operation>
            <value>VISITE DE CONFORMITE,VISITE D&apos;ENTRETIEN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.RequestState__c</field>
            <operation>equals</operation>
            <value>Intervention programmée</value>
        </criteriaItems>
        <description>Email de notification de RDV ou changement de RDV de maintenance</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>STOP_PROVISIONING</fullName>
        <actions>
            <name>STOP_PROVISIONING</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Tech_ProvisioningReady__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sico Fermeture de la tâche livraison</fullName>
        <actions>
            <name>SICo_FU_Status_Fermee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Envoi du pack Station énergie,Envoi du pack Station énergie + Thermostat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.DeliveryStatus__c</field>
            <operation>equals</operation>
            <value>Livrée,Retiré du relais colis</value>
        </criteriaItems>
        <description>Ce WF permet de fermer la tâche livraison</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sico Fermeture de la tâche livraison EXP</fullName>
        <actions>
            <name>SICo_FU_Status_Fermee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.LogisticProcessType__c</field>
            <operation>equals</operation>
            <value>EXP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.DeliveryStatus__c</field>
            <operation>equals</operation>
            <value>Livré,Retiré</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Ce WF permet de fermer la tâche livraison</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sico Fermeture de la tâche livraison RET</fullName>
        <actions>
            <name>SICo_FU_Status_Fermee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.LogisticProcessType__c</field>
            <operation>equals</operation>
            <value>RET</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.DeliveryReturnFeedback__c</field>
            <operation>equals</operation>
            <value>Livré,Retiré,Retourné au logisticien</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.PackageReturnsDiagnosis__c</field>
            <operation>equals</operation>
            <value>Colis complet et en bon état</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Ce WF permet de fermer la tâche livraison</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sico Fermeture de la tâche livraison RMA</fullName>
        <actions>
            <name>SICo_FU_Status_Fermee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.LogisticProcessType__c</field>
            <operation>equals</operation>
            <value>RMA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.DeliveryStatus__c</field>
            <operation>equals</operation>
            <value>Livré,Retiré</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.DeliveryReturnFeedback__c</field>
            <operation>equals</operation>
            <value>Livré,Retiré,Retourné au logisticien</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.PackageReturnsDiagnosis__c</field>
            <operation>equals</operation>
            <value>Colis complet et en bon état</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Ce WF permet de fermer la tâche livraison</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
