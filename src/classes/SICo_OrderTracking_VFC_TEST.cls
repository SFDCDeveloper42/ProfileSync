/*------------------------------------------------------------
Author:        David LEROUX
Company:       Salesforce.com
Description:   Test class for the SICo_OrderTracking_VFC Controller
History
27/09/2016     David LEROUX      Create version
01/02/2016     Boris Castellani  Modification  
------------------------------------------------------------*/
@isTest
public class SICo_OrderTracking_VFC_TEST {

  @isTest 
  static void doInit_Gas_Test() { 
    //Insert new community user

    User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
    insert communityUser1;
    communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

    //select account
    Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

    //Insert new site
    SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(communityUser1.AccountId);
    site1.IsActive__c = true;
    site1.HousingType__c = Label.SICo_House;
    insert site1;        
    
    //Insert new opportunity
    Opportunity testOpportunity1 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity1', communityUser1.AccountId, site1.Id);
    insert testOpportunity1;

    // Insert new Case 
    Case vCase = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, acc1.Id, testOpportunity1.Id, site1.Id);
    vCase.Subject = 'test Gaz';
    vCase.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'System'); // TODO Constant & label
    insert vCase; 

    //Insert Task
    ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_Gaz');
    Task vTask = SICo_UtilityTestDataSet.createTask('GA0001', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
    vTask.IsVisibleInSelfService = true;
    vTask.ShippingMode__c  = System.Label.SICo_Installateur;
    vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
    vTask.Subject = System.Label.SICo_ProvisionningGaz;
    vTask.RecordTypeId = rtTask;
    vTask.InterventionType__c = System.Label.SICo_OmegaMES;
    vTask.Status = 'Ouverte';
    vTask.InterventionPlannedDate__c = Date.today();
    vTask.RequestState__c = '';
    vTask.WhoId = communityUser1.ContactId;
    vTask.WhatId = vCase.Id;
    vTask.IsVisibleInSelfService = true;
    vTask.SiteID__c = site1.Id;
    vTask.Type = System.Label.SICo_ProvisionningGaz;
    insert vTask;

    //system.runAs(communityUser1) {

      Test.startTest();
      //set VF
        Test.setCurrentPageReference(new PageReference('Page.OrderTracking'));
        System.currentPageReference().getParameters().put('AccountId', acc1.Id);

        SICo_OrderTracking_VFC vController = new SICo_OrderTracking_VFC(communityUser1.AccountId);
        System.assert(vController.otw != null && vController.otw.isGasVisible == true, 'Otw gas is not visible whereas it should be');
        System.assert(vController.otw != null && vController.otw.gasOmegaRequestType == '1', 'Otw gasOmegaRequestType should be 1 but is : ' + vController.otw.gasOmegaRequestType);

        // reinit controller after having change the task omega type
        vTask.InterventionType__c = System.Label.SICo_OmegaCHF;
        update vTask;

        //set VF
        Test.setCurrentPageReference(new PageReference('Page.OrderTracking'));
        //set url parameter
        System.currentPageReference().getParameters().put('AccountId', communityUser1.AccountId);
        //set controller
        vController = new SICo_OrderTracking_VFC();

        System.assert(vController.otw != null && vController.otw.gasOmegaRequestType == '2', 'Otw gasOmegaRequestType should be 2 but is : ' + vController.otw.gasOmegaRequestType);
      Test.stopTest();
    //}

  }

  @isTest 
  static void doInit_Maintenance_Test() { 
    //Insert new community user

    User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
    insert communityUser1;
    communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

    //select account
    Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

    //Insert new site
    SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(communityUser1.AccountId);
    site1.IsActive__c = true;
    site1.HousingType__c = Label.SICo_House;
    insert site1;        
    
    //Insert new opportunity
    Opportunity testOpportunity1 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity1', communityUser1.AccountId, site1.Id);
    insert testOpportunity1;

    // Insert new Case 
    Case vCase = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, acc1.Id, testOpportunity1.Id, site1.Id);
    vCase.Subject = 'test maintenance';
    vCase.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'System'); // TODO Constant & label
    insert vCase; 

    //Insert Task
    ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
    Task vTask = SICo_UtilityTestDataSet.createTask('MA0002', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
    vTask.IsVisibleInSelfService = true;
    vTask.ShippingMode__c  = System.Label.SICo_Installateur;
    vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
    vTask.Subject = System.Label.SICo_ProvisionningGaz;
    vTask.RecordTypeId = rtTask;
    vTask.InterventionType__c = 'VISITE DE CONFORMITE';
    vTask.Status = 'Ouverte';
    vTask.InterventionPlannedDate__c = Date.today();
    vTask.RequestState__c = 'Annulée';
    vTask.WhoId = communityUser1.ContactId;
    vTask.WhatId = vCase.Id;
    vTask.IsVisibleInSelfService = true;
    vTask.SiteID__c = site1.Id;
    vTask.Type = 'Maintenance';
    insert vTask;

    //system.runAs(communityUser1) {

      Test.startTest();
        System.debug('#### Maintenance Test');
        SICo_OrderTracking_VFC vController = new SICo_OrderTracking_VFC(acc1.Id);
        System.assert(vController.otw != null && vController.otw.isMaintenanceVisible == true, 'Otw Maintenance is not visible whereas it should be');
      Test.stopTest();
    //}
  }

  @isTest 
  static void doInit_Maintenance_Cr_Test() { 
    //Insert new community user

    User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
    insert communityUser1;
    communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

    //select account
    Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

    //Insert new site
    SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(communityUser1.AccountId);
    site1.IsActive__c = true;
    site1.HousingType__c = Label.SICo_House;
    insert site1;        
    
    //Insert new opportunity
    Opportunity testOpportunity1 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity1', communityUser1.AccountId, site1.Id);
    insert testOpportunity1;

    // Insert new Case 
    Case vCase = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, acc1.Id, testOpportunity1.Id, site1.Id);
    vCase.Subject = 'test maintenance';
    vCase.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'System'); // TODO Constant & label
    insert vCase;

    SICo_CR__c vCr = SICo_UtilityTestDataSet.createCR();
    vCr.FirstReturnCode__c = 'DEVIS';
    insert vCr;

    //Insert Task
    ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
    Task vTask = SICo_UtilityTestDataSet.createTask('MA0002', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
    vTask.IsVisibleInSelfService = true;
    vTask.ShippingMode__c  = System.Label.SICo_Installateur;
    vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
    vTask.Subject = System.Label.SICo_ProvisionningGaz;
    vTask.RecordTypeId = rtTask;
    vTask.InterventionType__c = 'VISITE DE CONFORMITE';
    vTask.Status = 'Ouverte';
    vTask.InterventionPlannedDate__c = Date.today();
    vTask.RequestState__c = '';
    vTask.WhoId = communityUser1.ContactId;
    vTask.WhatId = vCase.Id;
    vTask.IsVisibleInSelfService = true;
    vTask.SiteID__c = site1.Id;
    vTask.Type = 'Maintenance';
    vTask.Cr__c = vCr.Id;
    insert vTask;

    //system.runAs(communityUser1) {

      Test.startTest();
        System.debug('#### Maintenance Test');
        SICo_OrderTracking_VFC vController = new SICo_OrderTracking_VFC(acc1.Id);
        System.assert(vController.otw != null && vController.otw.isMaintenanceVisible == true, 'Otw Maintenance is not visible whereas it should be');
      Test.stopTest();
    //}
  }

  @isTest 
  static void doInit_two_sites() { 
    //Insert new community user

    User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
    insert communityUser1;
    communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

    //select account
    Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

    //Insert new site
    SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(communityUser1.AccountId);
    site1.IsActive__c = true;
    site1.HousingType__c = Label.SICo_House;
    insert site1;

    SICo_Site__c site2 = SICo_UtilityTestDataSet.createSite(communityUser1.AccountId);
    site2.IsActive__c = true;
    site2.HousingType__c = Label.SICo_House;
    site2.IsSelectedInCustomerSpace__c = true;
    insert site2;       
    
    //Insert new opportunity
    Opportunity testOpportunity1 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity1', communityUser1.AccountId, site1.Id);
    insert testOpportunity1;

    // Insert new Case 
    Case vCase = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, acc1.Id, testOpportunity1.Id, site1.Id);
    vCase.Subject = 'test maintenance';
    vCase.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'System'); // TODO Constant & label
    insert vCase;

    SICo_CR__c vCr = SICo_UtilityTestDataSet.createCR();
    vCr.FirstReturnCode__c = 'test';
    insert vCr;

    //Insert Task
    ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
    Task vTask = SICo_UtilityTestDataSet.createTask('MA0002', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
    vTask.IsVisibleInSelfService = true;
    vTask.ShippingMode__c  = System.Label.SICo_Installateur;
    vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
    vTask.Subject = System.Label.SICo_ProvisionningGaz;
    vTask.RecordTypeId = rtTask;
    vTask.InterventionType__c = 'VISITE DE CONFORMITE';
    vTask.Status = 'Ouverte';
    vTask.InterventionPlannedDate__c = Date.today();
    vTask.RequestState__c = '';
    vTask.WhoId = communityUser1.ContactId;
    vTask.WhatId = vCase.Id;
    vTask.IsVisibleInSelfService = true;
    vTask.SiteID__c = site1.Id;
    vTask.Type = 'Maintenance';
    vTask.Cr__c = vCr.Id;
    insert vTask;

    //system.runAs(communityUser1) {

      Test.startTest();
        System.debug('#### Maintenance Test');
        SICo_OrderTracking_VFC vController = new SICo_OrderTracking_VFC(acc1.Id);
        System.assert(vController.otw != null && vController.otw.isMaintenanceVisible == true, 'Otw Maintenance is not visible whereas it should be');
      Test.stopTest();
    //}
  }

  @isTest 
  static void doInit_Equipement_Test() { 
    //Insert new community user

    User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
    insert communityUser1;
    communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

    //select account
    Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

    //Insert new site
    SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(communityUser1.AccountId);
    site1.IsActive__c = true;
    site1.HousingType__c = Label.SICo_House;
    insert site1;        
    
    //Insert new opportunity
    Opportunity testOpportunity1 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity1', communityUser1.AccountId, site1.Id);
    insert testOpportunity1;

    // Insert new Case 
    Case vCase = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, acc1.Id, testOpportunity1.Id, site1.Id);
    vCase.Subject = 'test equipement';
    vCase.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'System'); // TODO Constant & label
    insert vCase; 

    //Insert Task
    ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
    Task vTask = SICo_UtilityTestDataSet.createTask('IN0002', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
    vTask.IsVisibleInSelfService = true;
    vTask.ShippingMode__c  = System.Label.SICo_Installateur;
    vTask.DeliveryStatus__c  = '';
    vTask.Subject = System.Label.SICo_ProvisionningGaz;
    vTask.RecordTypeId = rtTask;
    vTask.InterventionType__c = 'VISITE DE CONFORMITE';
    vTask.Status = 'Ouverte';
    vTask.InterventionPlannedDate__c = Date.today();
    vTask.RequestState__c = '';
    vTask.WhoId = communityUser1.ContactId;
    vTask.WhatId = vCase.Id;
    vTask.IsVisibleInSelfService = true;
    vTask.SiteID__c = site1.Id;
    vTask.Type = 'Gaz';
    insert vTask;

    //system.runAs(communityUser1) {

      Test.startTest();
        System.debug('#### Equipement Test');
        SICo_OrderTracking_VFC vController = new SICo_OrderTracking_VFC(acc1.Id);
        System.assert(vController.otw != null && vController.otw.isEquipmentVisible == true, 'Otw equipment is not visible whereas it should be');
        System.assert(vController.otw != null && vController.otw.equipmentShippingMode  == '1', 'equipmentShippingMode is not 1');
        
        // Change task shipping mode andd reinit controller
        vTask.ShippingMode__c = System.Label.SICo_RelaisColis;
        update vTask;
        vController = new SICo_OrderTracking_VFC(acc1.Id);
        System.assert(vController.otw != null && vController.otw.equipmentShippingMode  == '2', 'equipmentShippingMode is not set to 2' );
        
        // Change task shipping mode andd reinit controller
        vTask.ShippingMode__c = 'Colissimo';
        update vTask;
        vController = new SICo_OrderTracking_VFC(acc1.Id);
        System.assert(vController.otw != null && vController.otw.equipmentShippingMode  == '3', 'equipmentShippingMode is not set to 3' );

      Test.stopTest();
    //}

  }

  @isTest 
  static void doInit_Equipement_Test_indexStatus() { 
    //Insert new community user

    User communityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
    insert communityUser1;
    communityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :communityUser1.Id LIMIT 1];

    //select account
    Account acc1 = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c FROM Account WHERE Id = :communityUser1.AccountId LIMIT 1];

    //Insert new site
    SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(communityUser1.AccountId);
    site1.IsActive__c = true;
    site1.HousingType__c = Label.SICo_House;
    insert site1;        
    
    //Insert new opportunity
    Opportunity testOpportunity1 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity1', communityUser1.AccountId, site1.Id);
    insert testOpportunity1;

    // Insert new Case 
    Case vCase = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, acc1.Id, testOpportunity1.Id, site1.Id);
    vCase.Subject = 'test equipement';
    vCase.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'System'); // TODO Constant & label
    insert vCase; 

    //Insert Task
    ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
    Task vTask = SICo_UtilityTestDataSet.createTask('IN0002', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
    vTask.IsVisibleInSelfService = true;
    vTask.ShippingMode__c  = System.Label.SICo_Installateur;
    vTask.DeliveryStatus__c  = System.Label.SICo_DispoRelaisColis;
    vTask.Subject = System.Label.SICo_ProvisionningGaz;
    vTask.RecordTypeId = rtTask;
    vTask.InterventionType__c = 'VISITE DE CONFORMITE';
    vTask.Status = 'Ouverte';
    vTask.InterventionPlannedDate__c = Date.today();
    vTask.RequestState__c = '';
    vTask.WhoId = communityUser1.ContactId;
    vTask.WhatId = vCase.Id;
    vTask.IsVisibleInSelfService = true;
    vTask.SiteID__c = site1.Id;
    vTask.Type = 'Gaz';
    insert vTask;

    //system.runAs(communityUser1) {

      Test.startTest();
        System.debug('#### Equipement Test');
        SICo_OrderTracking_VFC vController = new SICo_OrderTracking_VFC(acc1.Id);
        System.assert(vController.otw != null && vController.otw.isEquipmentVisible == true, 'Otw equipment is not visible whereas it should be');
        System.assert(vController.otw != null && vController.otw.equipmentShippingMode  == '1', 'equipmentShippingMode is not 1');
        
        // Change task shipping mode andd reinit controller
        vTask.ShippingMode__c = System.Label.SICo_RelaisColis;
        update vTask;
        vController = new SICo_OrderTracking_VFC(acc1.Id);
        System.assert(vController.otw != null && vController.otw.equipmentShippingMode  == '2', 'equipmentShippingMode is not set to 2' );
        
        // Change task shipping mode andd reinit controller
        vTask.ShippingMode__c = 'Colissimo';
        update vTask;
        vController = new SICo_OrderTracking_VFC(acc1.Id);
        System.assert(vController.otw != null && vController.otw.equipmentShippingMode  == '3', 'equipmentShippingMode is not set to 3' );

      Test.stopTest();
    //}

  }
}