global class SICo_DrupalUpsellConnectedAppPlugin extends Auth.ConnectedAppPlugin{

  //Return a user’s permission set assignments
  global override Map<String,String> customAttributes(Id userId, Id connectedAppId, Map<String,String> formulaDefinedAttributes, Auth.InvocationContext context)
  {  

    final Id accountId = [Select contact.AccountId from user where id =: Userinfo.getUserid()][0].contact.accountId;

    

    /* SITE
    Surface
    CAR du contrat Gaz
    Marque de la chaudière
    Modèle de la chaudière
    Chauffage alternatif
    Année de construction
    Agence CHAM - Installation
    Agence CHAM - Maintenance
    */
    // TODO fields
    final SICo_Site__c theSite = [Select Id, CHAMAgencyInstallation__c, CHAMAgencyEntretien__c FROM SICo_Site__c WHERE Subscriber__c = :accountId];


    /*
    ELIGIBILTY
    Résultat d'éligibilité
    Eligible au gaz
    Code Insee
    Eligible au thermostat
    Eligible à CHAM
    */
    // TODO query => add to the structure


    /*
    SOUSCRIPTION
    Produits détenus
    */  
    // TODO fields
    final List<Zuora__Subscription__c> subscriptions = [Select Id FROM Zuora__Subscription__c WHERE SiteId__c  = :theSite.id];

    final JSONUserInfo customerInfo = new JSONUserInfo(theSite, subscriptions);

    formulaDefinedAttributes.put('user_info_sico', JSON.serialize(customerInfo));
    return formulaDefinedAttributes;

  }


  private class JSONUserInfo {
    protected SICo_Site__c theSite;
    protected List<Zuora__Subscription__c> subscriptions;

    public JSONUserInfo(final SICo_Site__c theSite, final List<Zuora__Subscription__c> subscriptions){
      this.theSite = theSite;
      this.subscriptions = subscriptions;
    }

  }

}