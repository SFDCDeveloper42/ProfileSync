({
	doInit : function(pComponent, pEvent, pHelper) { 
		var vGetMOW = pComponent.get("c.getMOW"); 
		var vAccountId = pComponent.get("v.accountId"); 
		if (vGetMOW != null) {
			vGetMOW.setParams({ "pAccountId" : vAccountId});
			vGetMOW.setCallback(pComponent, function(pResponse) {
				var vState = pResponse.getState();
				if (vState === "SUCCESS") {   
					pComponent.set("v.mow", pResponse.getReturnValue());			
					console.log(pResponse.getReturnValue()); 
				} else { 
					pComponent.set("v.mow", null);
				}
			});
			$A.enqueueAction(vGetMOW);
		}
	}
})