({
	updateInformations : function (component, event, pHelper) {
		component.set("v.epw.electricityTariffOption", $("input[name='electricityTariffOption']:checked").val());
		component.set("v.epw.subscribedPower", $("input[name='subscribedPower']:checked").val());

		var option = component.get("v.epw.electricityTariffOption");
		var power = component.get("v.epw.subscribedPower");
		// "Disable" the validate button
		pHelper.disableValidate(); 
		if (option == undefined || power == undefined) {
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorNoValue"), "Error_12");    
		} else {
			// Save informations entered at screen on the current site
			var action = component.get("c.setInformations_12");
			action.setParams({
				siteId : component.get("v.epw.Id"),
				electricityTariffOption : component.get("v.epw.electricityTariffOption"),
				subscribedPower : component.get("v.epw.subscribedPower")
			});
			action.setCallback(this, function (response) {
				// Call consomation evaluation service
				var actionConso = component.get("c.getEvalConso");
				actionConso.setParams({
					siteId : component.get("v.epw.Id")
				});
				actionConso.setCallback(this, function (response) {		
					var idAccount = component.get("v.accountId");			
					var ismb = component.get("v.isMobile");
					window.location.href="/apex/EnergyProfile_13?AccountId=" + idAccount + (component.get("v.isMobile") ? '&isMobile=1' : '');
				});
				$A.enqueueAction(actionConso); 
			});
			$A.enqueueAction(action);
		}
	},	  
	
	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep(pComponent.get("v.epw.heating"));
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_12");
	}
})