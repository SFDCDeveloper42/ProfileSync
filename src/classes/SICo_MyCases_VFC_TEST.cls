/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Test class of the Controller for the Cases page (Cases)
History
26/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
@isTest
public class SICo_MyCases_VFC_TEST {

	//set test data
	@testSetup
	static void testData() {

		User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser;

        User us = [SELECT accountId FROM User WHERE Id =:communityUser.Id LIMIT 1];

        Case newCase = new Case(
            AccountId = us.accountId,
            Status = 'New',
            Subject__c = 'Offres & Services',
            Heading__c = 'Gaz / GRDF',
            RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'CustomerDemand')
        );
        insert newCase;
        
        newCase = new Case(
            AccountId = us.accountId,
            Status = 'New',
            Subject__c = 'Offres & Services',
            Heading__c = 'Gaz / GRDF'
        );
        insert newCase;
	}

	@isTest
	static void testCasesWithoutURLId() {

		User us = [SELECT Id, accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        //Run as Community User
	    system.runAs(us){
	        Test.startTest();
	            SICo_MyCases_VFC controller = new SICo_MyCases_VFC();
	            PageReference pRef = Page.Cases;
		        Test.setCurrentPageReference(pRef);

		        System.assertNotEquals(controller.listCases.size(), 0);
		        System.assertNotEquals(controller.listCaseWrapper.size(), 0);
	        Test.stopTest();
        }
	}
    
    @isTest
	static void testCasesWithURLId() {

		User us = [SELECT Id, accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        String accountId = us.accountId;

        //Run as Community User
	    system.runAs(us){
	        Test.startTest();
	            SICo_MyCases_VFC controller = new SICo_MyCases_VFC();
	            PageReference pRef = Page.Cases;
            	pRef.getParameters().put('accountId', accountId);
            	System.Debug(pRef.getUrl());
		        Test.setCurrentPageReference(pRef);

		        System.assertNotEquals(controller.listCases.size(), 0);
		        System.assertNotEquals(controller.listCaseWrapper.size(), 0);
	        Test.stopTest();
        }
	}

}