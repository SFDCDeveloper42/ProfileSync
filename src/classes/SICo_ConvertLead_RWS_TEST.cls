/**
* @author Boris Castellani
* @date 18/01/2017
*
* @description  TEST Class SICo_ConvertLead_RWS & SICo_LeadService
*/

@isTest
private class SICo_ConvertLead_RWS_TEST {

  @testSetup
  static void initializeData() {
    final Integer nbrDay = Integer.valueOf(label.SICo_weeklyCalendars_Number)*Integer.valueOf(label.SICo_weeklyCalendars_Days);
    Id rtAccount = SICo_Utility.m_RTbyDeveloperName.get('Account_Agency');
    Account prestaAccount = new Account(Name='CHAM', RecordTypeId=rtAccount);
    prestaAccount.MondayTimeRange1__c = 4; prestaAccount.MondayTimeRange2__c = 4;
    prestaAccount.MondayTimeRange3__c = 4; prestaAccount.MondayTimeRange4__c = 4;
    prestaAccount.TuesdayTimeRange1__c = 4; prestaAccount.TuesdayTimeRange2__c = 4;
    prestaAccount.TuesdayTimeRange3__c = 4; prestaAccount.TuesdayTimeRange4__c = 4;
    prestaAccount.WednesdayTimeRange1__c = 4; prestaAccount.WednesdayTimeRange2__c = 4;
    prestaAccount.WednesdayTimeRange3__c = 4; prestaAccount.WednesdayTimeRange4__c = 4;
    prestaAccount.ThursdayTimeRange1__c = 4; prestaAccount.ThursdayTimeRange2__c = 4;
    prestaAccount.ThursdayTimeRange3__c = 4; prestaAccount.ThursdayTimeRange4__c = 4;        
    prestaAccount.FridayTimeRange1__c = 4; prestaAccount.FridayTimeRange2__c = 4;
    prestaAccount.FridayTimeRange3__c = 4; prestaAccount.FridayTimeRange4__c = 4;            
    prestaAccount.SaturdayTimeRange1__c = 4; prestaAccount.SaturdayTimeRange2__c = 4;
    prestaAccount.SaturdayTimeRange3__c = 4; prestaAccount.SaturdayTimeRange4__c = 4;   
    INSERT prestaAccount;
    SICo_ChamJetonService_CLS chamJeton = new SICo_ChamJetonService_CLS();
    chamJeton.initCalendars(new List<Account>{prestaAccount});
  }

  static testMethod void convertLead() {        
    INSERT SICo_UtilityTestDataSet.createZuoraWSConfig();
    // Get Agency
    Account agencyAccount = [SELECT Id FROM Account Limit 1];   
    // Create Product2
    Product2 product = SICo_UtilityTestDataSet.createSalesforceProduct('1');
    INSERT product;
    zqu__ZProduct__c productZuora = SICo_UtilityTestDataSet.createZuoraProduct('1',product.id);
    INSERT productZuora;
    zqu__ProductRatePlan__c productPlan = SICo_UtilityTestDataSet.createZuoraProductRatePlan('1',product.id,productZuora.id, 'SE');
    INSERT productPlan;
    zqu__ProductRatePlanCharge__c productPlanCharge = SICo_UtilityTestDataSet.createZuoraProductRatePlanCharge('1',productPlan.id);
    INSERT productPlanCharge;
    // Create Lead - Create Basket - Ayant Droit
    Lead lead= SICo_UtilityTestDataSet.createLead(agencyAccount.Id, 'Colissimo');
    lead.CHAMInstallationAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
    lead.CHAMEntretienAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
    lead.CHAMInstallationTimeSlot__c= '08:00 - 10:00';
    lead.CHAMEntretienTimeSlot__c= '08:00 - 10:00';
    lead.PDL__c='123456798';
    INSERT lead;
    SICo_Basket__c sbasket= SICo_UtilityTestDataSet.createPanier(lead.id,'2c92c0f855a0a9d60155b65c8eb55e41','Colissimo','2c92c0f855a0a9d60155b65c8ec65e45','Frais de livraison');
    sbasket.ObjectType__c = 'Station';
    INSERT sbasket;
    SICo_LeadBeneficiary__c slead = SICo_UtilityTestDataSet.createLeadBeneficiary(lead.id,'Anaelle','Boomi','Mme');
    INSERT slead;

    Test.startTest();
        SICo_ConvertLead_RWS.doPut(lead.Id);
    Test.stopTest();
    List<Account> personalAccount = [SELECT id,RecordType.Name FROM account where ContactAddressFlat__c= :lead.ContactAddressFlat__c];
    system.debug('lead.personalAccount '+personalAccount.size());
    System.assertEquals(1,personalAccount.size());
  }

  static testMethod void convertLeadConverted() {
    INSERT SICo_UtilityTestDataSet.createZuoraWSConfig();
    Account agencyAccount = [SELECT Id FROM Account Limit 1];   
    Product2 product = SICo_UtilityTestDataSet.createSalesforceProduct('1');
    INSERT product;
    zqu__ZProduct__c productZuora = SICo_UtilityTestDataSet.createZuoraProduct('1',product.id);
    INSERT productZuora;
    zqu__ProductRatePlan__c productPlan = SICo_UtilityTestDataSet.createZuoraProductRatePlan('1',product.id,productZuora.id, 'SE');
    INSERT productPlan;
    zqu__ProductRatePlanCharge__c productPlanCharge = SICo_UtilityTestDataSet.createZuoraProductRatePlanCharge('1',productPlan.id);
    INSERT productPlanCharge;
    Lead lead= SICo_UtilityTestDataSet.createLead(agencyAccount.Id, 'Colissimo');
    lead.CHAMInstallationAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
    lead.CHAMEntretienAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
    lead.CHAMInstallationTimeSlot__c= '08:00 - 10:00';
    lead.CHAMEntretienTimeSlot__c= '08:00 - 10:00';
    INSERT lead;
    SICo_Basket__c sbasket= SICo_UtilityTestDataSet.createPanier(lead.id,'2c92c0f855a0a9d60155b65c8eb55e41','Colissimo','2c92c0f855a0a9d60155b65c8ec65e45','Frais de livraison');
    sbasket.ObjectType__c = 'Station';
    INSERT sbasket;
    SICo_ConvertLead_RWS.doPut(lead.Id);

    Test.startTest();
        SICo_ConvertLead_RWS.doPut(lead.Id);
    Test.stopTest();
    List<Account> personalAccount = [SELECT id,RecordType.Name FROM account where ContactAddressFlat__c= :lead.ContactAddressFlat__c];
    System.assertEquals(1,personalAccount.size());
  }

  static testMethod void convertLeadAmend() {
    INSERT SICo_UtilityTestDataSet.createZuoraWSConfig();
    Account agencyAccount = [SELECT Id FROM Account Limit 1]; 
    Product2 product = SICo_UtilityTestDataSet.createSalesforceProduct('1');
    INSERT product;
    zqu__ZProduct__c productZuora = SICo_UtilityTestDataSet.createZuoraProduct('1',product.id);
    INSERT productZuora;
    zqu__ProductRatePlan__c productPlan = SICo_UtilityTestDataSet.createZuoraProductRatePlan('1',product.id,productZuora.id, 'SE');
    INSERT productPlan;
    zqu__ProductRatePlanCharge__c productPlanCharge = SICo_UtilityTestDataSet.createZuoraProductRatePlanCharge('1',productPlan.id);
    INSERT productPlanCharge;
    Lead lead= SICo_UtilityTestDataSet.createLead(agencyAccount.Id, 'Colissimo');
    lead.CHAMInstallationAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
    lead.CHAMEntretienAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
    lead.CHAMInstallationTimeSlot__c= '08:00 - 10:00';
    lead.CHAMEntretienTimeSlot__c= '08:00 - 10:00';
    INSERT lead;

    SICo_Basket__c sbasket= SICo_UtilityTestDataSet.createPanier(lead.id,'2c92c0f855a0a9d60155b65c8eb55e41','Colissimo','2c92c0f855a0a9d60155b65c8ec65e45','Frais de livraison');
    sbasket.ObjectType__c = 'Station';
    INSERT sbasket;
    SICo_ConvertLead_RWS.doPut(lead.Id);

    Lead lead2= SICo_UtilityTestDataSet.createLead(agencyAccount.Id, 'Colissimo');
    lead.CHAMInstallationAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
    lead.CHAMEntretienAppointmentDate__c= Date.today().toStartOfWeek().addDays(3);
    lead.CHAMInstallationTimeSlot__c= '08:00 - 10:00';
    lead.CHAMEntretienTimeSlot__c= '08:00 - 10:00';
    INSERT lead2;
    
    SICo_Basket__c sbasket2 = SICo_UtilityTestDataSet.createPanier(lead2.id,'2c92c0f855a0a9d60155b65c8eb55e41','Colissimo','2c92c0f855a0a9d60155b65c8ec65e45','Frais de livraison');
    sbasket.ObjectType__c = 'Station';
    INSERT sbasket2;

    Test.startTest();
        SICo_ConvertLead_RWS.doPut(lead2.Id);
    Test.stopTest();
    List<Account> personalAccount = [SELECT id,RecordType.Name FROM account where ContactAddressFlat__c= :lead.ContactAddressFlat__c];
    System.assertEquals(1,personalAccount.size());
  }

  static testMethod void convertLeadBadId() {
    INSERT SICo_UtilityTestDataSet.createZuoraWSConfig();
    Account agencyAccount = [SELECT Id FROM Account Limit 1]; 
    Test.startTest();
        SICo_ConvertLead_RWS.doPut(agencyAccount.Id);
    Test.stopTest();
  }
}