/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Controller for the CancelContract Page 
Test Class:    SICo_CancelContract_VFC_TEST
History
07/09/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
public with sharing class SICo_CancelContract_VFC {

    public Id accountId{get;set;}

    public String objet{get;set;}
    public String timeSlots{get;set;}

    public List<SICo_CaseMotif__mdt> listMotif{get;set;}
    public List<SICo_CaseObjet__mdt> listObjet{get;set;}
    public List<WebCallBack__mdt> listWCB{get;set;}

    public String linkMyContracts{get;set;}

    public String labelTypeContract{set;get;}

    public String conType{get;set;}

    public SICo_CancelContract_VFC() {

        //Get AccountId
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        conType = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_CONTRACT_TYPE);

        if(requestedAccountId != null && requestedAccountId != '')
            linkMyContracts = Page.Contracts.getURL() + '?accountId=' + requestedAccountId;
        else
            linkMyContracts = Page.Contracts.getURL();
                
        try{
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }catch (exception e){
            System.debug('Unable to return accountId');
        }

        if(accountId != null){
            labelTypeContract = '';
            if(conType == 'gas'){
                labelTypeContract = '\nType de contrat : ' + Label.SICo_SoweeContract;
            }
            else if(conType == 'maint'){
                labelTypeContract = '\nType de contrat : ' + Label.SICo_MaintenanceContract;
            }

            listWCB = [SELECT CallBackTime__c 
                        FROM WebCallBack__mdt];

            String caseSubject = SICo_Constants_CLS.CANCEL_SUBJECT_DEVNAME;
            listMotif = [SELECT Id, MasterLabel
                        FROM SICo_CaseMotif__mdt
                        WHERE DeveloperName LIKE:caseSubject
                        LIMIT 1];

            if(listMotif != null && listMotif.size() != 0)
                listObjet = [SELECT Id, SalesforceLabel__c, Case_Motif__c
                            FROM SICo_CaseObjet__mdt
                            WHERE Case_Motif__c = :listMotif.get(0).Id
                            LIMIT 1000];
        }
        else{
            listMotif = new List<SICo_CaseMotif__mdt>();
            listObjet = new List<SICo_CaseObjet__mdt>();
            listWCB = new List<WebCallBack__mdt>();
        }

    }

    public PageReference createCase(){

        String defaultSubject = '';
        if(listMotif != null && listMotif.size() != 0) defaultSubject = this.listMotif.get(0).MasterLabel;

        Case newCase = new Case(
            RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + SICo_Constants_CLS.CUSTOMER_DEMAND_CASE_RECORD_TYPE),
            AccountId = this.accountId,
            Status = SICo_Constants_CLS.STATUS_PICKLIST_VALUE_NEW,
            Subject__c = defaultSubject,
            Heading__c = this.objet,
            Description = this.getDescription(),
            Origin = SICo_Constants_CLS.ORIGIN_PICKLIST_VALUE_WEB
        );
        insert newCase;


        PageReference pageRef = new PageReference(linkMyContracts);
        pageRef.setRedirect(true);
        return pageRef;

    }

    public String getDescription() {
        if(this.conType == 'gas')
           return 'Motif : ' + this.objet + '\nType de contrat : Gaz' +'\nCréneau de rappel souhaité : ' + timeSlots + labelTypeContract;

        else if(conType == 'maint')
            return 'Motif : ' + this.objet + '\nType de contrat : Maintenance' +'\nCréneau de rappel souhaité : ' + timeSlots + labelTypeContract;

        else
            return 'Motif : ' + this.objet +'\nCréneau de rappel souhaité : ' + timeSlots + labelTypeContract;
    }

    public List<SelectOption> getObjects() {

        List<SelectOption> options = new List<SelectOption>();

        if(listObjet != null){
            for(SICo_CaseObjet__mdt obj : listObjet)
                options.add(new SelectOption(obj.SalesforceLabel__c, obj.SalesforceLabel__c));
        }
 
        return options;
    }

    public List<SelectOption> getWcb() {

        List<SelectOption> options = new List<SelectOption>();

        for(WebCallBack__mdt obj : listWCB)
            options.add(new SelectOption(obj.CallBackTime__c, obj.CallBackTime__c));
 
        return options;
    }

    public PageReference cancelCancel(){
        PageReference pageRef = new PageReference(linkMyContracts);
        pageRef.setRedirect(true);
        return pageRef;
    }

}