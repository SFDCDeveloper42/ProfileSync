({  
	updateInformations : function (component, event, pHelper) { 
		var idAccount = component.get("v.accountId");
		component.set("v.epw.heating", $("input[name='heating']:checked").val());
		component.set("v.epw.age", $("input[name='age']:checked").val());
		
		var vHeating = component.get("v.epw.heating");
		var vAge = component.get("v.epw.age");
		if (vHeating == 'Gaz' && vAge == null) {
			pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorNoValue"), "Error_07");
		} else {
			// "Disable" the validate button
			pHelper.disableValidate();  		
			//put the values in the apex controller parameters
			var action = component.get("c.setInformations_07");
			action.setParams({
				siteId : component.get("v.epw.Id"),
				heating : component.get("v.epw.heating"),
				age : component.get("v.epw.age")
			});
			//redirect to the next page
			action.setCallback(this, function (response) {
				var heating = component.get("v.epw.heating");
				if (heating == undefined) {
					pHelper.displayError(component, $A.get("$Label.c.SICo_ErrorNoValue"), "Error_07");    
				} else { 
					if (heating == "Autre") {  
						$('#modal07')[0].style.display = 'block'; //workaround
						$('#modal07').modal('show');
					} else { 
						pHelper.moveToNextStep();
					}
				}
			});
			$A.enqueueAction(action);
		}
	},

	showDiv : function (component, event, helper) {
		//show the div with the class age
		$(".age").show();
	},

	hideDiv : function (component, event, helper) {
		//hide the div with the class age
		$(".age").hide();
	},	
 	
	moveToPreviousStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToPreviousStep();
		return false;
	},

	moveToNextStep : function(pComponent, pEvent, pHelper) {
		pHelper.moveToNextStep();
		return false;
	},

	resetValidation : function(pComponent, pEvent, pHelper) {
		pHelper.enableValidate();
		pHelper.resetErrorContainer(pComponent, "Error_07");
	}
})