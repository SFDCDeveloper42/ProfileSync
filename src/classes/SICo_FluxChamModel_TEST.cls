@isTest
private class SICo_FluxChamModel_TEST {

	@isTest static void serialiseChamClientContrat() {
		SICo_FluxChamModel.ChamClientContrat chamClientContrat = SICo_UtilityFluxDataSet.createFluxChamClientContrat();
		String responceJSON = JSON.serialize(chamClientContrat);
		
		SICo_CR__c cr = SICo_UtilityTestDataSet.createCR();
		insert cr;
		
		SICo_Anomaly__c ano = SICo_UtilityTestDataSet.createANO(cr);
		insert ano; 
		
		Test.startTest();
			SICo_FluxChamModel.ChamClientContrat newClientContrat = SICo_UtilityFluxDataSet.createFluxChamClientContrat();
			newClientContrat.parse(responceJSON);
			System.assertEquals(newClientContrat.chamClient.get(0).nomclient, 'Boomi');
		Test.stopTest();
	}

	@isTest static void serialiseChamRDV() {
		SICo_FluxChamModel.ChamRDV chamRDV = SICo_UtilityFluxDataSet.createFluxChamRDV();
		String responceJSON = JSON.serialize(chamRDV);

		Test.startTest();
			SICo_FluxChamModel.ChamRDV newChamRDV = SICo_UtilityFluxDataSet.createFluxChamRDV();
			newChamRDV.parse(responceJSON);
			System.assertEquals(newChamRDV.chamDemande.get(0).periode, 'M');
		Test.stopTest();
		}

	@isTest static void serialiseChamToken() {
		SICo_FluxChamModel.ChamToken chamToken = new SICo_FluxChamModel.ChamToken();
		System.assertEquals(chamToken.iss, SICo_Constants_CLS.STR_CHAMTOKEN_ISS);
	}
}