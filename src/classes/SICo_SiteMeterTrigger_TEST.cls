/**
* @author Boris Castellani
* @date 17/10/2016
*
* @description TEST SICo_SiteMeterTrigger_CLS Call by trigger
*/

@isTest
private class SICo_SiteMeterTrigger_TEST {

    static testMethod void handleBeforeUpsert() {

    	//DataSet
    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;

		final List<SICo_Site__c> sites = new List<SICo_Site__c>();	
    	sites.add(SICo_UtilityTestDataSet.createSite(acc.Id));
    	sites.add(SICo_UtilityTestDataSet.createSite(acc.Id));
    	Insert sites;

    	final List<SICo_Meter__c> meters = new List<SICo_Meter__c>();
    	meters.add(SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PCE)); 
    	meters.add(SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PCE)); 
    	meters.add(SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PDL)); 
    	meters.add(SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PCE)); 
    	meters.add(SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PCE));
    	meters.add(SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PDL));
    	meters.add(SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PDL));
    	Insert meters;

    	final List<SICo_SiteMeter__c> siteMeters = new List<SICo_SiteMeter__c>();
    	//first site
    	siteMeters.add(SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE, sites.get(0).Id, meters.get(0).Id));
    	siteMeters.add(SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PDL, sites.get(0).Id, meters.get(2).Id));
    	//second site
    	siteMeters.add(SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE, sites.get(1).Id, meters.get(3).Id));
    	siteMeters.add(SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PDL, sites.get(1).Id, meters.get(5).Id));
    	Insert siteMeters;

		// TEST & ASSERT
		Test.startTest();
    		final List<SICo_SiteMeter__c> newSiteMeters = new List<SICo_SiteMeter__c>();
	    	newSiteMeters.add(SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE, sites.get(0).Id, meters.get(1).Id));
			newSiteMeters.add(SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE, sites.get(1).Id, meters.get(4).Id));
			newSiteMeters.add(SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PDL, sites.get(1).Id, meters.get(6).Id));
			Insert newSiteMeters;

			//ASSERT
			final SICo_SiteMeter__c assertSiteMeter = [SELECT Id, IsActive__c FROM SICo_SiteMeter__c where Id=:siteMeters.get(0).Id Limit 1];
			System.assertEquals(false,assertSiteMeter.IsActive__c);
			final SICo_Site__c assertSite = [SELECT Id, ActiveSiteMeterGas__c FROM SICo_Site__c where Id=:sites.get(1).Id Limit 1];
			System.assertEquals(newSiteMeters.get(1).Id,assertSite.ActiveSiteMeterGas__c);
		Test.stopTest();


    }
}