/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Controller for the Community Forgot Password page (ForgotPassword)
Test Class:    SICo_ForgotPassword_VFC_TEST
History
04/08/2016      Mehdi Cherfaoui      Create version
27/10/2016      Selim BEAUJOUR       Test if the user is an agent or a customer for the reset password
------------------------------------------------------------*/
public with sharing class SICo_ForgotPassword_VFC {

    public transient String email {get; set;} //Email of the user who forgot his password
    public String startUrl {get; set;} //startURL parameter in the URL
    public String errorMessage {get; set;}
    public Id accountId {get;set;}
    public Id userId {get;set;}
    public Boolean isRegistrationAllowed {get; set;}

    public Boolean isFromDrupal {get;set;}


    public SICo_ForgotPassword_VFC(){

        this.startUrl = System.currentPageReference().getParameters().get('startURL');
        this.userId = System.currentPageReference().getParameters().get('userId');
        this.email = System.currentPageReference().getParameters().get('email');
        this.accountId =System.currentPageReference().getParameters().get('accountid');
        this.isRegistrationAllowed = SICo_UtilityCommunities_CLS.registrationUrl(startUrl) != '' ;

        if(Apexpages.currentPage().getUrl().contains('source%3D')){
            this.isFromDrupal = true;
        }
        else{
            this.isFromDrupal = false;
        }
    }


    public PageReference forgotPassword() {
        
        
        PageReference pr = apexpages.Currentpage();
        //Get username associated with this user

        String trimedEmail = email.trim();
        String username = SICo_CommunitiesLogin_VFC.getUsername(trimedEmail);
        if(username != null)
        {//Execute forgot password
            if (SICo_UtilityCommunities_CLS.isInternalUser() && userId != null){
                //If the password is reset by an agent, use System.restPassword
                System.resetPassword(userId, true);
                pr = Page.PersonalInformations;
                pr.getParameters().put('accountid', accountid);
                pr.setRedirect(true);
            }
            else{ //Use Site.forgotPassword()
                
                Site.forgotPassword(username);
                pr = Page.ForgotPasswordConfirm;   
                if(isFromDrupal){
                    pr.getParameters().put('isFromDrupal', 'true');
                }
                pr.setRedirect(true);
            
            }
        }else{//If no username is found, display an error message
            ApexPages.Message msgNoUsername = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SICo_UnkownUsername);
            ApexPages.addMessage(msgNoUsername);
            errorMessage = Label.SICo_UnkownUsername;
        }
        return pr;
    }

    /**
     * [Registration link calculation]
     * @return [URL of the registration page to use]
     */
    public String register() {
       return SICo_UtilityCommunities_CLS.registrationUrl(startUrl);
    }

    public PageReference closeErrorPopup(){
        errorMessage = '';
        return null;
    }

}