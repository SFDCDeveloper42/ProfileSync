/**
* @author Boris Castellani
* @date 23/11/2016
*
* @description Task Trigger Before update
*/
trigger SICo_TaskBeforeUpdate_TRG on Task(before update) {
	if(PAD.canTrigger('SICo_TaskBeforeUpdate_TRG')) {
		SICo_TaskTrigger_CLS.handleBeforeUpsert(trigger.new);
	}    
}