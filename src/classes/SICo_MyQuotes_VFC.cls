/*------------------------------------------------------------
Author:        David LEROUX
Company:       Ikumbi Solutions
Description:   Controller for MyQuotes page.
Test Class:    SICo_MyQuotes_VFC
History
21/10/2016     David LEROUX     Create version
------------------------------------------------------------*/
public with sharing class SICo_MyQuotes_VFC {
	
	// Account linked
	public String accountId { get; set; }

	public SICo_MyQuotes_VFC() {    
		String vAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        doInit(vAccountId);
    } 

    /**
    * Initialize the account id
    * @Param : pAccountId :  id of the current account
    */
    private void doInit(String pAccountId) {
        try {
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(pAccountId);
        } catch (exception e){
            System.debug('Unable to return accountId');
        }
    }

	/**
	* Return the list of quote of the current account
	* @Param : pAccountId :  id of the current account
	*/
	@AuraEnabled
	public static List<SICo_Quotes_Helper.QuoteWrapLite> queryQuotes(String pAccountId) {
		if (pAccountId == null) {
			return null;
		}
		List<SICo_Quotes_Helper.QuoteWrapLite> vQuotesList = SICo_Quotes_Helper.getQuotesWrappedByAccountId(pAccountId);

		return vQuotesList;
	}

}