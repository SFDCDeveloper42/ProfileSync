<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Devis_CHAM_disponible</fullName>
        <description>Devis CHAM disponible</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HTML_Devis_CHAM_disponible</template>
    </alerts>
    <rules>
        <fullName>SiCo - Devis Cham disponible</fullName>
        <actions>
            <name>Devis_CHAM_disponible</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MaintenancyQuote__c.MaintenancyQuoteStatus__c</field>
            <operation>equals</operation>
            <value>A valider</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
