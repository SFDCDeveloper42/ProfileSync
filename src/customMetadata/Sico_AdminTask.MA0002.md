<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MA0002</label>
    <protected>false</protected>
    <values>
        <field>BlockingCBPayment__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Cases__c</field>
        <value xsi:type="xsd:string">MAINT01</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Domain__c</field>
        <value xsi:type="xsd:string">Cham</value>
    </values>
    <values>
        <field>EndPointType__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>EndPoint__c</field>
        <value xsi:type="xsd:string">/cham/v1/demande/</value>
    </values>
    <values>
        <field>IsVisibleEspaceClient__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Parameters__c</field>
        <value xsi:type="xsd:string">MAINT;VISITE DE CONFORMITE</value>
    </values>
    <values>
        <field>ProvisioningCallOut__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">TaskProvisionning_CHAM</value>
    </values>
    <values>
        <field>TaskOrder__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>TaskSubject__c</field>
        <value xsi:type="xsd:string">Prise d&apos;un RDV de maintenance avec l&apos;agence CHAM</value>
    </values>
    <values>
        <field>TaskTechnique__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
