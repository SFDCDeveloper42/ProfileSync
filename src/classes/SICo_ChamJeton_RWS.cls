/**
* @author Boris Castellani
* @date 01/02/2017
* @LastModify 
*
* @description 
*/
@RestResource(urlMapping='/v1/ChamJeton')
global with sharing class SICo_ChamJeton_RWS {

  @HttpPost
  global static ChamJetonResponse doPost(Id accountId) {
  	final ChamJetonResponse aChamJetonResponse = new ChamJetonResponse();
		try{
      final SICo_ChamJetonService_CLS.SICo_accountDomain accountDomain = new SICo_ChamJetonService_CLS.SICo_accountDomain(accountId);
      if(!accountDomain.isAccount()) {
        throw new NotAccountException();
      }
      final SICo_ChamJetonService_CLS chamJeton = new SICo_ChamJetonService_CLS();
      aChamJetonResponse.token = chamJeton.buildJsonWeeklyCalendars(accountId);
      
    }catch(AccountException e) {
      aChamJetonResponse.result = false;
      aChamJetonResponse.message = e.getMessage();
    }catch(SICo_ChamJetonService_CLS.ChamTokenException e){
      aChamJetonResponse.result = false;
      aChamJetonResponse.message = e.getMessage();
    }catch(Exception e){
      aChamJetonResponse.result = false;
      aChamJetonResponse.message = 'Unexpected error';
		}
   
    return aChamJetonResponse;
  }

	global class ChamJetonResponse {
  	public Boolean result = true;
  	public String message;
    public SICo_ChamJetonModel.ChamJeton token;
	}

  global virtual class AccountException extends Exception {
  } 

  global class NotAccountException extends AccountException {
    public override String getMessage(){
      return 'Wrong id';
    }
  }
}