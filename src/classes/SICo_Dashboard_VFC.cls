/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Controller for Dashboard page
Test Class:    SICo_Dashboard_VFC_TEST
History
06/09/2016     Gaël BURNEAU      Create version
------------------------------------------------------------*/
public with sharing class SICo_Dashboard_VFC {

	// Number of activities to display
	private static Integer MAX_ACTIVITIES_DISPLAYED = 5;
    private static Decimal invoicelong = 9999.99;

	//AccountId for filtering all data displayed on the page
    public Id accountId {get;set;}

	public SICo_PersonalInformation_VFC.PersonalInformationsWrapper piw {get;set;}

	public SICo_MyContracts_VFP.SoweeContractWrapper soweeContract {get;set;}
	public SICo_MyContracts_VFP.MaintenanceContractWrapper maintenanceContract {get;set;}

    public SICo_Offers_VFC.MyOffersWrapper mow {get; set;}

	public Boolean isSoweeContract {get;set;}
	public Boolean isMaintenanceContract {get;set;}

	public String linkDownloadAttestation {get;set;}

    public Zuora__ZInvoice__c oZuoraZInvoice {get;set;}
    public Id invoiceAttachmentId {get;set;}
    public String sInvoiceDate {get;set;}
    public Decimal dInvoiceAmount {get;set;}
    public Boolean longInvoice{get;set;}
    public String sNextInvoiceDate {get;set;}
    public Boolean isUnpaid {get;set;}
    public Boolean isAwaitingTransfer {get;set;}
    public Boolean isPaymentInProgress {get;set;}
	public Boolean hasSubscriptionContract { get; set; }
    
    public String equipmentEventDateAndTime {get;set;}
    public Decimal totalUnpaidBillsAmount { get; set; }
    public Integer numberUnpaidBills { get; set; }

    //Offers  
    public Boolean hasContract {get; set;}
    public Boolean isPersonAccount {get;set;}

	/**
     * [Contructor]
     */
    public SICo_Dashboard_VFC() {    	
		//Get AccountId
		String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);

        try {
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        } catch (Exception e) {
            System.debug('Unable to return accountId');
        }
        
        try {
            //Call and load informations from Personal Informations page controller
    		SICo_PersonalInformation_VFC piController = new SICo_PersonalInformation_VFC(accountId);
    		this.piw =  piController.piw;
            this.isPersonAccount =  piController.piw.isPersonAccount;
            System.debug('### isPersonAccount VFC = ' + piController.piw.isPersonAccount );
                

            //Call and load informations from Contracts page controller
    		SICo_MyContracts_VFP myContractsController = new SICo_MyContracts_VFP(accountId);
            this.isSoweeContract = myContractsController.isSoweeContract;
    		this.soweeContract = myContractsController.soweeContract;
    		this.maintenanceContract = myContractsController.maintenanceContract;
    		this.linkDownloadAttestation = myContractsController.linkDownloadAttestation;
    		this.isSoweeContract = myContractsController.isSoweeContract;
    		this.isMaintenanceContract = myContractsController.isMaintenanceContract; 

            //Call and load informations from MyBills page controller
            SICo_MyBills_VFC myBillsController = new SICo_MyBills_VFC(accountId);
            this.oZuoraZInvoice = myBillsController.oZuoraZInvoice;
            this.dInvoiceAmount = myBillsController.oZuoraZInvoice.TotalInvoiceAmountTTC__c;
            this.longInvoice = this.dInvoiceAmount > SICo_Dashboard_VFC.invoicelong;
            this.isUnpaid = myBillsController.oZuoraZInvoice.isUnpaid__c;
            this.isAwaitingTransfer = myBillsController.oZuoraZInvoice.IsAwaitingTransfer__c;
            this.isPaymentInProgress = myBillsController.oZuoraZInvoice.isPaymentInProgress__c;
        	this.invoiceAttachmentId = myBillsController.attachmentId;
        	this.sInvoiceDate = myBillsController.sInvoiceDate;
        	this.sNextInvoiceDate = myBillsController.sNextInvoiceDate;
            this.totalUnpaidBillsAmount = myBillsController.totalUnpaidBillsAmount;
            this.numberUnpaidBills = myBillsController.numberUnpaidBills;   
            this.hasSubscriptionContract = myBillsController.hasSubscriptionContract;

        } catch (Exception ex) {
            String errorDescription = 'Unexpected error is SICo_Dashboard_VFC constructor. STACK_TRACE: '+ex.getStackTraceString()+' EXCEPTION: '+ex;
            System.debug(errorDescription);
        }
	}
 
	public Class ActivityWrapper {
		@AuraEnabled
		public String activityType { get; set; }
		@AuraEnabled
		public String activityMainDesc { get; set; }
		@AuraEnabled
		public DateTime activityDate { get; set; }
		@AuraEnabled
		public String label { get; set; }
		@AuraEnabled
		public String linkLabel { get; set; }
		@AuraEnabled
		public String linkURL { get; set; } 
		// Priority used to managed display order
		public Integer priority { get; set; }
	}


	@AuraEnabled
    public static List<ActivityWrapper> queryActvities(String pAccountId) {
    	if (pAccountId == null) {
    		return null;
    	}
    	// First step :  Get events
    	// List to fill in
    	List<ActivityWrapper> vActivities = new List<ActivityWrapper>(); 
    	List<SICo_DashboardActivities_CLS.ActivityLightWrapper> vTaskAndEventsList = SICo_DashboardActivities_CLS.getTasksAndEvents(pAccountId);
    	List<ActivityWrapper> vActivitiesToAdd = parseActivityDTOtoActivityWrapper(vTaskAndEventsList, pAccountId);
    	// 2nd step :Get Invoices
    	List<SICo_DashboardActivities_CLS.ActivityLightWrapper> vInvoicesList = SICo_DashboardActivities_CLS.getUnpaidInvoices(pAccountId);
    	vActivities = parseActivityDTOtoActivityWrapper(vInvoicesList, pAccountId);
    	// 3rd strap : get quotes 
    	List<SICo_DashboardActivities_CLS.ActivityLightWrapper> vQuotesDTOList = SICo_DashboardActivities_CLS.getQuotes(pAccountId);    	
    	List<ActivityWrapper> vActivitiesQuote = parseActivityDTOtoActivityWrapper(vQuotesDTOList, pAccountId);

    	if (vActivitiesToAdd != null) {
	    	vActivities.addAll(vActivitiesToAdd);
	    }
    	if (vActivitiesQuote != null) {
	    	vActivities.addAll(vActivitiesQuote);
	    }
	    if (vActivities.size() == 0) {
	    	return null;
	    } 
	    // Reorder activities
	    vActivities = reorderActivitiesByPriorities(vActivities);
	    // limit the number of activities displayed
	    vActivities = limitNumberOfActivitiesToDisplay(vActivities);

        return vActivities;
    }

    /**
    * limit the number of activities to display 
    * @param : The list of activities
    */
    public static List<ActivityWrapper> limitNumberOfActivitiesToDisplay(List<ActivityWrapper> pActivities) {
    	List<ActivityWrapper> vActivities = new List<ActivityWrapper>();
    	Integer vListSize = pActivities.size() > MAX_ACTIVITIES_DISPLAYED ? MAX_ACTIVITIES_DISPLAYED : pActivities.size();
    	for (Integer i = 0; i < vListSize; i++) {
    		vActivities.add(pActivities[i]);
    	}

    	return vActivities;
    }

    /**
    * Reorder activities to display 
    * Order by selection of the minimum is the algorithm used
    * @param : The list of activities
    */
    public static List<ActivityWrapper> reorderActivitiesByPriorities(List<ActivityWrapper> pActivities) {
    	if (pActivities == null) {
    		return null;
    	}
    	Integer vListSize = pActivities.size();
    	ActivityWrapper[] vOrderedActivityList = new ActivityWrapper[vListSize];
    	Integer vMin = -1;
    	Integer vMinIndex = -1;
    	for (Integer i = 0; i < pActivities.size(); i++) {
    		vMin = pActivities[i].priority;
    		vMinIndex = i;
    		for (Integer j = i + 1; j < pActivities.size(); j++) {
    			if (pActivities[j].priority <= vMin) {
    				vMinIndex = j;
    				vMin = pActivities[j].priority;
    			}
    		}
    		if (vMinIndex != i) {
    			ActivityWrapper vActivityTemp = pActivities[i];
    			pActivities[i] = pActivities[vMinIndex];
    			pActivities[vMinIndex] = vActivityTemp;
    		}
    	}

    	return pActivities;
    }

    /**
    * Parse an invoice wrapper in an Activity wrapper
    * @param: pActivityLightList => List of SICo_DashboardActivities_CLS.ActivityLightWrapper
    */
    public static List<ActivityWrapper> parseActivityDTOtoActivityWrapper(List<SICo_DashboardActivities_CLS.ActivityLightWrapper> pActivityLightList, String pAccountId) {
    	if (pActivityLightList == null || pActivityLightList.size() == 0) {
    		return new List<ActivityWrapper>();
    	}
		List<ActivityWrapper> vActivitiesWrapList = new List<ActivityWrapper>();
		for (SICo_DashboardActivities_CLS.ActivityLightWrapper vALWrap : pActivityLightList) {
			ActivityWrapper vActivityWrap = new ActivityWrapper();
			vActivityWrap.activityMainDesc = vALWrap.mainTitle;
			vActivityWrap.activityDate = vALWrap.eventDate;
			vActivityWrap.label = vALWrap.description;
			vActivityWrap.activityType = vALWrap.mType;
			vActivityWrap.priority = vALWrap.priority;
			if (vActivityWrap.activityType == SICo_DashboardActivities_CLS.Facture_TYPE) { 
				vActivityWrap.linkLabel = System.Label.SICo_MyBills_Pay; 
				vActivityWrap.linkURL = Page.UnpaidBills.getUrl() + '?accountId=' + pAccountId; 
			}
			if (vActivityWrap.activityType == SICo_DashboardActivities_CLS.RDV_TYPE && vALWrap.isPast == false) {
				vActivityWrap.linkLabel = System.Label.SICo_Modifier; 
				vActivityWrap.linkURL = '#'; 
			}			
			if (vActivityWrap.activityType == SICo_DashboardActivities_CLS.QUOTE_TYPE) { 
				vActivityWrap.linkLabel = System.Label.SICo_QuoteViewDetail;
				vActivityWrap.linkURL = Page.QuoteDetail.getUrl() +'?accountId=' + pAccountId + '&quoteId=' +  vALWrap.mId;
			}
			vActivitiesWrapList.add(vActivityWrap);
		}
		return vActivitiesWrapList;
    }
}