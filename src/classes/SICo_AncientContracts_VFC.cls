/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Controller for the AncientContracts page 
Test Class:    SICo_AncientContracts_VFC_TEST
History
29/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
public with sharing class SICo_AncientContracts_VFC {

    public Id accountId{get;set;}

    public List<Zuora__Subscription__c> subs;
    
    public String conType{get;set;}
    
    public List<ContractWrapper> subSowee{get;set;}
    public List<ContractWrapper> subMaintenance{get;set;}
    
    public Map<Id, Attachment> mapAttachments;
    public Map<Id, String> mapAttachmentURL;
    
    public Map<Integer, String> frenchDates = new Map<Integer, String>();

    public SICo_AncientContracts_VFC() {

        //Get AccountId
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
                
        try{
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }catch (exception e){
            System.debug('Unable to return accountId');
        }
        
        //Get contract type
        conType = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_CONTRACT_TYPE);

        if(accountId != null){
            
            frenchDates.put(1, 'Janvier');
            frenchDates.put(2, 'Février');
            frenchDates.put(3, 'Mars');
            frenchDates.put(4, 'Avril');
            frenchDates.put(5, 'Mai');
            frenchDates.put(6, 'Juin');
            frenchDates.put(7, 'Juillet');
            frenchDates.put(8, 'Aout');
            frenchDates.put(9, 'Septembre');
            frenchDates.put(10, 'Octobre');
            frenchDates.put(11, 'Novembre');
            frenchDates.put(12, 'Décembre');

            subs = [SELECT Id, Name, CreatedDate, IsMaintenance__c, MarketingOfferName__c
                    FROM Zuora__Subscription__c
                    WHERE Zuora__Account__c = :accountId];
            
            mapAttachments = new Map<Id, Attachment>([SELECT Id, ParentId, CreatedDate
                              FROM Attachment
                              WHERE ParentId IN :subs
                              AND Name LIKE '%_CPV%'
                              ORDER BY CreatedDate desc]);
            
            mapAttachmentURL = new Map<Id, String>();
            
            for(Attachment att: mapAttachments.values()){
                if(mapAttachmentURL.get(att.ParentId) == null)
                    mapAttachmentURL.put(att.ParentId, '/servlet/servlet.FileDownload?file=' + att.Id);
            }
            
            subSowee = new List<ContractWrapper>();
            subMaintenance = new List<ContractWrapper>();
            
            ContractWrapper wrapper;
            
            for(Zuora__Subscription__c sub : subs){
                if(!sub.IsMaintenance__c){
                    wrapper = new ContractWrapper(sub.MarketingOfferName__c, getFrenchDate(sub.CreatedDate), mapAttachmentURL.get(sub.Id));
                    subSowee.add(wrapper);
                }
                else{
                    wrapper = new ContractWrapper(sub.MarketingOfferName__c, getFrenchDate(sub.CreatedDate), mapAttachmentURL.get(sub.Id));
                    subMaintenance.add(wrapper);
                }
            }
        }
    }

    public class ContractWrapper{

        public String name{get;set;}
        public String createdDate{get;set;}
        public String downloadLink{get;set;}

        public ContractWrapper(String name, String createdDate, String downloadLink){
            
            this.name = name;

            this.createdDate = createdDate;

            this.downloadLink = downloadLink;
        }
    }
    
    public String getFrenchDate(DateTime createdDate){

        return createdDate.day() + ' ' + frenchDates.get(createdDate.month()) + ' ' + createdDate.year();
        
    }
}