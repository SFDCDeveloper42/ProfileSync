({
	doInit : function(component, event, helper) {

    var isUnpaid = component.get("v.isUnpaid");
	var isPaid = (isUnpaid === 'false'); 
	component.set("v.isPaid", isPaid);

	var invoiceAmountNotFormatted =  component.get("v.invoiceAmount");
	component.set("v.invoiceAmount", invoiceAmountNotFormatted.toFixed(2) +' €');

 },
})