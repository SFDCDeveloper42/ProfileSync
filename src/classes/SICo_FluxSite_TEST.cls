/*------------------------------------------------------------
Author:        Boris Castellani
Company:       Salesforce.com
Description:   Test SICo_FluxSite_CLS & SICo_FluxSite_TRIGGER
History
01/08/2016      Boris Castellani     Create version
26/09/2016		Anouar Badri 		 Modification version
------------------------------------------------------------*/
@isTest
private class SICo_FluxSite_TEST {

	@isTest static void succesCalloutProfile() {
		Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
		insert account;
		
		account.CustomerNumber__c = account.Id;
		update account;
		 
		List<SICo_Site__c> siteList = new List<SICo_Site__c>();

		SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
		site = SICo_UtilityTestDataSet.addFieldForSite(site);
		siteList.add(site);

		insert siteList;

		// TEST & ASSERT
		Test.startTest();
			//Database.SaveResult result = Database.update(site);
			SICo_FluxSite_CLS.BuildSiteGenericProfile(siteList);

		Test.stopTest();
		//Account accounts= [SELECT Id, ErrorLog__c FROM Account LIMIT 1];
		//System.assert(result.isSuccess());
		//System.assertEquals('50 Callout Profile Reached',accounts.ErrorLog__c);
		}

	//AB
	@isTest static void succesCalloutElecProfile() {
		Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
		insert account;
		
		account.CustomerNumber__c = account.Id;
		update account;
		 
		List<SICo_Site__c> siteList = new List<SICo_Site__c>();

		SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
		site = SICo_UtilityTestDataSet.addFieldForSite(site);
		siteList.add(site);

		insert siteList;

		// TEST & ASSERT
		Test.startTest();
			//Database.SaveResult result = Database.update(site);
			SICo_FluxSite_CLS.BuildSiteElecProfile(siteList);

		Test.stopTest();

	}

	//AB
	@isTest static void succesCalloutGasProfile() {
		Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
		insert account;
		
		account.CustomerNumber__c = account.Id;
		update account;
		 
		List<SICo_Site__c> siteList = new List<SICo_Site__c>();

		SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
		site = SICo_UtilityTestDataSet.addFieldForSite(site);
		siteList.add(site);

		insert siteList;

		// TEST & ASSERT
		Test.startTest();
			//Database.SaveResult result = Database.update(site);
			SICo_FluxSite_CLS.BuildSiteGasProfile(siteList);

		Test.stopTest();

	}

	//AB
	 @isTest static  void testGetCallout() {
	 	// Set mock callout class 
    	//Test.setMock(HttpCalloutMock.class, new SICo_FluxSiteMeterGAZ_MOCK());
	 	
	 }


	//@isTest 
	static void errorCalloutProfile() {
		Account account = SICo_UtilityTestDataSet.createAccount('Jean','Dupond','PersonAccount');
		insert account;
		SICo_Site__c site = SICo_UtilityTestDataSet.createSite(account.Id);
		site = SICo_UtilityTestDataSet.addFieldForSite(site);
		// Just for TEST - if Millesime__c equals 10 => Mock test return Error
		site.ConstructionDate__c=10;
		insert site;

		// TEST & ASSERT
		Test.startTest();
			Database.SaveResult result = Database.update(site);
		Test.stopTest();
		System.assert(result.isSuccess());
		}

	//@isTest 
	static void error55CalloutProfile() {
    list<Account> listAccounts= new list<Account> ();
    for(Integer i = 0, j = 0; i < 55; i++) {
      listAccounts.add(SICo_UtilityTestDataSet.createAccount(SICo_Utility.generateRandomString(5, 'CARACTERE'),'Dupond','PersonAccount'));
      }
    insert listAccounts;
		list<SICo_Site__c> listSite= new list<SICo_Site__c> ();
		for(Account acc : listAccounts) {
			listSite.add(SICo_UtilityTestDataSet.createSite(acc.Id));
			}
		insert listSite;

		// TEST & ASSERT
		Test.startTest();
			for(SICo_Site__c site : listSite) {
				site= SICo_UtilityTestDataSet.addFieldForSite(site);
				}
			List<Database.SaveResult> result = Database.update(listSite);
		Test.stopTest();
		Account accounts= [SELECT Id, ErrorLog__c FROM Account LIMIT 1];
		System.assertEquals('50 Callout Profile Reached',accounts.ErrorLog__c);
		}


}