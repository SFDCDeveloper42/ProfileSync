/**
* @author Boris Castellani
* @date 21/11/2016
*
* @description Zuora__SubscriptionProductCharge__c Trigger before insert
*/
trigger SICo_ZSubscriptionProductChargeBeforeInsert_TRG on Zuora__SubscriptionProductCharge__c(before insert) {
	if(PAD.canTrigger('SICo_ZSubscriptionProductChargeBeforeInsert_TRG')) {
		SICo_SubscriptionPrdCharge_Utility.handlerBeforeUpsert(trigger.New);
	}  
}