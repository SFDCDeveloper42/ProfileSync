/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Controller for the AttestationsReports page 
Test Class:    SICo_AttestationsReports_VFC_TEST
History
29/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
@IsTest 
public with sharing class SICo_AttestationsReports_VFC_TEST {

	@testSetup static void initData(){
        User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser;

        User us = [SELECT accountId FROM User WHERE Id =:communityUser.Id LIMIT 1];

	    SICo_Site__c site = SICo_UtilityTestDataSet.createSite(us.accountID);
        site.BoilerBrand__c = 'brand';
        site.BoilerModel__c = 'model';
        site.IsActive__c = true;
        site.IsSelectedInCustomerSpace__c = true;
        insert site;

        SICo_CR__c cr = new SICo_CR__c(
            Account__c = us.accountID,
        	Site__c = site.Id,
        	InterventionDate__c = Date.today(),
            FirstReturnCode__c = 'VEN'
        );
        insert cr;
        
        Attachment att = new Attachment(
        	ParentId = cr.Id,
            Name = 'CR_test name',
            Body = EncodingUtil.base64Decode('abcd')
        );
        insert att;
    }

    @isTest
	static void test_Attestation() {

		User us = [SELECT accountId FROM User WHERE FirstName LIKE 'TESTFIRSTNAME%'];

        //Run as Community User without cr
	    system.runAs(us){
	        Test.startTest();
	            SICo_AttestationsReports_VFC controller = new SICo_AttestationsReports_VFC();
	            PageReference pRef = Page.AttestationsReports;
		        Test.setCurrentPageReference(pRef);

		        System.assert(controller.listCrWrapper != null && controller.listCrWrapper.size() > 0, 'Attachment list shouldn t be null or empty');
	        Test.stopTest();
        }

	}

}