/**
* @author Sebastien Colladon
* @date 26/09/2016
*
* @description Use SICo batch pattern
* Called after CHAM03 to handle SICo-1024
* To Launch : Database.executeBatch(new SICo_Batch_CHAM03());
*/
global with sharing class SICo_Batch_CHAM03 extends SICo_Batch {
    
  global SICo_Batch_CHAM03() {}

  global override Database.QueryLocator start(Database.BatchableContext BC) {

    List<String> values = new List<string>();
    values.add(Label.SICO_CR_FIRSTRETURNCODE_DEPANNAGE);
    values.add(Label.SICO_CR_FIRSTRETURNCODE_ABSENCE);
    values.add(Label.SICO_CR_FIRSTRETURNCODE_INT_IMPOSSIBLE);
    values.add(Label.SICO_CR_FIRSTRETURNCODE_INT_FICHE_ANNU);
    values.add(Label.SICO_CR_FIRSTRETURNCODE_INSTALLATION);

    // add values here
    return Database.getQueryLocator([SELECT Id, Maintenancy1stRDV__c
                                     FROM SICo_CR__c 
                                     WHERE SICO_Batchable__c = true
                                     AND FirstReturnCode__c not in :values
                                     AND CRType__c = :Label.SICO_CRTYPE_COMPLET
                                     ]);
  }

  global override void execute(Database.BatchableContext BC, List<sObject> scope) {

    Map<Id,SICo_CR__c> crs = new Map<Id,SICo_CR__c>((List<SICo_CR__c>) scope);
    for(SICo_CR__c cr: crs.values()){
      cr.SICO_Batchable__c = false;
    }
    
    Map<ID,ID> casePerCR = new Map<ID,ID>();
    for(List<Event> evl : [SELECT id, WhatId, CR__c FROM Event WHERE CR__c in :crs.keySet()]){
      for(Event ev : evl) {
        if(ev.WhatId.getSObjectType() == Schema.Case.getSObjectType()){
          casePerCR.put(ev.WhatId,ev.CR__c);  
        }
      }
    }

    for(Case aCase : [SELECT Id FROM Case WHERE id IN :casePerCR.keySet() AND ProvisioningType__c != :Label.SICo_ProvisionningMaintenance]){
      casePerCR.remove(aCase.id);
    }

    List<Task> tasks = new List<Task>();
    Set<Id> caseToTreat = new Set<Id>();
    for(List<Task> tkl : [  SELECT id, WhatId, CR__c FROM Task 
                            WHERE whatid in :casePerCR.keySet()
                            AND Taskid__c = 'MA0003']){
      for(Task tk : tkl) {
        if(tk.WhatId.getSObjectType() == Schema.Case.getSObjectType()){
          tk.CR__c = casePerCR.get(tk.WhatId);
          crs.put(tk.CR__c, new SICo_CR__c(Id = tk.CR__c, Maintenancy1stRDV__c = true));
          tasks.add(tk);
          caseToTreat.add(tk.WhatId);              
        }
      }
    }

    // CHECK ELIGIBLE BILLING LIGNE
    for(SICo_CR__c cr: crs.values()){
      if(cr.Maintenancy1stRDV__c == false){
        CR.IsEligibleBillingLign__c = true;  
      }
    } 

    String CaseType = String.valueOf(Schema.Case.getSobjectType());
    ID QueueID = [SELECT QueueId FROM QueueSobject WHERE SobjectType = :CaseType AND Queue.DeveloperName = :Label.SICO_CASE_QUEUE_GENERALE LIMIT 1][0].QueueId; // TODO match the right name
    List<Case> cases = new List<Case>();
    for(ID caseId : caseToTreat) {
      cases.add(
        new Case(
          id = caseId,
          ownerid = QueueID
        )
      );
    }

    update crs.values();
    update tasks;
    update cases;
  }
}