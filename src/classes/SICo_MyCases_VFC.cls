/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Controller for the Cases page (Cases)
Test Class:    SICo_MyCases_VFC_TEST
History
25/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
public with sharing class SICo_MyCases_VFC {

	public Id accountId{get;set;}

	public List<Case> listCases{get;set;}

	public List<CaseWrapper> listCaseWrapper{get;set;}
    
    public String linkSendMessage{get;set;}

	public SICo_MyCases_VFC() {

        //Get AccountId
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        
        if(requestedAccountId != null)
        	linkSendMessage = Page.SendMessage.getURL() + '?accountId=' + requestedAccountId;
        else
            linkSendMessage = Page.SendMessage.getURL();
        
        try{
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }catch (exception e){
            System.debug('Unable to return accountId');
        }

        if(accountId != null){

        	listCases = [SELECT Id,
        				CaseNumber,
        				Status,
        				CreatedDate,
        				Subject__c,
        				Heading__c
        				FROM Case
        				WHERE AccountId = :accountId
                        AND RecordTypeId in (SELECT Id from RecordType WHERE DeveloperName !='System')
                        ORDER BY CaseNumber DESC
                        LIMIT 1000];

        	listCaseWrapper = new List<CaseWrapper>();
        	List<SICo_CaseStatut__mdt> lsCaseStatut = [SELECT CasePicklistValue__c, PublicLabel__c, IsDefaultValue__c FROM SICo_CaseStatut__mdt];
        	String defaultCaseStatutValue;
            if (listCases != null && !listCases.isEmpty()){
            	Map<String, String> mapCaseStatusMdtStatus = new Map<String, String>();
            	for (Case cas : listCases){
            		for (SICo_CaseStatut__mdt cs : lsCaseStatut){
            			if (cas.Status == cs.CasePicklistValue__c){
        					mapCaseStatusMdtStatus.put(cas.Status, cs.PublicLabel__c);
            			}
            		}
        		}
        		for (SICo_CaseStatut__mdt cs : lsCaseStatut){
        			if (cs.IsDefaultValue__c){
        				defaultCaseStatutValue = cs.PublicLabel__c;
        			}
        		}
        		for (Case cas : listCases){
            		listCaseWrapper.add(
        				new CaseWrapper(
        					cas.CaseNumber, 
        					(mapCaseStatusMdtStatus.containsKey(cas.Status) ? mapCaseStatusMdtStatus.get(cas.Status) : defaultCaseStatutValue), 
        					cas.Subject__c, 
        					cas.Heading__c, 
        					cas.CreatedDate
    					)
					);
        		}
            }

        }
    }

    public class CaseWrapper{

    	public String subject{get;set;}
    	public String reference{get;set;}
    	public String status{get;set;}
    	public String createdDate{get;set;}

	    public CaseWrapper(String caseNumber, String status, String reason, String heading, Datetime createdDate){

            if(reason != null){
                this.subject = reason + ' ';
            }
            else {
                this.subject = '';
            }
            
            if(heading != null){
                this.subject += heading;
            }

	    	this.reference = caseNumber;

	    	this.status = status;

	    	this.createdDate = createdDate.format('dd.MM.YYYY');
	    }
	}


}