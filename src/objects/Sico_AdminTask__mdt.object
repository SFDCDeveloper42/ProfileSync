<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>[SICo] Mapping for provisioning tasks creation with Case lookup</description>
    <fields>
        <fullName>BlockingCBPayment__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SICo] Allow to know if the feature is blocked if the CB payment is KO.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Blocking CB Payment</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Cases__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Provisionning Cases Admin</label>
        <referenceTo>Sico_AdminCase__mdt</referenceTo>
        <relationshipName>Provisionning_Tasks_Admin</relationshipName>
        <required>false</required>
        <type>MetadataRelationship</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Comments</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Domain__c</fullName>
        <description>[SICo]: Type of flux</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Domaine</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>GrDF</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Logistique</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Cham</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Services connectés</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EndPointType__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>EndPoint Type</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>POST</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>GET</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>PUT</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EndPoint__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>EndPoint</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IsVisibleEspaceClient__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Visible Espace Client</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Parameters__c</fullName>
        <description>[SICo] : Parameters used by integration flow</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Paramètre</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProvisioningCallOut__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SICo] If flagged, launch flow.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Lancer Flux à la création</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RecordType__c</fullName>
        <description>[SICo] : Name of the record type to use during the provisioning.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Type d&apos;enregistrement</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>[SICo] : Name of the record type to use during the provisioning.</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>TaskProvisioning</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>TaskProvisionning_CHAM</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>TaskProvisionning_Gaz</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>TaskProvisionning_Logistique</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>TaskProvisionning_SC</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>ChamDemand</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>TaskOrder__c</fullName>
        <description>[SICo] Define the launch Order for feeds.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Ordre de lancement</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TaskSubject__c</fullName>
        <description>[SICo] : Description used to populate record subject.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Sujet de la tâche</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TaskTechnique__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SICo] : Check if technical object not displayed to customer.</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>TaskTechnique</label>
        <type>Checkbox</type>
    </fields>
    <label>Provisioning Task Adm</label>
    <pluralLabel>Provisioning Tasks Adm</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
