/**
* @author Sebastien Colladon
* @date 18/10/2016
*
* @description Global Mock class to mock every callout in the tests.@author
* Allow to have only one mock and so respond to multiple call in one test class
*/
@isTest
global class SICo_GlobalRespond_MOCK implements HttpCalloutMock {

  private final Map<String,String> handlerPerEndpoint;
  // TODO SICO Service mdtt
  // TODO Boomi Endpoint mdt

  global SICo_GlobalRespond_MOCK(){
    this.handlerPerEndpoint = new Map<String,String>();
    for(CalloutConfig__c cc : CalloutConfig__c.getall().values()){
      handlerPerEndpoint.put(cc.Endpoint__c,cc.Name+'Mock');
    }
    for(NamedCredential nc : [SELECT DeveloperName,Endpoint FROM NamedCredential]){
      handlerPerEndpoint.put(nc.Endpoint,nc.DeveloperName+'NCMock');
    }
  }

  // Implement this interface method
  global HTTPResponse respond(HTTPRequest request) {
    String handlerName = this.handlerPerEndpoint.get(request.getEndpoint());

    System.Type objType = string.isNotBlank(handlerName) ? Type.forName(handlerName) : Type.forName('GenericMock');
    GenericMock gm = (GenericMock) objType.newInstance();
    return gm.getMock();
  }

  private virtual class GenericMock{
    public virtual HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }

  private class BoomiMock extends GenericMock{
    public override HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }

  private class CommunityMock extends GenericMock{
    public override HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }

  private class ConnectedObjectMock extends GenericMock{
    public override HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }

  private class OSFMock extends GenericMock{
    public override HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }

  private class SalesforceMock extends GenericMock{
    public override HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }

  private class BoomiNCMock extends GenericMock{
    public override HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }

  private class LogStashNCMock extends GenericMock{
    public override HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }

  private class zconnectNCMock extends GenericMock{
    public override HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }
  
  private class ChamMock extends GenericMock{
    public override HTTPResponse getMock(){
      final HttpResponse response = new HttpResponse(); 
      response.setStatusCode(200);
      return response;
    }
  }
  
}