global class SICo_UtilityCloneData_CLS {

  webservice static String cloneBundle(String idOffreSowee){
    SICo_Bundle__c currentOffreSowee = [SELECT Id, 
                                                OwnerId, 
                                                IsDeleted, 
                                                Name, 
                                                CreatedDate, 
                                                CreatedById, 
                                                LastModifiedDate, 
                                                LastModifiedById, 
                                                SystemModstamp, 
                                                LastActivityDate, 
                                                LastViewedDate, 
                                                LastReferencedDate, 
                                                StartDate__c, 
                                                EndDate__c, 
                                                Description__c, 
                                                GasPeriodicite__c, 
                                                SkuNumber__c, 
                                                BundleParent__c, 
                                                StandardGasConsumption__c, 
                                                MarketingOfferName__c, 
                                                TechOfferName__c, 
                                                DisplayedOrder__c, 
                                                Gas__c, GazType__c, 
                                                OutOfSoweeWebsite__c, 
                                                Periodicite__c, 
                                                Offer__c, 
                                                PublicLabelLine2__c, 
                                                Thermostat__c, 
                                                Maintenance__c, 
                                                Installation__c, 
                                                MaintenanceFrequency__c, 
                                                PublicLabel__c, 
                                                Station__c, 
                                                GsxPartners__c, 
                                                SalesChannel__c, 
                                                ElectricTerminal__c, 
                                                ElectricTerminalInstallation__c, 
                                                Available_FirstBuy__c, 
                                                Available_Upsell__c, 
                                                Eligibility_gas__c, 
                                                Eligibility_CHAM__c, 
                                                Eligibility_TH__c, 
                                                Eligibility_Elec__c, 
                                                Patrimoine_incompatible_upsell__c, 
                                                Patrimoine_necessaire_upsell__c 
                                        FROM SICo_Bundle__c 
                                        WHERE Id = :idOffreSowee];

    List<SICo_BundleRP__c> l_currentBundleRP = [SELECT Id, 
                                                        IsDeleted, 
                                                        Name, 
                                                        CreatedDate, 
                                                        CreatedById, 
                                                        LastModifiedDate, 
                                                        LastModifiedById, 
                                                        SystemModstamp, 
                                                        LastViewedDate, 
                                                        LastReferencedDate, 
                                                        Bundle__c, 
                                                        ProductRatePlan__c, 
                                                        Zone__c, 
                                                        GasConsumption__c, 
                                                        ShippingMode__c, 
                                                        StandardVAT__c, 
                                                        GasType__c, 
                                                        StationBillingMode__c, 
                                                        MaintenanceParts__c, 
                                                        Maintenance_Mode_de_facturation__c, 
                                                        DiscountType__c, 
                                                        EntretienBillingMode__c 
                                                  FROM SICo_BundleRP__c 
                                                  WHERE Bundle__c = :idOffreSowee];

    SICo_Bundle__c newOffreSowee = currentOffreSowee.clone(false, true, true, true);
    newOffreSowee.SkuNumber__c = currentOffreSowee.SkuNumber__c + '_clone';
    insert newOffreSowee;

    List<SICo_BundleRP__c> l_newBundleRP = new List<SICo_BundleRP__c>();

    for(SICo_BundleRP__c bundleRP : l_currentBundleRP){
      SICo_BundleRP__c newBundleRP = bundleRP.clone(false, true, true, true);
      newBundleRP.Bundle__c = newOffreSowee.Id;
      l_newBundleRP.add(newBundleRP);
    }

    insert l_newBundleRP;

    return newOffreSowee.Id;

  }

}