/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Class dedicated to provide custom JWT
Test Class:    SICo_SignedJson_RWS_TEST and all other calling apex test classes :-)
History
25/07/2016     Olivier Vrbanac     Create & develope first version
------------------------------------------------------------*/
public with sharing class SICo_JsonSignature_CLS {

	public static String getJWT( String sApplication,
                                 String sUsername,
                                 String sCustomerNumber,
                                 String sAccess_Token,
                                 String sIssued_at,
                                 String sSiteId ) {

        ConnectedApplication__c csCA = ConnectedApplication__c.getValues( sApplication );
        String sExp = String.valueOf( Long.valueOf( sIssued_at ) + ( Integer.valueOf( csCA.SessionTimeOut__c ) * 60 * 60 * 1000 ) );

        List<Asset> oAssetList = [ SELECT SerialNumber FROM Asset WHERE ( Status = 'Installé' OR Status = 'Retourné' ) AND Site__r.SiteNumber__c = : sSiteId ];

        String sDeviceId = '';
        if ( oAssetList != null ) {
            for ( Asset oA : oAssetList  ) {
                sDeviceId = (sDeviceId == '' ? '' : ';') + oA.SerialNumber;
            }
        }

        CalloutConfig__c csCC = CalloutConfig__c.getValues( 'Community' );

        String bodyJson = '{' +
        					' "iss": "' + csCA.ClientId__c  + '",' +
        					' "exp": "' + sExp + '",' +
        					' "iat": "' + sIssued_at + '",' +
        					' "sub": "' + sUsername + '",' +
       						' "aud": "' + csCC.Endpoint__c + '",' +
       						' "c43-sfdc-at": "' + sAccess_Token + '",' +
       						' "person-ext-id": "' + SICo_Utility.generate18CharId( sCustomerNumber ) + '",' +
                            ' "site-ext-id": "' + SICo_Utility.generate18CharId(sSiteId) + '",' +
                            ' "device-ext-id": "' + sDeviceId + '"' +
        		   		 ' }';

        return getSignature( bodyJson, 'JSON' );
	}

	public static String getStandardJWT(String csName, Boolean bOption) {
		JWTSettings__c csJWT = JWTSettings__c.getValues(csName);
		String sExp = String.valueOf( Long.valueOf(String.valueOf(DateTime.now().getTime())) + (Integer.valueOf(csJWT.Exp__c) * 60 * 60 * 1000 ));
		String bodyJson = '{' +
							' "sub": "' + csJWT.Sub__c  + '",' +
							' "iss": "' + csJWT.Iss__c + '",' +
							' "aud": "' + csJWT.Aud__c + '",' +
							' "exp": "' + sExp + '"' +
						 	' }';

        if ( csJWT.Token_Creation_Date__c == null || DateTime.now() >= csJWT.Token_Creation_Date__c.addHours( (Integer) csJWT.Exp__c - 1 ) ) {
            if ( bOption ) {
                csJWT.Token_Creation_Date__c = Datetime.now();
                update csJWT;
            }
            return getSignature( bodyJson, csName );    
        } else {
            return 'Use existing';
        }
	}

    public static String getSignature( String bodyJson , String partnerName) {
        String token = '';

        List<Partner_Certificate__mdt> key = [SELECT Certificate__c FROM Partner_Certificate__mdt 
            WHERE Partner_Name__c = :partnerName AND Is_Active__c = true LIMIT 1]; 

        String alg = 'RS256';
        String typ = 'JWT';
        String headerJson = JSON.serialize( new Header( alg, typ ) );
        token = EncodingUtil.base64Encode( Blob.valueOf( headerJson ) ).replace( '+', '-' ).replace( '/', '_' ) + '.' + EncodingUtil.base64Encode( Blob.valueOf( bodyJson ) ).replace( '+', '-' ).replace( '/', '_' );
        String signature = EncodingUtil.base64Encode( Crypto.signWithCertificate( 'RSA-SHA256', Blob.valueOf( token ), key.get(0).Certificate__c ) ).replace( '+', '-' ).replace( '/', '_' );
        token += '.' + signature;


        return token;
    }


    public static String getSignature( String bodyJson ) {

        String token = '';

        String alg = 'RS256';
        String typ = 'JWT';
        String headerJson = JSON.serialize( new Header( alg, typ ) );
        token = EncodingUtil.base64Encode( Blob.valueOf( headerJson ) ).replace( '+', '-' ).replace( '/', '_' ) + '.' + EncodingUtil.base64Encode( Blob.valueOf( bodyJson ) ).replace( '+', '-' ).replace( '/', '_' );
        String signature = EncodingUtil.base64Encode( Crypto.signWithCertificate( 'RSA-SHA256', Blob.valueOf( token ), 'c43' ) ).replace( '+', '-' ).replace( '/', '_' );
        token += '.' + signature;

        return token;

    }

    private class Header {

        String alg;
        String typ;
        Header( String alg, String typ ) {
            this.alg = alg;
            this.typ = typ;
        }

    }

}