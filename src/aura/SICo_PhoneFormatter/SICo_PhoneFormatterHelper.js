({
	formatPhone : function(pComponent) {
		var vPhone = pComponent.get('v.pPhone');
		var vPhoneFormatted = pComponent.get('v.phoneDisplayed');
		var vRegExp = new RegExp("\\+33([0-9])([0-9][0-9])([0-9][0-9])([0-9][0-9])([0-9][0-9])$", 'i');
		if (vRegExp.test(vPhone)) {
			vPhoneFormatted = vPhone.replace(vRegExp, '+33 $1 $2 $3 $4 $5'); 
			pComponent.set('v.phoneDisplayed', vPhoneFormatted);
		} else {
			pComponent.set('v.phoneDisplayed', vPhone);			
		}
	}
})