/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi-solutions
Description:   Test class for SICo_ChangePassword_VFC
History
20/09/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
@IsTest 
public with sharing class SICo_ChangePassword_VFC_TEST {

	@testSetup static void initData(){
        CommunityConfig__c commSett = SICo_UtilityTestDataSet.createCustomSetting ();
        insert commSett;
    }

    @IsTest
    public static void testChangePassword() {
    	User user = SICo_UtilityTestDataSet.createCommunityUser();
    	insert user;

    	SICo_ChangePassword_VFC controller = new SICo_ChangePassword_VFC();
    	PageReference changePwdVF = Page.ChangePassword;

    	Test.setCurrentPage(changePwdVF);
        Test.startTest();

        System.runAs(user){
	    	controller.changePassword();
	    	System.assertEquals(controller.errorMessage, Label.SICo_RegistrationError);

	    	controller.oldPassword = 'abc';
	    	controller.newPassword = 'def';
	    	controller.verifyNewPassword = 'ghi';
	    	controller.changePassword();
	    	System.assertEquals(controller.errorMessage, Label.site.passwords_dont_match);

			controller.closeErrorPopup();
			System.assertEquals(controller.errorMessage, '');

	    	controller.oldPassword = 'abc';
	    	controller.newPassword = 'def';
	    	controller.verifyNewPassword = 'def';
	    	controller.changePassword();
	    	System.assertEquals(controller.errorMessage, Label.SICo_errorMdpSyntax);

	    	controller.newPassword = 'Azerty123';
	    	controller.verifyNewPassword = 'Azerty123';
    		controller.changePassword();
    	}

    	Test.stopTest();

    }

}