<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Envoi_d_un_email_pour_anomalie</fullName>
        <description>Envoi d&apos;un email pour anomalie</description>
        <protected>false</protected>
        <recipients>
            <field>CustomerEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>user.madteam@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/HTML_Mail_pour_anomalie</template>
    </alerts>
    <rules>
        <fullName>SICo envoi d%27email pour anomalie</fullName>
        <actions>
            <name>Envoi_d_un_email_pour_anomalie</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SICo_Anomaly__c.AnomalyType__c</field>
            <operation>equals</operation>
            <value>A1,A2</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Ce WF permet d&apos;envoyer un courrier en cas d&apos;anomalie remontée par le CR.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
