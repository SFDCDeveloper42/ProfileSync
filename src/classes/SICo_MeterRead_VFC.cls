/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Controller for the MeterRead page
Test Class:    SICo_MeterRead_VFC_TEST
History
08/09/2016     Gaël BURNEAU     Create version
------------------------------------------------------------*/
public with sharing class SICo_MeterRead_VFC {

	//AccountId for filtering all data displayed on the page
    public Id accountId {get;set;}
    // public Id siteId {get;set;}

  	public SICo_MeterRead_VFC() {
  		//Get AccountId
  		String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
  		try { this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId); } catch (Exception e) { System.debug('Unable to return accountId'); }
  	}

    @TestVisible
  	private static SICo_Site__c getMyHome(Id accountId) {

		SICo_Site__c oSICoSite = null;
		List<SICo_Site__c> listSICoSites = [SELECT Id, PCE__c, IsSelectedInCustomerSpace__c
										      FROM SICo_Site__c
										     WHERE Subscriber__c = :accountId
										       AND IsActive__c = true
			              		          ORDER BY IsSelectedInCustomerSpace__c DESC];

		//If 1 single site is active, use it
		if (listSICoSites.size() == 1) {
			oSICoSite = listSICoSites[0];
		//if several Sites, use the one selected in Customer Space
	  	} else if(listSICoSites.size() > 1) {
			for (SICo_Site__c custSite : listSICoSites) {
				if (custSite.IsSelectedInCustomerSpace__c) oSICoSite = custSite;
			}
		}//ENDIF listSICoSites.size()

		return oSICoSite;
	}

	@AuraEnabled
	public static String updatePCEIndex(String accountId, Integer index, Boolean forced) {

		String message =  SICo_Constants_CLS.BOOMI_SERVICE_KO_RESPONSE_STATUS; //Default message in case something goes wrong
		SICo_Site__c oSICoSite = getMyHome(accountId);

		if (oSICoSite != null && oSICoSite.PCE__c != null && index != null) {

			System.debug(System.LoggingLevel.DEBUG, '#### request : ' + JSON.serialize(new StoreMeterReadRequest(oSICoSite.PCE__c, index, forced)));
			HttpResponse response;

            SICo_BoomiEndpoint__mdt boomiEndpoint = [SELECT EndPoint__c FROM SICo_BoomiEndpoint__mdt WHERE DeveloperName = 'StoreMeterRead'];
			try {
				response = SICo_Utility.getCallout(
											'POST',
											boomiEndpoint.EndPoint__c,
											JSON.serialize(new StoreMeterReadRequest(oSICoSite.PCE__c, index, forced))
										);
			}
            catch (Exception e) { SICo_LogManagement.sendErrorLog(SICo_Constants_CLS.SICO, SICo_Constants_CLS.BOOMI, e.getMessage()); return SICo_Constants_CLS.BOOMI_SERVICE_KO_RESPONSE_STATUS; }

			if (response != null) {
				System.debug(System.LoggingLevel.DEBUG, '#### response.getBody() : ' + response.getBody());

                StoreMeterReadResponse oStoreMeterReadResponse;
                try {
    				oStoreMeterReadResponse = (StoreMeterReadResponse)JSON.deserialize(response.getBody(), StoreMeterReadResponse.class);
                }
                catch(JSONException e) { SICo_LogManagement.sendErrorLog(SICo_Constants_CLS.SICO, SICo_Constants_CLS.BOOMI, e.getMessage()); return SICo_Constants_CLS.BOOMI_SERVICE_OK_RESPONSE_STATUS; }

				if (response.getStatus() == 'OK') {

	                // If status is OK
    				if (oStoreMeterReadResponse.statut == SICo_Constants_CLS.BOOMI_SERVICE_OK_RESPONSE_STATUS) {
    					message = SICo_Constants_CLS.BOOMI_SERVICE_OK_RESPONSE_STATUS;

    				// If status is WARNING
    				} else if (oStoreMeterReadResponse.statut == SICo_Constants_CLS.BOOMI_SERVICE_WARNING_RESPONSE_STATUS) {
    					message = oStoreMeterReadResponse.message;

    				// If status is KO
    				} else if (oStoreMeterReadResponse.statut == SICo_Constants_CLS.BOOMI_SERVICE_KO_RESPONSE_STATUS) {
    					message = SICo_Constants_CLS.BOOMI_SERVICE_KO_RESPONSE_STATUS;
                    }

				} else {
					SICo_LogManagement.sendErrorLog(SICo_Constants_CLS.SICO, Label.SICO_LOG_USER_Conso, oStoreMeterReadResponse.message);
					message = SICo_Constants_CLS.BOOMI_SERVICE_KO_RESPONSE_STATUS;
				}
			}
		}

		return message;
	}

	private class StoreMeterReadRequest {
        public String id_pce;
        public String typeEnergie;
        // public String energy_type;
        public String dateDeReleve;
        public String typeDeReleve;
        // public String raisonDeReleve;
        public String mr;
        public String forced;

        public StoreMeterReadRequest(String pceId, Integer index, Boolean forced) {
			this.id_pce = pceId;
	        this.typeEnergie = 'GAZ';
	        // this.energy_type = 'GAZ';
	        this.dateDeReleve = Datetime.now().format('YYYYMMdd');
	        this.typeDeReleve = 'N';
	        // this.raisonDeReleve = '76';
	        this.mr = (index != null ? index + '' : '');
	        this.forced = (forced ? 'true' : '');

			// "id_pce"          : "SAM_TEST_001",
			// "energy_type"     : "GAZ",
			// "dateDeReleve"    : "20160923",
			// "typeDeReleve"    : "N",
			// "mr"              : "12345",
			// "forced"          : "true"
		}
    }

    public class StoreMeterReadResponse {
        public String statut;
        public String message;
        public String value;

        public StoreMeterReadResponse(String statut, String message, String value) {
            this.statut = statut;
            this.message = message;
            this.value = value;
        }
    }
}