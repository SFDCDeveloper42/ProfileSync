/**
* @author Boris Castellani
* @date 15/12/2016
*
* @description TEST Trigger & SICo_ZPaymentTrigger_CLS
*/
@isTest
public with sharing class SICo_ZPaymentTrigger_TEST {

	@testSetup 
	static void init() {

    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Anakin', 'PersonAccount');
    	Insert acc;

    	Zuora__CustomerAccount__c billingAcc = new Zuora__CustomerAccount__c(Zuora__Account__c= acc.Id);
		Insert billingAcc;

		Zuora__Payment__c newpay = new Zuora__Payment__c();
		newpay.Zuora__BillingAccount__c = billingAcc.Id;
		newpay.Zuora__Account__c = acc.Id;
		Insert newpay;
	}

	@isTest
	static void testZPayment(){

    	Account acc = SICo_UtilityTestDataSet.createAccount('Jhonny', 'Cash', 'PersonAccount');
    	Insert acc;
    	Zuora__CustomerAccount__c billingAcc = new Zuora__CustomerAccount__c(Zuora__Account__c= acc.Id);
    	Insert billingAcc;

		Test.startTest();
			Zuora__Payment__c newpay = new Zuora__Payment__c();
			newpay.Zuora__BillingAccount__c = billingAcc.Id;
			newpay.Zuora__Account__c = acc.Id;
			Insert newpay;
		Test.stopTest();

		Zuora__Payment__c pay = [SELECT Email__c FROM Zuora__Payment__c Limit 1] ;
		System.assertEquals(pay.Email__c, 'test@email.com');
	}
}