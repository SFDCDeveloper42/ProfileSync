/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Mock responce for eval-conso request
History
26/08/2016     Gaël BURNEAU      Create version
------------------------------------------------------------*/
@isTest
public class SICo_FluxEvalConso_MOCK implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public SICo_FluxEvalConso_MOCK(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        for (String key : this.responseHeaders.keySet()) {
            res.setHeader(key, this.responseHeaders.get(key));
        }
        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }

}