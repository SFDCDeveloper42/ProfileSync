/**
* @author 
* @date 
* @modification 31/01/2017
*
* @description Use SICo batch - SICO-1509
*/
@isTest
private class SICo_Batch_GRD03_MESCHF_TEST {

	@testSetup 
	static void setup() {
		insert SICo_UtilityTestDataSet.createChamConfig();
		insert SICo_UtilityTestDataSet.createCustomSetting();
		insert SICo_UtilityTestDataSet.createConfig_Boomi();

		Account agency = SICo_UtilityTestDataSet.createBusinessAccount('Death Star','Agency');
		agency.AgencyNumber__c = 'ANUMBER';
		Insert agency;

  	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Anakin', 'PersonAccount');
  	Insert acc;

  	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
  	site.CHAMAgencyEntretien__c = agency.Id;
  	Insert site;

		Opportunity opportunity = SICo_UtilityTestDataSet.createOpportunity('Croiseur interstellaire',acc.Id,Site.Id);
		opportunity.MaintenanceParts__c = 'Avec pièces';
		opportunity.Maintenance_Mode_de_facturation__c = 'Annuel';
		Insert opportunity;

		zqu__Quote__c zuoraquote = SICo_UtilityTestDataSet.createZuoraQuote('Lightsaber', acc.Id, acc.PersonContactId,opportunity.id);
		Insert zuoraquote;

		Zuora__Subscription__c subscription = SICo_UtilityTestDataSet.createZuoraSubscription('Tatooine', acc.Id, zuoraquote.Id, 'R2D2', site.Id);
		Insert subscription;

		List<Case> cases = new List<Case>();
		cases.add(SICo_UtilityTestDataSet.createCase('Maintenance', acc.Id, opportunity.Id , site.Id));
		cases.add(SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, acc.Id, opportunity.Id , site.Id));

		for(Case theCase : cases){
			theCase.Subscription__c = subscription.Id;    	
		}
		Insert cases;

		Task thetask = SICo_UtilityTestDataSet.createProvisioningTask('MESCHF','TEST MESCHF',cases.get(1).Id,SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_Gaz'),true);
		thetask.Status = 'A traiter CRC';
		thetask.InterventionType__c = 'MES';
		Insert thetask;
 	}

	static testMethod void batchMESCHF() {
		Test.setMock(HttpCalloutMock.class, new SICo_GlobalRespond_MOCK());
		Test.startTest();
			SICo_Batch_GRD03_MESCHF meschf = new SICo_Batch_GRD03_MESCHF();
			Id batchId = Database.executeBatch(meschf);
		Test.stopTest();

		String CaseType = String.valueOf(Schema.Case.getSobjectType());
		ID QueueID = [SELECT QueueId 
									FROM QueueSobject 
									WHERE SobjectType = :CaseType 
									AND Queue.DeveloperName = :Label.SICO_CASE_QUEUE_GENERALE LIMIT 1][0].QueueId; // TODO match the right name

		List<Task> tasks = [SELECT Id 
												FROM Task WHERE ownerid = :QueueID];

		System.assertNotEquals(tasks.size(), 1);     
	}
}