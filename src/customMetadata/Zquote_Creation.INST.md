<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>INST</label>
    <protected>false</protected>
    <values>
        <field>ZquoteTemplate__c</field>
        <value xsi:type="xsd:string">Souscripton_One_Shot_active</value>
    </values>
    <values>
        <field>ZquoteToCreate__c</field>
        <value xsi:type="xsd:string">Station connectée</value>
    </values>
</CustomMetadata>
