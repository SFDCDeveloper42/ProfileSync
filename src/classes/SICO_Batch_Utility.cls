public with sharing class SICO_Batch_Utility {

    public static Case getNewCase(TaskCaseMapping__mdt caseParams, SICo_Site__c caseSite) {
        Case newCase = new Case();

        newCase.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + caseParams.CaseRecordType__c);
        newCase.Status = 'En cours';
        newCase.Origin = 'Flux';
        newCase.ProvisioningType__c = caseParams.CaseType__c;
        newCase.Site__c = caseSite.Id;
        newCase.AccountId = caseSite.Subscriber__c;
        newCase.ContactId = caseSite.Subscriber__r.PersonContactId;
        newCase.Subject__c = caseParams.CaseSubject__c;
        newCase.Heading__c = caseParams.CaseHeading__c;

        System.debug('NEW CASE CREATION : ' + newCase);

        return newCase;
    }

    private static Map<String, String> getTaskIdMap(String batchId) {
        List<BatchTaskIdMapping__mdt> l_taskIds = [SELECT TaskId__c, InterventionType__c
                                   FROM BatchTaskIdMapping__mdt 
                                    WHERE BatchCode__c = :batchId];

        Map<String, String> m_InvTypeTask = new Map<String, String>();
        for (BatchTaskIdMapping__mdt p : l_taskIds) {
          m_InvTypeTask.put(p.InterventionType__c, p.TaskId__c);
        }
        return m_InvTypeTask;
    }

    public static void getActivityCases(List<SObject> scope, String batchId) {
        System.debug('====> START ACTIVITY CASE');

        Map<Id, Case> newCases = new Map<Id, Case>();        
        Set<String> listSites = new Set<String>();
        Map<Id, List<Case>> caseMap = new Map<ID, List<Case>>();
        Map<Id, Case> attachedCaseMap = new Map<Id, Case>();
        Map<String, TaskCaseMapping__mdt> actTypeParamMap = new Map<String, TaskCaseMapping__mdt>();

        System.debug('====> GET List of parameters for : '+ batchId);
        List<TaskCaseMapping__mdt> caseParams = [SELECT CaseHeading__c, CaseType__c, CaseRecordType__c, CaseSubject__c, EventIntervType__c
                                            FROM TaskCaseMapping__mdt 
                                            WHERE BatchCode__c = :batchId];

        System.debug('====> GET List of parameters for : '+ caseParams);
        
        //Create a set with list of case RecordTypes
        Set<Id> caseRecordTypeIds = new Set<Id>();
        for (TaskCaseMapping__mdt p : caseParams) {

            System.debug('====> START BUILD LIST OF CASE RT');
        
            caseRecordTypeIds.add( SICo_Utility.m_RTbyDeveloperName.get('Case_' + p.CaseRecordType__c) );
            actTypeParamMap.put(p.EventIntervType__c, p);
        }

        System.debug('====> CASE RT : '+caseRecordTypeIds);
        System.debug('====> PARAMETERS MAP : '+actTypeParamMap);

        System.debug('====> END BUILD LIST OF CASE RT');


        //Build the set of SiteID   for case search
        System.debug('====> START BUILD LIST OF SITES');
        for(SObject t : scope)
            listSites.add(String.valueOf(t.get('SiteID__c')));

        Map<Id, SICo_Site__c> siteMap = new Map<Id, SICo_Site__c>([SELECT Id, Subscriber__c, Subscriber__r.PersonContactId FROM SICo_Site__c WHERE Id in :listSites]);
        

        System.debug('====> END BUILD LIST OF SITES');


        System.debug('====> START BUILD LIST OF EXISTING CASES / SITE');

        //Select the existing list of cases for each Site
        for(Case c :[SELECT Site__r.Id, Id, RecordTypeId, ProvisioningType__c, ContactId 
                        FROM Case WHERE Site__r.Id IN :listSites 
                        AND status not in ('Clos', 'Closed')
                        //AND ProvisioningType__c = :caseParams.CaseType__c 
                        AND RecordTypeId IN :caseRecordTypeIds]) {
            if ( caseMap.containsKey(c.Site__r.Id) ) {
                caseMap.get(c.Site__r.Id).add(c);
            } else {
                List<Case> l = new List<Case>();
                l.add(c);
                caseMap.put(c.Site__r.Id, l);
            }
        }
        
        System.debug('====> END BUILD LIST OF EXISTING CASES / SITE');

        System.debug('====> START BUILD RIGHT ASSOCIATION SITE / CASE');

        //Find the case or create the case
        for (SObject t : scope) if (siteMap.get(String.valueOf( t.get('SiteID__c'))) != null) {
            //if () {
            String actTypeParam = actTypeParamMap.get(String.valueOf( t.get('InterventionType__c'))) != null ? String.valueOf( t.get('InterventionType__c')) : 'Default';

                System.debug('====> START CASE SEARCH FOR: ' + t);

                if (caseMap.get(String.valueOf( t.get('SiteID__c'))) != null) {
                    List<Case> activityCases = caseMap.get(String.valueOf( t.get('SiteID__c')));
                    for (Case c : activityCases) {
                        if (
                            SICo_Utility.m_RTbyDeveloperName.get('Case_' + actTypeParamMap.get(actTypeParam).CaseRecordType__c) == c.RecordTypeId
                            && actTypeParamMap.get(actTypeParam).CaseType__c == c.ProvisioningType__c) {
                            
                            System.debug('====> ATTACH EXISTING CASE');
                            attachedCaseMap.put( String.valueOf( t.Id ), c);
                        }
                    }
                } else
                    caseMap.put(String.valueOf( t.get('SiteID__c')), new List<Case>());
                
                if (!attachedCaseMap.containsKey(String.valueOf( t.Id ))) {

                    System.debug('====> ATTACH NEW CASE');
                    Case newCase = getNewCase(actTypeParamMap.get(actTypeParam), siteMap.get(String.valueOf( t.get('SiteID__c'))));
                    attachedCaseMap.put(String.valueOf( t.Id ), newCase);

                    caseMap.get(String.valueOf( t.get('SiteID__c'))).add(newCase);
                }
                System.debug('====> END CASE SEARCH FOR: ' + t);
            //}
        }

        System.debug('====> END BUILD RIGHT ASSOCIATION SITE / CASE');

        //Create the missing cases
        Set<Case> toBeUpsertCases = new Set<Case>();
        toBeUpsertCases.addAll(attachedCaseMap.values());
        Database.upsert(New List<Case>(toBeUpsertCases));

        //Create Map for Task Record Types
        /*Map<String, String> invTypeMap = new Map<String, String>();
        for (TaskCaseMapping__mdt m : caseParams) {
            invTypeMap.put(m.EventIntervType__c, SICo_Utility.m_RTbyDeveloperName.get('Task_' + m.TaskRecordtype__c));
        } */

        Map<String, String> m_taskIds = getTaskIdMap(batchId);

        //Attach the tasks to the cases
        for (SObject t : scope) if (siteMap.get(String.valueOf( t.get('SiteID__c'))) != null) {
            t.put('WhatId', attachedCaseMap.get( String.valueOf( t.Id ) ).Id );
            t.put('WhoId', attachedCaseMap.get( String.valueOf( t.Id ) ).ContactId );
            //t.put('RecordTypeId', invTypeMap.get(String.valueOf( t.get('InterventionType__c') )));
            if (t.get('TaskId__c') == null) {
              String tId = m_taskIds.get(String.valueOf(t.get('InterventionType__c'))) != null ? m_taskIds.get(String.valueOf(t.get('InterventionType__c'))) : m_taskIds.get('Default');
              t.put('TaskId__c', tId);
            } 
        }

        Database.update(scope, false); 

        System.debug('====> END ACTIVITY CASE');
    }

}