<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EmailCustomerAreaActivation</fullName>
        <description>Email Activation Espace Client</description>
        <protected>false</protected>
        <recipients>
            <field>EmailAddress__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>user.madteam@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SiCoSubscriptionEmails/HTML_Mail_pour_initialisation_espace_client</template>
    </alerts>
    <fieldUpdates>
        <fullName>SICo_ResetSendWelcomeEmail</fullName>
        <field>SendWelcomeEmail__c</field>
        <literalValue>0</literalValue>
        <name>[SICo] Reset Send Welcome Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SiCo_EmailAddressCopy</fullName>
        <field>EmailAddress__c</field>
        <formula>PersonEmail</formula>
        <name>SiCo Recopie adresse email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>%5BSiCo%5D Recopie de l%27adresse email</fullName>
        <actions>
            <name>SiCo_EmailAddressCopy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.PersonEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>[SiCo] ce WF permet de recopier l&apos;adresse email du champ &quot;email&quot;&apos; standard vers un champ custom &apos;adresse email&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SiCo Email Activation Espace Client</fullName>
        <actions>
            <name>EmailCustomerAreaActivation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SICo_ResetSendWelcomeEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.IsPersonAccount</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SendWelcomeEmail__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EmailAddress__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Ce WF permet d&apos;envoyer un email au nouveau client afin qu&apos;il active son espace client</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
