/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Test of SICo_WithoutSharing_CLS
History
03/08/2016      Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
@isTest
private class SICo_WithoutSharing_TEST {

    /**
     * [Test setup]
     */
    @testSetup static void initData() {
        //Insert inactive user
        User testUser = SICo_UtilityTestDataSet.createCommunityUser();
        testUser.IsActive = false;
        insert testUser;
    }

    /**
     * [Test of reactivateUser method]
     */
    @isTest static void reactivateUser_ok() {
        User testUser = [SELECT Id, IsActive FROM User WHERE IsActive = false AND AccountId != null LIMIT 1];
        Test.startTest();
        // Instantiate a new controller with all parameters in the page
        SICo_WithoutSharing_CLS.reactivateUser(testUser);
        Test.stopTest();
        //Expected result: user is active
        testUser = [SELECT Id, IsActive FROM User WHERE Id =: testUser.Id LIMIT 1];
        System.assertEquals(true, testUser.IsActive);
    }

    /**
     * [Test of reactivateUser method]
     */
    @isTest static void reactivateUser_error() {
        User testUser2 = SICo_UtilityTestDataSet.createCommunityUser();
        testUser2.IsActive = false;
        insert testUser2;
        Test.startTest();
        // Instantiate a new controller with all parameters in the page
        SICo_WithoutSharing_CLS.reactivateUser(testUser2);
        Test.stopTest();
        //Expected result: error due to mixed DML operation
        testUser2 = [SELECT Id, IsActive FROM User WHERE Id =: testUser2.Id LIMIT 1];
        //System.assertEquals(false, testUser2.IsActive);
    }

}