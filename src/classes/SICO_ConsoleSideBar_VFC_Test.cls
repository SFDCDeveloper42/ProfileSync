@isTest
private class SICO_ConsoleSideBar_VFC_Test {

	@testSetup
    private static void setup() {
    	Account a = new Account(name='test');
    	insert a;

    	

    	SICo_Site__c c = new SICo_Site__c(Subscriber__c=a.id);
    	insert c;

    	Asset b = new Asset(name='test',accountid=a.id,site__c = c.id);
    	insert b;

    	Case d = new Case(Subject='case',AccountId=a.id);
    	insert d;

    	Task e = new Task(Subject='case',whatid=a.id);
    	insert e;
    }

	@isTest
    private static void testOne() {
    	Account a = [SELECT id FROM Account WHERE name = 'test'];
    	PageReference pageRef = Page.SICO_ConsoleSideBar_VFP;
    	Test.setCurrentPageReference(pageRef);
    	ApexPages.CurrentPage().getparameters().put('id', a.id);
    	ApexPages.StandardController con = new ApexPages.StandardController(a);
    	SICO_ConsoleSideBar_VFC c_myExtension = new SICO_ConsoleSideBar_VFC(con); 
    }
}