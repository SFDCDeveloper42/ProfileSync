/**
* @author Sébastien Colladon
* @date 10/01/2017
* @LastModify 11/01/2017
*
* @description 
*/
@RestResource(urlMapping='/v1/ConvertLead')
global with sharing class SICo_ConvertLead_RWS {

  @HttpPut
  global static ConvertLeadResponse doPut(Id leadId) {
    final Savepoint sp = Database.setSavepoint();
		final ConvertLeadResponse aConvertLeadResponse = new ConvertLeadResponse();
		try{
      final SICo_LeadService.SICo_LeadDomain leadDomain = new SICo_LeadService.SICo_LeadDomain(LeadId);

      if(!leadDomain.isLead()) {
        throw new NotLeadException();
      }

      if(String.isnotBlank(leadDomain.isConverted())) {
        throw new AlreadyConvertedLeadException();
      }

      final SICo_LeadService leadService = new SICo_LeadService(leadId);  
      leadService.convertLeadWithBusinessRules(leadDomain.isRelatedToAnExistingAccount());
      
		} catch(LeadException e) {
      System.debug(e.getMessage());
      System.debug(e.getStackTraceString());
      aConvertLeadResponse.result = false;
      aConvertLeadResponse.message = e.getMessage();
      Database.rollback(sp);
    } catch(Exception e) {
			System.debug(e.getMessage());
      System.debug(e.getStackTraceString());
      aConvertLeadResponse.result = false;
      aConvertLeadResponse.message = 'Unexpected error'; // TODO Custom Label
      Database.rollback(sp);
		}
		return aConvertLeadResponse;
	}

  global class ConvertLeadResponse {
    public Boolean result = true;
    public String message;
  }

  global virtual class LeadException extends Exception {
    
  }  

  global class NotLeadException extends LeadException {
    public override String getMessage(){
      return 'Wrong id';
    }
  }

  global class AlreadyConvertedLeadException extends LeadException {
    public override String getMessage(){
      return 'Lead already converted';
    }
  }
}