/**
* @author Boris Castellani
* @date 19/10/2016
*
* @description Handle SICo_Site__c Trigger Business Rule
*/
public with sharing class SICo_SiteTrigger_CLS {

	public static void handleBeforeInsert(final List<SICo_Site__c> sites){
		iniateSaveData(sites,false);
	}

	public static void handleBeforeUpdate(final List<SICo_Site__c> sites){
		iniateSaveData(sites,true);
	}


	private static void iniateSaveData(final List<SICo_Site__c> sites, boolean isUpdate){

		final Map<Id, List<SICo_SiteMeter__c>> activeSiteMeterBySiteId = new Map<Id, List<SICo_SiteMeter__c>>();
		final Set<Id> inactiveSiteMeter = new Set<Id>();

		if(isUpdate == true){
			final Set<Id> siteIds = new Set<Id>();
			for(SICo_Site__c site : sites){
				siteIds.add(site.Id);
			}
			List<SICo_SiteMeter__c> siteMeters = [ SELECT Id, Site__c,IsActive__c
											FROM SICo_SiteMeter__c
											WHERE Site__c IN :siteIds 
											/* AND IsActive__c = true */];

			for(SICo_SiteMeter__c siteMeter : siteMeters) {
				// if PCE OR PDL
				if (siteMeter.IsActive__c) {
					if(activeSiteMeterBySiteId.containsKey(siteMeter.Site__c)){
						activeSiteMeterBySiteId.get(siteMeter.Site__c).add(siteMeter);
					}else {
						activeSiteMeterBySiteId.put(siteMeter.Site__c, new List<SICo_SiteMeter__c>{siteMeter});
					}
				} else {
					inactiveSiteMeter.add(siteMeter.Site__c);
				}
			}
		}

		for (SICo_Site__c site : sites) {
			if(String.isEmpty(site.InitialPrincipalHeatingSystemType__c) && String.isNotEmpty(site.PrincipalHeatingSystemType__c)){
				site.InitialPrincipalHeatingSystemType__c = site.PrincipalHeatingSystemType__c;
			}
			if(String.isEmpty(site.InitialBoilerAge__c) && site.PrincipalHeatingSystemType__c == 'Gaz' && String.isNotEmpty(site.BoilerAge__c)){
				site.InitialBoilerAge__c = site.BoilerAge__c;
			}
			if(String.isEmpty(site.InitialGasSource__c) && String.isNotEmpty(site.GasSource__c)){
				site.InitialGasSource__c = site.GasSource__c;
			}
			if(site.InitialCeramicGlassPlatePresent__c != 'non' && site.IsCeramicGlassPlatePresent__c == true){
				site.InitialCeramicGlassPlatePresent__c = 'oui';
			}
			if(String.isEmpty(site.InitialPrincHeatingSystemTypeDetail__c) && String.isNotEmpty(site.PrincHeatingSystemTypeDetail__c)){
				site.InitialPrincHeatingSystemTypeDetail__c = site.PrincHeatingSystemTypeDetail__c;
			}
			if(String.isEmpty(site.InitialSanitaryHotWaterType__c) && String.isNotEmpty(site.SanitaryHotWaterType__c)){
				site.InitialSanitaryHotWaterType__c = site.SanitaryHotWaterType__c;
			}
			if(String.isEmpty(string.valueOf(site.InitialNoOfOccupants__c)) && String.isNotEmpty(string.valueOf(site.NoOfOccupants__c))){
				site.InitialNoOfOccupants__c = site.NoOfOccupants__c;
			}
			if(String.isEmpty(string.valueOf(site.InitialSurfaceInSqMeter__c)) && String.isNotEmpty(string.valueOf(site.SurfaceInSqMeter__c))){
				site.InitialSurfaceInSqMeter__c = site.SurfaceInSqMeter__c;
			}

			if (!activeSiteMeterBySiteId.containsKey(site.Id) && inactiveSiteMeter.contains(site.Id)){
				site.IsActive__c = false;
				site.ActiveSiteMeterGas__c = null;
			}
		}

	}

    
}