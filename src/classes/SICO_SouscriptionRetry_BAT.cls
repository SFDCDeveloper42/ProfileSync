global class SICO_SouscriptionRetry_BAT implements Database.Batchable<sObject>, Database.AllowsCallouts {
    public List<Id> caseIds;

    global SICO_SouscriptionRetry_BAT(List<Id> caseIds) {
        this.caseIds = caseIds;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {

        return Database.getQueryLocator([SELECT Id, ProvisioningCallOut__c, TaskId__c, WhatId, FluxStatus__c
                                         FROM Task
                                         WHERE Subject LIKE 'Souscription%'
                                         AND WhatId in : caseIds
                                         AND FluxStatus__c != 'OK'
                                         AND Tech_ProvisioningReady__c = true]);
    }

    global void execute(Database.BatchableContext BC, list<Sobject> scope) {
        List<Task> tasks = new List<Task>();
        for (Task aTask : (List<Task>) scope) {
          aTask.ProvisioningCallOut__c = true;
          tasks.add(aTask);
        }

        SICo_ProvisioningActivities_CLS.launchProvisioningActivitiesSync(tasks,null);
    }

    global void finish(Database.BatchableContext BC) {

    }
}