/**
* @author Boris Castellani
* @date 30/09/2016
* @LastModify 16/03/2017
*
* @description Deliver a CSV from a scheduled job
*/

global class SICo_ChamEmailSupportShedule_CLS implements Schedulable {

   global void execute(SchedulableContext SC){
      sendEmail();
   }

   @future(callout=true)
   public static void sendEmail(){
      final Set<String> zones = new Set<String>();
      List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
      Map<String,List<Task>> taskMap = new Map<String,List<Task>>();
      Map<String,List<Event>> eventMap = new Map<String,List<Event>>();
   
      List<Id> recordTypeIds = new List<Id>();
      recordTypeIds.add(SICo_Utility.m_RTbyDeveloperName.get('Task_TaskProvisionning_CHAM'));
      recordTypeIds.add(SICo_Utility.m_RTbyDeveloperName.get('Task_ChamDemand'));

      List<String> taskIds = new List<String>{'CH0001','IN0002','IN0004','MA0002'};

      // GET TASK
      List<Task> tasks = [ SELECT CHAMAgency__r.Name, Subject,Account.Name, Account.CustomerNumber__c, InterventionType__c, MaintenancyQuoteNumber__c, CreatedDate, Zone__c,ActivityDate
                           FROM Task 
                           WHERE recordTypeId IN :recordTypeIds
                           AND TaskId__c IN :taskIds
                           AND RequestState__c = '' AND Zone__c != ''
                           ];

      if(!tasks.isEmpty()) for(Task a : tasks){
         if(!taskMap.containsKey(a.Zone__c)){
            taskMap.put(a.Zone__c,new List<Task>());  
         } 
         taskMap.get(a.Zone__c).add(a);
      }
      zones.addAll(taskMap.keySet());

      //BUILD MAILS CONTENT
      Map<String,ChamEmailSupport__c> chamSettings = ChamEmailSupport__c.getall();
      String header = 'Agence cham ;Sujet ;Nom du Compte ;Date de creation ;Date d\'Activite ;Type d\'intervention ;Numero devis ;Numero client\n';
      String finalstr = '';

      for (String zone : zones){
        finalstr = header;
        if(taskMap.containsKey(zone)){
          for(Task a : taskMap.get(zone)){
             String recordString = '"'+a.CHAMAgency__r.Name+'";"'+String.valueOf(a.Subject)+'";"'+a.Account.Name+'";"'+a.CreatedDate +'";"'+a.ActivityDate+'";"'+a.InterventionType__c +'";"'+a.MaintenancyQuoteNumber__c +'";"'+a.Account.CustomerNumber__c+'"\n';
             finalstr = finalstr + recordString;               
          }
        }
        if(eventMap.containsKey(zone)){
          for(Event a : eventMap.get(zone)){
             String recordString = '"'+a.CHAMAgency__r.Name+'";"'+a.Subject+'";"'+a.Account.Name+'";"'+a.CreatedDate +'";"'+a.ActivityDate+'";"'+a.InterventionType__c +'";"'+a.MaintenancyQuoteNumber__c +'";"'+a.Account.CustomerNumber__c+'"\n';
             finalstr = finalstr + recordString; 
          }              
        }
        
        if(chamSettings.containsKey(zone)){
          // BUILD EMAIL MESSAGE
          ChamEmailSupport__c chamSetting = chamSettings.get(zone);
          Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
          attachment.setFileName(zone+'.csv');
          attachment.setBody(Blob.valueOf(finalstr.replace('null', '')));
          attachment.setContentType('text/csv;charset=UTF-8');         
          Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
          message.setFileAttachments( new Messaging.EmailFileAttachment[] {attachment} );
          message.setSubject('Rapport Cham '+zone);
          message.setPlainTextBody(chamSetting.ContenuEmail__c);
          if(!String.isEmpty(chamSetting.ManagerEmails__c)){
            message.setccAddresses(chamSetting.ManagerEmails__c.replace('\n',';').replace('\r',';').replace(';;',';').replace(' ','').split(';'));
          }
          if(!String.isEmpty(chamSetting.AgentEmails__c)){
            message.setToAddresses(chamSetting.AgentEmails__c.replace('\n',';').replace('\r',';').replace(';;',';').replace(' ','').split(';'));
            messages.add(message);
          }
        }
      }

      // SEND MESSAGE
      if(messages.size() < 100){
         List<Messaging.SendEmailResult> results = Messaging.sendEmail( messages );
         system.debug('results '+ results);              
      }
   }     
} //END