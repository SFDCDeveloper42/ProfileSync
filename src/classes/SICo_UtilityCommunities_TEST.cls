/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Tests of SICo_UtilityCommunities_CLS
History
10/08/2016      Mehdi Cherfaoui      Create version
------------------------------------------------------------*/
@isTest
private class SICo_UtilityCommunities_TEST {
    
    /**
     * [Test of getAccountId() method, with a community user]
     */
    @isTest static void getAccountId_CommunityUser() {
        //Insert new community user
        User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser;
        //Retrieve AccountId
        communityUser = [SELECT Id, AccountId FROM User WHERE Id =: communityUser.Id];
        //Run as Community User
        system.runAs(communityUser){
            Test.startTest();
            Id testAccountId = SICo_UtilityCommunities_CLS.getAccountId(null);
            Test.stopTest();
            //Expected result: AccountId of the Community User is returned
            system.assertEquals(communityUser.AccountId, testAccountId);
        }
    }

    /**
     * [Test of getAccountId() method, with a Salesforce user and a requested AccountId]
     */
    @isTest static void getAccountId_AllowedToRequestAccountId() {
        Account newAccount = SICo_UtilityTestDataSet.createAccount('Test First Name', 'Test Last Name', SICo_Constants_CLS.GUEST_PERSON_ACCOUNT_RECORD_TYPE);
        insert newAccount;
        //Run as current (non community) user and try to get this account Id
        Test.startTest();
        Id testAccountId = SICo_UtilityCommunities_CLS.getAccountId(newAccount.Id);
        Test.stopTest();
        //Expected result: AccountId of the requested account is returned
        system.assertNotEquals(null, testAccountId);
        system.assertEquals(newAccount.Id, testAccountId);
    }

    /**
     * [Test of getAccountId() method, with a community user and a requested AccountId, which is not allowed]
     */
    @isTest static void getAccountId_NotAllowedToRequestAccountId() {
        Account newAccount = new Account(Name = 'Test');
        insert newAccount;
        //Insert new community user
        User communityUser = SICo_UtilityTestDataSet.createCommunityUser();
        insert communityUser;
        //Retrieve its account Id
        communityUser = [SELECT Id, AccountId FROM User WHERE Id =:UserInfo.getUserId() LIMIT 1];
        //Run as Community User
        system.runAs(communityUser){
            Test.startTest();
            Id testAccountId = SICo_UtilityCommunities_CLS.getAccountId(newAccount.Id);
            Test.stopTest();
            //Expected result: AccountId of the requested account is NOT returned to the community user
            system.assertNotEquals(newAccount.Id, testAccountId);
            system.assertEquals(communityUser.AccountId, testAccountId);
        }
    }
}