/*toggleCssController.js*/
({
    //get all the sites for this account
    doInit  : function(component, event, helper) {

        component.set("v.currentPage", window.location.pathname + "?accountId=" + component.get("v.accountId"));

        var apexPath = '/'
        // if(window.location.pathname.includes('/apex/')){
        if (window.location.pathname.indexOf('/apex/') != -1) {
            apexPath = '/apex/';
        }
        component.set("v.apexPath", apexPath);


        var action = component.get("c.getSites");
        action.setParams({ idAccount : component.get("v.accountId") });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS"){
                component.set("v.sites", response.getReturnValue());
                if(response.getReturnValue().length != 0){
                 var taille = (response.getReturnValue()[0].SiteName__c.length);
                if (taille > 15){
                    var tooLong = true;
                }
                if (taille < 15){
                    var tooLong = false
                }
                component.set("v.tooLong", tooLong);
            }
        }
        });

        $A.enqueueAction(action);
    },
    //save the selected site
    saveSelectedSite  : function(component, event, helper) {

        var idSite = event.target.id;

        var action = component.get("c.setSelectSite");
        action.setParams({ idNewSite : idSite });

        action.setCallback(this, function(response) {
            var state = response.getState();
			window.location.reload();
            /*if (state === "SUCCESS"){
                setTimeout(function() {
                	$A.get('e.force:refreshView').fire();
                }, 10000);

            }*/
        });

        $A.enqueueAction(action);
    },
   
    //hide-show the differents sites
    toggle : function(component, event, helper) {
        var toggleMenu = component.find("toggleMenu");
        $A.util.toggleClass(toggleMenu, "toggle");
    }
})