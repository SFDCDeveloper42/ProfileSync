<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>[SICo] : Junction object between Bundle and RP (Custom Zuora Object)</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Bundle__c</fullName>
        <externalId>false</externalId>
        <label>Bundle</label>
        <referenceTo>SICo_Bundle__c</referenceTo>
        <relationshipName>Bundles_RP</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>DiscountType__c</fullName>
        <description>[SiCo] Discount type</description>
        <externalId>false</externalId>
        <label>Type de promotion</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>RoseDeal</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Starter</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EntretienBillingMode__c</fullName>
        <description>[SiCo] Billing Mode of Maintenance</description>
        <externalId>false</externalId>
        <label>Maintenance - Mode de facturation</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Annuel</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Mensuel</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>GasConsumption__c</fullName>
        <description>[SiCo] Gas consumer class</description>
        <externalId>false</externalId>
        <label>Consommation</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Conso1</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Conso2</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Conso3</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>GasType__c</fullName>
        <description>[SiCo] Gas Type</description>
        <externalId>false</externalId>
        <label>Gas Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>T1</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>T2</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>MaintenanceParts__c</fullName>
        <description>[SiCo] Separate Parts choice for Maintenance
]</description>
        <externalId>false</externalId>
        <label>Maintenance - Pièces</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Avec pièces</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Sans pièces</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Maintenance_Mode_de_facturation__c</fullName>
        <description>[SiCo] Billing Mode of Maintenance</description>
        <externalId>false</externalId>
        <label>Maintenance - Mode de facturation</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Annuel</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Mensuel</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ProductRatePlan__c</fullName>
        <externalId>false</externalId>
        <label>Product Rate Plan</label>
        <referenceTo>zqu__ProductRatePlan__c</referenceTo>
        <relationshipName>Bundles_RP</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ShippingMode__c</fullName>
        <externalId>false</externalId>
        <label>Mode de livraison</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Colissimo - standard</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Chronopost - express</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Chronorelais - relais colis</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Installateur</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>StandardVAT__c</fullName>
        <defaultValue>false</defaultValue>
        <description>[SiCo] Standard VAT to choose</description>
        <externalId>false</externalId>
        <label>TVA générique</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>StationBillingMode__c</fullName>
        <description>[SiCo] Billing mode of station</description>
        <externalId>false</externalId>
        <label>Station - Mode de facturation</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Comptant</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Récurrent</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Zone__c</fullName>
        <description>[SiCo] Zone of gas consumption</description>
        <externalId>false</externalId>
        <label>Zone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Zone 1</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Zone 2</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Zone 3</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Zone 4</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Zone 5</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Zone 6</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <gender>Masculine</gender>
    <label>Bundle Rate Plan</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>Tout</label>
    </listViews>
    <nameField>
        <displayFormat>B{0000}</displayFormat>
        <label>#</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Bundle Rate Plans</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
