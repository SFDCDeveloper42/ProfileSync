/**
* @author Boris Castellani
* @date 17/10/2016
*
* @description SICo_SiteMeterTrigger_CLS Call by trigger
*/
public with sharing class SICo_SiteMeterTrigger_CLS {

	public static void handleAfterInsertUpdate(final List<SICo_SiteMeter__c> siteMeters){

		final List<SICo_SiteMeter__c> siteMeterUpdate = new List<SICo_SiteMeter__c>();

		final Set<Id> setSite = new Set<Id>();
		final Set<Id> setSiteMeter = new Set<Id>();
		for(SICo_SiteMeter__c siteMeter : siteMeters){
			setSiteMeter.add(siteMeter.Id);
			setSite.add(siteMeter.Site__c);	
		}

		if(!setSiteMeter.isEmpty()){

			Map<Id,SICo_SiteMeter__c> allSiteMeters = new Map<Id,SICo_SiteMeter__c>([SELECT Id, Site__c, IsActive__c, MeterType__c, DeactivationDate__c, 
											Site__r.ActiveSiteMeterGas__c, Site__r.ActiveSiteMeterElec__c 
											FROM SICo_SiteMeter__c
											WHERE Site__c IN :setSite
											AND Id NOT IN :setSiteMeter
											AND IsActive__c = true
											]);

			final Map<Id,SICo_Site__c> sitesUpdate = new Map<Id,SICo_Site__c>();

			for(SICo_SiteMeter__c siteMeter : siteMeters){
				SICo_Site__c aSite = new SICo_Site__c(id=siteMeter.Site__c);
				if(siteMeter.IsActive__c == true){
					if(siteMeter.MeterType__c == SICo_Constants_CLS.STR_PCE){
						if(sitesUpdate.containsKey(aSite.Id)){
							sitesUpdate.get(aSite.Id).ActiveSiteMeterGas__c = siteMeter.Id;
						}else{
							aSite.ActiveSiteMeterGas__c = siteMeter.Id;
							sitesUpdate.put(aSite.Id,aSite);
						}				
					}else{
						if(sitesUpdate.containsKey(aSite.Id)){
							sitesUpdate.get(aSite.Id).ActiveSiteMeterElec__c = siteMeter.Id;
						}else{
							aSite.ActiveSiteMeterElec__c = siteMeter.Id;
							sitesUpdate.put(aSite.Id,aSite);
						}
					}	
					for(SICo_SiteMeter__c otherSiteMeter : allSiteMeters.values()){
						if(otherSiteMeter.Site__c == siteMeter.Site__c && otherSiteMeter.MeterType__c == siteMeter.MeterType__c){
							otherSiteMeter.IsActive__c = false;
							otherSiteMeter.DeactivationDate__c = date.today();
							siteMeterUpdate.add(otherSiteMeter);
						}
					}
				}else{
					sitesUpdate.put(aSite.Id,aSite);
				}
			}

			try{
				if(!siteMeterUpdate.isEmpty()){
					update siteMeterUpdate;
				}
				if(!sitesUpdate.isEmpty()){
					update sitesUpdate.values();
				}
			}catch(Exception e){
	            SICo_LogManagement.addErrorLog(SICo_Constants_CLS.SICO, 'SICo_SiteMeterTrigger_CLS - handleBeforeInsertUpdate', e.getMessage());
			}


		} //END IF
	}
}