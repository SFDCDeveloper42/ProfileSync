/*------------------------------------------------------------
Author:        Mehdi Cherfaoui
Company:       Salesforce.com
Description:   Utilities for the Community controllers
Test Class:    SICo_UtilityCommunities_TEST
History
10/08/2016      Mehdi Cherfaoui      Create version
03/10/2016      Ikumbi               Add callZuoraLogin
24/10/2016      Mehdi Cherfaoui      Differenciate between internal and external users for Zuora frame ids
------------------------------------------------------------*/
public with sharing class SICo_UtilityCommunities_CLS {

    /**
     * [Calculate the accountId associated with a user, and returns it, unless another account is requested]
     * @param  requestedAccountId [Id of the account requested - might be null]
     * @return                    [Id of the requested account (if allowed), or Id the Account associated to the current user]
     */
    public static Id getAccountId(String requestedAccountId){
        Id accountId = null;
        //only "full" Salesforce users are allowed to request a specific AccountId
        Boolean isAllowedToRequestAccountId = isInternalUser();
        system.debug('isAllowedToRequestAccountId: '+isAllowedToRequestAccountId);
        if(isAllowedToRequestAccountId && requestedAccountId != null && requestedAccountId != ''){
            //SELECT (with sharing) to make sure the accountId is valid and accessible to the requesting user
            List<Account> returnedAccountList = [SELECT Id FROM Account WHERE Id =:requestedAccountId AND IsPersonAccount = true LIMIT 1];
            if(returnedAccountList.size() > 0) accountId = returnedAccountList[0].Id;
        }else{//if no requested account, return AccountId associated with the current user
            User us = [SELECT Id, AccountId FROM User WHERE Id =:UserInfo.getUserId() LIMIT 1];
            accountId = us.AccountId;
        }
        return accountId;
    }

    /**
     * [Return the registration URL to be displayed in the Community Login Pages]
     * @param  startUrl [Community startUrl - used to calculate if the registration process is for Client or Guest]
     * @return          [registration Url]
     */
    public static String registrationUrl(String startUrl){
        //Default: Register as a Client
        String loginPageUrl = SICo_Constants_CLS.CLIENT_REGISTRATION_URL;
        //If in Public Community, register as a Guest
        if(startUrl != null)
        {
             if(startUrl.startsWith(SICo_Constants_CLS.CUSTOMER_COMMUNITY_START_URL) || startUrl.startsWith(SICo_Constants_CLS.CUSTOMER_COMMUNITY_ALT_START_URL))
            {
                loginPageUrl =Page.CommunitiesSelfReg.getUrl();
            }
        }
        return loginPageUrl;
    }

	/*
	* Call Zuora in REST and return result to the page (remote action serialized response)
	*/
    public static Map<String, String> callZuoraLogin(String pPageType){
    	//Get custom settings
    	ZuoraWSconfig__c zc = ZuoraWSconfig__c.getInstance();

        HttpRequest req = new HttpRequest();
        
        String uri = zc.Uri__c;

        String method = 'POST';

        //Get pageId
        String pageId = pPageType == 'CB' ? zc.ExternalPageIdCB__c : zc.ExternalPageIdSEPA__c;
        if(isInternalUser()) pageId = pPageType == 'CB' ? zc.PageIdCB__c : zc.PageIdSEPA__c;

        //Set HTTPRequest Method
        req.setMethod(method);

        //Set HTTPRequest header properties
        req.setHeader('Content-Type', 'application/json');
		req.setHeader('apiAccessKeyId', '{!HTMLENCODE($Credential.Username)}');
		req.setHeader('apiSecretAccessKey', '{!HTMLENCODE($Credential.Password)}');
		req.setHeader('pageId', pageId);

        // Set the HTTPRequest endpoint
        req.setEndpoint('callout:zuoraRsaSignatures');
        Map <String, String> mRequest = new Map <String, String>();
        mRequest.put('uri', uri);
        mRequest.put('method', method);
        mRequest.put('pageId', pageId);


        //Set the HTTPRequest body
        req.setBody(JSON.serialize(mRequest));

        Http http = new Http();
        system.debug('sending request to Zuora: '+req);
        system.debug('sending request to Zuora - body: '+req.getBody());
		HTTPResponse res = http.send(req);
		String response = res.getBody();
        system.debug('response received from Zuora: '+res);
        system.debug('response received from Zuora - body: '+response);
		Map<String, Object> mResponse = (Map<String, Object>) JSON.deserializeUntyped(response);

		String signature = (String) mResponse.get('signature');
		String token = (String) mResponse.get('token');
		String tenantId = (String) mResponse.get('tenantId');
		String key = (String) mResponse.get('key');

		Map<String, String> mapSignature = new Map<String, String>();
		mapSignature.put('tenantId', tenantId);
		mapSignature.put('pageId', pageId);
		mapSignature.put('signature', signature);
		mapSignature.put('token', token);
		mapSignature.put('key', key);

		return mapSignature;
	}

    /**
     * [Check if the current User is Internal or External]
     * @return [True if current User type = 'Standard']
     */
    public static Boolean isInternalUser(){
        //Internal Users have the "Standard" User Type
        return (UserInfo.getUserType() == SICo_Constants_CLS.USER_TYPE_SALESFORCE);
    }

}