/*------------------------------------------------------------
Author:        Mehdi BEGGAS
Company:       Salesforce.com
Description:   Controller for the SendMessage page 
Test Class:    SICo_SendMessage_VFP_TEST
History
30/08/2016      Mehdi BEGGAS      Create version
------------------------------------------------------------*/
public with sharing class SICo_SendMessage_VFP {

	public Id accountId{get;set;}

    public String motif{get;set;}
    public String objet{get;set;}
    public String description{get;set;}
    public boolean isWebCallBack{get;set;}
    public boolean checkboxrecall{get;set;}

    public List<SICo_CaseMotif__mdt> listMotif{get;set;}
    public List<SICo_CaseObjet__mdt> listObjet{get;set;}

    public String linkMyCases{get;set;}

	public SICo_SendMessage_VFP() {
        //Get AccountId
        String requestedAccountId = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_ACCOUNTID);
        String recall = System.currentPageReference().getParameters().get(SICo_Constants_CLS.URL_PARAM_CALLBACK);
            if(recall != null){
                isWebCallBack = true;
            }else{
                isWebCallBack = false;
            }
         if(requestedAccountId != null)
            linkMyCases = Page.Cases.getURL() + '?accountId=' + requestedAccountId;
        else
            linkMyCases = Page.Cases.getURL();

        try{
            this.accountId = SICo_UtilityCommunities_CLS.getAccountId(requestedAccountId);
        }catch (exception e){
            System.debug('Unable to return accountId');
        } 
        if(accountId != null){
            listMotif = [SELECT Id, SalesforceLabel__c, MasterLabel
                        FROM SICo_CaseMotif__mdt
                        ORDER BY SalesforceLabel__c asc
                        LIMIT 1000]; 

            listObjet = [SELECT Id, SalesforceLabel__c, MasterLabel, Case_Motif__c
                        FROM SICo_CaseObjet__mdt
                        WHERE Case_Motif__c = :listMotif.get(0).Id
                        ORDER BY SalesforceLabel__c asc
                        LIMIT 1000];
        }
        else{
            listMotif = new List<SICo_CaseMotif__mdt>();
            listObjet = new List<SICo_CaseObjet__mdt>();
        }
    }

    public PageReference createCase(){

        Boolean webCallBack = false;
        if(this.isWebCallBack != null) webCallBack = this.isWebCallBack;
        if(this.motif == null || this.motif =='None') return null; 
        Case newCase = new Case(
            RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + SICo_Constants_CLS.CUSTOMER_DEMAND_CASE_RECORD_TYPE),
            AccountId = this.accountId,
            Status = SICo_Constants_CLS.STATUS_PICKLIST_VALUE_NEW,
            Subject__c = this.motif,
            Heading__c = this.objet,
            Description = this.description,
            Origin = SICo_Constants_CLS.ORIGIN_PICKLIST_VALUE_WEB,
            IsWebCallBack__c = this.isWebCallBack
        );
        insert newCase;

        System.debug(newCase.Id);

        PageReference pageRef = new PageReference(linkMyCases);
        pageRef.setRedirect(true);
        return pageRef;

    }

    public PageReference cancelCase(){
        PageReference pageRef = new PageReference(linkMyCases);
        pageRef.setRedirect(true);
        return pageRef;
    }

    public List<SelectOption> getmotifs() {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('None', Label.SICo_Reason_CST));
        for(SICo_CaseMotif__mdt motif : listMotif)
            options.add(new SelectOption(motif.SalesforceLabel__c, motif.SalesforceLabel__c));
        return options;
    }

    public List<SelectOption> getObjects() {

        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None', Label.SICo_Reason_CST));

        for(SICo_CaseObjet__mdt obj : listObjet)
            options.add(new SelectOption(obj.SalesforceLabel__c, obj.SalesforceLabel__c));
        return options;
    }

    public PageReference updateObjects() {
        System.debug(motif);

        listObjet = [SELECT Id, SalesforceLabel__c, MasterLabel, Case_Motif__c
                    FROM SICo_CaseObjet__mdt
                    WHERE Case_Motif__c IN (SELECT Id
                                            FROM SICo_CaseMotif__mdt
                                            WHERE MasterLabel LIKE :motif)
                    LIMIT 1000];

        System.debug(listObjet.size());

        return null;
    }

}