/**
* @author Boris Castellani
* @date 20/10/2016
*
* @description TEST SICo_SiteMeterTrigger_CLS Call by trigger
*/

@isTest
private class SICo_SiteTrigger_TEST {

    static testMethod void handleBeforeInsert() {
    	
    	//DataSet
    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;
    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    		site.GasSource__c = 'Ville';
    		site.PrincipalHeatingSystemType__c = 'Gaz';
    		site.BoilerAge__c = 'Moins de 5 ans';
    		site.IsCeramicGlassPlatePresent__c = true;
    		site.PrincHeatingSystemTypeDetail__c = 'Convecteur';
    		site.NoOfOccupants__c = 3;
    		site.SurfaceInSqMeter__c = 100;
    	Insert site;

		// TEST & ASSERT
		Test.startTest();
			SICo_Site__c siteNow = [ SELECT Id, InitialBoilerAge__c,InitialGasSource__c,InitialCeramicGlassPlatePresent__c,
											InitialPrincHeatingSystemTypeDetail__c,InitialSanitaryHotWaterType__c,
											InitialNoOfOccupants__c,InitialSurfaceInSqMeter__c, SiteNumber__c,
											InitialPrincipalHeatingSystemType__c
											FROM SICo_Site__c
											WHERE Id = :site.Id ];
		Test.stopTest();

        //ASSERT
        System.assertEquals(site.BoilerAge__c, siteNow.InitialBoilerAge__c);
        System.assertEquals(site.GasSource__c , siteNow.InitialGasSource__c);
        System.assertEquals('oui', siteNow.InitialCeramicGlassPlatePresent__c);
        System.assertEquals(site.PrincHeatingSystemTypeDetail__c, siteNow.InitialPrincHeatingSystemTypeDetail__c);
        System.assertEquals(siteNow.InitialSanitaryHotWaterType__c, null);
        System.assertEquals(site.PrincipalHeatingSystemType__c, siteNow.InitialPrincipalHeatingSystemType__c);
        System.assertEquals(site.NoOfOccupants__c , siteNow.InitialNoOfOccupants__c);
        System.assertEquals(site.SurfaceInSqMeter__c , siteNow.InitialSurfaceInSqMeter__c);
    
    }

    static testMethod void handleBeforeUpsert() {
    	
    	//DataSet
    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;
    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    		site.PrincipalHeatingSystemType__c = 'Gaz';
    		site.BoilerAge__c = 'Moins de 5 ans';
    		site.GasSource__c = 'Ville';
    		site.IsCeramicGlassPlatePresent__c = true;
    		site.NoOfOccupants__c = 3;
    		site.SurfaceInSqMeter__c = 100;
    	Insert site;


    	// Update
    		site.GasSource__c = 'Bouteille';
    		site.PrincipalHeatingSystemType__c = 'Gaz';
    		site.BoilerAge__c = 'Moins de 5 ans';
    		site.IsCeramicGlassPlatePresent__c = true;
    		site.PrincHeatingSystemTypeDetail__c = 'Convecteur';
    		site.SanitaryHotWaterType__c = 'Collectif';
    		site.NoOfOccupants__c = 5;
    		site.SurfaceInSqMeter__c = 130;
		Update site;


		// TEST & ASSERT
		Test.startTest();
			SICo_Site__c siteNow = [ SELECT Id, InitialBoilerAge__c,InitialGasSource__c,InitialCeramicGlassPlatePresent__c,
											InitialPrincHeatingSystemTypeDetail__c,InitialSanitaryHotWaterType__c,
											InitialNoOfOccupants__c,InitialSurfaceInSqMeter__c,InitialPrincipalHeatingSystemType__c
											FROM SICo_Site__c
											WHERE Id = :site.Id ];
		Test.stopTest();

        //ASSERT            
        System.assertNotEquals(site.GasSource__c , siteNow.InitialGasSource__c);
        System.assertEquals(site.PrincipalHeatingSystemType__c, siteNow.InitialPrincipalHeatingSystemType__c);
        System.assertEquals('oui', siteNow.InitialCeramicGlassPlatePresent__c);
        System.assertEquals(site.PrincHeatingSystemTypeDetail__c, siteNow.InitialPrincHeatingSystemTypeDetail__c);
        System.assertEquals(site.SanitaryHotWaterType__c, siteNow.InitialSanitaryHotWaterType__c);      
        System.assertNotEquals(site.NoOfOccupants__c , siteNow.InitialNoOfOccupants__c);
        System.assertNotEquals(site.SurfaceInSqMeter__c , siteNow.InitialSurfaceInSqMeter__c);
        
    }

    static testMethod void handleBeforeUpdate() {

    	//DataSet
    	Account acc = SICo_UtilityTestDataSet.createAccount('Skywalker', 'Luck', 'PersonAccount');
    	Insert acc;
    	SICo_Site__c site = SICo_UtilityTestDataSet.createSite(acc.Id);
    	Insert site;

    	final List<SICo_Meter__c> meters = new List<SICo_Meter__c>();
    	meters.add(SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PCE)); 
    	meters.add(SICo_UtilityTestDataSet.createMeter(SICo_Constants_CLS.STR_PCE)); 
    	Insert meters;

    	SICo_SiteMeter__c siteMeter = SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE, site.Id, meters.get(0).Id);
    	Insert siteMeter;


 		// TEST & ASSERT
		Test.startTest();
			//ASSERT 1		
    		SICo_SiteMeter__c siteMeter2 = SICo_UtilityTestDataSet.createSiteMeter(SICo_Constants_CLS.STR_SITEMETER_PCE, site.Id, meters.get(1).Id);
    		Insert siteMeter2;
    		SICo_Site__c siteNow = [SELECT Id, ActiveSiteMeterGas__c FROM SICo_Site__c WHERE Id = :site.Id];
    		SICo_SiteMeter__c siteMeterNow = [SELECT Id, IsActive__c FROM SICo_SiteMeter__c WHERE Id = :siteMeter.Id];
			System.assertEquals(siteMeter2.Id, siteNow.ActiveSiteMeterGas__c);
			System.assertEquals(siteMeterNow.IsActive__c, false);

 			//ASSERT 2	
			siteMeter2.IsActive__c = false;
			Update siteMeter2;
    		SICo_Site__c siteNow2 = [SELECT Id, IsActive__c, ActiveSiteMeterGas__c FROM SICo_Site__c WHERE Id = :site.Id];
		Test.stopTest(); 

        System.assertEquals(siteNow2.IsActive__c, false);
        System.assertEquals(siteNow2.ActiveSiteMeterGas__c, null);  

    }

}