/**
* @author Boris Castellani
* @date 21/11/2016
*
* @description Zuora__SubscriptionProductCharge__c Trigger before update
*/
trigger SICo_ZSubscriptionProductChargeBeforeUpdate_TRG on Zuora__SubscriptionProductCharge__c(before update) {
	if(PAD.canTrigger('SICo_ZSubscriptionProductChargeBeforeUpdate_TRG')) {
		SICo_SubscriptionPrdCharge_Utility.handlerBeforeUpsert(trigger.New);
	}      
}