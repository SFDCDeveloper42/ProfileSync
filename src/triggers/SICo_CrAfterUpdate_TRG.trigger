/**
* @author Sebastien Colladon
* @date 07/11/2016
*
* @description SICo_CR__c Trigger After update
*/
trigger SICo_CrAfterUpdate_TRG on SICo_CR__c(after update) {
  if(PAD.canTrigger('SICo_CrAfterUpdate_TRG')) {
    SICo_CrTrigger_CLS.HandleAfterUpdate(Trigger.new);
  }
}