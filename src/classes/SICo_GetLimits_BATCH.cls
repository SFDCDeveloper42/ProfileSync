/*------------------------------------------------------------
Author:        Olivier Vrbanac
Company:       Salesforce.com
Description:   Batch Limits on SICo Objects and API Limits
Test Class:
History
17/09/2016     Olivier Vrbanac    Create version
------------------------------------------------------------*/
global with sharing class SICo_GetLimits_BATCH implements Schedulable, Database.Batchable<sObject>, Database.AllowsCallouts {
	
	String query = 'SELECT Id FROM Account LIMIT 1';
	
	global SICo_GetLimits_BATCH() {}
	
	global Database.QueryLocator start( Database.BatchableContext BC ) {
		return Database.getQueryLocator( query );
	}

   	global void execute( Database.BatchableContext BC, List<sObject> scope ) {
	
		try { 

			String sJWT = SICo_JsonSignature_CLS.getStandardJWT( 'Salesforce', false );
			System.debug( '##### sJWT = ' + sJWT );
	        CalloutConfig__c oCC = CalloutConfig__c.getValues( 'Salesforce' );
	        String sEndPoint = oCC.Endpoint__c;

	    	Http oHttp = new Http();
	   		String sRESTServerUrl = sEndPoint + '/services/oauth2/token';
	   		HttpRequest oRequest = new HttpRequest();
	    	oRequest.setEndpoint( sRESTServerUrl );
			oRequest.setMethod( 'POST' );
			oRequest.setHeader('content-type', 'application/x-www-form-urlencoded');
			oRequest.setBody( 'grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer' +
							  '&assertion=' + sJWT );

			HttpResponse oResponse = new HttpResponse();
			if (  Test.isRunningTest() ) {
				oResponse.setStatusCode( 200 );
				oResponse.setBody( '{ "access_token" : "00D4E0000008nfQ!AQwAQKgl1PxDh76bol64zbR8I3wz44gkhUpFXXdI0ulGwUtxeFESTSntIZU9yULiegq4oKWl81YCYjvICf_pyDYvuDVaooXn", "scope" : "full", "instance_url" : "https://test.salesforce.com", "id" : "https://test.salesforce.com/id/00D4E0000008nfQUAQ/00558000000yPtgAAE", "token_type" : "Bearer" }' );
			} else {
				oResponse = oHttp.send( oRequest );
			}

			String sSessionId = '';
			system.JSONParser oJSONParser;
			String fieldName;
			if ( oResponse.getStatusCode() == 200 ) {
				oJSONParser = JSON.createParser( oResponse.getBody() );
				while (oJSONParser.nextToken() != null) {
					oJSONParser.getText();
					if ( oJSONParser.getCurrentToken() == JSONToken.FIELD_NAME ){
						fieldName = oJSONParser.getText();
						oJSONParser.nextToken(); 
						if( fieldName == 'access_token' ){
							sSessionId = oJSONParser.getText();
							break;
						}
					}
				}
			}

			sRESTServerUrl = sEndPoint + '/services/data/v37.0';
	    	String sRESTLimits = sRESTServerUrl + '/limits';
	    	
	    	oHttp = new Http();
	    	oRequest = new HttpRequest();
	    	oRequest.setEndpoint( sRESTLimits );
			oRequest.setMethod( 'GET' );
			oRequest.setHeader( 'Authorization', 'Bearer ' + sSessionId );
			oRequest.setTimeout( 120000 );
			oRequest.setCompressed( true );

			oResponse = new HttpResponse();
			if (  Test.isRunningTest() ) {
				oResponse.setStatusCode( 200 );
				oResponse.setBody( '{ "DailyApiRequests" : {"Max" : 15000,"Remaining" : 14998},"DataStorageMB" : {"Max" : 5,"Remaining" : 5},"FileStorageMB" : {"Max" : 20,"Remaining" : 20}}' );
			} else {
				oResponse = oHttp.send( oRequest );
			}

	   		integer iMaxApi = 0;
	   		integer iRemainingApi = 0;
	   		integer iMaxData = 0;
	   		integer iRemainingData = 0;
	   		integer iMaxFile = 0;
	   		integer iRemainingFile = 0;

	   		Boolean bMaxApi = false;
	   		Boolean bRemainingApi = false;
	   		Boolean bMaxData = false;
	   		Boolean bRemainingData = false;
	   		Boolean bMaxFile = false;
	   		Boolean bRemainingFile = false;

			if ( oResponse.getStatusCode() == 200 ) {
				oJSONParser = JSON.createParser( oResponse.getBody() );
				while (oJSONParser.nextToken() != null) {
					oJSONParser.getText();
					if ( oJSONParser.getCurrentToken() == JSONToken.FIELD_NAME ){
						fieldName = oJSONParser.getText();
						oJSONParser.nextToken(); 

						if( fieldName == 'DailyApiRequests' ){
							if( oJSONParser.getCurrentToken() == JSONToken.START_OBJECT ){
						  		while( oJSONParser.nextToken() != null ){
						  			if( oJSONParser.getCurrentToken() == JSONToken.FIELD_NAME ){
						  				if( oJSONParser.getText() == 'Max' ){
						  					oJSONParser.nextToken();
						  					iMaxApi = integer.valueOf( oJSONParser.getText() );
						  					bMaxApi = true;
						  				}
						  				if( oJSONParser.getText() == 'Remaining' ){
						  					oJSONParser.nextToken();
						  					iRemainingApi = integer.valueOf( oJSONParser.getText() );
						  					bRemainingApi = true;
						  				}
						  			}
						  			if( bMaxApi && bRemainingApi ) break;
						  		}
							}
						}

						if( fieldName == 'DataStorageMB' ){
							if( oJSONParser.getCurrentToken() == JSONToken.START_OBJECT ){
						  		while( oJSONParser.nextToken() != null ){
						  			if( oJSONParser.getCurrentToken() == JSONToken.FIELD_NAME ){
						  				if( oJSONParser.getText() == 'Max' ){
						  					oJSONParser.nextToken();
						  					iMaxData = integer.valueOf( oJSONParser.getText() );
						  					bMaxData = true;
						  				}
						  				if( oJSONParser.getText() == 'Remaining' ){
						  					oJSONParser.nextToken();
						  					iRemainingData = integer.valueOf( oJSONParser.getText() );
						  					bRemainingData = true;
						  				}
						  			}
						  			if ( bMaxData && bRemainingData ) break;
						  		}
							}
						}

						if( fieldName == 'FileStorageMB' ){
							if( oJSONParser.getCurrentToken() == JSONToken.START_OBJECT ){
						  		while( oJSONParser.nextToken() != null ){
						  			if( oJSONParser.getCurrentToken() == JSONToken.FIELD_NAME ){
						  				if( oJSONParser.getText() == 'Max' ){
						  					oJSONParser.nextToken();
						  					iMaxFile = integer.valueOf( oJSONParser.getText() );
						  					bMaxFile = true;
						  				}
						  				if( oJSONParser.getText() == 'Remaining' ){
						  					oJSONParser.nextToken();
						  					iRemainingFile = integer.valueOf( oJSONParser.getText() );
						  					bRemainingFile = true;
						  				}
						  			}
						  			if( bMaxFile && bRemainingFile ) break;
						  		}
							}
						}

					}
				}	
			}

			/*sRESTServerUrl = sEndPoint + '/services/apexrest';
	    	sRESTLimits = sRESTServerUrl + '/sonde';
	    	
	    	oHttp = new Http();
	    	oRequest = new HttpRequest();
	    	oRequest.setEndpoint( sRESTLimits );
			oRequest.setMethod( 'POST' );
			oRequest.setHeader( 'Authorization', 'Bearer ' + sSessionId );
			oRequest.setHeader('content-type', 'application/json; charset=UTF-8');
			oRequest.setBody( '{ "nbApi" : "' + String.valueOf( iMaxApi - iRemainingApi ) + '", "nbDataStorage" : "' + String.valueOf( iMaxData - iRemainingData )  + '", "nbFileStorage" : "' + String.valueOf( iMaxFile - iRemainingFile ) + '" }'  );
			oRequest.setCompressed(true);

			oResponse = new HttpResponse();
			if (  !Test.isRunningTest() ) {
				oResponse = oHttp.send( oRequest );
			}*/

			//DEFINE PAGE TO PARSE
	        Map<String, String> infos = new Map<String, String>();
	        PageReference pRef = new PageReference('/setup/org/orgstorageusage.jsp?id=' + UserInfo.getOrganizationId());
	        pRef.setRedirect(true);
	        
	        String content = null;
	        
	        if (!System.Test.isRunningTest()) {
	            content = pRef.getContent().toString();
	        } else {
	            content = '<span  class="00T">Task</span>123</td>';
	        }

	        List<Element> elements = new List<Element>();

	        elements.add(new Element('NB_LEAD', '<span  class="00Q">', '</td>'));
	        elements.add(new Element('NB_ACCOUNT', '<span  class="001">', '</td>'));
	        elements.add(new Element('NB_CONTACT', '<span  class="003">', '</td>'));
	        elements.add(new Element('NB_BENEFICIARY', '<span  class="a1R">', '</td>'));
	        elements.add(new Element('NB_SITE', '<span  class="a1N">', '</td>'));
	        elements.add(new Element('NB_CASE', '<span  class="500">', '</td>'));
	        elements.add(new Element('NB_TASK', '<span  class="00T">', '</td>'));
	        elements.add(new Element('NB_EVENT', '<span  class="00U">', '</td>'));
	        elements.add(new Element('NB_OPPORTUNITY', '<span  class="006">', '</td>'));

	        elements.add(new Element('NB_INVOICE', '<span  class="a0J">', '</td>'));
	        elements.add(new Element('NB_INVOICELINEITEM', '<span  class="a1k">', '</td>'));
	        elements.add(new Element('NB_SUBSCRIPTIONPRODUCTCHARGE', '<span  class="a0C">', '</td>'));
	        elements.add(new Element('NB_SUBSCRIPTIONFEATURE', '<span  class="a0D">', '</td>'));
	        elements.add(new Element('NB_SUBSCRIPTIONRATEPLAN', '<span  class="a0F">', '</td>'));

	        elements.add(new Element('NB_PAIRING', '<span  class="a1L">', '</td>'));
	        elements.add(new Element('NB_BASKET', '<span  class="a1V">', '</td>'));
	        elements.add(new Element('NB_QUOTE', '<span  class="a11">', '</td>'));
	        elements.add(new Element('NB_QUOTEPRODUCTFEATURE', '<span  class="a0u">', '</td>'));
	        elements.add(new Element('NB_QUOTERATEPLAN', '<span  class="a0w">', '</td>'));
	        elements.add(new Element('NB_QUOTERATEPLANCHARGE', '<span  class="a0v">', '</td>'));

	        for (Element e : elements) {
	            infos.put(e.propertyName, '0');
	            Integer i = 0;
	            Integer j = 0;
	            String value;

	            System.debug('###CONTENT' + content);
	            i = content.indexOf(e.start);
	            if (i > -1) {
	                j = content.indexOf(e.stop, i);
	                System.debug('###content.i : ' + i);
	                System.debug('###content.j : ' + j);
	                
	                if (j > -1) {
	                    String subString = content.subString(i, j);
	                    subString = subString.replace(e.start, '');
	        
	                    i = subString.lastIndexOf('>');
	                    System.debug('###sub.i : ' + i);
	        
	                    value = subString.subString(i + 1).replace('&nbsp;', '').replaceAll('[^\\w]', '');
	        
	                    if (value.endsWith('KB')) {
	                        value = value.substring(0, value.length() - 2);
	                        value = Decimal.valueOf(value).divide(1000000, 6).toPlainString();
	                    }
	                    else if (value.endsWith('MB')) {
	                        value = value.substring(0, value.length() - 2);
	                        value = Decimal.valueOf(value).divide(1000, 6).toPlainString();
	                    }
	                    infos.put(e.propertyName, value);
	                    System.debug('####' + e.propertyName + ' : ' + value);
	                }
	            }
	        }
	        
	        //CREATE RECORD
	   		SICo_Limit__c oLimit = new SICo_Limit__c();
	   		oLimit.Timestamp__c = datetime.now();
	   		oLimit.TimestampText__c = String.valueOf( datetime.now().format('yy-MM-dd HH:mm') );

			oLimit.ApiCalls__c = Decimal.valueOf( iMaxApi - iRemainingApi );
			oLimit.DataStorage__c = Decimal.valueOf( iMaxData - iRemainingData );
			oLimit.FileStorage__c = Decimal.valueOf( iMaxFile - iRemainingFile );

	   		oLimit.Lead__c = Decimal.valueOf(infos.get('NB_LEAD'));
	   		oLimit.Account__c = Decimal.valueOf(infos.get('NB_ACCOUNT'));
	   		oLimit.Contact__c = Decimal.valueOf(infos.get('NB_CONTACT'));
	   		oLimit.Beneficiary__c = Decimal.valueOf(infos.get('NB_BENEFICIARY'));
	   		oLimit.Site__c = Decimal.valueOf(infos.get('NB_SITE'));
	   		oLimit.Case__c = Decimal.valueOf(infos.get('NB_CASE'));
	   		oLimit.Task__c = Decimal.valueOf(infos.get('NB_TASK'));
	   		oLimit.Event__c = Decimal.valueOf(infos.get('NB_EVENT'));
	   		oLimit.Opportunity__c = Decimal.valueOf(infos.get('NB_OPPORTUNITY'));

	   		oLimit.Invoice__c = Decimal.valueOf(infos.get('NB_INVOICE'));
	   		oLimit.InvoiceLineItem__c = Decimal.valueOf(infos.get('NB_INVOICELINEITEM'));
	   		oLimit.SubscriptionProductCharge__c = Decimal.valueOf(infos.get('NB_SUBSCRIPTIONPRODUCTCHARGE'));
	   		oLimit.SubscriptionProductFeature__c = Decimal.valueOf(infos.get('NB_SUBSCRIPTIONFEATURE'));
	   		oLimit.SubscriptionRatePlan__c = Decimal.valueOf(infos.get('NB_SUBSCRIPTIONRATEPLAN'));

	   		oLimit.Pairing__c = Decimal.valueOf(infos.get('NB_PAIRING'));
	   		oLimit.Basket__c = Decimal.valueOf(infos.get('NB_BASKET'));
	   		oLimit.Quote__c = Decimal.valueOf(infos.get('NB_QUOTE'));
	   		oLimit.QuoteProductFeature__c = Decimal.valueOf(infos.get('NB_QUOTEPRODUCTFEATURE'));
	   		oLimit.QuoteRatePlan__c = Decimal.valueOf(infos.get('NB_QUOTERATEPLAN'));
	   		oLimit.QuoteRatePlanCharge__c = Decimal.valueOf(infos.get('NB_QUOTERATEPLANCHARGE'));

			insert oLimit;

		} catch ( exception e ) {
			SICo_LogManagement.sendErrorLogAsync(
                'SICo',
                'SICo_GetLimits_BATCH',
                String.valueOf( e )
            );
		}

	}
	
	global void finish( Database.BatchableContext BC ) {}
    
    global void execute( SchedulableContext SC ) {
        
        Database.executeBatch( new SICo_GetLimits_BATCH() );
        
    }

    private class Element {

        String propertyName;
        String start;
        String stop;
        
        Element( String pN, String start, String stop ) {
            this.propertyName = pN;
            this.start = start;
            this.stop = stop;
        }
    }
	
}