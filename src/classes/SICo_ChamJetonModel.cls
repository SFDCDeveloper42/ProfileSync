/**
* @author Boris Castellani
* @date 08/02/2017
* @LastModify 
*
* @description Serialise & deserialize JSON CHAM
*/
public with sharing class SICo_ChamJetonModel {

	public class ChamJeton {
		public List<weeklyCalendar> weeklyCalendar;
		public ChamJeton parse (String json) {
			return (ChamJeton) System.JSON.deserialize(json, ChamJeton.class);
		}
	}

	/** 
	 * Wrapper 
	 */ 
	public class weeklyCalendar{
		public Date startDate;
		public List<Days> days;
	}

	public class Days {
		public String day;
		public String timeRange1;
		public String timeRange2;
		public String timeRange3;
		public String timeRange4;
	}
}