/*------------------------------------------------------------
Author:        Cécile Neaud
Company:       Ikumbi-solutions
Description:   Test Class for SICo_ChangeInformations_VFC
History
20/09/2016     Cécile Neaud     Create version
------------------------------------------------------------*/
@isTest
private class SICo_ChangeInformations_VFC_TEST {

	@testSetup static void initTestData() {

		Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
		insert account;
		SICo_Site__c oSICoSite = SICo_UtilityTestDataSet.createSite(account.Id);
		oSICoSite.IsActive__c = true;
		oSICoSite.IsSelectedInCustomerSpace__c=true;
		oSICoSite.PrincipalHeatingSystemType__c = 'Gaz';
		oSICoSite.SanitaryHotWaterType__c = 'Electrique';
		oSICoSite.NoOfOccupants__c = 4;
		oSICoSite.surfaceInSqMeter__c = 80;
		oSICoSite.InseeCode__c = '44109';
		oSICoSite.ConstructionDate__c = 1980;
		insert oSICoSite;

		CalloutConfig__c boomiConfig = SICo_UtilityTestDataSet.createCustomSetting('sicotest@edfelectricitedefrancesa-1PHLMS.8NBEEN','f6aabfd0-ecce-4625-bd38-8c298e668998','https://test.connect.boomi.com/ws/rest/c43/');
			insert boomiConfig;
	}

	@isTest static void methods_called_by_ligthning_test(){
		SICo_Site__c oSICoSite = [SELECT Id FROM SICo_Site__c WHERE Subscriber__r.FirstName = 'Test01' LIMIT 1];
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		SICo_ChangeInformations_VFC.getAccount(account.Id);
		SICo_ChangeInformations_VFC.updateInformations(account.Id,  '2 rue du test', 'impasse du test', '1', '2', '6',
																	'28', '44000', 'Nantes', 'France', 'testgmail.com', 
																	'0612345678', '0612345678' );

		Account acc  = new Account(Salutation = 'Madame', FirstName = 'Test', LastName = 'TestName', ContactAddressNumberStreet__c = '2 rue du test');
		System.assertEquals('2 rue du test', acc.ContactAddressNumberStreet__c);
	}

	@isTest static void constructor_test() {
		SICo_ChangeInformations_VFC controller = new SICo_ChangeInformations_VFC();
		SICo_Site__c oSICoSite = [SELECT Id FROM SICo_Site__c WHERE Subscriber__r.FirstName = 'Test01' LIMIT 1];
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		controller.accountId = account.Id;
	}

	/*test if there is several sites*/
	@isTest static void getSite_test(){
		Account account = [SELECT Id FROM Account WHERE FirstName = 'Test01' LIMIT 1];

		SICo_Site__c oSICoSite2 = SICo_UtilityTestDataSet.createSite(account.Id);
		oSICoSite2.IsActive__c = true;
		oSICoSite2.IsSelectedInCustomerSpace__c=true;
		insert oSICoSite2;

		Test.startTest();

		SICo_ChangeInformations_VFC.getAccount(account.Id);

		Test.stopTest();
	}

}