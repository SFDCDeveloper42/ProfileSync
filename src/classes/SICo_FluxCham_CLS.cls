/**
* @author Boris Castellani
* @date 13/07/2016
* @LastModify 20/02/2017
*
* @description Build JSON CHAM
*/
public with sharing class SICo_FluxCham_CLS {

  @future(callout=true)
  public static void executeFluxChamAsync(String methode, String endPoint, String body){
    executeFluxCham(methode,endPoint,body);
  }

  public static Boolean executeFluxCham(String methode, String endPoint, String body) {
    Boolean result = true;
    try {
      Map<String, String> headers = new Map<String, String>();
      headers.put('Content-Type', 'application/json;charset=UTF-8');
      headers.put('X-Cham-Token', SICo_JsonSignature_CLS.getStandardJWT('Cham', false));
      HttpResponse response = SICo_Utility.getCallout(methode,endPoint,body, headers);
    } catch (Exception e) {
      //Do not send to Logstash
      result = false;
    }
    return result;
  }

  /* -------- BUILD JSON -------- */    
  public static String builCHamClientContrat(Map<Id,Task> tasksTrigger){
    Set<Id> casesId = getCaseID(tasksTrigger.values());
    Map<ID,Case> caseMap = new Map<ID,Case>([SELECT CreatedDate, LastModifiedDate, Account.FirstName, Account.Salutation, Account.LastName, 
                                             Account.Phone, Account.PersonMobilePhone, Account.PersonEmail, Account.CustomerNumber__c,
                                             Site__r.AdditionToAddress__c, Site__r.NumberStreet__c, Site__r.Building__c, Site__r.Stairs__c, 
                                             Site__r.Floor__c, Site__r.PostalCode__c, Site__r.City__c, Site__r.SiteNumber__c,  
                                             Site__r.HousingType__c, Site__r.PCE__c, Site__r.BoilerBrand__c, Site__r.BoilerModel__c
                                             FROM Case where Id= :casesId]);

    final Map<ID,Task> tasks = new Map<ID,Task>([SELECT Id, CHAMAgency__r.AgencyNumber__c,Subscription__r.MaintenanceParts__c, InterventionType__c,
                                                 Subscription__r.Zuora__SubscriptionStartDate__c, Subscription__r.Zuora__ContractEffectiveDate__c, 
                                                 Subscription__r.Zuora__SubscriptionEndDate__c, TaskId__c
                                                 FROM Task WHERE Id= :tasksTrigger.keySet()]);

    TimeZone tz = Timezone.getTimeZone(LABEL.SICo_TimeZone);
    SICo_FluxChamModel.ChamClientContrat chamClientContrat = new SICo_FluxChamModel.ChamClientContrat();
    Case aCase = new case();
    Task aTask = new Task();
    for(Task theTask : tasksTrigger.values()){
      if(caseMap.containsKey(theTask.WhatId)){
        aCase = caseMap.get(theTask.WhatId);
        aTask = tasks.get(theTask.Id);
        SICo_FluxChamModel.ChamClient chamClient = new SICo_FluxChamModel.ChamClient();
        SICo_FluxChamModel.Gaz gaz = new SICo_FluxChamModel.Gaz();
        SICo_FluxChamModel.Entretien entretien = new SICo_FluxChamModel.Entretien();
        SICo_FluxChamModel.Appar appar = new SICo_FluxChamModel.Appar();
        chamClient.idTask = aTask.Id;
        chamClient.id_orchidee = aCase.Account.CustomerNumber__c + aCase.Site__r.SiteNumber__c;
        chamClient.id_societe = LABEL.SICo_IdSociete;
        chamClient.id_agence = aTask.CHAMAgency__r.AgencyNumber__c;

        chamClient.date_creation = aCase.CreatedDate.AddSeconds(tz.getOffset(aCase.CreatedDate)/1000);
        chamClient.date_modification = Datetime.now().AddSeconds(tz.getOffset(aCase.CreatedDate)/1000);
        chamClient.quano = aCase.Account.Salutation;
        chamClient.nomclient = aCase.Account.LastName;
        chamClient.preno = aCase.Account.FirstName;
        String allAddress ='';
        if(aCase.Site__r.AdditionToAddress__c == null){
          allAddress = aCase.Site__r.NumberStreet__c;
          chamClient.rue1 = (allAddress.length() <= 30) ? allAddress : allAddress.substring(0,30);
          chamClient.rue2 = (allAddress.length() > 30) ? allAddress.substring(30,Math.min(allAddress.length(),60)) : '';
          chamClient.rue3 = (allAddress.length() > 60) ? allAddress.substring(60,Math.min(allAddress.length(),90)) : '';
        } else if(aCase.Site__r.NumberStreet__c.length() <= 30 && aCase.Site__r.AdditionToAddress__c.length() <= 30){
            chamClient.rue1 = aCase.Site__r.NumberStreet__c;
            chamClient.rue2 = aCase.Site__r.AdditionToAddress__c;
        }else {
          allAddress = aCase.Site__r.NumberStreet__c +' '+ aCase.Site__r.AdditionToAddress__c;
          chamClient.rue1 = (allAddress.length() <= 30) ? allAddress : allAddress.substring(0,30);
          chamClient.rue2 = (allAddress.length() > 30) ? allAddress.substring(30,Math.min(allAddress.length(),60)) : '';
          chamClient.rue3 = (allAddress.length() > 60) ? allAddress.substring(60,Math.min(allAddress.length(),90)) : '';
        }   
        chamClient.codepo = aCase.Site__r.PostalCode__c;
        chamClient.ville = aCase.Site__r.City__c;
        chamClient.batim = aCase.Site__r.Building__c;
        chamClient.escal = aCase.Site__r.Stairs__c;
        chamClient.etage = aCase.Site__r.Floor__c;
        if (aCase.Account.Phone != null){
          chamClient.teldo = String.valueOf (aCase.Account.Phone).replace(' ','').replace('+330','0').replace('+33','0');
        }
        if (aCase.Account.PersonMobilePhone != null){
          chamClient.telmo = String.valueOf (aCase.Account.PersonMobilePhone).replace(' ','').replace('+330','0').replace('+33','0');
        }
        chamClient.typlog = aCase.Site__r.HousingType__c;
        chamClient.tatva = LABEL.SICo_TaTva;
        chamClient.email = aCase.Account.PersonEmail;
        gaz.pce = aCase.Site__r.PCE__c;

        Final String STAR = '**';
        Final String IN0001 = 'IN0001';
        Final String IN0003 = 'IN0003';
        DateTime starDateTime = DateTime.newInstance(1970, 1, 1, 0, 0, 0);
        entretien.datesign = (aTask.TaskId__c == IN0001 || aTask.TaskId__c == IN0003)
          ? starDateTime.AddSeconds(tz.getOffset(starDateTime)/1000)
          : aTask.Subscription__r.Zuora__SubscriptionStartDate__c;    
        entretien.debcontrat =  (aTask.TaskId__c == IN0001 || aTask.TaskId__c == IN0003)
          ? starDateTime.AddSeconds(tz.getOffset(starDateTime)/1000)
          : aTask.Subscription__r.Zuora__ContractEffectiveDate__c;
        entretien.fincontrat = (aTask.TaskId__c == IN0001 || aTask.TaskId__c == IN0003)
          ? starDateTime.AddSeconds(tz.getOffset(starDateTime)/1000)
          : aTask.Subscription__r.Zuora__SubscriptionEndDate__c;            
        entretien.typco = (aTask.TaskId__c == IN0001 || aTask.TaskId__c == IN0003)
          ? STAR
          : aTask.Subscription__r.MaintenanceParts__c;       
        entretien.desigcontrat = (aTask.TaskId__c == IN0001 || aTask.TaskId__c == IN0003)
          ? STAR
          : aTask.Subscription__r.MaintenanceParts__c;
          
        entretien.prico = LABEL.SICo_Prico;
        entretien.paye = LABEL.SICo_Paye;
        appar.libelmarque = aCase.Site__r.BoilerBrand__c;
        appar.libelmodele = aCase.Site__r.BoilerModel__c;        
        chamClient.gaz = gaz;
        chamClient.entretien = entretien;
        chamClient.appar = new List<SICo_FluxChamModel.Appar>{appar};
        if(chamClientContrat.chamClient == null){
          chamClientContrat.chamClient = new List<SICo_FluxChamModel.ChamClient>();
        }
        chamClientContrat.chamClient.add(chamClient);
      }
    }

    String requestJSON;
    requestJSON = JSON.serialize(chamClientContrat);  
    return requestJSON;
  }

  public static String builCHamDemande(Map<Id,Task> tasksTrigger){
    Set<Id> casesId = getCaseID(tasksTrigger.values());
    final Map<ID,Case> cases = new Map<ID,Case>([SELECT Account.CustomerNumber__c, Site__r.SiteNumber__c,
                                                 CreatedDate, LastModifiedDate
                                                 FROM Case WHERE Id= :casesId]);

    final Map<ID,Task> tasks = new Map<ID,Task>([SELECT Id, CHAMAgency__r.AgencyNumber__c, PlannedRangedHours__c,
                                                 InterventionPlannedDate__c, InterventionType__c, Description
                                                 FROM Task WHERE Id= :tasksTrigger.keySet()]);

    TimeZone tz = Timezone.getTimeZone(LABEL.SICo_TimeZone);
    Case aCase = new case();
    Task aTask = new Task();
    DateTime[] dateForThisLoop = new DateTime[]{};
    SICo_FluxChamModel.ChamRDV fluxChamRDV = new SICo_FluxChamModel.ChamRDV();
    for(Task theTask : tasksTrigger.values()){
      aCase = cases.get(theTask.WhatId);
      aTask = tasks.get(theTask.Id);
      SICo_FluxChamModel.ChamDemande chamDemande = new SICo_FluxChamModel.ChamDemande();
      List<SICo_FluxChamModel.Diagnostique> diagnostique = new List<SICo_FluxChamModel.Diagnostique>();
      SICo_FluxChamModel.Diagnostique diag = new SICo_FluxChamModel.Diagnostique();
      diagnostique.add(diag);
      chamDemande.diagnostique= diagnostique;
      chamDemande.id_ticket_orchidee = aTask.Id;
      chamDemande.id_orchidee = aCase.Account.CustomerNumber__c + aCase.Site__r.SiteNumber__c;
      chamDemande.id_societe = LABEL.SICo_IdSociete;
      chamDemande.type= aTask.InterventionType__c;
      chamDemande.id_agence = aTask.CHAMAgency__r.AgencyNumber__c;
      if(aTask.Description != null){
        String description = aTask.Description;
        chamDemande.motif  = (description.length() < 90) ? description : description.substring(0,90);
        chamDemande.motif2 = (description.length() >= 90) ? description.substring(90,Math.min(description.length()-1,180)) : '';
        chamDemande.motif3 = (description.length() >= 180) ? description.substring(180,Math.min(description.length()-1,240)) : '';
      }
      if(aTask.PlannedRangedHours__c != null){
        chamDemande.heurd = aTask.PlannedRangedHours__c.trim().substringBefore('-');
        chamDemande.heurf = aTask.PlannedRangedHours__c.trim().substringAfter('-');
      }
      if(aTask.InterventionPlannedDate__c != null){
        chamDemande.dateinter = aTask.InterventionPlannedDate__c;
        chamDemande.periode = chamDemande.dateinter.hour() > 12 ? 'M' : 'A';      
      }
      chamDemande.date_creation = aCase.CreatedDate.AddSeconds(tz.getOffset(aCase.CreatedDate)/1000);
      chamDemande.date_modification = aCase.LastModifiedDate.AddSeconds(tz.getOffset(aCase.CreatedDate)/1000);
      if(fluxChamRDV.chamDemande == null){
        fluxChamRDV.chamDemande = new List<SICo_FluxChamModel.ChamDemande>();
      }
      fluxChamRDV.chamDemande.add(chamDemande);
    }
    String requestJSON;
    requestJSON = JSON.serialize(fluxChamRDV);
    return requestJSON;
  }

  /* -------- PRIVATE METHODE -------- */    
  private static Set<Id> getCaseID(list<Task> tasks){
    Set<Id> CaseIDSet = new Set<Id>();
    for(Task taskNow : tasks){
      CaseIDSet.add(taskNow.WhatId);
    }
    return CaseIDSet;
  }
}