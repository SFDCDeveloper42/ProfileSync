/*------------------------------------------------------------
Author:        Gaël BURNEAU
Company:       Ikumbi Solutions
Description:   Controller for Dashboard page
Test Class:    SICo_Dashboard_VFC_TEST
History
06/09/2016     Gaël BURNEAU      Create version
------------------------------------------------------------*/
@isTest
private class SICo_Dashboard_VFC_TEST {

    private final static String COMMUNITY_USER_NAME = 'userTest1'; 

    @testSetup 
    static void initTestData() {
        Account account = SICo_UtilityTestDataSet.createAccount('Test01', 'Test01', 'PersonAccount');
        insert account;

        //Insert new community user
        User vCommunityUser1 = SICo_UtilityTestDataSet.createCommunityUser();
        vCommunityUser1.LastName = COMMUNITY_USER_NAME;
        insert vCommunityUser1;
        vCommunityUser1 = [SELECT AccountId, ContactId FROM User WHERE Id = :vCommunityUser1.Id LIMIT 1];

        //select account
        Account vAccount = [SELECT Id, IsPersonAccount, SoweeOptin__c, PartnerOptin__c, PersonContactId FROM Account WHERE Id = :vCommunityUser1.AccountId LIMIT 1];
 
        //Insert new site
        SICo_Site__c site1 = SICo_UtilityTestDataSet.createSite(vAccount.Id);
        site1.IsActive__c = true; 
        site1.HousingType__c = Label.SICo_House;
       	site1.IsSelectedInCustomerSpace__c = true;
        insert site1;        

        MaintenancyQuote__c vQuote = SICo_UtilityTestDataSet.createMaintenancyQuote(site1.Id, account.Id);
        vQuote.MaintenancyQuoteStatus__c = 'Accepté';
        vQuote.Customer__c = vAccount.Id;
        insert vQuote;
        
        //Insert new opportunity
        Opportunity testOpportunity1 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity1', vCommunityUser1.AccountId, site1.Id);
        insert testOpportunity1;

        // Insert new Case Equipment
        Case vCase = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningObjet, vAccount.Id, testOpportunity1.Id, site1.Id);
        vCase.Subject = 'test Equipement';
        vCase.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'System'); // TODO Constant & label
        insert vCase; 

        // Insert Task
        Task vTask = SICo_UtilityTestDataSet.createTask('IN0001', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
        insert vTask;

        // Insert Event
        SICo_CR__c vCR = new SICo_CR__c(
            Site__c = site1.Id,
            InterventionDate__c = Date.today(),
            FirstReturnCode__c = System.Label.SICo_InstallStatE
        );
        insert vCR;
        /*Event vEvent = SICo_UtilityTestDataSet.createEvent('IN0002', System.Label.SICo_RDVInstallCHAM, vCase.Id, 'Event', false);
        vEvent.IsVisibleInSelfService = true;
        vEvent.CR__c = vCr.Id;
        vEvent.ActivityDateTime = Datetime.now() - 5;
        vEvent.StartDateTime = vEvent.ActivityDateTime;
        vEvent.RDVCancelled__c = false;
        insert vEvent;*/

        // Maintenance
        Case vCaseMaintenance = SICo_UtilityTestDataSet.createCase('Maintenance', vAccount.Id, testOpportunity1.Id, site1.Id);
        vCaseMaintenance.Subject = 'Test Maintenance';
        vCaseMaintenance.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'System'); // TODO Constant & label
        insert vCaseMaintenance;

        // Insert Event
        /*Event vEventMaintenance = SICo_UtilityTestDataSet.createEvent('IN0001', System.Label.SICo_RDVCHAM, vCaseMaintenance.Id, 'Event', false);
        DateTime vNow = Datetime.now();
        vEventMaintenance.ActivityDateTime = vNow.addDays(5);
        vEventMaintenance.StartDateTime = vNow.addDays(5);
        vEventMaintenance.EndDateTime = vNow.addDays(6); 
        vEventMaintenance.DurationInMinutes = 1440;
        vEventMaintenance.IsVisibleInSelfService = true;
        insert vEventMaintenance;*/

        //Case Gas
        Case vCaseGas = SICo_UtilityTestDataSet.createCase(System.Label.SICo_ProvisionningGaz, vAccount.Id, testOpportunity1.Id, site1.Id);
        vCaseGas.Subject = System.Label.SICo_ProvisionningGaz;
        vCaseGas.RecordTypeId = SICo_Utility.m_RTbyDeveloperName.get('Case_' + 'System'); // TODO Constant & label
        insert vCaseGas;


        List<Task> l_tasks = new List<Task>();

        // Insert Task
        Task vTaskGas = SICo_UtilityTestDataSet.createTask('IN0001', System.Label.SICo_PackStationEnergieThermo, vCaseGas.Id, 'Task', false);
        vTaskGas.InterventionType__c = 'VISITE DE CONFORMITE';
        vTaskGas.IsVisibleInSelfService = true;
        vTaskGas.SiteID__c = site1.Id;
        vTaskGas.InterventionPlannedDate__c = DateTime.now().date() - 1;
        vTaskGas.RequestState__c = System.Label.SICo_Cloturee;
        vTaskGas.NotTakenByCRC__c  = false;
        //insert vTaskGas;
        l_tasks.Add(vTaskGas);

        //Insert Task Maintenance
        ID rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
        vTask = SICo_UtilityTestDataSet.createTask('MA0002', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
        vTask.Subject = System.Label.SICo_ProvisionningGaz;
        vTask.RecordTypeId = rtTask;
        vTask.InterventionType__c = 'VISITE DE CONFORMITE';
        vTask.Status = 'Ouverte';
        vTask.InterventionPlannedDate__c = Date.today();
        vTask.RequestState__c = '';
        vTask.WhoId = vCommunityUser1.ContactId;
        vTask.WhatId = vCase.Id;
        vTask.IsVisibleInSelfService = true;
        vTask.SiteID__c = site1.Id;
        vTask.Type = 'Maintenance';
        //insert vTask;
        l_tasks.Add(vTask);

        //Insert Task Maintenance
        rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
        vTask = SICo_UtilityTestDataSet.createTask('MA0002', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
        vTask.Subject = System.Label.SICo_ProvisionningGaz;
        vTask.RecordTypeId = rtTask;
        vTask.InterventionType__c = 'VISITE DE CONFORMITE';
        vTask.Status = 'Ouverte';
        vTask.InterventionPlannedDate__c = Date.today().addDays(4);
        vTask.RequestState__c = '';
        vTask.WhoId = vCommunityUser1.ContactId;
        vTask.WhatId = vCase.Id;
        vTask.IsVisibleInSelfService = true;
        vTask.SiteID__c = site1.Id;
        vTask.Type = 'Maintenance';
        //insert vTask;
        l_tasks.Add(vTask);

        //Insert Task Equipement
        rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
        vTask = SICo_UtilityTestDataSet.createTask('SE0001', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = null;
        vTask.Subject = System.Label.SICo_ProvisionningGaz;
        vTask.RecordTypeId = rtTask;
        vTask.InterventionType__c = 'VISITE DE CONFORMITE';
        vTask.Status = 'Ouverte';
        vTask.InterventionPlannedDate__c = Date.today();
        vTask.RequestState__c = '';
        vTask.WhoId = vCommunityUser1.ContactId;
        vTask.WhatId = vCase.Id;
        vTask.IsVisibleInSelfService = true;
        vTask.SiteID__c = site1.Id;
        vTask.Type = 'Gaz';
        //insert vTask;
        l_tasks.Add(vTask);

        //Insert Task Equipement
        rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
        vTask = SICo_UtilityTestDataSet.createTask('IN0002', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = null;
        vTask.Subject = System.Label.SICo_ProvisionningGaz;
        vTask.RecordTypeId = rtTask;
        vTask.InterventionType__c = 'VISITE DE CONFORMITE';
        vTask.Status = 'Ouverte';
        vTask.InterventionPlannedDate__c = Date.today();
        vTask.RequestState__c = '';
        vTask.WhoId = vCommunityUser1.ContactId;
        vTask.WhatId = vCase.Id;
        vTask.IsVisibleInSelfService = true;
        vTask.SiteID__c = site1.Id;
        vTask.Type = 'Gaz';
        //insert vTask;
        l_tasks.Add(vTask);

        //Insert Task Equipement
        rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
        vTask = SICo_UtilityTestDataSet.createTask('IN0002', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
        vTask.Subject = System.Label.SICo_ProvisionningGaz;
        vTask.RecordTypeId = rtTask;
        vTask.InterventionType__c = 'VISITE DE CONFORMITE';
        vTask.Status = 'Ouverte';
        vTask.InterventionPlannedDate__c = Date.today().addDays(4);
        vTask.RequestState__c = '';
        vTask.WhoId = vCommunityUser1.ContactId;
        vTask.WhatId = vCase.Id;
        vTask.IsVisibleInSelfService = true;
        vTask.SiteID__c = site1.Id;
        vTask.Type = 'Gaz';
        //insert vTask;
        l_tasks.Add(vTask);

        //Insert Task Equipement
        rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_CHAM');
        vTask = SICo_UtilityTestDataSet.createTask('SE0001', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
        vTask.Subject = System.Label.SICo_ProvisionningGaz;
        vTask.RecordTypeId = rtTask;
        vTask.InterventionType__c = 'VISITE DE CONFORMITE';
        vTask.Status = 'Ouverte';
        vTask.InterventionPlannedDate__c = Date.today().addDays(4);
        vTask.RequestState__c = '';
        vTask.WhoId = vCommunityUser1.ContactId;
        vTask.WhatId = vCase.Id;
        vTask.IsVisibleInSelfService = true;
        vTask.SiteID__c = site1.Id;
        vTask.Type = 'Gaz';
//        insert vTask;
        l_tasks.Add(vTask);

        //Insert Task Gaz
        rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_Gaz');
        vTask = SICo_UtilityTestDataSet.createTask('GA0001', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
        vTask.Subject = System.Label.SICo_ProvisionningGaz;
        vTask.RecordTypeId = rtTask;
        vTask.InterventionType__c = System.Label.SICo_OmegaMES;
        vTask.Status = 'Ouverte';
        vTask.InterventionPlannedDate__c = Date.today();
        vTask.RequestState__c = System.Label.SICo_Cloturee;
        vTask.WhoId = vCommunityUser1.ContactId;
        vTask.WhatId = vCase.Id;
        vTask.IsVisibleInSelfService = true;
        vTask.SiteID__c = site1.Id;
        vTask.Type = System.Label.SICo_ProvisionningGaz;
//        insert vTask;
        l_tasks.Add(vTask);

        //Insert Task Gaz
        rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_Gaz');
        vTask = SICo_UtilityTestDataSet.createTask('GA0001', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
        vTask.Subject = System.Label.SICo_ProvisionningGaz;
        vTask.RecordTypeId = rtTask;
        vTask.InterventionType__c = System.Label.SICo_OmegaMES;
        vTask.Status = 'Ouverte';
        vTask.InterventionPlannedDate__c = Date.today().addDays(4);
        vTask.RequestState__c = '';
        vTask.WhoId = vCommunityUser1.ContactId;
        vTask.WhatId = vCase.Id;
        vTask.IsVisibleInSelfService = true;
        vTask.SiteID__c = site1.Id;
        vTask.Type = System.Label.SICo_ProvisionningGaz;
//        insert vTask;
        l_tasks.Add(vTask);

        //Insert Task Gaz
        rtTask = SICo_Utility.m_RTbyDeveloperName.get('Task_' + 'TaskProvisionning_Gaz');
        vTask = SICo_UtilityTestDataSet.createTask('GA0001', System.Label.SICo_PackStationEnergie, vCase.Id, 'Task', false);
        vTask.IsVisibleInSelfService = true;
        vTask.ShippingMode__c  = System.Label.SICo_Installateur;
        vTask.DeliveryStatus__c  = System.Label.SICo_ColisEnIncident;
        vTask.Subject = System.Label.SICo_ProvisionningGaz;
        vTask.RecordTypeId = rtTask;
        vTask.InterventionType__c = System.Label.SICo_OmegaMES;
        vTask.Status = 'Ouverte';
        vTask.InterventionPlannedDate__c = Date.today();
        vTask.RequestState__c = '';
        vTask.WhoId = vCommunityUser1.ContactId;
        vTask.WhatId = vCase.Id;
        vTask.IsVisibleInSelfService = true;
        vTask.SiteID__c = site1.Id;
        vTask.Type = System.Label.SICo_ProvisionningGaz;
//        insert vTask;
        l_tasks.Add(vTask);

        insert l_tasks;

        Zuora__ZInvoice__c vZInvoice = SICo_UtilityTestDataSet.createZuoraInvoice(vAccount.Id);
        vZInvoice.IsPaymentInProgress__c = false;
        vZInvoice.Zuora__Balance2__c = 6000;
        vZInvoice.ScheduleDebitDate__c = Date.today() - 1;
        vZInvoice.Zuora__TotalAmount__c = 156;
        insert vZInvoice;

        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf('test');
        attachment.Name = String.valueOf('test.txt');
        attachment.ParentId = vZInvoice.Id;
        insert attachment;

        SICo_InvoiceItem__c vInvoice1Item1 = SICo_UtilityTestDataSet.createZuoraInvoiceItem(vZInvoice.Id, 'invoice1Item1', 10); 
        vInvoice1Item1.ChargeAmount__c = 30;
        insert vInvoice1Item1; 
        vInvoice1Item1 = [SELECT Id, TotalAmount__c, TotalAmountTTC__c FROM SICo_InvoiceItem__c WHERE Id = :vInvoice1Item1.Id]; 
        vZInvoice = [SELECT Id, IsUnpaid__c, TotalInvoiceAmountTTC__c, Zuora__Balance2__c FROM Zuora__ZInvoice__c WHERE Id = :vZInvoice.Id];
 
        /// Offer part
 
        //Insert new opportunity
        Opportunity testOpportunity2 = SICo_UtilityTestDataSet.createOpportunity('testOpportunity2', vCommunityUser1.AccountId, site1.Id);
        insert testOpportunity2;

        //Insert new quote
        zqu__Quote__c testQuote1 = SICo_UtilityTestDataSet.createZuoraQuote('testQuote1', vCommunityUser1.AccountId, vCommunityUser1.ContactId, testOpportunity1.Id);
        insert testQuote1;
        testQuote1 = [SELECT Id, zqu__Number__c FROM zqu__Quote__c WHERE Id = :testQuote1.Id LIMIT 1];

        //Insert new quote
        zqu__Quote__c testQuote2 = SICo_UtilityTestDataSet.createZuoraQuote('testQuote2', vCommunityUser1.AccountId, vCommunityUser1.ContactId, testOpportunity2.Id);
        insert testQuote2;
        testQuote2 = [SELECT Id, zqu__Number__c FROM zqu__Quote__c WHERE Id = :testQuote2.Id LIMIT 1];

        //Insert new subscription
        Zuora__Subscription__c testSubscription1 = SICo_UtilityTestDataSet.createZuoraSubscription('testSubscription1', vCommunityUser1.AccountId, testQuote1.Id, testQuote1.zqu__Number__c, site1.Id);
        testSubscription1.SICO_Subscribed_offers__c = 'GAZ';
        insert testSubscription1;
        testSubscription1 = [SELECT Id, Zuora__Account__c, SiteId__c FROM Zuora__Subscription__c WHERE Id = :testSubscription1.Id LIMIT 1];

        //Insert new subscription
        Zuora__Subscription__c testSubscription2 = SICo_UtilityTestDataSet.createZuoraSubscription('testSubscription2', vCommunityUser1.AccountId, testQuote2.Id, testQuote2.zqu__Number__c, site1.Id);
        testSubscription2.SICO_Subscribed_offers__c = 'MAINT';
        insert testSubscription2;
        testSubscription2 = [SELECT Id, Zuora__Account__c, SiteId__c FROM Zuora__Subscription__c WHERE Id = :testSubscription2.Id LIMIT 1];

        //Insert new payment method
        Zuora__PaymentMethod__c zuoraPM1 = SICo_UtilityTestDataSet.createZuoraPaymentMethod(vCommunityUser1.AccountId, true);
        insert zuoraPM1;
    }

    
 
    @isTest 
    static void query_Activities_Test() { 
        User vCommunityUser1 = [SELECT Id, AccountId FROM User WHERE lastName = :COMMUNITY_USER_NAME LIMIT 1];   

        //Run as Community User
        system.runAs(vCommunityUser1) {
            Test.startTest();

                Test.setCurrentPageReference(new PageReference('Page.Dashboard'));
                //set url parameter
                System.currentPageReference().getParameters().put(SICo_Constants_CLS.URL_PARAM_ACCOUNTID, vCommunityUser1.AccountId);
                SICo_Dashboard_VFC vController = new SICo_Dashboard_VFC();

                List<SICo_Dashboard_VFC.ActivityWrapper> vActivitiesQueried = SICo_Dashboard_VFC.queryActvities(vCommunityUser1.AccountId);
                System.Assert(vActivitiesQueried != null && vActivitiesQueried.size() > 0, 'Error : activities retrieved list is empty or null');

                String idAccount = null;

                List<SICo_DashboardActivities_CLS.ActivityLightWrapper> vDashboardActivities = SICo_DashboardActivities_CLS.getQuotes(idAccount);
                System.Assert(vDashboardActivities == null , 'Error : activities retrieved list is not null');

                vDashboardActivities = SICo_DashboardActivities_CLS.getUnpaidInvoices(idAccount);
                System.Assert(vDashboardActivities == null, 'Error : activities retrieved list is not null');
            
            Test.stopTest();
        }
    }
}