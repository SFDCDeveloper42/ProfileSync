/*------------------------------------------------------------
Author:        Mehdi CHERFAOUI
Company:       Salesforce
Description:   Utility class for managing the Platform Cache
Test Class:    SICo_PlatformCacheManager_TEST
History
Nov. 2016      Mehdi CHERFAOUI     Create version
------------------------------------------------------------*/
public with sharing class SICo_PlatformCacheManager_CLS {

    public static final Integer MIN_CACHE_TTL = 300;
    
    /**
     * @description getObject - get generic Object from OrgCache 
    */
    public static Object getObject (String cacheKey) {
        return !String.isBlank(cacheKey) ? Cache.Org.get(cacheKey) : null;
    }
    
    /**
     * @description putObject - put generic Object from OrgCache 
    */
    public static void putObject (String cacheKey, Object obj, Integer ttl) {
        Integer cacheTTL = ttl != null ? Math.max(ttl, MIN_CACHE_TTL) : MIN_CACHE_TTL;
        if (!String.isBlank(cacheKey) && obj != null) {
            Cache.Org.put(cacheKey, obj, cacheTTL);
        } 
    }
    
    /**
     * @description getSessionObject - get generic Object from SessionCache 
    */
    public static Object getSessionObject (String cacheKey) {
        return !String.isBlank(cacheKey) ? Cache.Session.get(cacheKey) : null;
    }
    
    /**
     * @description putSessionObject - put generic Object from SessionCache 
    */
    
    public static void putSessionObject (String cacheKey, Object obj, Integer ttl) {
        Integer cacheTTL = ttl != null ? Math.max(ttl, MIN_CACHE_TTL) : MIN_CACHE_TTL;
        if (!String.isBlank(cacheKey) && obj != null) {
            Cache.Session.put(cacheKey, obj, cacheTTL);
        }
    }

}