/**
* @author Boris Castellani
* @date 13/12/2016
*
* @description Schedulable: Update MaintenancyQuote statut 
*/
global class SICo_MAJCha01B_Schedule_CLS implements Schedulable {
	// default: 200
    global void execute(SchedulableContext sc) {
 		Database.executeBatch(new SICo_Batch_MAJCha01B());       
    }
}